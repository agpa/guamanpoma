<?php
require('vendor/autoload.php');
require('inc/config.php');
require('inc/functions.php');


$insert = true; // Permite activar guardado en BBDD




if ($insert){
for ($a=0; $a < 500; $a++) { 
	$file="files/FICHA ".$a.".xlsx";
	if (file_exists($file)) {

		$Reader = PHPExcel_IOFactory::createReaderForFile($file);
		$Reader->setReadDataOnly(true);
		$objXLS = $Reader->load($file);

		$UNIX_DATE = ($objXLS->getSheet(0)->getCell('C4')->getValue() - 25569) * 86400;
		$date = gmdate("Y-m-d", $UNIX_DATE);
		DB::insert('visita', array(
		  'fecha'							=> $date,
		  'comunidad' 						=> $objXLS->getSheet(0)->getCell('C5')->getValue(),
		  'distrito'						=> $objXLS->getSheet(0)->getCell('C6')->getValue(),
		  'nro_historia'					=> $objXLS->getSheet(0)->getCell('C7')->getValue(),
		  'radica_comunidad'				=> radica_comunidad($objXLS),
		  'nombre_padre'					=> $objXLS->getSheet(0)->getCell('C12')->getValue(),
		  'dni_padre'						=> $objXLS->getSheet(0)->getCell('E12')->getValue(),
		  'nombre_madre'					=> $objXLS->getSheet(0)->getCell('C11')->getValue(),
		  'dni_madre'						=> $objXLS->getSheet(0)->getCell('E11')->getValue(),
		  'religion'						=> religion($objXLS),
		  'nro_miembros'					=> $objXLS->getSheet(0)->getCell('C15')->getValue(),
		  'nombre_entrevistado'				=> $objXLS->getSheet(0)->getCell('C16')->getValue(),
		  'parentesco_entrevistado'			=> parentesco($objXLS),
		  'participa_programa_social'		=> si_o_no($objXLS,'D40'),
		  'programa_social'					=> programa_social($objXLS),
		  '2_normas_1'						=> si_o_no($objXLS,'D43'),
		  '2_normas_2'						=> si_o_no($objXLS,'D44'),
		  '2_normas_3'						=> si_o_no($objXLS,'D45'),
		  '2_normas_4'						=> si_o_no($objXLS,'D46'),
		  '3_sobre_1'						=> si_o_no($objXLS,'D49'),
		  '3_sobre_2'						=> si_o_no($objXLS,'D51'),
		  '3_aplica_ninos_3'				=> marcado($objXLS,'C53'),
		  'signo_ira_a'						=> marcado($objXLS,'C142'),
		  'signo_ira_b'						=> marcado($objXLS,'C143'),
		  'signo_ira_c'						=> marcado($objXLS,'C144'),
		  'signo_ira_d'						=> marcado($objXLS,'E142'),
		  'signo_ira_e'						=> marcado($objXLS,'E143'),
		  'signo_ira_f'						=> marcado($objXLS,'E144'),
		  'signo_eda_a'						=> marcado($objXLS,'C146'),
		  'signo_eda_b'						=> marcado($objXLS,'C147'),
		  'signo_eda_c'						=> marcado($objXLS,'C148'),
		  'signo_eda_d'						=> marcado($objXLS,'E146'),
		  'signo_eda_e'						=> marcado($objXLS,'E147'),
		  'alimentos_hierro_a'				=> marcado($objXLS,'C151'),
		  'alimentos_hierro_b'				=> marcado($objXLS,'C152'),
		  'alimentos_hierro_c'				=> marcado($objXLS,'C153'),
		  'alimentos_hierro_d'				=> marcado($objXLS,'E151'),
		  'alimentos_hierro_e'				=> marcado($objXLS,'E152'),
		  'veces_consume_hierro'			=> veces_consume_hierro($objXLS),
		  '4_vivienda_1'					=> marcado($objXLS,'G161'),
		  '4_vivienda_2'					=> marcado($objXLS,'G162'),
		  '4_vivienda_3'					=> marcado($objXLS,'G163'),
		  '4_vivienda_4'					=> marcado($objXLS,'G164'),
		  '4_material_const'				=> material($objXLS),
		  '4_material_piso'					=> tipo_piso($objXLS),
		  '4_material_techo'				=> tipo_techo($objXLS),
		  '4_material_elec'					=> si_o_no($objXLS,'C173'),
		  '4_material_alumbra'				=> material_alumbra($objXLS),
		  '4_material_medidor'				=> si_o_no($objXLS,'C175'),
		  '4_material_vivienda'				=> material_vivienda($objXLS),
		  '4_material_ventilado'			=> si_o_no($objXLS,'D178'),
		  '4_cocina_1'						=> si_o_no($objXLS,'D180'),
		  '4_cocina_2'						=> si_o_no($objXLS,'D181'),
		  '4_cocina_3'						=> si_o_no($objXLS,'D182'),
		  '4_cocina_4'						=> si_o_no($objXLS,'D183'),
		  '4_cocina_5'						=> si_o_no($objXLS,'D184'),
		  '4_cocina_6'						=> si_o_no($objXLS,'D185'),
		  '4_cocina_7'						=> si_o_no($objXLS,'D186'),
		  '4_alimentos_1'					=> si_o_no($objXLS,'D188'),
		  '4_alimentos_2'					=> si_o_no($objXLS,'D189'),
		  '4_alimentos_3'					=> si_o_no($objXLS,'D190'),
		  '4_alimentos_4'					=> si_o_no($objXLS,'D191'),
		  '4_alimentos_5'					=> si_o_no($objXLS,'D192'),
		  '4_sshh_1'						=> si_o_no($objXLS,'D194'),
		  '4_sshh_2'						=> $objXLS->getSheet(0)->getCell('D195')->getValue(),
		  '4_sshh_3'						=> si_o_no($objXLS,'D196'),
		  '4_sshh_4'						=> si_o_no($objXLS,'D197'),
		  '4_sshh_5'						=> si_o_no($objXLS,'D198'),
		  '4_sshh_6'						=> si_o_no($objXLS,'D199'),
		  '4_sshh_7'						=> si_o_no($objXLS,'D200'),
		  '4_patio_1'						=> si_o_no($objXLS,'D202'),
		  '4_patio_2'						=> si_o_no($objXLS,'D203'),
		  '4_patio_3'						=> si_o_no($objXLS,'D204'),
		  '4_patio_4'						=> si_o_no($objXLS,'D205'),
		  '4_patio_5'						=> si_o_no($objXLS,'D206'),
		  '4_patio_6'						=> si_o_no($objXLS,'D207'),
		  '4_patio_7'						=> si_o_no($objXLS,'D208'),
		  '5_aplica_gestante_1'				=> marcado($objXLS,'D211'),
		  '5_gestante_1'					=> si_o_no($objXLS,'D212'),
		  '5_gestante_2'					=> si_o_no($objXLS,'D213'),
		  '5_gestante_3'					=> si_o_no($objXLS,'D214'),
		  '5_conoce_peligro_1'				=> marcado($objXLS,'C216'),
		  '5_conoce_peligro_2'				=> marcado($objXLS,'D216'),
		  '5_conoce_peligro_3'				=> marcado($objXLS,'E216'),
		  '5_conoce_peligro_4'				=> marcado($objXLS,'F216'),
		  '5_conoce_peligro_5'				=> marcado($objXLS,'G216'),
		  '5_conoce_peligro_6'				=> marcado($objXLS,'H216'),
		  '5_conoce_peligro_7'				=> marcado($objXLS,'I216'),
		  '5_gestante_4'					=> si_o_no($objXLS,'D217'),
		  '5_gestante_5'					=> si_o_no($objXLS,'D218'),
		  '5_gestante_6'					=> $objXLS->getSheet(0)->getCell('D219')->getValue(),
		  '5_gestante_7'					=> si_o_no($objXLS,'D220'),
		  '6_animales_1'					=> si_o_no($objXLS,'D222'),
		  '6_animales_2'					=> si_o_no($objXLS,'D223'),
		  '6_animales_3'					=> perros($objXLS),
		  '6_animales_4'					=> si_o_no($objXLS,'D224'),
		  '6_animales_5'					=> gatos($objXLS),
		  '6_animales_6'					=> si_o_no($objXLS,'D225'),
		  '6_animales_7'					=> si_o_no($objXLS,'D226'),
		  '7_desarrollo_e_1'				=> si_o_no($objXLS,'H229'),
		  '7_desarrollo_e_2'				=> si_o_no($objXLS,'H230'),
		  '7_desarrollo_e_3'				=> fitotoldo($objXLS),
		  '7_desarrollo_e_4'				=> si_o_no($objXLS,'H233'),
		  '7_desarrollo_e_5'				=> sistema_riego($objXLS),
		  '7_desarrollo_e_6'				=> si_o_no($objXLS,'C237'),
		  '7_desarrollo_e_7'				=> destino_consumo($objXLS),
		  '7_desarrollo_e_8'				=> frecuencia_consumo($objXLS),
		  '7_desarrollo_e_9'				=> si_o_no($objXLS,'C239'),
		  '7_desarrollo_e_10'				=> destino_cuyes($objXLS),
		  '7_desarrollo_e_11'				=> frecuencia_cuyes($objXLS),
		  '7_desarrollo_e_12'				=> total_cuyes($objXLS),
		  '7_desarrollo_e_13'				=> cuyes_madres($objXLS),
		  '7_desarrollo_e_14'				=> si_o_no($objXLS,'C245'),
		  '7_desarrollo_e_15'				=> destino_hongos($objXLS),
		  '7_desarrollo_e_16'				=> frecuencia_hongos($objXLS),
		  '7_desarrollo_e_17'				=> si_o_no($objXLS,'C246'),
		  '7_desarrollo_e_18'				=> si_o_no($objXLS,'F246'),
		  '7_desarrollo_e_19'				=> si_o_no($objXLS,'F247'),
		  '7_desarrollo_e_20'				=> si_o_no($objXLS,'F248'),
		  '8_residuos_1'					=> bota_basura($objXLS),
		  '8_residuos_2'					=> si_o_no($objXLS,'D255'),
		  '8_residuos_3'					=> si_o_no($objXLS,'D256'),
		  '8_residuos_4'					=> si_o_no($objXLS,'D257'),
		  '8_residuos_5'					=> si_o_no($objXLS,'D258'),
		  '9_servicios_1'					=> calificacion($objXLS,261),
		  '9_servicios_2'					=> visita_personal($objXLS),
		  '9_servicios_3'					=> calificacion($objXLS,263),
		  '9_servicios_4'					=> calificacion($objXLS,264),
		  '9_servicios_5'					=> calificacion($objXLS,265),
		  '9_servicios_6'					=> calificacion($objXLS,266),
		  '9_servicios_7'					=> calificacion($objXLS,267),
		  '9_servicios_8'					=> calificacion($objXLS,268),
		  '9_servicios_9'					=> calificacion($objXLS,269),
		  '9_servicios_10'					=> calificacion($objXLS,270),
		  '9_servicios_11'					=> calificacion($objXLS,271),
		  '9_servicios_12'					=> calificacion($objXLS,272),
		  '9_servicios_13'					=> calificacion($objXLS,273),
		  '9_servicios_14'					=> calificacion($objXLS,274),
		  '9_servicios_15'					=> calificacion($objXLS,275),
		  '9_servicios_16'					=> calificacion($objXLS,276),
		  '10_consumo_agua_1'				=> si_o_no($objXLS,'H278'),
		  '10_consumo_agua_2'				=> si_o_no($objXLS,'H279'),
		  '10_consumo_agua_3'				=> si_o_no($objXLS,'H280'),
		  '10_consumo_agua_4'				=> si_o_no($objXLS,'H281'),
		  '10_consumo_agua_5'				=> si_o_no($objXLS,'H282')
		));
		$id = DB::insertId();
		$i = 22;
		while ( $i <= 39) {
			if ($objXLS->getSheet(0)->getCell('B'.$i)->getValue() != ''){
				$UNIX_DATE = ($objXLS->getSheet(0)->getCell('D'.$i)->getValue() - 25569) * 86400;
				$date = gmdate("Y-m-d", $UNIX_DATE);
				$edad = edad($date);
				DB::insert('miembros', array(
				  'codigo_visita'		=> $id,
				  'dni' 				=> $objXLS->getSheet(0)->getCell('F'.$i)->getValue(),
				  'nombre'				=> $objXLS->getSheet(0)->getCell('B'.$i)->getValue(),
				  'parentesco'			=> $objXLS->getSheet(0)->getCell('C'.$i)->getValue(),
				  'fecha_nacimiento'	=> $date,
				  'edad' 				=> $edad,
				  'sexo' 				=> $objXLS->getSheet(0)->getCell('G'.$i)->getValue(),
				  'estado_civil' 		=> $objXLS->getSheet(0)->getCell('H'.$i)->getValue(),
				  'es_gestante' 		=> $objXLS->getSheet(0)->getCell('I'.$i)->getValue(),
				  'nivel_educativo' 	=> $objXLS->getSheet(0)->getCell('J'.$i)->getValue(),
				  'seguro' 				=> $objXLS->getSheet(0)->getCell('K'.$i)->getValue(),
				  'nro_discapacidad' 	=> $objXLS->getSheet(0)->getCell('L'.$i)->getValue(),
				  'tipo_disc' 			=> $objXLS->getSheet(0)->getCell('M'.$i)->getValue(),
				  'elector_distrito' 	=> $objXLS->getSheet(0)->getCell('N'.$i)->getValue()
				));
			}
			$i++;
		}
		$j = 56;
		while ($j <= 65) {
			$k = $j+12;
			$l = $j+23;
			$m = $j+34;
			$n = $j+45;
			$o = $j+56;
			$p = $j+67;
			$q = $j+77;
			if ($objXLS->getSheet(0)->getCell('B'.$j)->getValue() != ''){
				DB::insert('cartilla_cred', array(
				  'codigo_visita'			=> $id,
				  'nombre'					=> $objXLS->getSheet(0)->getCell('B'.$j)->getValue(),
				  'desnutricion_cronica'	=> desnutricion_cronica($objXLS,$j),
				  'tiene_anemia'			=> $objXLS->getSheet(0)->getCell('C'.$k)->getValue(),
				  'no_examen'				=> marcado($objXLS,'D'.$k),
				  'recibe_tratamiento' 		=> $objXLS->getSheet(0)->getCell('C'.$k)->getValue(),
				  'recibe_multinutrientes' 	=> $objXLS->getSheet(0)->getCell('C'.$l)->getValue(),
				  'consume_multinutrientes'	=> $objXLS->getSheet(0)->getCell('D'.$l)->getValue(),
				  'cuenta_controles' 		=> $objXLS->getSheet(0)->getCell('C'.$m)->getValue(),
				  'cuenta_vacunas' 			=> $objXLS->getSheet(0)->getCell('C'.$n)->getValue(),
				  'tubo_parasitosis' 		=> $objXLS->getSheet(0)->getCell('C'.$o)->getValue(),
				  'examen_parasitosis'		=> marcado($objXLS,'D'.$o),
				  'tratamiento_parasitosis' => $objXLS->getSheet(0)->getCell('E'.$o)->getValue(),
				  'presento_diarrea' 		=> $objXLS->getSheet(0)->getCell('C'.$p)->getValue(),
				  'llevo_establecimiento'	=> marcado($objXLS,'D'.$p),
				  'tratamiento_diarrea' 	=> $objXLS->getSheet(0)->getCell('E'.$p)->getValue(),
				  'presento_ira'			=> $objXLS->getSheet(0)->getCell('C'.$q)->getValue()
				));
			}
			$j++;
		}
	}
}
}
?>