<?php
function radica_comunidad($objXLS){
	if ($objXLS->getSheet(0)->getCell('D8')->getValue() != ''){
		return 'P';
	} else {
		return 'T';
	}
}

function religion($objXLS){
	if ($objXLS->getSheet(0)->getCell('C14')->getValue() != ''){
		return 'Catolico';
	} elseif ($objXLS->getSheet(0)->getCell('D14')->getValue() != ''){
		return 'Evangelico';
	}  elseif ($objXLS->getSheet(0)->getCell('E14')->getValue() != ''){
		return 'Adventista';
	} elseif ($objXLS->getSheet(0)->getCell('F14')->getValue() != ''){
		return 'Testigo'; // T = Temporal
	} elseif ($objXLS->getSheet(0)->getCell('G14')->getValue() != ''){
		return $objXLS->getSheet(0)->getCell('H14')->getValue();
	}
}

function parentesco($objXLS){
	if ($objXLS->getSheet(0)->getCell('H16')->getValue() != ''){
		return 'Papa';
	} elseif ($objXLS->getSheet(0)->getCell('H17')->getValue() != ''){
		return 'Mama';
	}  elseif ($objXLS->getSheet(0)->getCell('H18')->getValue() != ''){
		return 'Otro';
	}
}

function si_o_no($objXLS,$cell){
	if ($objXLS->getSheet(0)->getCell($cell)->getValue() == 'S'){
		return 'S';
	} elseif ($objXLS->getSheet(0)->getCell($cell)->getValue() == 'N') {
		return 'N';
	} elseif ($objXLS->getSheet(0)->getCell($cell)->getValue() == 'NA') {
		return 'NA';
	} else {
		return '';
	}
}

function programa_social($objXLS){
	if ($objXLS->getSheet(0)->getCell('G41')->getValue() != ''){
		return 'Juntos';
	} elseif ($objXLS->getSheet(0)->getCell('H41')->getValue() != ''){
		return 'CunaMas';
	}  elseif ($objXLS->getSheet(0)->getCell('I41')->getValue() != ''){
		return 'Qalywarma';
	} elseif ($objXLS->getSheet(0)->getCell('J41')->getValue() != ''){
		return 'VasoLeche';
	} elseif ($objXLS->getSheet(0)->getCell('K41')->getValue() != ''){
		return 'Beca18';
	} elseif ($objXLS->getSheet(0)->getCell('L41')->getValue() != ''){
		return 'Pension65';
	} elseif ($objXLS->getSheet(0)->getCell('M41')->getValue() != ''){
		return 'Otros';
	}
}

function marcado($objXLS,$cell){
	return ($objXLS->getSheet(0)->getCell($cell)->getValue() != '') ? 1 : 0;
}

function veces_consume_hierro($objXLS){
	if ($objXLS->getSheet(0)->getCell('C155')->getValue() != ''){
		return '1 vez a la semana';
	} elseif ($objXLS->getSheet(0)->getCell('C156')->getValue() != ''){
		return '2 veces a la semana';
	}  elseif ($objXLS->getSheet(0)->getCell('C157')->getValue() != ''){
		return '3 o 4 veces a la semana';
	} elseif ($objXLS->getSheet(0)->getCell('E155')->getValue() != ''){
		return 'Todos los dias de la semana';
	} elseif ($objXLS->getSheet(0)->getCell('E156')->getValue() != ''){
		return 'Otros';
	}
}

function material($objXLS){
	if ($objXLS->getSheet(0)->getCell('C167')->getValue() != ''){
		return 'Adobe';
	} elseif ($objXLS->getSheet(0)->getCell('D167')->getValue() != ''){
		return 'Material noble';
	}  elseif ($objXLS->getSheet(0)->getCell('E167')->getValue() != ''){
		return 'Piedra con barro';
	} elseif ($objXLS->getSheet(0)->getCell('F167')->getValue() != ''){
		return 'Otros';
	}
}

function tipo_piso($objXLS){
	if ($objXLS->getSheet(0)->getCell('C169')->getValue() != ''){
		return 'Tierra';
	} elseif ($objXLS->getSheet(0)->getCell('D169')->getValue() != ''){
		return 'Madera entablada';
	}  elseif ($objXLS->getSheet(0)->getCell('E169')->getValue() != ''){
		return 'Cemento';
	} elseif ($objXLS->getSheet(0)->getCell('F169')->getValue() != ''){
		return 'Otros';
	}
}

function tipo_techo($objXLS){
	if ($objXLS->getSheet(0)->getCell('C171')->getValue() != ''){
		return 'Paja';
	} elseif ($objXLS->getSheet(0)->getCell('D171')->getValue() != ''){
		return 'Calamina';
	}  elseif ($objXLS->getSheet(0)->getCell('E171')->getValue() != ''){
		return 'Teja';
	} elseif ($objXLS->getSheet(0)->getCell('F171')->getValue() != ''){
		return 'Otros';
	}
}

function material_alumbra($objXLS){
	if ($objXLS->getSheet(0)->getCell('E173')->getValue() != ''){
		return 'Vela';
	} elseif ($objXLS->getSheet(0)->getCell('F173')->getValue() != ''){
		return 'Lampara';
	}  elseif ($objXLS->getSheet(0)->getCell('G173')->getValue() != ''){
		return 'Linterna';
	}
}

function material_vivienda($objXLS){
	if ($objXLS->getSheet(0)->getCell('C177')->getValue() != ''){
		return 'Alquilada';
	} elseif ($objXLS->getSheet(0)->getCell('D177')->getValue() != ''){
		return 'Propia';
	}  elseif ($objXLS->getSheet(0)->getCell('E177')->getValue() != ''){
		return 'Padres';
	} elseif ($objXLS->getSheet(0)->getCell('F177')->getValue() != ''){
		return 'Otros';
	}
}

function perros($objXLS){
	if ($objXLS->getSheet(0)->getCell('G223')->getValue() != ''){
		return '1 a 2';
	} elseif ($objXLS->getSheet(0)->getCell('I223')->getValue() != ''){
		return '3 a 4';
	}  elseif ($objXLS->getSheet(0)->getCell('K223')->getValue() != ''){
		return '5 a mas';
	}
}

function gatos($objXLS){
	if ($objXLS->getSheet(0)->getCell('G224')->getValue() != ''){
		return '1 a 2';
	} elseif ($objXLS->getSheet(0)->getCell('I224')->getValue() != ''){
		return '3 a 4';
	}  elseif ($objXLS->getSheet(0)->getCell('K224')->getValue() != ''){
		return '5 a mas';
	}
}

function fitotoldo($objXLS){
	if ($objXLS->getSheet(0)->getCell('F232')->getValue() != ''){
		return 'Hortaliza';
	} elseif ($objXLS->getSheet(0)->getCell('G232')->getValue() != ''){
		return 'Frutas';
	}  elseif ($objXLS->getSheet(0)->getCell('H232')->getValue() != ''){
		return 'Flores';
	}
}

function sistema_riego($objXLS){
	if ($objXLS->getSheet(0)->getCell('C235')->getValue() != ''){
		return 'Aspersion';
	} elseif ($objXLS->getSheet(0)->getCell('D235')->getValue() != ''){
		return 'Inundacion';
	}  elseif ($objXLS->getSheet(0)->getCell('E235')->getValue() != ''){
		return 'Goteo';
	} elseif ($objXLS->getSheet(0)->getCell('F235')->getValue() != ''){
		return 'No tiene';
	}
}

function destino_consumo($objXLS){
	if ($objXLS->getSheet(0)->getCell('E237')->getValue() != ''){
		return 'Consumo';
	} elseif ($objXLS->getSheet(0)->getCell('F237')->getValue() != ''){
		return 'Venta';
	}  elseif ($objXLS->getSheet(0)->getCell('G237')->getValue() != ''){
		return 'Consumo y venta';
	}
}

function frecuencia_consumo($objXLS){
	if ($objXLS->getSheet(0)->getCell('I237')->getValue() != ''){
		return 'Diario';
	} elseif ($objXLS->getSheet(0)->getCell('J237')->getValue() != ''){
		return 'Semanal';
	}  elseif ($objXLS->getSheet(0)->getCell('K237')->getValue() != ''){
		return 'Quincenal';
	}
}

function destino_cuyes($objXLS){
	if ($objXLS->getSheet(0)->getCell('E239')->getValue() != ''){
		return 'Consumo';
	} elseif ($objXLS->getSheet(0)->getCell('F239')->getValue() != ''){
		return 'Venta';
	}  elseif ($objXLS->getSheet(0)->getCell('G239')->getValue() != ''){
		return 'Consumo y venta';
	}
}

function frecuencia_cuyes($objXLS){
	if ($objXLS->getSheet(0)->getCell('I239')->getValue() != ''){
		return 'Diario';
	} elseif ($objXLS->getSheet(0)->getCell('J239')->getValue() != ''){
		return 'Semanal';
	}  elseif ($objXLS->getSheet(0)->getCell('K239')->getValue() != ''){
		return 'Quincenal';
	}  elseif ($objXLS->getSheet(0)->getCell('L239')->getValue() != ''){
		return 'Mensual a mas';
	}
}

function total_cuyes($objXLS){
	if ($objXLS->getSheet(0)->getCell('C241')->getValue() != ''){
		return '1 a 10';
	} elseif ($objXLS->getSheet(0)->getCell('D241')->getValue() != ''){
		return '11 a 20';
	} elseif ($objXLS->getSheet(0)->getCell('E241')->getValue() != ''){
		return '21 a 30';
	} elseif ($objXLS->getSheet(0)->getCell('F241')->getValue() != ''){
		return '30 a 50';
	} elseif ($objXLS->getSheet(0)->getCell('G241')->getValue() != ''){
		return 'mas de 50';
	}
}

function cuyes_madres($objXLS){
	if ($objXLS->getSheet(0)->getCell('C243')->getValue() != ''){
		return '<4';
	} elseif ($objXLS->getSheet(0)->getCell('D243')->getValue() != ''){
		return '5 a 10';
	} elseif ($objXLS->getSheet(0)->getCell('E243')->getValue() != ''){
		return '11 a 20';
	} elseif ($objXLS->getSheet(0)->getCell('F243')->getValue() != ''){
		return '21 a mas';
	}
}

function destino_hongos($objXLS){
	if ($objXLS->getSheet(0)->getCell('E245')->getValue() != ''){
		return 'Consumo';
	} elseif ($objXLS->getSheet(0)->getCell('F245')->getValue() != ''){
		return 'Venta';
	}  elseif ($objXLS->getSheet(0)->getCell('G245')->getValue() != ''){
		return 'Consumo y venta';
	}
}

function frecuencia_hongos($objXLS){
	if ($objXLS->getSheet(0)->getCell('I245')->getValue() != ''){
		return 'Diario';
	} elseif ($objXLS->getSheet(0)->getCell('J245')->getValue() != ''){
		return 'Semanal';
	}  elseif ($objXLS->getSheet(0)->getCell('K245')->getValue() != ''){
		return 'Quincenal';
	}  elseif ($objXLS->getSheet(0)->getCell('L245')->getValue() != ''){
		return 'Mensual';
	}
}

function bota_basura($objXLS){
	if ($objXLS->getSheet(0)->getCell('C252')->getValue() != ''){
		return 'Micro relleno sanitario';
	} elseif ($objXLS->getSheet(0)->getCell('C253')->getValue() != ''){
		return 'Carro basurero';
	}  elseif ($objXLS->getSheet(0)->getCell('C254')->getValue() != ''){
		return 'Contenedor o tachos';
	}  elseif ($objXLS->getSheet(0)->getCell('E252')->getValue() != ''){
		return 'La tira al barranco';
	}  elseif ($objXLS->getSheet(0)->getCell('E253')->getValue() != ''){
		return 'La quema';
	} elseif ($objXLS->getSheet(0)->getCell('E254')->getValue() != ''){
		return 'Rio o acequia';
	}
}

function calificacion($objXLS,$row){
	if ($objXLS->getSheet(0)->getCell('D'.$row)->getValue() != ''){
		return 'Bueno';
	} elseif ($objXLS->getSheet(0)->getCell('F'.$row)->getValue() != ''){
		return 'Regular';
	}  elseif ($objXLS->getSheet(0)->getCell('H'.$row)->getValue() != ''){
		return 'Deficiente';
	}  elseif ($objXLS->getSheet(0)->getCell('J'.$row)->getValue() != ''){
		return 'No cuento';
	}
}

function visita_personal($objXLS){
	if ($objXLS->getSheet(0)->getCell('D262')->getValue() != ''){
		return 'S';
	} elseif ($objXLS->getSheet(0)->getCell('F262')->getValue() != ''){
		return 'N';
	}
}

function edad($date){
	return $date_edad = date_diff(date_create($date), date_create(date('Y-m-d')))->y;
}

function desnutricion_cronica($objXLS,$row){
	if ($objXLS->getSheet(0)->getCell('E'.$row)->getValue() != ''){
		return 'S';
	} elseif ($objXLS->getSheet(0)->getCell('F'.$row)->getValue() != ''){
		return 'N';
	} elseif ($objXLS->getSheet(0)->getCell('G'.$row)->getValue() != ''){
		return 'R';
	}
}