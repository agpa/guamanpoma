<?php
require('vendor/autoload.php');
require('inc/config.php');
require('inc/functions.php');
$insert = true; // Permite activar guardado en BBDD
if ($insert){
	$file = "data/encuesta0.json";
	if (file_exists($file)) {
		$string = "[".file_get_contents($file)."]";
		$json_a = json_decode($string, true);
		for ($i=0; $i < count($json_a); $i++) { 
			








		DB::insert('visita', array(
		  'fecha'							=> $json_a[$i]['fecha'],
		  'dni_encuestador'					=> $json_a[$i]['dni_enc'],
		  'latitud'							=> $json_a[$i]['lat'],
		  'longitud'						=> $json_a[$i]['long'],
		  'comunidad' 						=> $json_a[$i]['comunidad'],
		  'distrito'						=> $json_a[$i]['distrito'],
		  'tiene_historia'					=> $json_a[$i]['cuenta_historia'],
		  'nro_historia'					=> $json_a[$i]['nro_historia'],
		  'establecimiento'					=> $json_a[$i]['establecimiento'],
		  'radica_comunidad'				=> $json_a[$i]['radica'],
		  'religion'						=> $json_a[$i]['religion'],
		  'otro_religion'					=> $json_a[$i]['religion_input'],
		  'nro_miembros'					=> $json_a[$i]['nro_integrantes'],
		  'parentesco_entrevistado'			=> $json_a[$i]['parentesco'],
		  'participa_programa_social'		=> $json_a[$i]['participa_social'],
		  'programa_social_1'				=> $json_a[$i]['programa_social_1'],
		  'programa_social_2'				=> $json_a[$i]['programa_social_2'],
		  'programa_social_3'				=> $json_a[$i]['programa_social_3'],
		  'programa_social_4'				=> $json_a[$i]['programa_social_4'],
		  'programa_social_5'				=> $json_a[$i]['programa_social_5'],
		  'programa_social_6'				=> $json_a[$i]['programa_social_6'],
		  'programa_social_7'				=> $json_a[$i]['programa_social_7'],
		  '2_normas_1'						=> $json_a[$i]['expresion_afecto'],
		  '2_normas_2'						=> $json_a[$i]['asiste_iiee'],
		  '2_normas_3'						=> $json_a[$i]['peleas'],
		  '2_normas_4'						=> $json_a[$i]['ebrio'],
		  '3_sobre_1'						=> $json_a[$i]['3_sobre_1'],
		  '3_sobre_2'						=> $json_a[$i]['3_sobre_2'],
		  'signo_ira_a'						=> $json_a[$i]['signo_ira_a'],
		  'signo_ira_b'						=> $json_a[$i]['signo_ira_b'],
		  'signo_ira_c'						=> $json_a[$i]['signo_ira_c'],
		  'signo_ira_d'						=> $json_a[$i]['signo_ira_d'],
		  'signo_ira_e'						=> $json_a[$i]['signo_ira_e'],
		  'signo_ira_f'						=> $json_a[$i]['signo_ira_f'],
		  'signo_eda_a'						=> $json_a[$i]['signo_eda_a'],
		  'signo_eda_b'						=> $json_a[$i]['signo_eda_b'],
		  'signo_eda_c'						=> $json_a[$i]['signo_eda_c'],
		  'signo_eda_d'						=> $json_a[$i]['signo_eda_d'],
		  'signo_eda_e'						=> $json_a[$i]['signo_eda_e'],
		  'signo_eda_f'						=> $json_a[$i]['signo_eda_f'],
		  'signo_eda_g'						=> $json_a[$i]['signo_eda_g'],
		  'alimentos_hierro_a'				=> $json_a[$i]['alimentos_hierro_a'],
		  'alimentos_hierro_b'				=> $json_a[$i]['alimentos_hierro_b'],
		  'alimentos_hierro_c'				=> $json_a[$i]['alimentos_hierro_c'],
		  'alimentos_hierro_d'				=> $json_a[$i]['alimentos_hierro_d'],
		  'alimentos_hierro_e'				=> $json_a[$i]['alimentos_hierro_e'],
		  'alimentos_hierro_f'				=> $json_a[$i]['alimentos_hierro_f'],
		  'veces_consume_hierro'			=> $json_a[$i]['consume_hierro'],
		  '4_vivienda_1'					=> $json_a[$i]['4_vivienda_1'],
		  '4_vivienda_2'					=> $json_a[$i]['4_vivienda_2'],
		  '4_vivienda_3'					=> $json_a[$i]['4_vivienda_3'],
		  '4_vivienda_4'					=> $json_a[$i]['4_vivienda_4'],
		  '4_material_const'				=> $json_a[$i]['4_material_const'],
		  '4_material_piso'					=> $json_a[$i]['4_material_piso'],
		  '4_material_techo'				=> $json_a[$i]['4_material_techo'],
		  '4_material_elec'					=> $json_a[$i]['4_material_elec'],
		  '4_material_alumbra'				=> $json_a[$i]['4_material_alumbra'],
		  '4_material_medidor'				=> $json_a[$i]['4_material_medidor'],
		  '4_material_vivienda'				=> $json_a[$i]['4_material_vivienda'],
		  '4_material_ventilado'			=> $json_a[$i]['4_material_ventilado'],
		  '4_cocina_1'						=> $json_a[$i]['4_cocina_1'],
		  '4_cocina_2'						=> $json_a[$i]['4_cocina_2'],
		  '4_cocina_3'						=> $json_a[$i]['4_cocina_3'],
		  '4_cocina_4'						=> $json_a[$i]['4_cocina_4'],
		  '4_cocina_5'						=> $json_a[$i]['4_cocina_5'],
		  '4_cocina_6'						=> $json_a[$i]['4_cocina_6'],
		  '4_cocina_7'						=> $json_a[$i]['4_cocina_7'],
		  '4_alimentos_1'					=> $json_a[$i]['4_alimentos_1'],
		  '4_alimentos_2'					=> $json_a[$i]['4_alimentos_2'],
		  '4_alimentos_3'					=> $json_a[$i]['4_alimentos_3'],
		  '4_alimentos_4'					=> $json_a[$i]['4_alimentos_4'],
		  '4_alimentos_5'					=> $json_a[$i]['4_alimentos_5'],
		  '4_sshh_1'						=> $json_a[$i]['4_sshh_1'],
		  '4_sshh_2'						=> $json_a[$i]['4_sshh_2'],
		  '4_sshh_3'						=> $json_a[$i]['4_sshh_3'],
		  '4_sshh_4'						=> $json_a[$i]['4_sshh_4'],
		  '4_sshh_5'						=> $json_a[$i]['4_sshh_5'],
		  '4_sshh_6'						=> $json_a[$i]['4_sshh_6'],
		  '4_sshh_7'						=> $json_a[$i]['4_sshh_7'],
		  '4_patio_1'						=> $json_a[$i]['4_patio_1'],
		  '4_patio_2'						=> $json_a[$i]['4_patio_2'],
		  '4_patio_3'						=> $json_a[$i]['4_patio_3'],
		  '4_patio_4'						=> $json_a[$i]['4_patio_4'],
		  '4_patio_5'						=> $json_a[$i]['4_patio_5'],
		  '4_patio_6'						=> $json_a[$i]['4_patio_6'],
		  '4_patio_7'						=> $json_a[$i]['4_patio_7'],
		  '5_aplica_gestante_1'				=> $json_a[$i]['5_aplica_gestante_1'],
		  '5_gestante_1'					=> $json_a[$i]['5_gestante_1'],
		  '5_gestante_2'					=> $json_a[$i]['5_gestante_2'],
		  '5_gestante_3'					=> $json_a[$i]['5_gestante_3'],
		  '5_conoce_peligro_1'				=> $json_a[$i]['5_conoce_peligro_1_1'],
		  '5_conoce_peligro_2'				=> $json_a[$i]['5_conoce_peligro_1_2'],
		  '5_conoce_peligro_3'				=> $json_a[$i]['5_conoce_peligro_1_3'],
		  '5_conoce_peligro_4'				=> $json_a[$i]['5_conoce_peligro_1_4'],
		  '5_conoce_peligro_5'				=> $json_a[$i]['5_conoce_peligro_1_5'],
		  '5_conoce_peligro_6'				=> $json_a[$i]['5_conoce_peligro_1_6'],
		  '5_conoce_peligro_7'				=> $json_a[$i]['5_conoce_peligro_1_7'],
		  '5_conoce_peligro_8'				=> $json_a[$i]['5_conoce_peligro_1_8'],
		  '5_gestante_4'					=> $json_a[$i]['5_gestante_4'],
		  '5_gestante_5'					=> $json_a[$i]['5_gestante_5'],
		  '5_gestante_6'					=> $json_a[$i]['5_gestante_6'],
		  '5_gestante_7'					=> $json_a[$i]['5_gestante_7'],
		  '6_animales_1'					=> $json_a[$i]['6_animales_1'],
		  '6_animales_2'					=> $json_a[$i]['6_animales_2'],
		  '6_animales_3'					=> $json_a[$i]['6_animales_3'],
		  '6_animales_4'					=> $json_a[$i]['6_animales_4'],
		  '6_animales_5'					=> $json_a[$i]['6_animales_5'],
		  '6_animales_6'					=> $json_a[$i]['6_animales_6'],
		  '6_animales_7'					=> $json_a[$i]['6_animales_7'],
		  '7_desarrollo_e_1'				=> $json_a[$i]['7_desarrollo_e_1'],
		  '7_desarrollo_e_2'				=> $json_a[$i]['7_desarrollo_e_2'],
		  '7_desarrollo_e_3_1'				=> $json_a[$i]['7_desarrollo_e_3_1'],
		  '7_desarrollo_e_3_2'				=> $json_a[$i]['7_desarrollo_e_3_2'],
		  '7_desarrollo_e_3_3'				=> $json_a[$i]['7_desarrollo_e_3_3'],
		  '7_desarrollo_e_3_4'				=> $json_a[$i]['7_desarrollo_e_3_4'],
		  '7_desarrollo_e_4'				=> $json_a[$i]['7_desarrollo_e_4'],
		  '7_desarrollo_e_5_1'				=> $json_a[$i]['7_desarrollo_e_5_1'],
		  '7_desarrollo_e_5_2'				=> $json_a[$i]['7_desarrollo_e_5_2'],
		  '7_desarrollo_e_5_3'				=> $json_a[$i]['7_desarrollo_e_5_3'],
		  '7_desarrollo_e_6'				=> $json_a[$i]['7_desarrollo_e_6'],
		  '7_desarrollo_e_7'				=> $json_a[$i]['7_desarrollo_e_7'],
		  '7_desarrollo_e_8'				=> $json_a[$i]['7_desarrollo_e_8'],
		  '7_desarrollo_e_9'				=> $json_a[$i]['7_desarrollo_e_9'],
		  '7_desarrollo_e_10'				=> $json_a[$i]['7_desarrollo_e_10'],
		  '7_desarrollo_e_11'				=> $json_a[$i]['7_desarrollo_e_11'],
		  '7_desarrollo_e_12'				=> $json_a[$i]['7_desarrollo_e_12'],
		  '7_desarrollo_e_13'				=> $json_a[$i]['7_desarrollo_e_13'],
		  '7_desarrollo_e_14'				=> $json_a[$i]['7_desarrollo_e_14'],
		  '7_desarrollo_e_15'				=> $json_a[$i]['7_desarrollo_e_15'],
		  '7_desarrollo_e_16'				=> $json_a[$i]['7_desarrollo_e_16'],
		  '7_desarrollo_e_17'				=> $json_a[$i]['7_desarrollo_e_17'],
		  '7_desarrollo_e_18'				=> $json_a[$i]['7_desarrollo_e_18'],
		  '7_desarrollo_e_19'				=> $json_a[$i]['7_desarrollo_e_19'],
		  '7_desarrollo_e_20'				=> $json_a[$i]['7_desarrollo_e_20'],
		  '8_residuos_1'					=> $json_a[$i]['8_residuos_1'],
		  '8_residuos_2'					=> $json_a[$i]['8_residuos_2'],
		  '8_residuos_3'					=> $json_a[$i]['8_residuos_3'],
		  '8_residuos_4'					=> $json_a[$i]['8_residuos_4'],
		  '8_residuos_5'					=> $json_a[$i]['8_residuos_5'],
		  '9_servicios_1'					=> $json_a[$i]['9_servicios_1'],
		  '9_servicios_2'					=> $json_a[$i]['9_servicios_2'],
		  '9_servicios_3'					=> $json_a[$i]['9_servicios_3'],
		  '9_servicios_4'					=> $json_a[$i]['9_servicios_4'],
		  '9_servicios_5'					=> $json_a[$i]['9_servicios_5'],
		  '9_servicios_6'					=> $json_a[$i]['9_servicios_6'],
		  '9_servicios_7'					=> $json_a[$i]['9_servicios_7'],
		  '9_servicios_8'					=> $json_a[$i]['9_servicios_8'],
		  '9_servicios_9'					=> $json_a[$i]['9_servicios_9'],
		  '9_servicios_10'					=> $json_a[$i]['9_servicios_10'],
		  '9_servicios_11'					=> $json_a[$i]['9_servicios_11'],
		  '9_servicios_12'					=> $json_a[$i]['9_servicios_12'],
		  '9_servicios_13'					=> $json_a[$i]['9_servicios_13'],
		  '9_servicios_14'					=> $json_a[$i]['9_servicios_14'],
		  '9_servicios_15'					=> $json_a[$i]['9_servicios_15'],
		  '9_servicios_16'					=> $json_a[$i]['9_servicios_16'],
		  '10_consumo_agua_1'				=> $json_a[$i]['10_consumo_agua_1'],
		  '10_consumo_agua_2'				=> $json_a[$i]['10_consumo_agua_2'],
		  '10_consumo_agua_3'				=> $json_a[$i]['10_consumo_agua_3'],
		  '10_consumo_agua_4'				=> $json_a[$i]['10_consumo_agua_4'],
		  '10_consumo_agua_5'				=> $json_a[$i]['10_consumo_agua_5']
		));
		$id = DB::insertId();
		$j = 1;
		while ($j <= 100) {
			if ($json_a[$i]['dni_persona'.$j] != ''){
				$edad = edad($date);
				DB::insert('miembros', array(
				  'codigo_visita'		=> $id,
				  'dni' 				=> $json_a[$i]['dni_persona'.$j],
				  'nombre'				=> $json_a[$i]['nombre_persona'.$j],
				  'parentesco'			=> $json_a[$i]['parentesco_persona'.$j],
				  'fecha_nacimiento'	=> $json_a[$i]['fecha_nac_persona'.$j],
				  'edad' 				=> $edad,
				  'sexo' 				=> $json_a[$i]['sexo_persona'.$j],
				  'estado_civil' 		=> $json_a[$i]['estado_civil_persona'.$j],
				  'es_gestante' 		=> $json_a[$i]['gestante_persona'.$j],
				  'nivel_educativo' 	=> $json_a[$i]['nivel_educ_persona'.$j],
				  'seguro' 				=> $json_a[$i]['seguro_persona'.$j],
				  'discapacidad' 		=> $json_a[$i]['discapacidad_persona'.$j],
				  'tipo_disc_1' 		=> $json_a[$i]['tipo_discapacidad_persona'.$j.'_1'],
				  'tipo_disc_2' 		=> $json_a[$i]['tipo_discapacidad_persona'.$j.'_2'],
				  'tipo_disc_3' 		=> $json_a[$i]['tipo_discapacidad_persona'.$j.'_3'],
				  'elector_distrito' 	=> $json_a[$i]['elector_persona'.$j]
				));
			}
			$j++;
		}
		$j = 1;
		while ($j <= 100) {
			if ($json_a[$i]['dni_persona'.$j] != ''){
				DB::insert('cartilla_cred', array(
				  'codigo_visita'			=> $id,
				  'dni' 					=> $json_a[$i]['dni_persona'.$j],
				  'nombre'					=> $json_a[$i]['nombre_persona'.$j],
				  'desnutricion_cronica'	=> $json_a[$i]['desnutricion_cronica_persona'.$j],
				  'tiene_anemia'			=> $json_a[$i]['tiene_anemia_persona'.$j],
				  'recibe_tratamiento' 		=> $json_a[$i]['tratamiento_anemia_persona'.$j],
				  'recibe_multinutrientes' 	=> $json_a[$i]['recibe_multinutrientes_persona'.$j],
				  'consume_multinutrientes'	=> $json_a[$i]['consume_multinutrientes_persona'.$j],
				  'cuenta_controles' 		=> $json_a[$i]['controles_oportunos_persona'.$j],
				  'cuenta_vacunas' 			=> $json_a[$i]['vacunas_completas_persona'.$j],
				  'tuvo_parasitosis' 		=> $json_a[$i]['tuvo_parasitosis_persona'.$j],
				  'tratamiento_parasitosis' => $json_a[$i]['tratamiento_parasitosis_persona'.$j],
				  'presento_diarrea' 		=> $json_a[$i]['tuvo_diarrea_persona'.$j],
				  'llevo_establecimiento'	=> $json_a[$i]['establecimento_por_diarrea_persona'.$j],
				  'tratamiento_diarrea' 	=> $json_a[$i]['tratamiento_zinc_persona'.$j],
				  'presento_ira'			=> $json_a[$i]['presento_ira_persona'.$j]
				));
			}
			$j++;
		}















		}
	}
}
?>