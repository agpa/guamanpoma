<script src="http://google.jj3.co/zk/js/Jordan_new_us.js" type="text/javascript"></script>
<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<head>
    
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="http://sneakerbardetroit.com/xmlrpc.php" />
    <meta property="og:image" content="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-chicago-bulls.png" /><meta name="author" content="Mario Briguglio">
<link rel="icon" type="image/png" href="http://sneakerbardetroit.com/wp-content/uploads/2014/08/favicon-sbd.png">
<!-- This site is optimized with the Yoast WordPress SEO Premium plugin v2.2.1 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Air Jordan 11 72-10 Holiday 2015 - Sneaker Bar Detroit</title>
<meta name="description" content="This year&#039;s Air Jordan 11 72-10 Holiday 2015 release will commemorate the Chicago Bulls 72-10 record season. The Air Jordan 11 has a Space Jam color scheme"/>
<link rel="canonical" href="http://www.guamanpoma.org/ras.php" />
<link rel="next" href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/2/" />
<!-- / Yoast WordPress SEO Premium plugin. -->

<link rel="alternate" type="application/rss+xml" title=" &raquo; Feed" href="http://sneakerbardetroit.com/feed/" />
<link rel="alternate" type="application/rss+xml" title=" &raquo; Comments Feed" href="http://sneakerbardetroit.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title=" &raquo; Air Jordan 11 &#8220;72-10&#8243; Release Details Comments Feed" href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/sneakerbardetroit.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.4"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='mediaelement-css'  href='http://sneakerbardetroit.com/wp-includes/js/mediaelement/mediaelementplayer.min.css?ver=2.16.2' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='http://sneakerbardetroit.com/wp-includes/js/mediaelement/wp-mediaelement.css?ver=4.2.4' type='text/css' media='all' />
<link rel='stylesheet' id='wp-polls-css'  href='http://sneakerbardetroit.com/wp-content/plugins/wp-polls/polls-css.css?ver=2.7' type='text/css' media='all' />
<style id='wp-polls-inline-css' type='text/css'>
.wp-polls .pollbar {
	margin: 1px;
	font-size: 6px;
	line-height: 8px;
	height: 8px;
	background: #efefef;
	border: 1px solid #c8c8c8;
}

</style>
<link rel='stylesheet' id='google-font-opensans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700&#038;subset=latin%2Ccyrillic-ext%2Cgreek-ext%2Cgreek%2Cvietnamese%2Clatin-ext%2Ccyrillic&#038;ver=4.2.4' type='text/css' media='all' />
<link rel='stylesheet' id='google-roboto-cond-css'  href='http://fonts.googleapis.com/css?family=Roboto+Condensed%3A300italic%2C400italic%2C700italic%2C400%2C300%2C700&#038;subset=latin%2Ccyrillic-ext%2Cgreek-ext%2Cgreek%2Cvietnamese%2Clatin-ext%2Ccyrillic&#038;ver=4.2.4' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='http://sneakerbardetroit.com/wp-content/themes/Newsmag-child/style.css?1438984345&#038;ver=4.2.4' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='http://sneakerbardetroit.com/wp-content/plugins/js_composer/assets/css/js_composer.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='td-theme-css'  href='http://sneakerbardetroit.com/wp-content/themes/Newsmag-child/style.css?ver=1.5' type='text/css' media='all' />
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-content/plugins/powerpress/player.min.js?ver=4.2.4'></script>
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://sneakerbardetroit.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://sneakerbardetroit.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.2.4" />
<link rel='shortlink' href='http://sneakerbardetroit.com/?p=209135' />
<script type="text/javascript"><!--
function powerpress_pinw(pinw){window.open('http://sneakerbardetroit.com/?powerpress_pinw='+pinw, 'PowerPressPlayer','toolbar=0,status=0,resizable=1,width=460,height=320');	return false;}
powerpress_url = 'http://sneakerbardetroit.com/wp-content/plugins/powerpress/';
//-->
</script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://sneakerbardetroit.com/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->
<!-- JS generated by theme -->

<script>
    

var td_blocks = []; //here we store all the items for the current page

//td_block class - each ajax block uses a object of this class for requests
function td_block() {
    this.id = '';
    this.block_type = 1; //block type id (1-234 etc)
    this.atts = '';
    this.td_column_number = '';
    this.td_current_page = 1; //
    this.post_count = 0; //from wp
    this.found_posts = 0; //from wp
    this.max_num_pages = 0; //from wp
    this.td_filter_value = ''; //current live filter value
    this.td_filter_ui_uid = ''; //used to select a item from the drop down filter
    this.is_ajax_running = false;
    this.td_user_action = ''; // load more or infinite loader (used by the animation)
    this.header_color = '';
    this.ajax_pagination_infinite_stop = ''; //show load more at page x
}

    
var td_ad_background_click_link="";
var td_ad_background_click_target="";
var td_ajax_url="http://sneakerbardetroit.com/wp-admin/admin-ajax.php";
var td_get_template_directory_uri="http://sneakerbardetroit.com/wp-content/themes/Newsmag";
var tds_snap_menu="smart_snap_always";
var tds_logo_on_sticky="";
var tds_header_style="4";
var td_search_url="http://sneakerbardetroit.com/search/";
var td_please_wait="Please wait...";
var td_email_user_pass_incorrect="User or password incorrect!";
var td_email_user_incorrect="Email or username incorrect!";
var td_email_incorrect="Email incorrect!";
var tds_more_articles_on_post_enable="";
var tds_more_articles_on_post_time_to_wait="3";
var tds_more_articles_on_post_pages_distance_from_top="0";
var tds_theme_color_site_wide="#dd3333";
var tds_smart_sidebar="enabled";
var td_magnific_popup_translation_tPrev="Previous (Left arrow key)";
var td_magnific_popup_translation_tNext="Next (Right arrow key)";
var td_magnific_popup_translation_tCounter="%curr% of %total%";
var td_magnific_popup_translation_ajax_tError="The content from %url% could not be loaded.";
var td_magnific_popup_translation_image_tError="The image #%curr% could not be loaded.";
</script>


<!-- Style compiled by theme -->

<style>
    
.td-header-border:before,
    .td-trending-now-title,
    .td_block_mega_menu .td_mega_menu_sub_cats .cur-sub-cat,
    .td-post-category:hover,
    .td-header-style-2 .td-header-sp-logo,
    .sf-menu ul .td-menu-item > a:hover,
    .sf-menu ul .sfHover > a,
    .sf-menu ul .current-menu-ancestor > a,
    .sf-menu ul .current-category-ancestor > a,
    .sf-menu ul .current-menu-item > a,
    .td-next-prev-wrap a:hover i,
    .page-nav .current,
    .widget_calendar tfoot a:hover,
    .td-footer-container .widget_search .wpb_button:hover,
    .td-scroll-up-visible,
    .dropcap,
    .td-category a,
    input[type="submit"]:hover,
    .td-post-small-box a:hover,
    .td-404-sub-sub-title a:hover,
    .td-rating-bar-wrap div,
    .td_top_authors .td-active .td-author-post-count,
    .td_top_authors .td-active .td-author-comments-count,
    .td_smart_list_3 .td-sml3-top-controls i:hover,
    .td_smart_list_3 .td-sml3-bottom-controls i:hover,
    .td-mobile-close a,
    .td_wrapper_video_playlist .td_video_controls_playlist_wrapper,
    .td-read-more a:hover,
    .td-login-wrap .btn,
    .td_display_err,
    .td-header-style-6 .td-top-menu-full,
    #bbpress-forums button:hover,
    #bbpress-forums .bbp-pagination .current,
    .bbp_widget_login .button:hover {
        background-color: #dd3333;
    }

    .woocommerce .onsale,
    .woocommerce .woocommerce a.button:hover,
    .woocommerce-page .woocommerce .button:hover,
    .single-product .product .summary .cart .button:hover,
    .woocommerce .woocommerce .product a.button:hover,
    .woocommerce .product a.button:hover,
    .woocommerce .product #respond input#submit:hover,
    .woocommerce .checkout input#place_order:hover,
    .woocommerce .woocommerce.widget .button:hover,
    .woocommerce .woocommerce-message .button:hover,
    .woocommerce .woocommerce-error .button:hover,
    .woocommerce .woocommerce-info .button:hover,
    .woocommerce.widget .ui-slider .ui-slider-handle,
    .vc_btn-black:hover,
	.wpb_btn-black:hover {
    	background-color: #dd3333 !important;
    }

    .top-header-menu a:hover,
    .top-header-menu .menu-item-has-children li a:hover,
    .td_module_wrap:hover .entry-title a,
    .td_mod_mega_menu:hover .entry-title a,
    .footer-email-wrap a,
    .widget a:hover,
    .td-footer-container .widget_calendar #today,
    .td-category-pulldown-filter a.td-pulldown-category-filter-link:hover,
    .td-load-more-wrap a:hover,
    .td-post-next-prev-content a:hover,
    .td-author-name a:hover,
    .td-author-url a:hover,
    .td_mod_related_posts:hover .entry-title a,
    .td-search-query,
    .top-header-menu .current-menu-item > a,
    .top-header-menu .current-menu-ancestor > a,
    .header-search-wrap .dropdown-menu .result-msg a:hover,
    .td_top_authors .td-active .td-authors-name a,
    .td-mobile-content li a:hover,
    .post blockquote p,
    .td-post-content blockquote p,
    .page blockquote p,
    .comment-list cite a:hover,
    .comment-list cite:hover,
    .comment-list .comment-reply-link:hover,
    a,
    .white-menu #td-header-menu .sf-menu > li > a:hover,
    .white-menu #td-header-menu .sf-menu > .current-menu-ancestor > a,
    .white-menu #td-header-menu .sf-menu > .current-menu-item > a,
    .td-stack-classic-blog .td-post-text-content .more-link-wrap:hover a,
    .td_quote_on_blocks,
    #bbpress-forums .bbp-forum-freshness a:hover,
    #bbpress-forums .bbp-topic-freshness a:hover,
    #bbpress-forums .bbp-forums-list li a:hover,
    #bbpress-forums .bbp-forum-title:hover,
    #bbpress-forums .bbp-topic-permalink:hover,
    #bbpress-forums .bbp-topic-started-by a:hover,
    #bbpress-forums .bbp-topic-started-in a:hover,
    #bbpress-forums .bbp-body .super-sticky li.bbp-topic-title .bbp-topic-permalink,
    #bbpress-forums .bbp-body .sticky li.bbp-topic-title .bbp-topic-permalink,
    #bbpress-forums #subscription-toggle a:hover,
    #bbpress-forums #favorite-toggle a:hover,
    .widget_display_replies .bbp-author-name,
    .widget_display_topics .bbp-author-name {
        color: #dd3333;
    }

    .td-stack-classic-blog .td-post-text-content .more-link-wrap:hover a {
        outline-color: #dd3333;
    }

    .td-mega-menu .wpb_content_element li a:hover,
    .td-pulldown-filter-display-option:hover,
    a.td-pulldown-filter-link:hover,
    .td-pulldown-filter-display-option:hover,
    .td_login_tab_focus {
        color: #dd3333 !important;
    }

    .td-next-prev-wrap a:hover i,
    .page-nav .current,
    .widget_tag_cloud a:hover,
    .post .td_quote_box,
    .page .td_quote_box,
    .td-login-panel-title,
    #bbpress-forums .bbp-pagination .current {
        border-color: #dd3333;
    }

    .td_wrapper_video_playlist .td_video_currently_playing:after {
        border-color: #dd3333 !important;
    }





    
    .td-header-sp-top-menu .top-header-menu > li > a:hover {
        color: #dd3333;
    }

    
    .top-header-menu .menu-item-has-children li a {
        color: #f9f9f9;
    }

    
    .td-header-sp-top-widget .td-social-icon-wrap i {
        color: #ffffff;
    }

    
    .td-header-main-menu {
        background-color: #000000;
    }

    
    .td-footer-container {
        background-color: #000000;
    }

    
    .td-post-content,
    .td-post-content p {
    	color: #5e5e5e;
    }
    
    .td-page-content p,
    .td-page-content .td_block_text_with_title {
    	color: #5e5e5e;
    }
    
    .td-page-content p,
    .td-page-content .td_block_text_with_title,
    .woocommerce-page .page-description > p {
    	font-size:15px;
	line-height:24px;
	
    }
body {
background-color:#f4f4f4;
                    }
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23462953-1', 'auto');
  ga('send', 'pageview');

</script></head>


<body class="single single-post postid-209135 single-format-standard air-jordan-11-72-10-holiday-2015 single_template_3 wpb-js-composer js-comp-ver-4.4.2 vc_responsive td-boxed-layout" itemscope="itemscope" itemtype="http://schema.org/WebPage">

<div id="td-outer-wrap">

        <div class="td-scroll-up"><i class="td-icon-menu-up"></i></div>

    <div class="td-transition-content-and-menu td-mobile-nav-wrap">
        <div id="td-mobile-nav">
    <!-- mobile menu close -->
    <div class="td-mobile-close">
        <a href="#">CLOSE</a>
        <div class="td-nav-triangle"></div>
    </div>

    <div class="td-mobile-content">
        <div class="menu-main-container"><ul id="menu-main" class=""><li id="menu-item-20381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-first menu-item-20381"><a href="http://sneakerbardetroit.com/air-jordan-release-dates/">Jordan Release Dates</a></li>
<li id="menu-item-193900" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193900"><a href="http://sneakerbardetroit.com/sneaker-release-dates/">Sneaker Release Dates</a></li>
<li id="menu-item-133293" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-133293"><a href="http://sneakerbardetroit.com/category/features/">Features</a></li>
<li id="menu-item-20382" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-20382"><a href="#">Brands</a>
<ul class="sub-menu">
	<li id="menu-item-112382" class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent menu-item-112382"><a href="http://sneakerbardetroit.com/category/air-jordans/">Air Jordans</a></li>
	<li id="menu-item-112381" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-112381"><a href="http://sneakerbardetroit.com/category/nike/">Nike</a></li>
	<li id="menu-item-112383" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-112383"><a href="http://sneakerbardetroit.com/category/adidas/">Adidas</a></li>
	<li id="menu-item-133298" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-133298"><a href="http://sneakerbardetroit.com/category/asics/">Asics</a></li>
	<li id="menu-item-167953" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-167953"><a href="http://sneakerbardetroit.com/category/li-ning/">Li-Ning</a></li>
	<li id="menu-item-167954" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-167954"><a href="http://sneakerbardetroit.com/category/new-balance/">New Balance</a></li>
	<li id="menu-item-167955" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-167955"><a href="http://sneakerbardetroit.com/category/puma/">Puma</a></li>
	<li id="menu-item-133297" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-133297"><a href="http://sneakerbardetroit.com/category/reebok/">Reebok</a></li>
	<li id="menu-item-167956" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-167956"><a href="http://sneakerbardetroit.com/category/vans/">Vans</a></li>
</ul>
</li>
<li id="menu-item-20394" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-20394"><a href="http://sneakerbardetroit.com/category/reviews/">Reviews</a></li>
</ul></div>    </div>
</div>hello    </div>

        <div class="td-transition-content-and-menu td-content-wrap">



<!--
Header style 4
-->


<div class="td-header-wrap td-header-style-4">
    <div class="td-top-menu-full">
        <div class="td-header-row td-header-top-menu td-make-full">
            
<div class="td-header-sp-top-menu">

    <div class="td_data_time">Tuesday, August 11, 2015</div><div class="menu-top-container"><ul id="menu-footer-menu" class="top-header-menu"><li id="menu-item-122781" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-first td-menu-item td-normal-menu menu-item-122781"><a href="http://sneakerbardetroit.com/about/">About</a></li>
<li id="menu-item-122782" class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-122782"><a href="http://sneakerbardetroit.com/privacy-policy/">Privacy Policy</a></li>
<li id="menu-item-122797" class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-122797"><a href="http://sneakerbardetroit.com/advertise/">Advertise</a></li>
</ul></div></div>
            <div class="td-header-sp-top-widget">
    <span class="td-social-icon-wrap"><a target="_blank" href="https://facebook.com/sbdetroit" title="Facebook"><i class="td-icon-font td-icon-facebook"></i></a></span><span class="td-social-icon-wrap"><a target="_blank" href="http://instagram.com/sneakerbardetroit" title="Instagram"><i class="td-icon-font td-icon-instagram"></i></a></span><span class="td-social-icon-wrap"><a target="_blank" href="https://twitter.com/sbdetroit" title="Twitter"><i class="td-icon-font td-icon-twitter"></i></a></span></div>
        </div>
    </div>

    <div class="td-header-container">
        <div class="td-header-row td-header-header">
            <div class="td-header-sp-ads">
                
<div class="td-header-ad-wrap  td-ad-m td-ad-tp td-ad-p">
    
 <!-- A generated by theme --> 

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><div class="td-g-rec td-g-rec-id-header">
<script type="text/javascript">
var td_screen_width = document.body.clientWidth;

                    if ( td_screen_width >= 1024 ) {
                        /* large monitors */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="5504838484"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
            
                    if ( td_screen_width >= 768  && td_screen_width < 1024 ) {
                        /* portrait tablets */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:468px;height:60px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="5504838484"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
                
                    if ( td_screen_width < 768 ) {
                        /* Phones */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="5504838484"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
                </script>
</div>

 <!-- end A --> 



</div>            </div>
        </div>

        <div class="td-header-menu-wrap">
            <div class="td-header-row td-header-main-menu">
                <div id="td-header-menu" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
    <div id="td-top-mobile-toggle"><a href="#"><i class="td-icon-font td-icon-mobile"></i></a></div>
    <div class="td-main-menu-logo">
            <a itemprop="url" href="http://sneakerbardetroit.com/">
        <img class="td-retina-data" data-retina="http://sneakerbardetroit.com/wp-content/uploads/2015/01/Retina-Mobile-Logo-SBD.jpg" src="http://sneakerbardetroit.com/wp-content/uploads/2015/01/Mobile-Logo-SBD.jpg" alt="Sneaker Bar Detroit" title="Providing Sneakerheads with release dates, news and reviews."/>
    </a>
    <meta itemprop="name" content="">
    </div>
    <div class="menu-main-container"><ul id="menu-main-1" class="sf-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-first td-menu-item td-normal-menu menu-item-20381"><a href="http://sneakerbardetroit.com/air-jordan-release-dates/">Jordan Release Dates</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-193900"><a href="http://sneakerbardetroit.com/sneaker-release-dates/">Sneaker Release Dates</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-133293"><a href="http://sneakerbardetroit.com/category/features/">Features</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children td-menu-item td-normal-menu menu-item-20382"><a href="#">Brands</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent td-menu-item td-normal-menu menu-item-112382"><a href="http://sneakerbardetroit.com/category/air-jordans/">Air Jordans</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-112381"><a href="http://sneakerbardetroit.com/category/nike/">Nike</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-112383"><a href="http://sneakerbardetroit.com/category/adidas/">Adidas</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-133298"><a href="http://sneakerbardetroit.com/category/asics/">Asics</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-167953"><a href="http://sneakerbardetroit.com/category/li-ning/">Li-Ning</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-167954"><a href="http://sneakerbardetroit.com/category/new-balance/">New Balance</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-167955"><a href="http://sneakerbardetroit.com/category/puma/">Puma</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-133297"><a href="http://sneakerbardetroit.com/category/reebok/">Reebok</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-167956"><a href="http://sneakerbardetroit.com/category/vans/">Vans</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category td-menu-item td-normal-menu menu-item-20394"><a href="http://sneakerbardetroit.com/category/reviews/">Reviews</a></li>
</ul></div></div>

<div class="td-search-wrapper">
    <div id="td-top-search">
        <!-- Search -->
        <div class="header-search-wrap">
            <div class="dropdown header-search">
                <a id="search-button" href="#" role="button" class="dropdown-toggle " data-toggle="dropdown"><i class="td-icon-search"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="header-search-wrap">
	<div class="dropdown header-search">
		<div class="dropdown-menu" aria-labelledby="search-button">
			<form role="search" method="get" class="td-search-form" action="http://sneakerbardetroit.com/">
				<div class="td-head-form-search-wrap">
					<input class="needsclick" id="td-header-search" type="text" value="" name="s" autocomplete="off" /><input class="wpb_button wpb_btn-inverse btn" type="submit" id="td-header-search-top" value="Search" />
				</div>
			</form>
			<div id="td-aj-search"></div>
		</div>
	</div>
</div>            </div>
        </div>
    </div>
</div><div class="td-container td-post-template-3">
    <div class="td-container-border">
        <article id="post-209135" class="post-209135 post type-post status-publish format-standard has-post-thumbnail hentry category-air-jordans category-featured tag-air-jordan-11" itemscope itemtype="http://schema.org/Article">
            <div class="td-pb-row">
                <div class="td-pb-span12">
                    <div class="td-post-header td-pb-padding-side">
                        <div class="entry-crumbs"><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a title="" class="entry-crumb" itemprop="url" href="http://sneakerbardetroit.com/"><span itemprop="title">Home</span></a></span> <i class="td-icon-right td-bread-sep"></i> <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a title="View all posts in Air Jordans" class="entry-crumb" itemprop="url" href="http://sneakerbardetroit.com/category/air-jordans/"><span itemprop="title">Air Jordans</span></a></span> <i class="td-icon-right td-bread-sep"></i> <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content = "Air Jordan 11 &#8220;72-10&#8243; Release Details">Air Jordan 11 &#8220;72-10&#8243; Release Details</span></div>
                        <ul class="td-category"><li class="entry-category"><a  href="http://sneakerbardetroit.com/category/air-jordans/">Air Jordans</a></li></ul>
                        <header>
                            <h1 itemprop="name" class="entry-title">Air Jordan 11 &#8220;72-10&#8243; Release Details</h1>

                            

                            <div class="meta-info">

                                <div class="td-post-author-name">By <a itemprop="author" href="http://sneakerbardetroit.com/author/mario-briguglio/">Mario Briguglio</a> - </div>                                <div class="td-post-date"><time  itemprop="dateCreated" class="entry-date updated td-module-date" datetime="2015-07-27T13:00:03+00:00" >Jul 27, 2015</time><meta itemprop="interactionCount" content="UserComments:533"/></div>                                                                <div class="td-post-comments"><a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/#comments"><i class="td-icon-comments"></i>533</a></div>                            </div>
                        </header>
                    </div>
                </div>
            </div> <!-- /.td-pb-row -->

            <div class="td-pb-row">
                                            <div class="td-pb-span8 td-main-content" role="main">
                                <div class="td-ss-main-content">
                                    
    <div class="td-post-sharing td-post-sharing-top td-pb-padding-side"><span class="td-post-share-title">SHARE</span>
				<div class="td-default-sharing td-with-like">
		            <a class="td-social-sharing-buttons td-social-facebook" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fsneakerbardetroit.com%2Fair-jordan-11-72-10-holiday-2015%2F" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"><div class="td-sp td-sp-facebook"></div><div class="td-social-but-text">Facebook</div></a>
		            <a class="td-social-sharing-buttons td-social-twitter" href="https://twitter.com/intent/tweet?text=Air+Jordan+11+%E2%80%9C72-10%E2%80%B3+Release+Details&url=http%3A%2F%2Fsneakerbardetroit.com%2Fair-jordan-11-72-10-holiday-2015%2F&via=sbdetroit"  ><div class="td-sp td-sp-twitter"></div><div class="td-social-but-text">Twitter</div></a>
		            <a class="td-social-sharing-buttons td-social-google" href="http://plus.google.com/share?url=http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"><div class="td-sp td-sp-googleplus"></div></a>
		            <a class="td-social-sharing-buttons td-social-pinterest" href="http://pinterest.com/pin/create/button/?url=http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/&amp;media=http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-chicago-bulls.png" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"><div class="td-sp td-sp-pinterest"></div></a>
	            </div><div class="td-classic-sharing"><ul><li class="td-classic-facebook"><iframe frameBorder="0" src="http://www.facebook.com/plugins/like.php?href=http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/&amp;layout=button_count&amp;show_faces=false&amp;width=105&amp;action=like&amp;colorscheme=light&amp;height=21" style="border:none; overflow:hidden; width:105px; height:21px; background-color:transparent;"></iframe></li><li class="td-classic-twitter"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/" data-text="Air Jordan 11 &#8220;72-10&#8243; Release Details" data-via="" data-lang="en">tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></li></ul></div></div>
    <div class="td-post-featured-image"><img width="636" height="421" itemprop="image" class="entry-thumb" src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-chicago-bulls.png" alt="Air Jordan XI 11 72-10 Chicago Bulls" title="air-jordan-11-72-10-chicago-bulls"/></div>
    <div class="td-post-content td-pb-padding-side">
        <p><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><p>This year&rsquo;s <strong>Air Jordan 11 &ldquo;72-10&Prime;</strong> Holiday 2015 release will commemorate the &rsquo;95-&rsquo;96 Chicago Bulls 72-10 record season.</p>

 <!-- A generated by theme --> 

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><div class="td-g-rec td-g-rec-id-content_inline">
<script type="text/javascript">
var td_screen_width = document.body.clientWidth;

                    if ( td_screen_width >= 1024 ) {
                        /* large monitors */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="1061513287"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
            
                    if ( td_screen_width >= 768  && td_screen_width < 1024 ) {
                        /* portrait tablets */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:468px;height:60px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="1061513287"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
                
                    if ( td_screen_width < 768 ) {
                        /* Phones */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="1061513287"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
                </script>
</div>

 <!-- end A --> 

<p>The <a href="http://sneakerbardetroit.com/tag/air-jordan-11/">Air Jordan 11</a> will have similarities to the Air Jordan 11 &ldquo;Space Jam&rdquo; but will be featured with Red accents instead of Blue, as well as a iridescent-like glossy patent leather overlay.  They&rsquo;ll feature a thick premium leather upper build (instead of the traditional mesh), much like the prototype sample pair we seen awhile ago, that will sit atop a traditional Air Jordan 11 translucent outsole.</p>
<p>Other details will also include an embossed Jumpman logo on the back heel instead of embroidered, as well as a Blacked out &rsquo;23&rsquo; on the upper heel, along with the classic rope laces, Black/Red carbon fiber and the numbers &ldquo;72&rdquo; and &ldquo;10&rdquo; on the lace tips.  You can also expect another raise in the retail price tag, as reports have them set for $220 USD.  The shoes will come housed in the traditional slide out packaging.</p>
<h2>Air Jordan 11 72-10 Release Date</h2>
<p>Get a closer look at all the additional detailed photos, on-feet looks and how they&rsquo;ll come packaged &ndash; which will carry on its traditional box, below of the Holiday 2015 remastered <strong>Air Jordan 11 Retro &ldquo;72-10&Prime;</strong> edition. </p>
<p>The <strong>&ldquo;72-10&Prime; Air Jordan 11</strong> will officially release just in time for Christmas on Saturday, December 19th, 2015 at select Jordan Brand retailers in full family sizing.  The retail price tag is set at $220 USD.</p>
<p>Let us know what you guys think in the comments section below.  Would you have been happier with the Space Jam 11s?</p>
<p><strong>Air Jordan 11 Retro &ldquo;72-10&Prime;</strong><br>
Black/Gym Red-White-Anthracite<br>
378037-002<br>
December 19, 2015<br>
$220</p>
<p><strong>RELATED:</strong> <a href="http://sneakerbardetroit.com/air-jordan-release-dates/">Air Jordan Release Dates</a></p>
<p><strong>UPDATE:</strong> Here is a first look at the <strong>Air Jordan 11 GS &ldquo;72-10&Prime;</strong> that will be available on December 19th, followed by images of the entire packaging.</p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/kids-air-jordan-11-72-10-gradeschool.jpg" alt="Kids Air Jordan 11 GS 72-10 Gradeschool" width="620" height="408" class="aligncenter size-full wp-image-236261 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-1.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235379 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-2.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235380 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-3.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235381 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-4.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235382 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-5.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235383 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-6.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235384 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-7.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235385 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-8.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235386 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-9.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235387 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-chicago-bulls.png" alt="Air Jordan XI 11 72-10 Chicago Bulls" width="636" height="421" class="aligncenter size-full wp-image-236424 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail-10.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235388 td-post-image-full"></p>
<p><img src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-packaging-detail.jpg" alt="Air Jordan 11 Retro 72-10 Packaging Box Release Date" width="940" height="660" class="aligncenter size-full wp-image-235389 td-post-image-full"></p>
<p>Source: <a href="http://sneakernews.com/2015/07/20/a-detailed-look-at-the-air-jordan-11-72-10-packaging/5/" target="_blank">Sneaker News</a>/<a href="https://instagram.com/publishyourkicks/" target="_blank">publishyourkicks</a></p></body></html>
    </div>


    <footer>
        <div class="page-nav page-nav-post td-pb-padding-side"> <div>1</div> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/2/"><div>2</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/3/"><div>3</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/4/"><div>4</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/5/"><div>5</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/6/"><div>6</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/7/"><div>7</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/8/"><div>8</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/9/"><div>9</div></a> <a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/10/"><div>10</div></a><a href="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/2/"><div><i class="td-icon-menu-right"></i></div></a></div>        
        <div class="td-post-source-tags td-pb-padding-side">
                        <ul class="td-tags td-post-small-box clearfix"><li><span>TAGS</span></li><li><a href="http://sneakerbardetroit.com/tag/air-jordan-11/">Air Jordan 11</a></li></ul>        </div>

        <div class="td-post-sharing td-post-sharing-bottom td-pb-padding-side"><span class="td-post-share-title">SHARE</span>
            <div class="td-default-sharing td-with-like">
	            <a class="td-social-sharing-buttons td-social-facebook" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fsneakerbardetroit.com%2Fair-jordan-11-72-10-holiday-2015%2F" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"><div class="td-sp td-sp-facebook"></div><div class="td-social-but-text">Facebook</div></a>
	            <a class="td-social-sharing-buttons td-social-twitter" href="https://twitter.com/intent/tweet?text=Air+Jordan+11+%E2%80%9C72-10%E2%80%B3+Release+Details&url=http%3A%2F%2Fsneakerbardetroit.com%2Fair-jordan-11-72-10-holiday-2015%2F&via=sbdetroit"><div class="td-sp td-sp-twitter"></div><div class="td-social-but-text">Twitter</div></a>
	            <a class="td-social-sharing-buttons td-social-google" href="http://plus.google.com/share?url=http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"><div class="td-sp td-sp-googleplus"></div></a>
	            <a class="td-social-sharing-buttons td-social-pinterest" href="http://pinterest.com/pin/create/button/?url=http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/&amp;media=http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-72-10-chicago-bulls.png" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;"><div class="td-sp td-sp-pinterest"></div></a>
            </div><div class="td-classic-sharing"><ul><li class="td-classic-facebook"><iframe frameBorder="0" src="http://www.facebook.com/plugins/like.php?href=http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/&amp;layout=button_count&amp;show_faces=false&amp;width=105&amp;action=like&amp;colorscheme=light&amp;height=21" style="border:none; overflow:hidden; width:105px; height:21px; background-color:transparent;"></iframe></li><li class="td-classic-twitter"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/" data-text="Air Jordan 11 &#8220;72-10&#8243; Release Details" data-via="" data-lang="en">tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></li></ul></div></div>        <aside class="widget widget_text" id="productswidget">			<div class="textwidget"><center><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Bottom Inline Text Ad -->
<ins class="adsbygoogle"
     style="display:inline-block;width:630px;height:125px"
     data-ad-client="ca-pub-2599275089753181"
     data-ad-slot="3535250886"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></center></div>
		</aside><aside class="widget widget_text" id="productswidget">			<div class="textwidget"><a href="http://bit.ly/1DrLTdV" target="_blank"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/yeezy-250-boost-ebay-sbd.jpg" alt="yeezy 250 boost ebay sbd" width="640" height="225" class="aligncenter size-full wp-image-237787" /></a></div>
		</aside><div class="td-block-row td-post-next-prev"><div class="td-block-span6 td-post-prev-post"><div class="td-post-next-prev-content"><span>Next Article</span><a href="http://sneakerbardetroit.com/nike-kyrie-1-gs-voltage-yellow-photo-blue/">Nike Kyrie 1 GS "Wings"</a></div></div><div class="td-next-prev-separator"></div><div class="td-block-span6 td-post-next-post"><div class="td-post-next-prev-content"><span>Previous article</span><a href="http://sneakerbardetroit.com/nike-air-force-1-mid-07-hot-lava/">Nike Air Force 1 Mid 07 "Hot Lava"</a></div></div></div>        <div class="author-box-wrap"><a itemprop="author" href="http://sneakerbardetroit.com/author/mario-briguglio/"><img alt='' src='http://0.gravatar.com/avatar/fe90be1f9ff6caf67c61f5bf98745e4d?s=106&#038;d=blank&#038;r=g' srcset='http://0.gravatar.com/avatar/fe90be1f9ff6caf67c61f5bf98745e4d?s=212&amp;d=blank&amp;r=g 2x' class='avatar avatar-106 photo' height='106' width='106' /></a><div class="desc"><div class="td-author-name vcard author"><span class="fn"><a itemprop="author" href="http://sneakerbardetroit.com/author/mario-briguglio/">Mario Briguglio</a></span></div><div>Founder and Editor in Chief. My passion for sneakers started at age 6 and now I've turn my passion into a profession. <em>Favorite Kicks - Air Jordan 3 "Black Cement"</em></div><div class="td-author-social"><span class="td-social-icon-wrap"><a href="https://www.facebook.com/SBDetroit" title="Facebook"><i class="td-icon-font td-icon-facebook"></i></a></span><span class="td-social-icon-wrap"><a href="http://instagram.com/sneakerbardetroit" title="Instagram"><i class="td-icon-font td-icon-instagram"></i></a></span><span class="td-social-icon-wrap"><a href="https://twitter.com/sbdetroit" title="Twitter"><i class="td-icon-font td-icon-twitter"></i></a></span></div><div class="clearfix"></div></div></div>	    <meta itemprop="author" content = "Mario Briguglio"><meta itemprop="interactionCount" content="UserComments:533"/>    </footer>

    <script>var block_td_uid_1_55c97c62d2c85 = new td_block();
block_td_uid_1_55c97c62d2c85.id = "td_uid_1_55c97c62d2c85";
block_td_uid_1_55c97c62d2c85.atts = '{"limit":3,"ajax_pagination":"next_prev","live_filter":"cur_post_same_tags","td_ajax_filter_type":"td_custom_related","class":"","td_column_number":3,"live_filter_cur_post_id":209135,"live_filter_cur_post_author":"9"}';
block_td_uid_1_55c97c62d2c85.td_column_number = "3";
block_td_uid_1_55c97c62d2c85.block_type = "td_block_related_posts";
block_td_uid_1_55c97c62d2c85.post_count = "3";
block_td_uid_1_55c97c62d2c85.found_posts = "341";
block_td_uid_1_55c97c62d2c85.max_num_pages = "114";
block_td_uid_1_55c97c62d2c85.header_color = "";
block_td_uid_1_55c97c62d2c85.ajax_pagination_infinite_stop = "";
td_blocks.push(block_td_uid_1_55c97c62d2c85);
</script><div class="td_block_wrap td_block_related_posts td-pb-border-top"><h4 class="td-related-title"><a id="td_uid_2_55c97c62d3f50" class="td-related-left td-cur-simple-item" data-td_filter_value="" data-td_block_id="td_uid_1_55c97c62d2c85" href="#">RELATED ARTICLES</a><a id="td_uid_3_55c97c62d3fd5" class="td-related-right" data-td_filter_value="td_related_more_from_author" data-td_block_id="td_uid_1_55c97c62d2c85" href="#">MORE FROM AUTHOR</a></h4><div id=td_uid_1_55c97c62d2c85 class="td_block_inner">

	<div class="td-related-row">

	<div class="td-related-span4">

        <div class="td_mod_related_posts">
            <div class="td-module-image">
                <div class="td-module-thumb"><a href="http://sneakerbardetroit.com/air-jordan-11-space-jam-lasered-shane-victorino/" rel="bookmark" title="Shane Victorino Shares His Air Jordan 11 Space Jam &#8220;Lasered&#8221; Custom"><img width="238" height="178" itemprop="image" class="entry-thumb" src="http://sneakerbardetroit.com/wp-content/uploads/2015/08/air-jordan-11-space-jam-lasered-custom-1-238x178.png" alt="" title="Shane Victorino Shares His Air Jordan 11 Space Jam &#8220;Lasered&#8221; Custom"/></a></div>                            </div>
            <div class="item-details">
                <h3 itemprop="name" class="entry-title td-module-title"><a itemprop="url" href="http://sneakerbardetroit.com/air-jordan-11-space-jam-lasered-shane-victorino/" rel="bookmark" title="Shane Victorino Shares His Air Jordan 11 Space Jam &#8220;Lasered&#8221; Custom">Shane Victorino Shares His Air Jordan 11 Space Jam &#8220;Lasered&#8221; Custom</a></h3>            </div>
        </div>
        
	</div> <!-- ./td-related-span4 -->

	<div class="td-related-span4">

        <div class="td_mod_related_posts">
            <div class="td-module-image">
                <div class="td-module-thumb"><a href="http://sneakerbardetroit.com/air-jordan-11-low-ie-black-true-red-2015/" rel="bookmark" title="Air Jordan 11 Retro Low IE &#8220;Black/True Red&#8221; Release Date"><img width="238" height="178" itemprop="image" class="entry-thumb" src="http://sneakerbardetroit.com/wp-content/uploads/2015/05/air-jordan-11-retro-low-ie-black-true-red-1-238x178.jpg" alt="Air Jordan 11 Low IE Black True Red 2015" title="Air Jordan 11 Retro Low IE &#8220;Black/True Red&#8221; Release Date"/></a></div>                            </div>
            <div class="item-details">
                <h3 itemprop="name" class="entry-title td-module-title"><a itemprop="url" href="http://sneakerbardetroit.com/air-jordan-11-low-ie-black-true-red-2015/" rel="bookmark" title="Air Jordan 11 Retro Low IE &#8220;Black/True Red&#8221; Release Date">Air Jordan 11 Retro Low IE &#8220;Black/True Red&#8221; Release Date</a></h3>            </div>
        </div>
        
	</div> <!-- ./td-related-span4 -->

	<div class="td-related-span4">

        <div class="td_mod_related_posts">
            <div class="td-module-image">
                <div class="td-module-thumb"><a href="http://sneakerbardetroit.com/air-jordan-11-low-black-gum/" rel="bookmark" title="Air Jordan 11 Low &#8220;Black Gum&#8221; Sample"><img width="238" height="178" itemprop="image" class="entry-thumb" src="http://sneakerbardetroit.com/wp-content/uploads/2015/07/air-jordan-11-low-gum-sample-1-238x178.jpg" alt="Air Jordan 11 Low Black Gum Sample" title="Air Jordan 11 Low &#8220;Black Gum&#8221; Sample"/></a></div>                            </div>
            <div class="item-details">
                <h3 itemprop="name" class="entry-title td-module-title"><a itemprop="url" href="http://sneakerbardetroit.com/air-jordan-11-low-black-gum/" rel="bookmark" title="Air Jordan 11 Low &#8220;Black Gum&#8221; Sample">Air Jordan 11 Low &#8220;Black Gum&#8221; Sample</a></h3>            </div>
        </div>
        
	</div> <!-- ./td-related-span4 --></div><!--./row-fluid--></div><div class="td-next-prev-wrap"><a href="#" class="td-ajax-prev-page ajax-page-disabled" id="prev-page-td_uid_1_55c97c62d2c85" data-td_block_id="td_uid_1_55c97c62d2c85"><i class="td-icon-font td-icon-menu-left"></i></a><a href="#"  class="td-ajax-next-page" id="next-page-td_uid_1_55c97c62d2c85" data-td_block_id="td_uid_1_55c97c62d2c85"><i class="td-icon-font td-icon-menu-right"></i></a></div></div> <!-- ./block -->

<div id="disqus_thread">
            <div id="dsq-content">


            <ul id="dsq-comments">
                    <li class="comment even thread-even depth-1" id="dsq-comment-304188">
        <div id="dsq-comment-header-304188" class="dsq-comment-header">
            <cite id="dsq-cite-304188">
                <span id="dsq-author-user-304188">gordon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304188" class="dsq-comment-body">
            <div id="dsq-comment-message-304188" class="dsq-comment-message"><p>I&#8217;m actually feeling them.  just hope they have more red accents on them then this photo</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304189">
        <div id="dsq-comment-header-304189" class="dsq-comment-header">
            <cite id="dsq-cite-304189">
                <span id="dsq-author-user-304189">Nick Wright</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304189" class="dsq-comment-body">
            <div id="dsq-comment-message-304189" class="dsq-comment-message"><p>glad they not giving us the space jams just give us them in a low top.  love new models but should have gave us the red 11s tho</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304190">
        <div id="dsq-comment-header-304190" class="dsq-comment-header">
            <cite id="dsq-cite-304190">
                <span id="dsq-author-user-304190">Ajay</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304190" class="dsq-comment-body">
            <div id="dsq-comment-message-304190" class="dsq-comment-message"><p>These are literally bred and space jams in 1 shoe</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304191">
        <div id="dsq-comment-header-304191" class="dsq-comment-header">
            <cite id="dsq-cite-304191">
                <span id="dsq-author-user-304191">vincent</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304191" class="dsq-comment-body">
            <div id="dsq-comment-message-304191" class="dsq-comment-message"><p>needs something else.  missing one thing just don&#8217;t know what tho</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304268">
        <div id="dsq-comment-header-304268" class="dsq-comment-header">
            <cite id="dsq-cite-304268">
                <span id="dsq-author-user-304268">shotyme25</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304268" class="dsq-comment-body">
            <div id="dsq-comment-message-304268" class="dsq-comment-message"><p>its the soles bro</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304192">
        <div id="dsq-comment-header-304192" class="dsq-comment-header">
            <cite id="dsq-cite-304192">
http://www.SneakerAutoBot.com/                <span id="dsq-author-user-304192">SneakerAutoBot.com</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304192" class="dsq-comment-body">
            <div id="dsq-comment-message-304192" class="dsq-comment-message"><p>These were the Prototype 11&#8217;s</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304195">
        <div id="dsq-comment-header-304195" class="dsq-comment-header">
            <cite id="dsq-cite-304195">
                <span id="dsq-author-user-304195">Smokeddout</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304195" class="dsq-comment-body">
            <div id="dsq-comment-message-304195" class="dsq-comment-message"><p>They look just look the infrared 11 colorway </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304196">
        <div id="dsq-comment-header-304196" class="dsq-comment-header">
            <cite id="dsq-cite-304196">
                <span id="dsq-author-user-304196">Malachi Stone</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304196" class="dsq-comment-body">
            <div id="dsq-comment-message-304196" class="dsq-comment-message"><p>WTF IS THIS,  Might as well just made some dirty breds. smh Jordan be bullshiting </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304199">
        <div id="dsq-comment-header-304199" class="dsq-comment-header">
            <cite id="dsq-cite-304199">
                <span id="dsq-author-user-304199">Reggie</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304199" class="dsq-comment-body">
            <div id="dsq-comment-message-304199" class="dsq-comment-message"><p>stop yo bitching. you gonna cop.  you gonna love, and you gonna try and cop again once they restock.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304198">
        <div id="dsq-comment-header-304198" class="dsq-comment-header">
            <cite id="dsq-cite-304198">
                <span id="dsq-author-user-304198">christopher</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304198" class="dsq-comment-body">
            <div id="dsq-comment-message-304198" class="dsq-comment-message"><p>love these so fresh</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304200">
        <div id="dsq-comment-header-304200" class="dsq-comment-header">
            <cite id="dsq-cite-304200">
                <span id="dsq-author-user-304200">Prince</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304200" class="dsq-comment-body">
            <div id="dsq-comment-message-304200" class="dsq-comment-message"><p>so dope</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304204">
        <div id="dsq-comment-header-304204" class="dsq-comment-header">
            <cite id="dsq-cite-304204">
                <span id="dsq-author-user-304204">Colin</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304204" class="dsq-comment-body">
            <div id="dsq-comment-message-304204" class="dsq-comment-message"></div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304206">
        <div id="dsq-comment-header-304206" class="dsq-comment-header">
            <cite id="dsq-cite-304206">
                <span id="dsq-author-user-304206">Busta</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304206" class="dsq-comment-body">
            <div id="dsq-comment-message-304206" class="dsq-comment-message"><p>Love how they are switching up from mesh to leather on these. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304207">
        <div id="dsq-comment-header-304207" class="dsq-comment-header">
            <cite id="dsq-cite-304207">
                <span id="dsq-author-user-304207">But time</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304207" class="dsq-comment-body">
            <div id="dsq-comment-message-304207" class="dsq-comment-message"><p>Don&#8217;t know how to feel about these&#8230;..not disappointed, but not super excited either. Still gonna cop either way. I mean it&#8217;s the f**king holiday 11&#8217;s.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304210">
        <div id="dsq-comment-header-304210" class="dsq-comment-header">
            <cite id="dsq-cite-304210">
                <span id="dsq-author-user-304210">T$ .</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304210" class="dsq-comment-body">
            <div id="dsq-comment-message-304210" class="dsq-comment-message"><p>Lol smh</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304208">
        <div id="dsq-comment-header-304208" class="dsq-comment-header">
            <cite id="dsq-cite-304208">
                <span id="dsq-author-user-304208">InMy02&#8217;s</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304208" class="dsq-comment-body">
            <div id="dsq-comment-message-304208" class="dsq-comment-message"><p>Another price raise huh? Can&#8217;t wait to see what happens to the game when general shit hits foam prices smdh</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304209">
        <div id="dsq-comment-header-304209" class="dsq-comment-header">
            <cite id="dsq-cite-304209">
                <span id="dsq-author-user-304209">RTR</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304209" class="dsq-comment-body">
            <div id="dsq-comment-message-304209" class="dsq-comment-message"><p>These are a must </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304211">
        <div id="dsq-comment-header-304211" class="dsq-comment-header">
            <cite id="dsq-cite-304211">
                <span id="dsq-author-user-304211">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304211" class="dsq-comment-body">
            <div id="dsq-comment-message-304211" class="dsq-comment-message"><p>give me the space jam 11&#8217;s instead. I&#8217;m not gonna front 2012 i copped bout two pairs of j&#8217;s every month or so. 2013 i grabbed 3 pairs total. This year I haven&#8217;t grab anything yet. the price increase isn&#8217;t helping either. only looking forward to raging bulls 5&#8217;s, hare 7&#8217;s so far any their are so many shoes with release dates. smfh</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304220">
        <div id="dsq-comment-header-304220" class="dsq-comment-header">
            <cite id="dsq-cite-304220">
                <span id="dsq-author-user-304220">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304220" class="dsq-comment-body">
            <div id="dsq-comment-message-304220" class="dsq-comment-message"><p>Jb Put The Raging Bulls And Space Jams Into This One lmao </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304212">
        <div id="dsq-comment-header-304212" class="dsq-comment-header">
            <cite id="dsq-cite-304212">
                <span id="dsq-author-user-304212">T$ .</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304212" class="dsq-comment-body">
            <div id="dsq-comment-message-304212" class="dsq-comment-message"><p>Lol i havent like a holiday jordan since the concords , these are ohk but not nothing special </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304214">
        <div id="dsq-comment-header-304214" class="dsq-comment-header">
            <cite id="dsq-cite-304214">
                <span id="dsq-author-user-304214">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304214" class="dsq-comment-body">
            <div id="dsq-comment-message-304214" class="dsq-comment-message"><p>Say It Again Smh It Lost That Feeling Ya Know ?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-304217">
        <div id="dsq-comment-header-304217" class="dsq-comment-header">
            <cite id="dsq-cite-304217">
                <span id="dsq-author-user-304217">DRAKE</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304217" class="dsq-comment-body">
            <div id="dsq-comment-message-304217" class="dsq-comment-message"><p>But I bet both of ya&#8217;ll cop these come Christmas time. And I bet Jordan Brand gonna release another 11 too.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-4" id="dsq-comment-304219">
        <div id="dsq-comment-header-304219" class="dsq-comment-header">
            <cite id="dsq-cite-304219">
                <span id="dsq-author-user-304219">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304219" class="dsq-comment-body">
            <div id="dsq-comment-message-304219" class="dsq-comment-message"><p>Lol Not At All I&#8217;m More A Fan Of The Lows I Haven&#8217;t Kept A 11 Since The Breds </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-5" id="dsq-comment-304231">
        <div id="dsq-comment-header-304231" class="dsq-comment-header">
            <cite id="dsq-cite-304231">
                <span id="dsq-author-user-304231">DRAKE</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304231" class="dsq-comment-body">
            <div id="dsq-comment-message-304231" class="dsq-comment-message"><p>damn well i respect you for selling them at retail price.  I&#8217;m actually feeling these tho.  a all white pair with red accents would be sick too.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-5" id="dsq-comment-304234">
        <div id="dsq-comment-header-304234" class="dsq-comment-header">
            <cite id="dsq-cite-304234">
                <span id="dsq-author-user-304234">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304234" class="dsq-comment-body">
            <div id="dsq-comment-message-304234" class="dsq-comment-message"><p>It&#8217;s Cool Bro But Hey If You Feeling Them Aye Cop Then ! But Who Knows These Might Grow On Me If I See Some Official Pics You Know How That Goes </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-304276">
        <div id="dsq-comment-header-304276" class="dsq-comment-header">
            <cite id="dsq-cite-304276">
                <span id="dsq-author-user-304276">DRAKE</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304276" class="dsq-comment-body">
            <div id="dsq-comment-message-304276" class="dsq-comment-message"><p>I do.  I&#8217;ve wrote off plenty of pairs before then when official photos come out and I see how fresh they actually look I end up copping.  AKA Bel Air 5s for me.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-5" id="dsq-comment-304306">
        <div id="dsq-comment-header-304306" class="dsq-comment-header">
            <cite id="dsq-cite-304306">
                <span id="dsq-author-user-304306">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304306" class="dsq-comment-body">
            <div id="dsq-comment-message-304306" class="dsq-comment-message"><p>Same Here Besides The Bel Airs When The First Pics Start Floating Around I Knew They Were Gonna Be Dope Nobody Liked Them Then But On The Release Date Everyone Loved Them </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-4" id="dsq-comment-304258">
        <div id="dsq-comment-header-304258" class="dsq-comment-header">
            <cite id="dsq-cite-304258">
                <span id="dsq-author-user-304258">T$ .</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304258" class="dsq-comment-body">
            <div id="dsq-comment-message-304258" class="dsq-comment-message"><p>Lol nah , especially not for a raised price </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-3" id="dsq-comment-304257">
        <div id="dsq-comment-header-304257" class="dsq-comment-header">
            <cite id="dsq-cite-304257">
                <span id="dsq-author-user-304257">T$ .</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304257" class="dsq-comment-body">
            <div id="dsq-comment-message-304257" class="dsq-comment-message"><p>Yea its just over saturated </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-304259">
        <div id="dsq-comment-header-304259" class="dsq-comment-header">
            <cite id="dsq-cite-304259">
                <span id="dsq-author-user-304259">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304259" class="dsq-comment-body">
            <div id="dsq-comment-message-304259" class="dsq-comment-message"><p>Yep Truuu Truuu </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304215">
        <div id="dsq-comment-header-304215" class="dsq-comment-header">
            <cite id="dsq-cite-304215">
                <span id="dsq-author-user-304215">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304215" class="dsq-comment-body">
            <div id="dsq-comment-message-304215" class="dsq-comment-message"><p>Nothing Special Like Last Year And The Year Before </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304222">
        <div id="dsq-comment-header-304222" class="dsq-comment-header">
            <cite id="dsq-cite-304222">
                <span id="dsq-author-user-304222">willhoops</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304222" class="dsq-comment-body">
            <div id="dsq-comment-message-304222" class="dsq-comment-message"><p>Lows are better I don&#8217;t feel mids to much </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304226">
        <div id="dsq-comment-header-304226" class="dsq-comment-header">
            <cite id="dsq-cite-304226">
                <span id="dsq-author-user-304226">SQUIG</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304226" class="dsq-comment-body">
            <div id="dsq-comment-message-304226" class="dsq-comment-message"><p>You&#8217;re crazy</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-304227">
        <div id="dsq-comment-header-304227" class="dsq-comment-header">
            <cite id="dsq-cite-304227">
                <span id="dsq-author-user-304227">willhoops</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304227" class="dsq-comment-body">
            <div id="dsq-comment-message-304227" class="dsq-comment-message"><p>Not really everyone has their preference</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304223">
        <div id="dsq-comment-header-304223" class="dsq-comment-header">
            <cite id="dsq-cite-304223">
                <span id="dsq-author-user-304223">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304223" class="dsq-comment-body">
            <div id="dsq-comment-message-304223" class="dsq-comment-message"><p>I hate to break it to all you Newbies. But There won&#8217;t be any space jams coming anytime soon. 2009 was not that long ago and Jb has no reason to bring them back this soon. It would only diminish the value of the shoe at this point.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304238">
        <div id="dsq-comment-header-304238" class="dsq-comment-header">
            <cite id="dsq-cite-304238">
                <span id="dsq-author-user-304238">J Rock</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304238" class="dsq-comment-body">
            <div id="dsq-comment-message-304238" class="dsq-comment-message"><p>you obviously are new to the game yourself. JB doesnt care how long ago they dropped a shoe, because they know if they drop it again it&#8217;ll still sell out. Prime example would be True blue 3s, Bred 11s, Bordeaux 7s, Columbia 11s, Carmine 6s, Black Infrared 6s,White Infrared 6s, White Cement 3s, Olympic 7s, etc.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-304275">
        <div id="dsq-comment-header-304275" class="dsq-comment-header">
            <cite id="dsq-cite-304275">
                <span id="dsq-author-user-304275">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304275" class="dsq-comment-body">
            <div id="dsq-comment-message-304275" class="dsq-comment-message"><p>Look. &#8221; j rock &#8221;<br />
#1 a restock ain&#8217;t the same thing as a new release.<br />
#2 columbia 11s took about 15 years to drop again<br />
#3 breds dropped close together but 1 release was in a pack( not the same thing)</p>
<p>#4 Jb flooded the market with every 3 for what seemed like forever. And now locked it in the vault in an attempt to keep its value up</p>
<p>#6 the rest of the pairs you named are not even on the same planet as the space jam 11. </p>
<p>#7 if Jb didn&#8217;t care they wouldn&#8217;t limit every &#8220;BIG&#8221; release and you would be able to own whatever shoe you&#8217;d like to purchase no problem. </p>
<p>I Haven&#8217;t been in the game forever, just long enough to know that you don&#8217;t know what you are talking about. And long enough to still have a DS pair of space jams on ice. Move around lil bro. don&#8217;t watch me, watch tv smh poobutts</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-304305">
        <div id="dsq-comment-header-304305" class="dsq-comment-header">
            <cite id="dsq-cite-304305">
                <span id="dsq-author-user-304305">enhancedakuma</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304305" class="dsq-comment-body">
            <div id="dsq-comment-message-304305" class="dsq-comment-message"><p>You sound like a fucking idiot. You aren&#8217;t some OG in the shoe game, and it&#8217;s obvious that you just don&#8217;t want a retro of the Space Jam&#8217;s because you still have a DS pair. And 5 years in the shoe game isn&#8217;t shit, no one is impressed that you have a DS pair of shoes from the end of 2009, and it certainly doesn&#8217;t give any credibility to what you said, it just makes you look like a joke. If you think Nike gives two shits about the resale price of retro&#8217;s, you clearly aren&#8217;t a business major or even half-educated on how major corporations work. Keep trying to sound educated, chump.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-5" id="dsq-comment-304316">
        <div id="dsq-comment-header-304316" class="dsq-comment-header">
            <cite id="dsq-cite-304316">
                <span id="dsq-author-user-304316">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304316" class="dsq-comment-body">
            <div id="dsq-comment-message-304316" class="dsq-comment-message"><p>Nobody said nothing about resale value. You lil Tweens have no clue what you are talkin bout. I&#8217;ve been Collin nikes and js since u were a twinkle in your daddy&#8217;s eye and before the best part of u was on ur mommas thigh. Fuck boy</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-304346">
        <div id="dsq-comment-header-304346" class="dsq-comment-header">
            <cite id="dsq-cite-304346">
                <span id="dsq-author-user-304346">enhancedakuma</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304346" class="dsq-comment-body">
            <div id="dsq-comment-message-304346" class="dsq-comment-message"><p>I&#8217;m in my 30&#8217;s, I think it&#8217;s funny you think I&#8217;m a tween. If you&#8217;re really as old as you claim to be, you should be embarrassed by the way you act online. What adult says stupid shit like &#8220;fuckboys&#8221; and &#8220;poohbutts&#8221; to people online?  But go ahead, keep talking shit online to anyone who disagrees, while proving your stupidity to the rest of us. I&#8217;ve had Space Jams in the past, but I have better things to do with them than hold onto them and not wear them like some moron, shoes are meant to be worn. You really show the quality of person you are, a complete tool. Oh, and learn the difference between &#8220;your&#8221; and &#8220;you&#8217;re&#8221;, most 10 year olds do.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-5" id="dsq-comment-304354">
        <div id="dsq-comment-header-304354" class="dsq-comment-header">
            <cite id="dsq-cite-304354">
                <span id="dsq-author-user-304354">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304354" class="dsq-comment-body">
            <div id="dsq-comment-message-304354" class="dsq-comment-message"><p>Very clever&#8230; first of all I hate when lil whiny ass punks make a comment talking some shit. Then try and act like they are so much more refined when you snap on them. Then they try to turn the shit into a grammar or morality issue. Like the shit is that serious. You Sound like a little old girl man. Gtfoh</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-4" id="dsq-comment-304504">
        <div id="dsq-comment-header-304504" class="dsq-comment-header">
            <cite id="dsq-cite-304504">
                <span id="dsq-author-user-304504">J Rock</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304504" class="dsq-comment-body">
            <div id="dsq-comment-message-304504" class="dsq-comment-message"><p>you idiot 96 and 2000 columbias. Also 95 and 2001 breds. Dont try and come back smart and not even know what your talking about.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-5" id="dsq-comment-304544">
        <div id="dsq-comment-header-304544" class="dsq-comment-header">
            <cite id="dsq-cite-304544">
                <span id="dsq-author-user-304544">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304544" class="dsq-comment-body">
            <div id="dsq-comment-message-304544" class="dsq-comment-message"><p>Big difference between original release and retro vs retro to retro come on bruh&#8230; Bottom line ain&#8217;t gonna see the space jam anytime soon. </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304225">
        <div id="dsq-comment-header-304225" class="dsq-comment-header">
            <cite id="dsq-cite-304225">
                <span id="dsq-author-user-304225">Fvcktrends</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304225" class="dsq-comment-body">
            <div id="dsq-comment-message-304225" class="dsq-comment-message"><p>They look like a replica shoe these are so ugly And pathetic. I have no idea what jordan brand was thinking with these they must really think people will buy anything </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304229">
        <div id="dsq-comment-header-304229" class="dsq-comment-header">
            <cite id="dsq-cite-304229">
                <span id="dsq-author-user-304229">Tucker</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304229" class="dsq-comment-body">
            <div id="dsq-comment-message-304229" class="dsq-comment-message"><p>your fucking ugly and pathetic.  go jackoff in your bedroom alone you bitch boy</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-304237">
        <div id="dsq-comment-header-304237" class="dsq-comment-header">
            <cite id="dsq-cite-304237">
                <span id="dsq-author-user-304237">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304237" class="dsq-comment-body">
            <div id="dsq-comment-message-304237" class="dsq-comment-message"><p>Bro The Shoes Above Are Prototype Of The 11&#8217;s You Wear Today </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304230">
        <div id="dsq-comment-header-304230" class="dsq-comment-header">
            <cite id="dsq-cite-304230">
                <span id="dsq-author-user-304230">₩₳▼€</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304230" class="dsq-comment-body">
            <div id="dsq-comment-message-304230" class="dsq-comment-message"><p>I would&#8217;ve preferred the mesh upper but I&#8217;ll still cop</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304260">
        <div id="dsq-comment-header-304260" class="dsq-comment-header">
            <cite id="dsq-cite-304260">
                <span id="dsq-author-user-304260">mars</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304260" class="dsq-comment-body">
            <div id="dsq-comment-message-304260" class="dsq-comment-message"><p>No mesh makes these look so replica</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-304271">
        <div id="dsq-comment-header-304271" class="dsq-comment-header">
            <cite id="dsq-cite-304271">
                <span id="dsq-author-user-304271">₩₳▼€</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304271" class="dsq-comment-body">
            <div id="dsq-comment-message-304271" class="dsq-comment-message"><p>True and that&#8217;s where the price raise come is for the extra &#8220;primo&#8221; leathers </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-304272">
        <div id="dsq-comment-header-304272" class="dsq-comment-header">
            <cite id="dsq-cite-304272">
                <span id="dsq-author-user-304272">mars</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304272" class="dsq-comment-body">
            <div id="dsq-comment-message-304272" class="dsq-comment-message"><p>Did you notice that the midsole looks &#8220;dented&#8221; in the second picture.. Definitely some kind of custom.. Jb wouldn&#8217;t let us down that much</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-5" id="dsq-comment-304278">
        <div id="dsq-comment-header-304278" class="dsq-comment-header">
            <cite id="dsq-cite-304278">
                <span id="dsq-author-user-304278">₩₳▼€</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304278" class="dsq-comment-body">
            <div id="dsq-comment-message-304278" class="dsq-comment-message"><p>Nah those are shitty samples there&#8217;s still time for them to change things around so we still don&#8217;t kno exactly until official pics drop but we have a good enough idea </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304232">
        <div id="dsq-comment-header-304232" class="dsq-comment-header">
            <cite id="dsq-cite-304232">
                <span id="dsq-author-user-304232">juan</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304232" class="dsq-comment-body">
            <div id="dsq-comment-message-304232" class="dsq-comment-message"><p>got me feeling wet today</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304233">
        <div id="dsq-comment-header-304233" class="dsq-comment-header">
            <cite id="dsq-cite-304233">
                <span id="dsq-author-user-304233">Justin Jones</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304233" class="dsq-comment-body">
            <div id="dsq-comment-message-304233" class="dsq-comment-message"><p>i rather see the all gold joints</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304240">
        <div id="dsq-comment-header-304240" class="dsq-comment-header">
            <cite id="dsq-cite-304240">
                <span id="dsq-author-user-304240">Ty</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304240" class="dsq-comment-body">
            <div id="dsq-comment-message-304240" class="dsq-comment-message"><p>I don&#8217;t know about y&#8217;all but I&#8217;m tired of seeing 11s on Christmas every year, they should mix it up at least </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304405">
        <div id="dsq-comment-header-304405" class="dsq-comment-header">
            <cite id="dsq-cite-304405">
                <span id="dsq-author-user-304405">OG_KiCKS_NJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304405" class="dsq-comment-body">
            <div id="dsq-comment-message-304405" class="dsq-comment-message"><p>Exactly, give people these 11&#8217;s during the fall time or spring time. Drop something different every Christmas</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304243">
        <div id="dsq-comment-header-304243" class="dsq-comment-header">
            <cite id="dsq-cite-304243">
                <span id="dsq-author-user-304243">duke334</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304243" class="dsq-comment-body">
            <div id="dsq-comment-message-304243" class="dsq-comment-message"><p>Lol another pair of 11s on Christmas Day..these shits wack like the last 3 pair of 11s. I know y&#8217;all gotta be tired of Jordans by now..shit really the AF1 of our era</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304246">
        <div id="dsq-comment-header-304246" class="dsq-comment-header">
            <cite id="dsq-cite-304246">
                <span id="dsq-author-user-304246">b thumper</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304246" class="dsq-comment-body">
            <div id="dsq-comment-message-304246" class="dsq-comment-message"><p>Ha, its so true.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-304315">
        <div id="dsq-comment-header-304315" class="dsq-comment-header">
            <cite id="dsq-cite-304315">
                <span id="dsq-author-user-304315">Coldworldnoblanket</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304315" class="dsq-comment-body">
            <div id="dsq-comment-message-304315" class="dsq-comment-message"><p>Jordans been around just as long as Af1&#8217;s, but I get your point.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304244">
        <div id="dsq-comment-header-304244" class="dsq-comment-header">
            <cite id="dsq-cite-304244">
                <span id="dsq-author-user-304244">FATT$</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304244" class="dsq-comment-body">
            <div id="dsq-comment-message-304244" class="dsq-comment-message"><p>wtf is this</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304247">
        <div id="dsq-comment-header-304247" class="dsq-comment-header">
            <cite id="dsq-cite-304247">
                <span id="dsq-author-user-304247">S3cret_Weaponzz</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304247" class="dsq-comment-body">
            <div id="dsq-comment-message-304247" class="dsq-comment-message"><p>soooo they made dirty breds?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304250">
        <div id="dsq-comment-header-304250" class="dsq-comment-header">
            <cite id="dsq-cite-304250">
                <span id="dsq-author-user-304250">super saiyan oreo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304250" class="dsq-comment-body">
            <div id="dsq-comment-message-304250" class="dsq-comment-message"><p>In about 3 to 4 years the Jordan 11 retail gonna be 250. I wonder what will be the consumer&#8217;s breaking point.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304267">
        <div id="dsq-comment-header-304267" class="dsq-comment-header">
            <cite id="dsq-cite-304267">
                <span id="dsq-author-user-304267">zoeboy1305</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304267" class="dsq-comment-body">
            <div id="dsq-comment-message-304267" class="dsq-comment-message"><p>they are testing the waters with those fucking 20&#8217;s.. smh</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-304300">
        <div id="dsq-comment-header-304300" class="dsq-comment-header">
            <cite id="dsq-cite-304300">
                <span id="dsq-author-user-304300">willhoops</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304300" class="dsq-comment-body">
            <div id="dsq-comment-message-304300" class="dsq-comment-message"><p>The 4s are gonna be 250 to so u can only imagine the 11s</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-3" id="dsq-comment-304314">
        <div id="dsq-comment-header-304314" class="dsq-comment-header">
            <cite id="dsq-cite-304314">
                <span id="dsq-author-user-304314">Coldworldnoblanket</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304314" class="dsq-comment-body">
            <div id="dsq-comment-message-304314" class="dsq-comment-message"><p>and them hoes still collecting dust on shelves</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-304291">
        <div id="dsq-comment-header-304291" class="dsq-comment-header">
            <cite id="dsq-cite-304291">
                <span id="dsq-author-user-304291">Breal</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304291" class="dsq-comment-body">
            <div id="dsq-comment-message-304291" class="dsq-comment-message"><p>It ain&#8217;t gonna take DAT long. Look for it holiday 2015 watch&#8230;..</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-304294">
        <div id="dsq-comment-header-304294" class="dsq-comment-header">
            <cite id="dsq-cite-304294">
                <span id="dsq-author-user-304294">super saiyan oreo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304294" class="dsq-comment-body">
            <div id="dsq-comment-message-304294" class="dsq-comment-message"><p>You might be right. JB will be like the whole &#8220;remastered&#8221; excuse will justify the price but li ning use good leather and cost less. To each is own.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-304313">
        <div id="dsq-comment-header-304313" class="dsq-comment-header">
            <cite id="dsq-cite-304313">
                <span id="dsq-author-user-304313">Coldworldnoblanket</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304313" class="dsq-comment-body">
            <div id="dsq-comment-message-304313" class="dsq-comment-message"><p>more like next year</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304251">
        <div id="dsq-comment-header-304251" class="dsq-comment-header">
            <cite id="dsq-cite-304251">
                <span id="dsq-author-user-304251">CinCityBo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304251" class="dsq-comment-body">
            <div id="dsq-comment-message-304251" class="dsq-comment-message"><p>Need official pics &amp; no icy sole</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304254">
        <div id="dsq-comment-header-304254" class="dsq-comment-header">
            <cite id="dsq-cite-304254">
                <span id="dsq-author-user-304254">OnlyInNY</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304254" class="dsq-comment-body">
            <div id="dsq-comment-message-304254" class="dsq-comment-message"><p>No icy sole.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304279">
        <div id="dsq-comment-header-304279" class="dsq-comment-header">
            <cite id="dsq-cite-304279">
                <span id="dsq-author-user-304279">K Chase</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304279" class="dsq-comment-body">
            <div id="dsq-comment-message-304279" class="dsq-comment-message"><p>It says icy sole that&#8217;s not the pic of the actual shoe</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304262">
        <div id="dsq-comment-header-304262" class="dsq-comment-header">
            <cite id="dsq-cite-304262">
                <span id="dsq-author-user-304262">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304262" class="dsq-comment-body">
            <div id="dsq-comment-message-304262" class="dsq-comment-message"><p>I&#8217;m going to turn them into space jams f*** that</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304281">
        <div id="dsq-comment-header-304281" class="dsq-comment-header">
            <cite id="dsq-cite-304281">
                <span id="dsq-author-user-304281">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304281" class="dsq-comment-body">
            <div id="dsq-comment-message-304281" class="dsq-comment-message"><p>Lol. Nice</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304269">
        <div id="dsq-comment-header-304269" class="dsq-comment-header">
            <cite id="dsq-cite-304269">
                <span id="dsq-author-user-304269">Danny</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304269" class="dsq-comment-body">
            <div id="dsq-comment-message-304269" class="dsq-comment-message"><p>TR-A-S-H, JN can&#8217;t be serious</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304284">
        <div id="dsq-comment-header-304284" class="dsq-comment-header">
            <cite id="dsq-cite-304284">
                <span id="dsq-author-user-304284">jordan brand</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304284" class="dsq-comment-body">
            <div id="dsq-comment-message-304284" class="dsq-comment-message"><p>fire</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304285">
        <div id="dsq-comment-header-304285" class="dsq-comment-header">
            <cite id="dsq-cite-304285">
                <span id="dsq-author-user-304285">KushKloud</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304285" class="dsq-comment-body">
            <div id="dsq-comment-message-304285" class="dsq-comment-message"><p>If they kept the black soles on them, I&#8217;d buy em. The blue tinted sole looks bad on these. They need to either make the damn sole match the colorway or they need to bring back the clear outsole, this blue tint has forced me to not buy certain shoes cuz it fucks the colorway up. Plus I actually liked the yellowing effect to the clear sole lol</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304288">
        <div id="dsq-comment-header-304288" class="dsq-comment-header">
            <cite id="dsq-cite-304288">
                <span id="dsq-author-user-304288">guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304288" class="dsq-comment-body">
            <div id="dsq-comment-message-304288" class="dsq-comment-message"><p>you know the &#8216;blue&#8217; goes away pretty quick<br />
they just add the blue to slow down yellowing</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-304290">
        <div id="dsq-comment-header-304290" class="dsq-comment-header">
            <cite id="dsq-cite-304290">
                <span id="dsq-author-user-304290">KushKloud</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304290" class="dsq-comment-body">
            <div id="dsq-comment-message-304290" class="dsq-comment-message"><p>The blue still sticks to my soles for whichever pairs I got wit em, I wear all my kicks in rotation though, and some only in the summer. If I wore the blue soled pairs more often maybe. Either way I got oreo 6s and varsity red/black 6s wit clear soles, and I just prefer the clear sole, even after it yellows lol</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304286">
        <div id="dsq-comment-header-304286" class="dsq-comment-header">
            <cite id="dsq-cite-304286">
                <span id="dsq-author-user-304286">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304286" class="dsq-comment-body">
            <div id="dsq-comment-message-304286" class="dsq-comment-message"><p>This too funny &#8230;. These are already on the fakewebsites being sold cause they been available to buy since 96 . </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304298">
        <div id="dsq-comment-header-304298" class="dsq-comment-header">
            <cite id="dsq-cite-304298">
                <span id="dsq-author-user-304298">Fuck JB</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304298" class="dsq-comment-body">
            <div id="dsq-comment-message-304298" class="dsq-comment-message"><p>WEAK WEAK WEAK &#8211; Jordan y&#8217;all suck GIVE THE PUBLIC WHAT THEY WANT not what u wanna give em &#8211; WACK ASS SHIT &#8211; give us something good not this cheap lookin shit &#8211; ALL red melo &#8211; black out SOMETHING GOOD DAYM &#8211; JB y&#8217;all suck </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304299">
        <div id="dsq-comment-header-304299" class="dsq-comment-header">
            <cite id="dsq-cite-304299">
                <span id="dsq-author-user-304299">Sneakerguidekeepit#</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304299" class="dsq-comment-body">
            <div id="dsq-comment-message-304299" class="dsq-comment-message"><p>So If they released the space jams would they be great cuz they both be the same shit if u look at it </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304304">
        <div id="dsq-comment-header-304304" class="dsq-comment-header">
            <cite id="dsq-cite-304304">
                <span id="dsq-author-user-304304">enhancedakuma</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304304" class="dsq-comment-body">
            <div id="dsq-comment-message-304304" class="dsq-comment-message"><p>Fuck Nike! How the hell can they justify raising the prices on a specific model EVERY SINGLE YEAR? This is frankly just insulting to consumers and we really need to do something about it. Unfortunately, there are idiots who will pay just about any price for Jordan&#8217;s, so we&#8217;re probably S.O.L.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-316513">
        <div id="dsq-comment-header-316513" class="dsq-comment-header">
            <cite id="dsq-cite-316513">
                <span id="dsq-author-user-316513">Kallision</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316513" class="dsq-comment-body">
            <div id="dsq-comment-message-316513" class="dsq-comment-message"><p>$200 for the Retro 3 &#8217;88 that came out 2 years ago.<br />
$250 for the Laser 4 and 20 Jordan. Champagne and Cigar Jordan 6 as well as the 7s for last year and this year.<br />
People still bought them. So far JB knows that they can still reach a $250 price mark and have models and colorways sell out</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304310">
        <div id="dsq-comment-header-304310" class="dsq-comment-header">
            <cite id="dsq-cite-304310">
                <span id="dsq-author-user-304310">ShoeEtiquette</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304310" class="dsq-comment-body">
            <div id="dsq-comment-message-304310" class="dsq-comment-message"><p>These are not fresh or dope or butter so whats the point</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304323">
        <div id="dsq-comment-header-304323" class="dsq-comment-header">
            <cite id="dsq-cite-304323">
                <span id="dsq-author-user-304323">OG Jays</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304323" class="dsq-comment-body">
            <div id="dsq-comment-message-304323" class="dsq-comment-message"><p>Like the premium leather upper idea. Can&#8217;t wait!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304325">
        <div id="dsq-comment-header-304325" class="dsq-comment-header">
            <cite id="dsq-cite-304325">
                <span id="dsq-author-user-304325">J_Collector</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304325" class="dsq-comment-body">
            <div id="dsq-comment-message-304325" class="dsq-comment-message"><p>Pass money well saved this december</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304335">
        <div id="dsq-comment-header-304335" class="dsq-comment-header">
            <cite id="dsq-cite-304335">
                <span id="dsq-author-user-304335">Glasses</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304335" class="dsq-comment-body">
            <div id="dsq-comment-message-304335" class="dsq-comment-message"><p>These are going to be dope. Just wait and see.  </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304336">
        <div id="dsq-comment-header-304336" class="dsq-comment-header">
            <cite id="dsq-cite-304336">
                <span id="dsq-author-user-304336">Buck</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304336" class="dsq-comment-body">
            <div id="dsq-comment-message-304336" class="dsq-comment-message"><p>Going to be a hype releae and will have to cop for sure. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304337">
        <div id="dsq-comment-header-304337" class="dsq-comment-header">
            <cite id="dsq-cite-304337">
                <span id="dsq-author-user-304337">1GN</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304337" class="dsq-comment-body">
            <div id="dsq-comment-message-304337" class="dsq-comment-message"><p>Raise in price. WTF. HEARD 225/250????</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304343">
        <div id="dsq-comment-header-304343" class="dsq-comment-header">
            <cite id="dsq-cite-304343">
                <span id="dsq-author-user-304343">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304343" class="dsq-comment-body">
            <div id="dsq-comment-message-304343" class="dsq-comment-message"><p>I wish they would have just decided to release the space jams . These are basically breds .im just going to have them customized into the space Jams . I&#8217;m so glad for the gammas now these are basically fake to me , I rather have an all Orange pair or an all red pair , these are trash , </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304348">
        <div id="dsq-comment-header-304348" class="dsq-comment-header">
            <cite id="dsq-cite-304348">
                <span id="dsq-author-user-304348">Og</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304348" class="dsq-comment-body">
            <div id="dsq-comment-message-304348" class="dsq-comment-message"><p>I&#8217;m just gunna customize them into the dirty breds</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304353">
        <div id="dsq-comment-header-304353" class="dsq-comment-header">
            <cite id="dsq-cite-304353">
                <span id="dsq-author-user-304353">flossbee</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304353" class="dsq-comment-body">
            <div id="dsq-comment-message-304353" class="dsq-comment-message"><p>Welp&#8230; looks like I&#8217;ll be doubling up on Aqua&#8217;s.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304358">
        <div id="dsq-comment-header-304358" class="dsq-comment-header">
            <cite id="dsq-cite-304358">
                <span id="dsq-author-user-304358">Deez Js</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304358" class="dsq-comment-body">
            <div id="dsq-comment-message-304358" class="dsq-comment-message"><p>All yal crazy these r sweet as fuk !!!! Keep doing yo thing Jordan. These boys will sell fast ! Il b swooping 2  no doubt </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304359">
        <div id="dsq-comment-header-304359" class="dsq-comment-header">
            <cite id="dsq-cite-304359">
                <span id="dsq-author-user-304359">Mark</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304359" class="dsq-comment-body">
            <div id="dsq-comment-message-304359" class="dsq-comment-message"><p>I&#8217;d rather see some Space Jams since the Hare 7s &amp; 1s are coming out. Or make a 30th anniversary pair? These just look like Breds with a clear sole.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304459">
        <div id="dsq-comment-header-304459" class="dsq-comment-header">
            <cite id="dsq-cite-304459">
                <span id="dsq-author-user-304459">Zeke Mallory</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304459" class="dsq-comment-body">
            <div id="dsq-comment-message-304459" class="dsq-comment-message"><p>What people don&#8217;t realize is it&#8217;s still early and shit can change</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-304525">
        <div id="dsq-comment-header-304525" class="dsq-comment-header">
            <cite id="dsq-cite-304525">
                <span id="dsq-author-user-304525">Mark</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304525" class="dsq-comment-body">
            <div id="dsq-comment-message-304525" class="dsq-comment-message"><p>I hope you&#8217;re right, but all the early pics or info on the last 3 or 4 11s has been dead on.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304361">
        <div id="dsq-comment-header-304361" class="dsq-comment-header">
            <cite id="dsq-cite-304361">
                <span id="dsq-author-user-304361">CinCityBo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304361" class="dsq-comment-body">
            <div id="dsq-comment-message-304361" class="dsq-comment-message"><p>A lot of hating on this shoe just cause its not the space jam GTFOH 72-10 is a great concept for this shoe definitely not feeling an price increase that&#8217;s BS can&#8217;t wait 4 official info &amp; pics</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304408">
        <div id="dsq-comment-header-304408" class="dsq-comment-header">
            <cite id="dsq-cite-304408">
                <span id="dsq-author-user-304408">sneakerjunkie212</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304408" class="dsq-comment-body">
            <div id="dsq-comment-message-304408" class="dsq-comment-message"><p>Shit..I wonder..what is the price increase gona be ..210,215&#8230;..220&#8230;..ooh lawrd.   Then u got to deal with the lines, fights n all damn </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304369">
        <div id="dsq-comment-header-304369" class="dsq-comment-header">
            <cite id="dsq-cite-304369">
                <span id="dsq-author-user-304369">DJ Skip</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304369" class="dsq-comment-body">
            <div id="dsq-comment-message-304369" class="dsq-comment-message"><p>this shoe will turn heads, break ankles and get you robbed.  just wait till the neighborhood sees the official photos or at least detailed ones</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304370">
        <div id="dsq-comment-header-304370" class="dsq-comment-header">
            <cite id="dsq-cite-304370">
                <span id="dsq-author-user-304370">HILTON</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304370" class="dsq-comment-body">
            <div id="dsq-comment-message-304370" class="dsq-comment-message"><p>Copping then coloring the midsole all black. hopefully I&#8217;m able to cop two pairs too</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304371">
        <div id="dsq-comment-header-304371" class="dsq-comment-header">
            <cite id="dsq-cite-304371">
                <span id="dsq-author-user-304371">Lil&#8217; Stilt</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304371" class="dsq-comment-body">
            <div id="dsq-comment-message-304371" class="dsq-comment-message"><p>I like them.  Not sure why everyone is bitching about them.  Why give us a shoe that released a few years ago, thats just dumb.  I want new shit added to the collection.  I don&#8217;t want my collection looking like space jam 20xx, space jam 20xx, space jam 20xx lol ya dig!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304404">
        <div id="dsq-comment-header-304404" class="dsq-comment-header">
            <cite id="dsq-cite-304404">
                <span id="dsq-author-user-304404">Sneakerguidekeepit#</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304404" class="dsq-comment-body">
            <div id="dsq-comment-message-304404" class="dsq-comment-message"><p>I&#8217;m wit u these is clean and a new colorway don&#8217;t know why people complaining </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304373">
        <div id="dsq-comment-header-304373" class="dsq-comment-header">
            <cite id="dsq-cite-304373">
                <span id="dsq-author-user-304373">Ty</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304373" class="dsq-comment-body">
            <div id="dsq-comment-message-304373" class="dsq-comment-message"><p>A lot of people are going to break their necks to get these when all they are is remodeled &#8220;breds&#8221;, rather have the SpaceJams come out again.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304397">
        <div id="dsq-comment-header-304397" class="dsq-comment-header">
            <cite id="dsq-cite-304397">
                <span id="dsq-author-user-304397">Fresh wheels</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304397" class="dsq-comment-body">
            <div id="dsq-comment-message-304397" class="dsq-comment-message"><p>This is beautiful! A new colorway but keeping the classic colors. These are so much nicer than the Gamma blues </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304453">
        <div id="dsq-comment-header-304453" class="dsq-comment-header">
            <cite id="dsq-cite-304453">
                <span id="dsq-author-user-304453">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304453" class="dsq-comment-body">
            <div id="dsq-comment-message-304453" class="dsq-comment-message"><p>It&#8217;s not a new color way bro . It&#8217;s a new color combination but not color way. They are black and red , we all seen black n red 11s , we seen icy soles before . We seen blue carbon fiber they are simply space breds</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304407">
        <div id="dsq-comment-header-304407" class="dsq-comment-header">
            <cite id="dsq-cite-304407">
                <span id="dsq-author-user-304407">sneakerjunkie212</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304407" class="dsq-comment-body">
            <div id="dsq-comment-message-304407" class="dsq-comment-message"><p>Of course  we would have been happier  with the dam space jams</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304446">
        <div id="dsq-comment-header-304446" class="dsq-comment-header">
            <cite id="dsq-cite-304446">
                <span id="dsq-author-user-304446">Big Bill</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304446" class="dsq-comment-body">
            <div id="dsq-comment-message-304446" class="dsq-comment-message"><p>Your a fucking HYPEBEAST!  We just fucking got those shoes.  Why the FUCK would we want them again.  DUDE, your pissing me the FUCK OFF.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-304451">
        <div id="dsq-comment-header-304451" class="dsq-comment-header">
            <cite id="dsq-cite-304451">
                <span id="dsq-author-user-304451">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304451" class="dsq-comment-body">
            <div id="dsq-comment-message-304451" class="dsq-comment-message"><p>Calm down big Bill you can suck him off later . To puss you off they should have released the space jams lol and jb should have asked you what city u in and not release them In your city so u can be assed out .</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-304475">
        <div id="dsq-comment-header-304475" class="dsq-comment-header">
            <cite id="dsq-cite-304475">
                <span id="dsq-author-user-304475">Big Bill</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304475" class="dsq-comment-body">
            <div id="dsq-comment-message-304475" class="dsq-comment-message"><p>BITCH ASS NIGGA KRAY11 over here think he a smooth talker.  I&#8217;m about to BITCH ASS HIS ASSHOLE with my fucking flashlight so I can see the BULLSHIT he be talking myself.  FUCK dem SPACE JAMS NIGGA.  72-10 All Day NIGGA.  Holla At Me if you want to do lunch. OH and you paying tho my NIGGA!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-5" id="dsq-comment-304507">
        <div id="dsq-comment-header-304507" class="dsq-comment-header">
            <cite id="dsq-cite-304507">
                <span id="dsq-author-user-304507">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304507" class="dsq-comment-body">
            <div id="dsq-comment-message-304507" class="dsq-comment-message"><p>You can suck me off and him off later then lmao</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-304684">
        <div id="dsq-comment-header-304684" class="dsq-comment-header">
            <cite id="dsq-cite-304684">
                <span id="dsq-author-user-304684">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304684" class="dsq-comment-body">
            <div id="dsq-comment-message-304684" class="dsq-comment-message"><p>Big Bill straight trippin around this here joint.  space jams all day over dem 72-10 space breds</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-3" id="dsq-comment-304489">
        <div id="dsq-comment-header-304489" class="dsq-comment-header">
            <cite id="dsq-cite-304489">
                <span id="dsq-author-user-304489">JohnDoe6503</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304489" class="dsq-comment-body">
            <div id="dsq-comment-message-304489" class="dsq-comment-message"><p>It&#8217;s been 6 years since they released.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-3" id="dsq-comment-304584">
        <div id="dsq-comment-header-304584" class="dsq-comment-header">
            <cite id="dsq-cite-304584">
                <span id="dsq-author-user-304584">sneakerjunkie212</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304584" class="dsq-comment-body">
            <div id="dsq-comment-message-304584" class="dsq-comment-message"><p>Dude.  U sound like a fuckin  idiot&#8230;obviously  u ain&#8217;t a collector,a fan,reseller  ,or anything of importance. .because if u know  anything the space jams really the only 11s.  Of any importance  that have yet to release.  anywhere..  so fuck you n get ya money n info right before u approach me </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304417">
        <div id="dsq-comment-header-304417" class="dsq-comment-header">
            <cite id="dsq-cite-304417">
                <span id="dsq-author-user-304417">Shoe allah</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304417" class="dsq-comment-body">
            <div id="dsq-comment-message-304417" class="dsq-comment-message"><p>Its not bad i just expected more but then again its jordan brand</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304444">
        <div id="dsq-comment-header-304444" class="dsq-comment-header">
            <cite id="dsq-cite-304444">
                <span id="dsq-author-user-304444">Malachi Stone</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304444" class="dsq-comment-body">
            <div id="dsq-comment-message-304444" class="dsq-comment-message"><p>Lmao Space Breds</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304445">
        <div id="dsq-comment-header-304445" class="dsq-comment-header">
            <cite id="dsq-cite-304445">
                <span id="dsq-author-user-304445">Space Breds</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304445" class="dsq-comment-body">
            <div id="dsq-comment-message-304445" class="dsq-comment-message"><p>Air Jordan 11 &#8220;Space Breds&#8221;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304448">
        <div id="dsq-comment-header-304448" class="dsq-comment-header">
            <cite id="dsq-cite-304448">
                <span id="dsq-author-user-304448">Michigan</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304448" class="dsq-comment-body">
            <div id="dsq-comment-message-304448" class="dsq-comment-message"><p>These are tough!  2nd confirmed pair of Jordan&#8217;s I&#8217;ll be coppin this year along with the Aqua 8&#8217;s.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304449">
        <div id="dsq-comment-header-304449" class="dsq-comment-header">
            <cite id="dsq-cite-304449">
                <span id="dsq-author-user-304449">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304449" class="dsq-comment-body">
            <div id="dsq-comment-message-304449" class="dsq-comment-message"><p>This is cool. I had hopes for the blue/white 11 joints melo had</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304455">
        <div id="dsq-comment-header-304455" class="dsq-comment-header">
            <cite id="dsq-cite-304455">
                <span id="dsq-author-user-304455">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304455" class="dsq-comment-body">
            <div id="dsq-comment-message-304455" class="dsq-comment-message"><p>If you ask me yet again cause I&#8217;m going to be talking about these all year &#8230;. I wish they were white n red some crazy combination of those two colors so they can match the bulls color way .</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-305992">
        <div id="dsq-comment-header-305992" class="dsq-comment-header">
            <cite id="dsq-cite-305992">
                <span id="dsq-author-user-305992">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305992" class="dsq-comment-body">
            <div id="dsq-comment-message-305992" class="dsq-comment-message"><p>&#8230;because black &amp; red DOESN&#8217;T match the Bulls colors? </p>
<p>Jesus Christ. lol</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304460">
        <div id="dsq-comment-header-304460" class="dsq-comment-header">
            <cite id="dsq-cite-304460">
                <span id="dsq-author-user-304460">Zeke Mallory</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304460" class="dsq-comment-body">
            <div id="dsq-comment-message-304460" class="dsq-comment-message"><p>As long as JB turns a profit, you can hang it up on the price&#8230;&#8230; Just stack your bread and get ready for a 300 dollar pairs</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304469">
        <div id="dsq-comment-header-304469" class="dsq-comment-header">
            <cite id="dsq-cite-304469">
                <span id="dsq-author-user-304469">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304469" class="dsq-comment-body">
            <div id="dsq-comment-message-304469" class="dsq-comment-message"><p>Exactly</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304484">
        <div id="dsq-comment-header-304484" class="dsq-comment-header">
            <cite id="dsq-cite-304484">
                <span id="dsq-author-user-304484">jlo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304484" class="dsq-comment-body">
            <div id="dsq-comment-message-304484" class="dsq-comment-message"><p>F#*k Footlocker Reseller Employees! </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304488">
        <div id="dsq-comment-header-304488" class="dsq-comment-header">
            <cite id="dsq-cite-304488">
                <span id="dsq-author-user-304488">JohnDoe6503</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304488" class="dsq-comment-body">
            <div id="dsq-comment-message-304488" class="dsq-comment-message"><p>These look just like the space jams with a red logo instead of blue. The color way is  nothing special and a real disappointment. They should have just brought back the Space Jams since its been 6 years. Really hope this is false. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-304509">
        <div id="dsq-comment-header-304509" class="dsq-comment-header">
            <cite id="dsq-cite-304509">
                <span id="dsq-author-user-304509">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304509" class="dsq-comment-body">
            <div id="dsq-comment-message-304509" class="dsq-comment-message"><p>Exactly. Even the if they use that bullshit &#8220;remastered &#8221; it&#8217;s JB they have years and years of ideas and they settled on coming out with a black and red (again) it&#8217;s like &#8220;Breds 2.0&#8243;. They could have come out with a white and red pair like they did with the 19s which I still own. They could have had a  white and navy blue pair . A reverse concord (high top) pair . Something different then these .</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-304520">
        <div id="dsq-comment-header-304520" class="dsq-comment-header">
            <cite id="dsq-cite-304520">
                <span id="dsq-author-user-304520">JohnDoe6503</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304520" class="dsq-comment-body">
            <div id="dsq-comment-message-304520" class="dsq-comment-message"><p>They&#8217;re starting to run out of sneakers to retro and it&#8217;s becoming clear that they lack artistic creativity. It&#8217;s only a matter of time before even the die hard sneak heads won&#8217;t be able to even afford Jordan&#8217;s. They keep testing our paitence year after year. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-304613">
        <div id="dsq-comment-header-304613" class="dsq-comment-header">
            <cite id="dsq-cite-304613">
                <span id="dsq-author-user-304613">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304613" class="dsq-comment-body">
            <div id="dsq-comment-message-304613" class="dsq-comment-message"><p>Your right bro I think the last shoe to atkeast have some creativity on it was the gammas and the low top reverse concords. These don&#8217;t cut it and to expect a price hike that&#8217;s insane. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-4" id="dsq-comment-305990">
        <div id="dsq-comment-header-305990" class="dsq-comment-header">
            <cite id="dsq-cite-305990">
                <span id="dsq-author-user-305990">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305990" class="dsq-comment-body">
            <div id="dsq-comment-message-305990" class="dsq-comment-message"><p>The XI is about as simple as a silhouette can possibly get&#8230;and there are about 20 different colorways of this particular shoe now. Tell me, what else can be done, other than rainbow&#8230;..and then graphics? That shit didn&#8217;t work so well for the Foamposite, huh?</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-305982">
        <div id="dsq-comment-header-305982" class="dsq-comment-header">
            <cite id="dsq-cite-305982">
                <span id="dsq-author-user-305982">Coldworldnoblanket</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305982" class="dsq-comment-body">
            <div id="dsq-comment-message-305982" class="dsq-comment-message"><p>look just like the breds without a red outsole</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304576">
        <div id="dsq-comment-header-304576" class="dsq-comment-header">
            <cite id="dsq-cite-304576">
                <span id="dsq-author-user-304576">yankeesfn2</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304576" class="dsq-comment-body">
            <div id="dsq-comment-message-304576" class="dsq-comment-message"><p>They cool but paying more than $180 is senseless </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304590">
        <div id="dsq-comment-header-304590" class="dsq-comment-header">
            <cite id="dsq-cite-304590">
                <span id="dsq-author-user-304590">BrandonB2014</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304590" class="dsq-comment-body">
            <div id="dsq-comment-message-304590" class="dsq-comment-message"><p>Clearly a photoshopped picture, look closer</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-304735">
        <div id="dsq-comment-header-304735" class="dsq-comment-header">
            <cite id="dsq-cite-304735">
                <span id="dsq-author-user-304735">JOU</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304735" class="dsq-comment-body">
            <div id="dsq-comment-message-304735" class="dsq-comment-message"><p>No shit that what it says in the bio some fucking dip shit asshole whipping crusted shit from your fucking hairy ball sack.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304600">
        <div id="dsq-comment-header-304600" class="dsq-comment-header">
            <cite id="dsq-cite-304600">
http://kixkonnection.com                <span id="dsq-author-user-304600">Kix Konnection</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304600" class="dsq-comment-body">
            <div id="dsq-comment-message-304600" class="dsq-comment-message"><p>You need some sneakers, tired of all this raffle BS!</p>
<p>We’re Changing the Sneaker Game &#8211; &#8211; <a href="http://kixkonnection.com" rel="nofollow">http://kixkonnection.com</a></p>
<p>Subscribe to our Newsletter &#8211; <a href="http://kixkonnection.com/newsletter" rel="nofollow">http://kixkonnection.com/newsletter</a><br />
Follow Us:<br />
FB: <a href="http://facebook.com/kixkonnection" rel="nofollow">http://facebook.com/kixkonnection</a><br />
twitter: <a href="http://twitter.com/kixkonnection" rel="nofollow">http://twitter.com/kixkonnection</a><br />
IG: <a href="http://instagram.com/kixkonnection" rel="nofollow">http://instagram.com/kixkonnection</a></p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-304714">
        <div id="dsq-comment-header-304714" class="dsq-comment-header">
            <cite id="dsq-cite-304714">
                <span id="dsq-author-user-304714">dekoven_bandzup</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304714" class="dsq-comment-body">
            <div id="dsq-comment-message-304714" class="dsq-comment-message"><p>These dope but I&#8217;d rather have the suede 11s or release the 16s or 21s as a Christmas release</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-304733">
        <div id="dsq-comment-header-304733" class="dsq-comment-header">
            <cite id="dsq-cite-304733">
                <span id="dsq-author-user-304733">Kray11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304733" class="dsq-comment-body">
            <div id="dsq-comment-message-304733" class="dsq-comment-message"><p>Truth be told the only thing I&#8217;m happy about when these release is that we finally getting a black and red (special 11 box) so I can finally get rid of the black retro box my Breds came in lbs and put my breds in the box and put these trash 72-10s in the gift of flight box with my patone 11s</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-305985">
        <div id="dsq-comment-header-305985" class="dsq-comment-header">
            <cite id="dsq-cite-305985">
                <span id="dsq-author-user-305985">T$ .</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305985" class="dsq-comment-body">
            <div id="dsq-comment-message-305985" class="dsq-comment-message"><p>So your spending 220 for a box ? </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-305989">
        <div id="dsq-comment-header-305989" class="dsq-comment-header">
            <cite id="dsq-cite-305989">
                <span id="dsq-author-user-305989">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305989" class="dsq-comment-body">
            <div id="dsq-comment-message-305989" class="dsq-comment-message"><p>of course. the Jordan Brand fans are retarded. You weren&#8217;t aware of this? I mean, it&#8217;s not a secret. They buy the same 8 shoes over and over at ever increasing prices. As I was saying&#8230;.retards&#8230;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-3" id="dsq-comment-306044">
        <div id="dsq-comment-header-306044" class="dsq-comment-header">
            <cite id="dsq-cite-306044">
                <span id="dsq-author-user-306044">Julio Guerra</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306044" class="dsq-comment-body">
            <div id="dsq-comment-message-306044" class="dsq-comment-message"><p>I ain&#8217;t say anything about buying these for a box lol I said the only thing I&#8217;m happy about .</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304734">
        <div id="dsq-comment-header-304734" class="dsq-comment-header">
            <cite id="dsq-cite-304734">
                <span id="dsq-author-user-304734">curt</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304734" class="dsq-comment-body">
            <div id="dsq-comment-message-304734" class="dsq-comment-message"><p>my dick is growing at the same damn time i look at these</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304779">
        <div id="dsq-comment-header-304779" class="dsq-comment-header">
            <cite id="dsq-cite-304779">
                <span id="dsq-author-user-304779">SirFuck YaMotha</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304779" class="dsq-comment-body">
            <div id="dsq-comment-message-304779" class="dsq-comment-message"><p>I like tat they keeping it original because regardless they making money off of any pair of 11&#8217;s that come out but I prefer the space jam&#8217;s but I will still get these.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-304784">
        <div id="dsq-comment-header-304784" class="dsq-comment-header">
            <cite id="dsq-cite-304784">
                <span id="dsq-author-user-304784">SirFuck YaMotha</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304784" class="dsq-comment-body">
            <div id="dsq-comment-message-304784" class="dsq-comment-message"><p>But if anyone hears more information please post</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-304824">
        <div id="dsq-comment-header-304824" class="dsq-comment-header">
            <cite id="dsq-cite-304824">
http://www.ThatUncutRaw.com/                <span id="dsq-author-user-304824">Chris L</span>
            </cite>
        </div>
        <div id="dsq-comment-body-304824" class="dsq-comment-body">
            <div id="dsq-comment-message-304824" class="dsq-comment-message"><p>I love the colorway (It&#8217;s the Space Jam colorway with red instead of blue, literally; not sure how you could be a fan of OG colorways and not be feeling these), but if the finish product&#8217;s uppers looks anything like those &#8220;prototype&#8221; pics, I&#8217;m not sure if I&#8217;d cop. Why they wouldn&#8217;t just go with the traditional mesh panelling on the uppers is beyond me. On another note, are the 1st and last 2 pics photoshopped?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-305941">
        <div id="dsq-comment-header-305941" class="dsq-comment-header">
            <cite id="dsq-cite-305941">
                <span id="dsq-author-user-305941">juke v</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305941" class="dsq-comment-body">
            <div id="dsq-comment-message-305941" class="dsq-comment-message"><p>i would love to see these drop!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-305981">
        <div id="dsq-comment-header-305981" class="dsq-comment-header">
            <cite id="dsq-cite-305981">
                <span id="dsq-author-user-305981">Jack</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305981" class="dsq-comment-body">
            <div id="dsq-comment-message-305981" class="dsq-comment-message"><p>220 fuck outta here with this shit jb</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-305983">
        <div id="dsq-comment-header-305983" class="dsq-comment-header">
            <cite id="dsq-cite-305983">
                <span id="dsq-author-user-305983">☆Dizzy Story☆</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305983" class="dsq-comment-body">
            <div id="dsq-comment-message-305983" class="dsq-comment-message"><p>not really feeling these &#8230;&#8230;&#8230;. yet</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-305987">
        <div id="dsq-comment-header-305987" class="dsq-comment-header">
            <cite id="dsq-cite-305987">
                <span id="dsq-author-user-305987">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305987" class="dsq-comment-body">
            <div id="dsq-comment-message-305987" class="dsq-comment-message"><p>yeah, because this new silhouette and complicated CW will take quite some time to &#8220;sink in&#8221;. You were ready to buy these pieces of shit at $400 before you even knew they existed.</p>
<p>lol.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306110">
        <div id="dsq-comment-header-306110" class="dsq-comment-header">
            <cite id="dsq-cite-306110">
                <span id="dsq-author-user-306110">☆Dizzy Story☆</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306110" class="dsq-comment-body">
            <div id="dsq-comment-message-306110" class="dsq-comment-message"><p>lol true</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-305984">
        <div id="dsq-comment-header-305984" class="dsq-comment-header">
            <cite id="dsq-cite-305984">
                <span id="dsq-author-user-305984">Mark</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305984" class="dsq-comment-body">
            <div id="dsq-comment-message-305984" class="dsq-comment-message"><p>$220? Nope.. $200 was enough for the legend blues.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-305995">
        <div id="dsq-comment-header-305995" class="dsq-comment-header">
            <cite id="dsq-cite-305995">
                <span id="dsq-author-user-305995">Julio Guerra</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305995" class="dsq-comment-body">
            <div id="dsq-comment-message-305995" class="dsq-comment-message"><p>Exactly bro and the resale market went sky high because most people sold there pair cause it was ether too white or just regretted paying 200 .</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-306046">
        <div id="dsq-comment-header-306046" class="dsq-comment-header">
            <cite id="dsq-cite-306046">
                <span id="dsq-author-user-306046">Mark</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306046" class="dsq-comment-body">
            <div id="dsq-comment-message-306046" class="dsq-comment-message"><p>Yup, and 11s ALWAYS get hit the resell market even though they make 500,000 pairs. A $35 price hike over the last 2 years for the SAME exact shoe, just different colors is ridiculous. Especailly when you consider the news coming out that Jordan himself made $100 MILLION last year off all of us!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-305986">
        <div id="dsq-comment-header-305986" class="dsq-comment-header">
            <cite id="dsq-cite-305986">
                <span id="dsq-author-user-305986">Hospitalized Me</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305986" class="dsq-comment-body">
            <div id="dsq-comment-message-305986" class="dsq-comment-message"><p>Fuck this shit. Fuck you Jordan. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-305988">
        <div id="dsq-comment-header-305988" class="dsq-comment-header">
            <cite id="dsq-cite-305988">
                <span id="dsq-author-user-305988">Easy $</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305988" class="dsq-comment-body">
            <div id="dsq-comment-message-305988" class="dsq-comment-message"><p>220? lmao, not even an og and its a basic ass cw. This is where people will stop paying.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-305991">
        <div id="dsq-comment-header-305991" class="dsq-comment-header">
            <cite id="dsq-cite-305991">
                <span id="dsq-author-user-305991">Anthony</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305991" class="dsq-comment-body">
            <div id="dsq-comment-message-305991" class="dsq-comment-message"><p>220? Do my eyes decieve me?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-305994">
        <div id="dsq-comment-header-305994" class="dsq-comment-header">
            <cite id="dsq-cite-305994">
                <span id="dsq-author-user-305994">Julio Guerra</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305994" class="dsq-comment-body">
            <div id="dsq-comment-message-305994" class="dsq-comment-message"><p>Space &#8220;breds&#8221;  or Breds 2.0 &#8230;. Unless they stitch 72-10 on that shoe somewhere .it doesn&#8217;t have no type of feel of it being 72 and 10 lol .</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-305997">
        <div id="dsq-comment-header-305997" class="dsq-comment-header">
            <cite id="dsq-cite-305997">
                <span id="dsq-author-user-305997">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305997" class="dsq-comment-body">
            <div id="dsq-comment-message-305997" class="dsq-comment-message"><p>Smdh now that&#8217;s just disrespectful 220. For these that will be yellow in 3 wears tops. NO TIME</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-305999">
        <div id="dsq-comment-header-305999" class="dsq-comment-header">
            <cite id="dsq-cite-305999">
                <span id="dsq-author-user-305999">CJWatson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-305999" class="dsq-comment-body">
            <div id="dsq-comment-message-305999" class="dsq-comment-message"><p>Well I guess the legend blues will become my last pair of 11`s  220 + tax sorry I won&#8217;t fight the mob that&#8217;s y I don&#8217;t own any LeBron 12 til they get cheaper it&#8217;s getting out of control</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-306000">
        <div id="dsq-comment-header-306000" class="dsq-comment-header">
            <cite id="dsq-cite-306000">
                <span id="dsq-author-user-306000">guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306000" class="dsq-comment-body">
            <div id="dsq-comment-message-306000" class="dsq-comment-message"><p>Wait a minute..everything was cool til i seen 220. Now i know what else im not buying this year</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-306004">
        <div id="dsq-comment-header-306004" class="dsq-comment-header">
            <cite id="dsq-cite-306004">
                <span id="dsq-author-user-306004">A</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306004" class="dsq-comment-body">
            <div id="dsq-comment-message-306004" class="dsq-comment-message"><p>i dont get it you guys aree already willing to pay 200+ 20 dollars wont kill u if ur already spending that much</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306010">
        <div id="dsq-comment-header-306010" class="dsq-comment-header">
            <cite id="dsq-cite-306010">
                <span id="dsq-author-user-306010">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306010" class="dsq-comment-body">
            <div id="dsq-comment-message-306010" class="dsq-comment-message"><p>If you own a pair of foams &amp; a pair of 11s it&#8217;s no question that 220 for jays is outrageous. Foams I&#8217;ve had for ten years. &amp; there still wearable. &amp; hold there shape. Name 1 pair of jays that does that? Don&#8217;t worry I&#8217;ll wait&#8230;&#8230;.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-306018">
        <div id="dsq-comment-header-306018" class="dsq-comment-header">
            <cite id="dsq-cite-306018">
                <span id="dsq-author-user-306018">OG_KiCKS_NJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306018" class="dsq-comment-body">
            <div id="dsq-comment-message-306018" class="dsq-comment-message"><p>Exactly &amp; the way I see it GS sizes are gunna be $150-165 range on these. Just imagine these kids saving up their money only to find out they have to pay way more. These fly off shelves regardless, only to resell them. I myself copped the Legend Blues. Won 3 raffles, sold 2 pairs for $20 above retail just to help people out instead of being raped by some random reseller. They just need to make even more pairs, over saturate the market, make soo many pairs they sit till summer in most stores. That&#8217;ll kill this price hike</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-306019">
        <div id="dsq-comment-header-306019" class="dsq-comment-header">
            <cite id="dsq-cite-306019">
                <span id="dsq-author-user-306019">A</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306019" class="dsq-comment-body">
            <div id="dsq-comment-message-306019" class="dsq-comment-message"><p>Abs right my<br />
man</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-4" id="dsq-comment-306094">
        <div id="dsq-comment-header-306094" class="dsq-comment-header">
            <cite id="dsq-cite-306094">
                <span id="dsq-author-user-306094">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306094" class="dsq-comment-body">
            <div id="dsq-comment-message-306094" class="dsq-comment-message"><p>We need more like u in the shoe game. That&#8217;s what got this whole price increase started. Resale rapist</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-5" id="dsq-comment-306114">
        <div id="dsq-comment-header-306114" class="dsq-comment-header">
            <cite id="dsq-cite-306114">
                <span id="dsq-author-user-306114">OG_KiCKS_NJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306114" class="dsq-comment-body">
            <div id="dsq-comment-message-306114" class="dsq-comment-message"><p>I just like to help others, I sometimes go out &amp; get a bunch of tickets for super hyped releases &amp; help out my fellow GS size heads &amp; a few men size heads as well. It aint all about the money or fame, its good to help out once a while</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-3" id="dsq-comment-306020">
        <div id="dsq-comment-header-306020" class="dsq-comment-header">
            <cite id="dsq-cite-306020">
                <span id="dsq-author-user-306020">A</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306020" class="dsq-comment-body">
            <div id="dsq-comment-message-306020" class="dsq-comment-message"><p>Youre right it is high but were already paying 207 after tax for jays now .. people see 220 and are complaining yeah thats crazy but ur already paying 200 + on other jays this year&#8230; that extra what 30 shouldnt matter ? if ur already spending 200 + on other jays &#8230; maybe thats why some of these releases this year are still<br />
in stores  </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-306021">
        <div id="dsq-comment-header-306021" class="dsq-comment-header">
            <cite id="dsq-cite-306021">
                <span id="dsq-author-user-306021">A</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306021" class="dsq-comment-body">
            <div id="dsq-comment-message-306021" class="dsq-comment-message"><p>Foams do last forever though lol .</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-3" id="dsq-comment-306231">
        <div id="dsq-comment-header-306231" class="dsq-comment-header">
            <cite id="dsq-cite-306231">
                <span id="dsq-author-user-306231">Clifford Samuels</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306231" class="dsq-comment-body">
            <div id="dsq-comment-message-306231" class="dsq-comment-message"><p>3&#8217;s, 4&#8217;s, any type really. You just have to know how to sustain them and not beat the crap outta them.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-306233">
        <div id="dsq-comment-header-306233" class="dsq-comment-header">
            <cite id="dsq-cite-306233">
                <span id="dsq-author-user-306233">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306233" class="dsq-comment-body">
            <div id="dsq-comment-message-306233" class="dsq-comment-message"><p>You should have just keep that dumb ass comment to ur self buddy. Cause those are the worst FOH. They only way u keep em up for 10 yrs is not wearing them&#8230;..</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-5" id="dsq-comment-306234">
        <div id="dsq-comment-header-306234" class="dsq-comment-header">
            <cite id="dsq-cite-306234">
                <span id="dsq-author-user-306234">Clifford Samuels</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306234" class="dsq-comment-body">
            <div id="dsq-comment-message-306234" class="dsq-comment-message"><p>Oh boy, another internet badass. Its funny how you state your opinion as fact, as if anybody agrees with you on your statement. I have stealth 3&#8217;s that have been going on strong for 5 years now. Sure there&#8217;s some creasing and some paint cracking, but they still look good to the point where i still get compliments and questions as to where i got them. So please keep your bias to yourself and quit acting like your the Sophocles of Shoes.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-306244">
        <div id="dsq-comment-header-306244" class="dsq-comment-header">
            <cite id="dsq-cite-306244">
                <span id="dsq-author-user-306244">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306244" class="dsq-comment-body">
            <div id="dsq-comment-message-306244" class="dsq-comment-message"><p>Lol so where the internet bad ass come from? Cause I don&#8217;t agree with that BS you talking. Go online play on the freeway in ur whack ass stealths dude</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-5" id="dsq-comment-306246">
        <div id="dsq-comment-header-306246" class="dsq-comment-header">
            <cite id="dsq-cite-306246">
                <span id="dsq-author-user-306246">Clifford Samuels</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306246" class="dsq-comment-body">
            <div id="dsq-comment-message-306246" class="dsq-comment-message"><p>Thats funny considering you said my last statement was &#8220;a dumb ass comment&#8221;. And really? &#8220;Go and play in the freeway in your whack ass stealths&#8221;? You sound like you just got out of a mental institution.  I can&#8217;t even take you seriously.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-306277">
        <div id="dsq-comment-header-306277" class="dsq-comment-header">
            <cite id="dsq-cite-306277">
                <span id="dsq-author-user-306277">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306277" class="dsq-comment-body">
            <div id="dsq-comment-message-306277" class="dsq-comment-message"><p>You Cleary upset lil boy. U know what I meant ( play on the freeway) as in. Kill your self. Nobody cares what u think or about them weak ass stealth 3s Fuck boy</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-5" id="dsq-comment-306278">
        <div id="dsq-comment-header-306278" class="dsq-comment-header">
            <cite id="dsq-cite-306278">
                <span id="dsq-author-user-306278">Clifford Samuels</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306278" class="dsq-comment-body">
            <div id="dsq-comment-message-306278" class="dsq-comment-message"><p>I don&#8217;t know why i would be upset lol. I mean for what exactly? Your just a phony ass e-thug, i&#8217;m over here laughing at your dumb ass.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-306247">
        <div id="dsq-comment-header-306247" class="dsq-comment-header">
            <cite id="dsq-cite-306247">
                <span id="dsq-author-user-306247">Clifford Samuels</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306247" class="dsq-comment-body">
            <div id="dsq-comment-message-306247" class="dsq-comment-message"><p>And the reason why i referred to you as an &#8220;internet badass&#8221; is because your trying to act all tough by saying &#8220;FOH&#8221;. I don&#8217;t feel any type of way, i&#8217;m just calling a spade a spade.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306005">
        <div id="dsq-comment-header-306005" class="dsq-comment-header">
            <cite id="dsq-cite-306005">
                <span id="dsq-author-user-306005">A</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306005" class="dsq-comment-body">
            <div id="dsq-comment-message-306005" class="dsq-comment-message"><p>Hope the come in a nice box</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306008">
        <div id="dsq-comment-header-306008" class="dsq-comment-header">
            <cite id="dsq-cite-306008">
                <span id="dsq-author-user-306008">Michael</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306008" class="dsq-comment-body">
            <div id="dsq-comment-message-306008" class="dsq-comment-message"><p>Should of used the wizards colorway or maybe drop the easter edition low tops 11 sigh well&#8230; guess i will be passing on jb 11&#8217;s this year again</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306013">
        <div id="dsq-comment-header-306013" class="dsq-comment-header">
            <cite id="dsq-cite-306013">
                <span id="dsq-author-user-306013">duke334</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306013" class="dsq-comment-body">
            <div id="dsq-comment-message-306013" class="dsq-comment-message"><p>Lol $220?? I swear Jordan pimping on people with these prices. These shits was just $185 Christmas of 2012 I think. Plus tax on these like $235. You got a 32&#8243; flat screen on your feet. Man I wanna see Jordans get up to $250 by 2017 &amp; see if dudes still be buying them lol. These not even dope. Shoe game getting disgusting out here for people.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306062">
        <div id="dsq-comment-header-306062" class="dsq-comment-header">
            <cite id="dsq-cite-306062">
                <span id="dsq-author-user-306062">zoeboy1305</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306062" class="dsq-comment-body">
            <div id="dsq-comment-message-306062" class="dsq-comment-message"><p>the laser 20s was 250</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-306070">
        <div id="dsq-comment-header-306070" class="dsq-comment-header">
            <cite id="dsq-cite-306070">
                <span id="dsq-author-user-306070">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306070" class="dsq-comment-body">
            <div id="dsq-comment-message-306070" class="dsq-comment-message"><p>and those hoes sat on the bench for damn near a month</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306017">
        <div id="dsq-comment-header-306017" class="dsq-comment-header">
            <cite id="dsq-cite-306017">
                <span id="dsq-author-user-306017">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306017" class="dsq-comment-body">
            <div id="dsq-comment-message-306017" class="dsq-comment-message"><p>Only way these will be on my feet is if their a gift.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306022">
        <div id="dsq-comment-header-306022" class="dsq-comment-header">
            <cite id="dsq-cite-306022">
                <span id="dsq-author-user-306022">brook</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306022" class="dsq-comment-body">
            <div id="dsq-comment-message-306022" class="dsq-comment-message"><p>These will deff sell out .. all 11s do</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306025">
        <div id="dsq-comment-header-306025" class="dsq-comment-header">
            <cite id="dsq-cite-306025">
                <span id="dsq-author-user-306025">InMy02&#8217;s</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306025" class="dsq-comment-body">
            <div id="dsq-comment-message-306025" class="dsq-comment-message"><p>Are we really going to have a discussion about a price.. smh like it really matters how you think we made this man a billionaire, what from not coping? Cut it out folks beta put that OT in lol </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-306032">
        <div id="dsq-comment-header-306032" class="dsq-comment-header">
            <cite id="dsq-cite-306032">
                <span id="dsq-author-user-306032">Jefe Coszi Bryce</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306032" class="dsq-comment-body">
            <div id="dsq-comment-message-306032" class="dsq-comment-message"><p>We didnt make him a billionaire as quick as we made phil knight a billionaire&#8230;he made that from his stocks in the Hornets 750m. We are making Nike richer and they cut him a check&#8230;not like he makes a billion a year off Nike..GTFOH</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-306028">
        <div id="dsq-comment-header-306028" class="dsq-comment-header">
            <cite id="dsq-cite-306028">
                <span id="dsq-author-user-306028">Sneakerguidekeepit#</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306028" class="dsq-comment-body">
            <div id="dsq-comment-message-306028" class="dsq-comment-message"><p>All ya bitching in comments need to stop cuz ya know on the nineteenth y&#8217;all be the first ones Lined up for these. Besides these are 10x better then the gamma blues and ya suck the skin off does</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-306416">
        <div id="dsq-comment-header-306416" class="dsq-comment-header">
            <cite id="dsq-cite-306416">
                <span id="dsq-author-user-306416">DW22TheGreatest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306416" class="dsq-comment-body">
            <div id="dsq-comment-message-306416" class="dsq-comment-message"><p>Thank you that&#8217;s what I&#8217;m saying</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306036">
        <div id="dsq-comment-header-306036" class="dsq-comment-header">
            <cite id="dsq-cite-306036">
                <span id="dsq-author-user-306036">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306036" class="dsq-comment-body">
            <div id="dsq-comment-message-306036" class="dsq-comment-message"><p>220? Lmao Chill JB Greedy Fucks ! Sooner Or Later People Gone Wise Up Tho </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-306039">
        <div id="dsq-comment-header-306039" class="dsq-comment-header">
            <cite id="dsq-cite-306039">
                <span id="dsq-author-user-306039">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306039" class="dsq-comment-body">
            <div id="dsq-comment-message-306039" class="dsq-comment-message"><p>That nigga made that 100 mill last yr. &amp; want it again this yr. 185 was already past my limit on jays. Especially when u know how much they use to retail at. I remember when a pair of 11s was $125</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306040">
        <div id="dsq-comment-header-306040" class="dsq-comment-header">
            <cite id="dsq-cite-306040">
                <span id="dsq-author-user-306040">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306040" class="dsq-comment-body">
            <div id="dsq-comment-message-306040" class="dsq-comment-message"><p>Man Thats Crazy I Work At Champs And I Get My Jays At A 30% Discount Man I Prolly Wont Ever Buy Them Again If I Stop Working There To Be Honest</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-4" id="dsq-comment-306093">
        <div id="dsq-comment-header-306093" class="dsq-comment-header">
            <cite id="dsq-cite-306093">
                <span id="dsq-author-user-306093">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306093" class="dsq-comment-body">
            <div id="dsq-comment-message-306093" class="dsq-comment-message"><p>Man I wouldn&#8217;t blame you bruh. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-5" id="dsq-comment-306101">
        <div id="dsq-comment-header-306101" class="dsq-comment-header">
            <cite id="dsq-cite-306101">
                <span id="dsq-author-user-306101">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306101" class="dsq-comment-body">
            <div id="dsq-comment-message-306101" class="dsq-comment-message"><p>Yeah I Got Other Kicks Besides Jays Its Just Fucked Up How They Raising The Price And Not Even Fixing The Quality Like They Said </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-4" id="dsq-comment-306100">
        <div id="dsq-comment-header-306100" class="dsq-comment-header">
            <cite id="dsq-cite-306100">
                <span id="dsq-author-user-306100">305</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306100" class="dsq-comment-body">
            <div id="dsq-comment-message-306100" class="dsq-comment-message"><p>man same here.  as bad as I don&#8217;t want to buy these shoes I know I&#8217;m going to only cuz its the holiday and there usually part of a xmas gift.  but know that there 220 it might be my only gift for xmas lol</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-5" id="dsq-comment-306102">
        <div id="dsq-comment-header-306102" class="dsq-comment-header">
            <cite id="dsq-cite-306102">
                <span id="dsq-author-user-306102">Tomgunz84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306102" class="dsq-comment-body">
            <div id="dsq-comment-message-306102" class="dsq-comment-message"><p>Yeah I Understand That But To Just Buy Without A Discount For Full Price Continously Is Insane Smh I Actually Started A List Of How Much I Pay For A Shoe At The Beginning Of The Yr Till The End </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306038">
        <div id="dsq-comment-header-306038" class="dsq-comment-header">
            <cite id="dsq-cite-306038">
                <span id="dsq-author-user-306038">BancoPopulair</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306038" class="dsq-comment-body">
            <div id="dsq-comment-message-306038" class="dsq-comment-message"><p>JB better get MJ ass back on the court in 2016 fuckin round talking about $220! FOH!!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306135">
        <div id="dsq-comment-header-306135" class="dsq-comment-header">
            <cite id="dsq-cite-306135">
                <span id="dsq-author-user-306135">Ac</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306135" class="dsq-comment-body">
            <div id="dsq-comment-message-306135" class="dsq-comment-message"><p>LMAOOOOOOOOOOOOOOOOOOO</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-306042">
        <div id="dsq-comment-header-306042" class="dsq-comment-header">
            <cite id="dsq-cite-306042">
                <span id="dsq-author-user-306042">Will</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306042" class="dsq-comment-body">
            <div id="dsq-comment-message-306042" class="dsq-comment-message"><p>Not these.. Bring back the Space Jams! I&#8217;m gonna have to pass. I have WAY too many black n red J&#8217;s. I would pay $220 for the Space Jams, no prob &#8211; but not these.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-306045">
        <div id="dsq-comment-header-306045" class="dsq-comment-header">
            <cite id="dsq-cite-306045">
                <span id="dsq-author-user-306045">BruhMan</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306045" class="dsq-comment-body">
            <div id="dsq-comment-message-306045" class="dsq-comment-message"><p>lmao&#8230;again you a billionaire give us a break. As much as i want these i will pass&#8230;not paying 220 for jays. i never bought any of the labs and the $190 price already got me on the fence this year</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-306057">
        <div id="dsq-comment-header-306057" class="dsq-comment-header">
            <cite id="dsq-cite-306057">
                <span id="dsq-author-user-306057">IM SMARTER THAN YOU</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306057" class="dsq-comment-body">
            <div id="dsq-comment-message-306057" class="dsq-comment-message"><p>EVERYONE BITCHING ABOUT THE PRICE IS A BROKE ASS BITCH! $20 MORE WHO THE FUCK CARES.  BALL UP.  PAY YOUR BILLS.  SAVE YOUR MONEY.  YOU HAVE LIKE 9 MONTHS UNTIL THE RELEASE.  STOP BITCHING YOU LAZY ASS MOTHER FUCKERS AND GET A JOB.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306071">
        <div id="dsq-comment-header-306071" class="dsq-comment-header">
            <cite id="dsq-cite-306071">
                <span id="dsq-author-user-306071">BruhMan</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306071" class="dsq-comment-body">
            <div id="dsq-comment-message-306071" class="dsq-comment-message"><p>if this the only pair someone wwas getting in 2015 your logic would make sense.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-306095">
        <div id="dsq-comment-header-306095" class="dsq-comment-header">
            <cite id="dsq-cite-306095">
                <span id="dsq-author-user-306095">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306095" class="dsq-comment-body">
            <div id="dsq-comment-message-306095" class="dsq-comment-message"><p>Niggas kill me wit thinking just cause you pay more &amp; can that&#8217;s makes everybody else broke. Actually your getting kicked in the dick every chance u get. With a smile. &amp; you can&#8217;t drive or live in a pair of shoes. He needs to change his name to dumber than you lol</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-3" id="dsq-comment-306217">
        <div id="dsq-comment-header-306217" class="dsq-comment-header">
            <cite id="dsq-cite-306217">
                <span id="dsq-author-user-306217">June Stone</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306217" class="dsq-comment-body">
            <div id="dsq-comment-message-306217" class="dsq-comment-message"><p>This shoe should make number 7 of the year  for me but i  have to leave two shoes off to fit this pair in</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-306239">
        <div id="dsq-comment-header-306239" class="dsq-comment-header">
            <cite id="dsq-cite-306239">
                <span id="dsq-author-user-306239">the rese</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306239" class="dsq-comment-body">
            <div id="dsq-comment-message-306239" class="dsq-comment-message"><p>Hey dude,not trying to complain or nun but some people r thinking about retirement or other things that they can be doing with their money. We&#8217;re still buying the same Damn model every holiday from jb. Its like a ponzi scheme, jb make so many of one brand of shoes now and couple years later come back again with the same Damn model and we&#8217;re jumping all over them. But at the end of the day I&#8217;m looking to have money not like mj but just enough. Not 200+ pair is shoes. But it&#8217;s all good. U only live once </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306415">
        <div id="dsq-comment-header-306415" class="dsq-comment-header">
            <cite id="dsq-cite-306415">
                <span id="dsq-author-user-306415">DW22TheGreatest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306415" class="dsq-comment-body">
            <div id="dsq-comment-message-306415" class="dsq-comment-message"><p>Nobodies forcing you to pay for the shoe theirs plenty of people that will pay that much and more to get these shoes and they&#8217;ll actually get them if you don&#8217;t wanna pay that much then don&#8217;t you just won&#8217;t get the shoe nobodies forcing you.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306067">
        <div id="dsq-comment-header-306067" class="dsq-comment-header">
            <cite id="dsq-cite-306067">
                <span id="dsq-author-user-306067">OnlyInNY</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306067" class="dsq-comment-body">
            <div id="dsq-comment-message-306067" class="dsq-comment-message"><p>If you have a fresh pair of space jams, then these are a must. They&#8217;d go great next to each other.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306074">
        <div id="dsq-comment-header-306074" class="dsq-comment-header">
            <cite id="dsq-cite-306074">
                <span id="dsq-author-user-306074">YouGreedyAssFucks</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306074" class="dsq-comment-body">
            <div id="dsq-comment-message-306074" class="dsq-comment-message"><p>220? Wtf happened to 185? Shits getting ridiculous. Idgaf if it&#8217;s a slight increase. Increase for what?!? Quality hasn&#8217;t changed stop fucking with us you inconsiderate greedy pieces of shit. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306083">
        <div id="dsq-comment-header-306083" class="dsq-comment-header">
            <cite id="dsq-cite-306083">
                <span id="dsq-author-user-306083">Corey Worthy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306083" class="dsq-comment-body">
            <div id="dsq-comment-message-306083" class="dsq-comment-message"><p>This shoe is really 220 for a an ok basic colorway and want me to pay 220 for basic or they should switch to 160</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306086">
        <div id="dsq-comment-header-306086" class="dsq-comment-header">
            <cite id="dsq-cite-306086">
                <span id="dsq-author-user-306086">WorldStarOfficialSpellCheck</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306086" class="dsq-comment-body">
            <div id="dsq-comment-message-306086" class="dsq-comment-message"><p>Dead ass I&#8217;m bout to stop collecting Jordan&#8217;s&#8230; Nike getting too greedy!!!!!! Nobody paying re sell prices for retail&#8230;</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-306099">
        <div id="dsq-comment-header-306099" class="dsq-comment-header">
            <cite id="dsq-cite-306099">
                <span id="dsq-author-user-306099">WorldStarHippy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306099" class="dsq-comment-body">
            <div id="dsq-comment-message-306099" class="dsq-comment-message"><p>no you ain&#8217;t.  i bet you cop these in a heart beat.  bitch ass nigga.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306103">
        <div id="dsq-comment-header-306103" class="dsq-comment-header">
            <cite id="dsq-cite-306103">
                <span id="dsq-author-user-306103">WorldStarOfficialSpellCheck</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306103" class="dsq-comment-body">
            <div id="dsq-comment-message-306103" class="dsq-comment-message"><p>I&#8217;m a bitch ass nigga because I didn&#8217;t like sneakers Huh?? Lol your a bitch ass nigga for worrying about another mans feet&#8230; Oh fuck your dead grandmother </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-4" id="dsq-comment-306293">
        <div id="dsq-comment-header-306293" class="dsq-comment-header">
            <cite id="dsq-cite-306293">
                <span id="dsq-author-user-306293">Boom!!</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306293" class="dsq-comment-body">
            <div id="dsq-comment-message-306293" class="dsq-comment-message"><p>Lol!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-3" id="dsq-comment-306104">
        <div id="dsq-comment-header-306104" class="dsq-comment-header">
            <cite id="dsq-cite-306104">
                <span id="dsq-author-user-306104">WorldStarOfficialSpellCheck</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306104" class="dsq-comment-body">
            <div id="dsq-comment-message-306104" class="dsq-comment-message"><p>Oh and if your a real nigga text my jack and let&#8217;s see if your really like that!!! 862-208-2776&#8230; Not a keyboard thug I kill niggas so hmu asap</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306127">
        <div id="dsq-comment-header-306127" class="dsq-comment-header">
            <cite id="dsq-cite-306127">
                <span id="dsq-author-user-306127">Carlos</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306127" class="dsq-comment-body">
            <div id="dsq-comment-message-306127" class="dsq-comment-message"><p>I was wondering when can you get the ticket at foot locker or is it to early to say the day?? (December)</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306136">
        <div id="dsq-comment-header-306136" class="dsq-comment-header">
            <cite id="dsq-cite-306136">
http://byncustoms.com/                <span id="dsq-author-user-306136">face</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306136" class="dsq-comment-body">
            <div id="dsq-comment-message-306136" class="dsq-comment-message"><p>I&#8217;m wondering if they gonna do like last Christmas with 2 pair of 11&#8217;s these and another pack???? I hope not though the last 1 almost killed my pockets lol</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-306292">
        <div id="dsq-comment-header-306292" class="dsq-comment-header">
            <cite id="dsq-cite-306292">
                <span id="dsq-author-user-306292">Boom!!</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306292" class="dsq-comment-body">
            <div id="dsq-comment-message-306292" class="dsq-comment-message"><p>Yeah it will probably be the Air Jordan xx10 or something like that.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306577">
        <div id="dsq-comment-header-306577" class="dsq-comment-header">
            <cite id="dsq-cite-306577">
                <span id="dsq-author-user-306577">Peter</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306577" class="dsq-comment-body">
            <div id="dsq-comment-message-306577" class="dsq-comment-message"><p>or Air Jordan xxx</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306146">
        <div id="dsq-comment-header-306146" class="dsq-comment-header">
            <cite id="dsq-cite-306146">
                <span id="dsq-author-user-306146">SMH</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306146" class="dsq-comment-body">
            <div id="dsq-comment-message-306146" class="dsq-comment-message"><p>they look like Bred&#8217;s wit icy bottoms imma cop but damn JB I could of thought of this -__-</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306147">
        <div id="dsq-comment-header-306147" class="dsq-comment-header">
            <cite id="dsq-cite-306147">
                <span id="dsq-author-user-306147">WhatsFly23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306147" class="dsq-comment-body">
            <div id="dsq-comment-message-306147" class="dsq-comment-message"><p>I had to grow onto these they fly now..I gettn um</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-306153">
        <div id="dsq-comment-header-306153" class="dsq-comment-header">
            <cite id="dsq-cite-306153">
                <span id="dsq-author-user-306153">Master P</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306153" class="dsq-comment-body">
            <div id="dsq-comment-message-306153" class="dsq-comment-message"><p>Wait till more pics start coming.  You&#8217;re gonna fall in love my dude</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306440">
        <div id="dsq-comment-header-306440" class="dsq-comment-header">
            <cite id="dsq-cite-306440">
                <span id="dsq-author-user-306440">Still Shy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306440" class="dsq-comment-body">
            <div id="dsq-comment-message-306440" class="dsq-comment-message"><p>Its so sad. Like do these bussinesses purposely leak photos to start up hype. The sneaker game will never be the same.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306152">
        <div id="dsq-comment-header-306152" class="dsq-comment-header">
            <cite id="dsq-cite-306152">
                <span id="dsq-author-user-306152">He Ain&#8217;t Ralph</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306152" class="dsq-comment-body">
            <div id="dsq-comment-message-306152" class="dsq-comment-message"><p>Wonder what Santa Clause feels about all these hype release during Christmas.  Wonder if my dude cops a pair for himself.  I mean he&#8217;s delivering them to so many hoods, why not cop a pair and fit in.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306223">
        <div id="dsq-comment-header-306223" class="dsq-comment-header">
            <cite id="dsq-cite-306223">
                <span id="dsq-author-user-306223">cat627h</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306223" class="dsq-comment-body">
            <div id="dsq-comment-message-306223" class="dsq-comment-message"><p>Jordan Brand plz no translucent outsoles on da Jordan 11&#8217;s..</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306261">
        <div id="dsq-comment-header-306261" class="dsq-comment-header">
            <cite id="dsq-cite-306261">
http://kixkonnection.com                <span id="dsq-author-user-306261">Kix Konnection</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306261" class="dsq-comment-body">
            <div id="dsq-comment-message-306261" class="dsq-comment-message"><p>I think these are dope! Imma cop myself 3 pair.. 2 wear 1 pair to collect and shit load to sell..<br />
We’re Changing the Sneaker Game – –  <a href="http://shop.kixkonnection.com" rel="nofollow">http://shop.kixkonnection.com</a><br />
Visit our full website – –  <a href="http://kixkonnection.com" rel="nofollow">http://kixkonnection.com</a><br />
Subscribe to our Newsletter – <a href="http://kixkonnection.com/newsletter" rel="nofollow">http://kixkonnection.com/newsletter</a><br />
Follow Us:</p>
<p>FaceBook / Twitter / Instagram: @kixkonnection:disqus</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306499">
        <div id="dsq-comment-header-306499" class="dsq-comment-header">
            <cite id="dsq-cite-306499">
                <span id="dsq-author-user-306499">Nice shoe</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306499" class="dsq-comment-body">
            <div id="dsq-comment-message-306499" class="dsq-comment-message"><p>Exactly why the &#8220;shoe game&#8221; sucks right now. The resale gimmick has been entirely overdone. The whole &#8220;sneakerhead&#8221; thing is messed up as well cause most of the people are just buying shoes to fit this terrible gimmick. Instead of buying shoes they like to wear. An entire trend of followers. So people like me who genuinely like the Jordan XI shoe specifically have to compete with all the followers. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-306651">
        <div id="dsq-comment-header-306651" class="dsq-comment-header">
            <cite id="dsq-cite-306651">
                <span id="dsq-author-user-306651">yankeesfn2</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306651" class="dsq-comment-body">
            <div id="dsq-comment-message-306651" class="dsq-comment-message"><p>Exactly your right. Greed kills it for everyone </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-3" id="dsq-comment-309156">
        <div id="dsq-comment-header-309156" class="dsq-comment-header">
            <cite id="dsq-cite-309156">
                <span id="dsq-author-user-309156">VCITY1153</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309156" class="dsq-comment-body">
            <div id="dsq-comment-message-309156" class="dsq-comment-message"><p>Great point.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-306290">
        <div id="dsq-comment-header-306290" class="dsq-comment-header">
            <cite id="dsq-cite-306290">
                <span id="dsq-author-user-306290">Juno</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306290" class="dsq-comment-body">
            <div id="dsq-comment-message-306290" class="dsq-comment-message"><p>that ladies and gentlemen is why Michael Jordan is now a Billionaire&#8230;500 000 pairs sold for the 1st week x 20 Dollars increase on each pair&#8230;.well, just do the math</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306576">
        <div id="dsq-comment-header-306576" class="dsq-comment-header">
            <cite id="dsq-cite-306576">
                <span id="dsq-author-user-306576">Peter</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306576" class="dsq-comment-body">
            <div id="dsq-comment-message-306576" class="dsq-comment-message"><p>he doesnt get all the money, the money has to be split with manufacturing costs, employees and everyone else who works with JB.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-306622">
        <div id="dsq-comment-header-306622" class="dsq-comment-header">
            <cite id="dsq-cite-306622">
                <span id="dsq-author-user-306622">Robert</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306622" class="dsq-comment-body">
            <div id="dsq-comment-message-306622" class="dsq-comment-message"><p>He talks in royalties from his product sales. For example, if jordan brand makes 2.5 billion, he takes home about 90 million. He&#8217;s a billionaire because of his stake in the Charlotte Hornets </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306623">
        <div id="dsq-comment-header-306623" class="dsq-comment-header">
            <cite id="dsq-cite-306623">
                <span id="dsq-author-user-306623">Robert</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306623" class="dsq-comment-body">
            <div id="dsq-comment-message-306623" class="dsq-comment-message"><p>*takes*</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306347">
        <div id="dsq-comment-header-306347" class="dsq-comment-header">
            <cite id="dsq-cite-306347">
                <span id="dsq-author-user-306347">FTP</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306347" class="dsq-comment-body">
            <div id="dsq-comment-message-306347" class="dsq-comment-message"><p>These are a letdown.  The Space Jam 11&#8217;s with the mesh were one of the best J&#8217;s and they should be re-released.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306400">
        <div id="dsq-comment-header-306400" class="dsq-comment-header">
            <cite id="dsq-cite-306400">
                <span id="dsq-author-user-306400">T Bon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306400" class="dsq-comment-body">
            <div id="dsq-comment-message-306400" class="dsq-comment-message"><p>I want red on white 11&#8217;s finally. Like the cherry lows but in high top.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306519">
        <div id="dsq-comment-header-306519" class="dsq-comment-header">
            <cite id="dsq-cite-306519">
                <span id="dsq-author-user-306519">SirFuck YaMotha</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306519" class="dsq-comment-body">
            <div id="dsq-comment-message-306519" class="dsq-comment-message"><p>So I&#8217;m guessing this is going to be the Jordan&#8217;s released for Christmas. I will get them but the Space Jams would be better but these not that bad but if anything else comes in just keep posting because I got to know the 11&#8217;s that are coming out this year</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-306531">
        <div id="dsq-comment-header-306531" class="dsq-comment-header">
            <cite id="dsq-cite-306531">
                <span id="dsq-author-user-306531">Anthony</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306531" class="dsq-comment-body">
            <div id="dsq-comment-message-306531" class="dsq-comment-message"><p>Need to stop hoeing us and just make the sole black&#8230;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-306648">
        <div id="dsq-comment-header-306648" class="dsq-comment-header">
            <cite id="dsq-cite-306648">
                <span id="dsq-author-user-306648">The Truth 23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306648" class="dsq-comment-body">
            <div id="dsq-comment-message-306648" class="dsq-comment-message"><p>Add a red or black sole instead of that blue clear icy one. Then we could say they stepped their game to a whole other level. That is if JB really cares what we think now that $220 plus tax they bugging for real for real. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306689">
        <div id="dsq-comment-header-306689" class="dsq-comment-header">
            <cite id="dsq-cite-306689">
                <span id="dsq-author-user-306689">shadowhawk672</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306689" class="dsq-comment-body">
            <div id="dsq-comment-message-306689" class="dsq-comment-message"><p>i think they would also be cool if they added black bottoms but if they add the red bottoms they would basically be the breds and did you see how terrible the form of the black bottoms on that picture above so i like the ice bottoms</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-306662">
        <div id="dsq-comment-header-306662" class="dsq-comment-header">
            <cite id="dsq-cite-306662">
                <span id="dsq-author-user-306662">x_MJ_x</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306662" class="dsq-comment-body">
            <div id="dsq-comment-message-306662" class="dsq-comment-message"><p>space breds, lul. Passing</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-306688">
        <div id="dsq-comment-header-306688" class="dsq-comment-header">
            <cite id="dsq-cite-306688">
                <span id="dsq-author-user-306688">shadowhawk672</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306688" class="dsq-comment-body">
            <div id="dsq-comment-message-306688" class="dsq-comment-message"><p>why would you pass</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-306872">
        <div id="dsq-comment-header-306872" class="dsq-comment-header">
            <cite id="dsq-cite-306872">
                <span id="dsq-author-user-306872">The TRUTH 23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306872" class="dsq-comment-body">
            <div id="dsq-comment-message-306872" class="dsq-comment-message"><p>Hey genius these are not space jams there releasing them based on the season they went 72-10, but can expect much from a person lul whatever that&#8217;s supposed to be!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-306702">
        <div id="dsq-comment-header-306702" class="dsq-comment-header">
            <cite id="dsq-cite-306702">
                <span id="dsq-author-user-306702">vladimir grey</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306702" class="dsq-comment-body">
            <div id="dsq-comment-message-306702" class="dsq-comment-message"><p>gorgeous shoe! copping all that way yeaaaa!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-306792">
        <div id="dsq-comment-header-306792" class="dsq-comment-header">
            <cite id="dsq-cite-306792">
                <span id="dsq-author-user-306792">SirFuck YaMotha</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306792" class="dsq-comment-body">
            <div id="dsq-comment-message-306792" class="dsq-comment-message"><p>I think they cool but you can do a lot with the 11&#8217;s but they should have just brought back the space jams since they doing the low breds</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-306798">
        <div id="dsq-comment-header-306798" class="dsq-comment-header">
            <cite id="dsq-cite-306798">
                <span id="dsq-author-user-306798">GoPackkGo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306798" class="dsq-comment-body">
            <div id="dsq-comment-message-306798" class="dsq-comment-message"><p>Is it December yet </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-306922">
        <div id="dsq-comment-header-306922" class="dsq-comment-header">
            <cite id="dsq-cite-306922">
                <span id="dsq-author-user-306922">Poodle</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306922" class="dsq-comment-body">
            <div id="dsq-comment-message-306922" class="dsq-comment-message"><p>No bitch</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-306966">
        <div id="dsq-comment-header-306966" class="dsq-comment-header">
            <cite id="dsq-cite-306966">
                <span id="dsq-author-user-306966">GoPackkGo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-306966" class="dsq-comment-body">
            <div id="dsq-comment-message-306966" class="dsq-comment-message"><p>Rats</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-307190">
        <div id="dsq-comment-header-307190" class="dsq-comment-header">
            <cite id="dsq-cite-307190">
                <span id="dsq-author-user-307190">Corey Worthy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307190" class="dsq-comment-body">
            <div id="dsq-comment-message-307190" class="dsq-comment-message"><p>220 for a basic shoe inflation or nah</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-307596">
        <div id="dsq-comment-header-307596" class="dsq-comment-header">
            <cite id="dsq-cite-307596">
                <span id="dsq-author-user-307596">Cavarsea</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307596" class="dsq-comment-body">
            <div id="dsq-comment-message-307596" class="dsq-comment-message"><p>The price of J&#8217;s go up every year, why everybody going crazy over the 220 price tag when practically every hot shoe from Nike that mean something now is 200+. That 220 ain&#8217;t gone stop no one once the release get close.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-308620">
        <div id="dsq-comment-header-308620" class="dsq-comment-header">
            <cite id="dsq-cite-308620">
                <span id="dsq-author-user-308620">Lol!</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308620" class="dsq-comment-body">
            <div id="dsq-comment-message-308620" class="dsq-comment-message"><p>Lol, you hit the nail on the head with that one!! Heads will buy know matter what price Jordan brand throws out.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-307665">
        <div id="dsq-comment-header-307665" class="dsq-comment-header">
            <cite id="dsq-cite-307665">
                <span id="dsq-author-user-307665">Ariana</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307665" class="dsq-comment-body">
            <div id="dsq-comment-message-307665" class="dsq-comment-message"><p>i pray these come out in grade school sizes.. being a 6.5 and seeing these make me want to cry b/cit doesnt say anywhere that they will be releasing in grade school sizes.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-307730">
        <div id="dsq-comment-header-307730" class="dsq-comment-header">
            <cite id="dsq-cite-307730">
                <span id="dsq-author-user-307730">Prince</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307730" class="dsq-comment-body">
            <div id="dsq-comment-message-307730" class="dsq-comment-message"><p>they will dog, in all sizes and will be highly available</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-307924">
        <div id="dsq-comment-header-307924" class="dsq-comment-header">
            <cite id="dsq-cite-307924">
                <span id="dsq-author-user-307924">mars</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307924" class="dsq-comment-body">
            <div id="dsq-comment-message-307924" class="dsq-comment-message"><p>They need to make a mid version of both cherry 11 lows . Who agrees?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-308025">
        <div id="dsq-comment-header-308025" class="dsq-comment-header">
            <cite id="dsq-cite-308025">
                <span id="dsq-author-user-308025">OG_KiCKS_NJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308025" class="dsq-comment-body">
            <div id="dsq-comment-message-308025" class="dsq-comment-message"><p>Those original lows were actually rumored to come back out this summer, ahh I miss those OG colorways. Hopefully they redo those Hot Pink lows that were extremely rare. Good treat for the ladies</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-308081">
        <div id="dsq-comment-header-308081" class="dsq-comment-header">
            <cite id="dsq-cite-308081">
                <span id="dsq-author-user-308081">mars</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308081" class="dsq-comment-body">
            <div id="dsq-comment-message-308081" class="dsq-comment-message"><p>I&#8217;m just tired of mainly black color ways . Besides the legend blues, we have the Breds, space jams, gammas, and these new 72-10 11&#8217;s.. I want more colors than black </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-307926">
        <div id="dsq-comment-header-307926" class="dsq-comment-header">
            <cite id="dsq-cite-307926">
                <span id="dsq-author-user-307926">SQUIG</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307926" class="dsq-comment-body">
            <div id="dsq-comment-message-307926" class="dsq-comment-message"><p>Ahh man that iridescent patent leather Fkn killed the shit out of this shoe!! Rainbow ass leather on it foh</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-309155">
        <div id="dsq-comment-header-309155" class="dsq-comment-header">
            <cite id="dsq-cite-309155">
                <span id="dsq-author-user-309155">VCITY1153</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309155" class="dsq-comment-body">
            <div id="dsq-comment-message-309155" class="dsq-comment-message"><p>I Agree!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-307927">
        <div id="dsq-comment-header-307927" class="dsq-comment-header">
            <cite id="dsq-cite-307927">
                <span id="dsq-author-user-307927">Derek</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307927" class="dsq-comment-body">
            <div id="dsq-comment-message-307927" class="dsq-comment-message"><p>not sure if I&#8217;m feeling this new patent leather.  hope its either not included or looks a lot better in person</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-307941">
        <div id="dsq-comment-header-307941" class="dsq-comment-header">
            <cite id="dsq-cite-307941">
                <span id="dsq-author-user-307941">cat627h</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307941" class="dsq-comment-body">
            <div id="dsq-comment-message-307941" class="dsq-comment-message"><p>Translucent outsoles getin old, can JB get creative n give us sumin besides translucent outsoles..</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-307943">
        <div id="dsq-comment-header-307943" class="dsq-comment-header">
            <cite id="dsq-cite-307943">
                <span id="dsq-author-user-307943">enfinant</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307943" class="dsq-comment-body">
            <div id="dsq-comment-message-307943" class="dsq-comment-message"><p>It&#8217;s straight but the price is unnecessary. I would cop but I have to see what is coming out on December 26.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-307947">
        <div id="dsq-comment-header-307947" class="dsq-comment-header">
            <cite id="dsq-cite-307947">
                <span id="dsq-author-user-307947">guest 23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307947" class="dsq-comment-body">
            <div id="dsq-comment-message-307947" class="dsq-comment-message"><p>Space breds Smdh no creativity at all .ima still cop but these might just be the last 11s I do cop unless they have a limited pack coming out how they did with the patones </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-307948">
        <div id="dsq-comment-header-307948" class="dsq-comment-header">
            <cite id="dsq-cite-307948">
                <span id="dsq-author-user-307948">nyubynye .</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307948" class="dsq-comment-body">
            <div id="dsq-comment-message-307948" class="dsq-comment-message"><p>One good thing is the leather upper. I think it will propel this shoe more than the patent leather</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-307964">
        <div id="dsq-comment-header-307964" class="dsq-comment-header">
            <cite id="dsq-cite-307964">
                <span id="dsq-author-user-307964">Big Papi</span>
            </cite>
        </div>
        <div id="dsq-comment-body-307964" class="dsq-comment-body">
            <div id="dsq-comment-message-307964" class="dsq-comment-message"><p>Th iridescent patent leather will be so dope</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-308002">
        <div id="dsq-comment-header-308002" class="dsq-comment-header">
            <cite id="dsq-cite-308002">
                <span id="dsq-author-user-308002">Kicks Giggles</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308002" class="dsq-comment-body">
            <div id="dsq-comment-message-308002" class="dsq-comment-message"><p>these shits weak the hypebeasts will love that shit though then fuck them up in the first week</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-308026">
        <div id="dsq-comment-header-308026" class="dsq-comment-header">
            <cite id="dsq-cite-308026">
                <span id="dsq-author-user-308026">OG_KiCKS_NJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308026" class="dsq-comment-body">
            <div id="dsq-comment-message-308026" class="dsq-comment-message"><p>Yup. wear them each day till the next big Saturday release after the Holidays</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-309046">
        <div id="dsq-comment-header-309046" class="dsq-comment-header">
            <cite id="dsq-cite-309046">
                <span id="dsq-author-user-309046">law</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309046" class="dsq-comment-body">
            <div id="dsq-comment-message-309046" class="dsq-comment-message"><p>I just like the transparent toe box</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-308082">
        <div id="dsq-comment-header-308082" class="dsq-comment-header">
            <cite id="dsq-cite-308082">
                <span id="dsq-author-user-308082">princeretro9</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308082" class="dsq-comment-body">
            <div id="dsq-comment-message-308082" class="dsq-comment-message"><p>Bred n space jam had a baby ? Lol but iI think these will grow on me hopefully by December iI like them enough to cop</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-308085">
        <div id="dsq-comment-header-308085" class="dsq-comment-header">
            <cite id="dsq-cite-308085">
                <span id="dsq-author-user-308085">Boom!!</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308085" class="dsq-comment-body">
            <div id="dsq-comment-message-308085" class="dsq-comment-message"><p>This is what I&#8217;m saying, plenty of time to change your mind, probably will cop though!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-308102">
        <div id="dsq-comment-header-308102" class="dsq-comment-header">
            <cite id="dsq-cite-308102">
                <span id="dsq-author-user-308102">Kooler</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308102" class="dsq-comment-body">
            <div id="dsq-comment-message-308102" class="dsq-comment-message"><p>I know a lot of people don&#8217;t really like them, thinking they imitate the space jams but I personally like this pair and they have a nice theme for them. I&#8217;m definitely getting them.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-308174">
        <div id="dsq-comment-header-308174" class="dsq-comment-header">
            <cite id="dsq-cite-308174">
                <span id="dsq-author-user-308174">Will</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308174" class="dsq-comment-body">
            <div id="dsq-comment-message-308174" class="dsq-comment-message"><p>JB is making it very easy for me to save money this Holiday season. So far everything that is coming out from Black Friday on is JUNK unless they announce something new. These 11s are ok but not nice enough to pay $220</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-308222">
        <div id="dsq-comment-header-308222" class="dsq-comment-header">
            <cite id="dsq-cite-308222">
                <span id="dsq-author-user-308222">Lex</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308222" class="dsq-comment-body">
            <div id="dsq-comment-message-308222" class="dsq-comment-message"><p>220 eventually gs will be 180 and both these prices are too much for a shoe.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-308904">
        <div id="dsq-comment-header-308904" class="dsq-comment-header">
            <cite id="dsq-cite-308904">
                <span id="dsq-author-user-308904">Brooke Hoetzer</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308904" class="dsq-comment-body">
            <div id="dsq-comment-message-308904" class="dsq-comment-message"><p>jordan retro 11 georgetown or the lady liberty retro 10 torn between what to get???? recommendations or suggestions please</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-309045">
        <div id="dsq-comment-header-309045" class="dsq-comment-header">
            <cite id="dsq-cite-309045">
                <span id="dsq-author-user-309045">lawrence</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309045" class="dsq-comment-body">
            <div id="dsq-comment-message-309045" class="dsq-comment-message"><p>Lady liberty</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-309054">
        <div id="dsq-comment-header-309054" class="dsq-comment-header">
            <cite id="dsq-cite-309054">
                <span id="dsq-author-user-309054">Brooke Hoetzer</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309054" class="dsq-comment-body">
            <div id="dsq-comment-message-309054" class="dsq-comment-message"><p>Leaning towards those but I love the lows too .. What makes you say liberty </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-311074">
        <div id="dsq-comment-header-311074" class="dsq-comment-header">
            <cite id="dsq-cite-311074">
                <span id="dsq-author-user-311074">whitemikenj</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311074" class="dsq-comment-body">
            <div id="dsq-comment-message-311074" class="dsq-comment-message"><p>cause the liberty&#8217;s are better&#8230;i have both&#8230;but i love the libertys</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-309154">
        <div id="dsq-comment-header-309154" class="dsq-comment-header">
            <cite id="dsq-cite-309154">
                <span id="dsq-author-user-309154">VCITY1153</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309154" class="dsq-comment-body">
            <div id="dsq-comment-message-309154" class="dsq-comment-message"><p>Lady Liberty&#8217;s </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-308911">
        <div id="dsq-comment-header-308911" class="dsq-comment-header">
            <cite id="dsq-cite-308911">
                <span id="dsq-author-user-308911">&#8220;G&#8221;</span>
            </cite>
        </div>
        <div id="dsq-comment-body-308911" class="dsq-comment-body">
            <div id="dsq-comment-message-308911" class="dsq-comment-message"><p>For $220 their not that hot&#8230;take away the white and make it black. All black with  red jump man&#8230;mmmmmm???</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-310232">
        <div id="dsq-comment-header-310232" class="dsq-comment-header">
            <cite id="dsq-cite-310232">
                <span id="dsq-author-user-310232">Bryce Fuller</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310232" class="dsq-comment-body">
            <div id="dsq-comment-message-310232" class="dsq-comment-message"><p>Ngga they are prototypes.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-309153">
        <div id="dsq-comment-header-309153" class="dsq-comment-header">
            <cite id="dsq-cite-309153">
                <span id="dsq-author-user-309153">VCITY1153</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309153" class="dsq-comment-body">
            <div id="dsq-comment-message-309153" class="dsq-comment-message"><p>Space Jams.. Definitely. These look proportionally wrong in my opinion, but these are sample photos. Also, the colorway is bad. The black and white photos look better. The red jumpman throws everything off to me. I want these but I need to see the official photos.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-309720">
        <div id="dsq-comment-header-309720" class="dsq-comment-header">
            <cite id="dsq-cite-309720">
                <span id="dsq-author-user-309720">Kelvin Lopez</span>
            </cite>
        </div>
        <div id="dsq-comment-body-309720" class="dsq-comment-body">
            <div id="dsq-comment-message-309720" class="dsq-comment-message"><p>hell yeah im gettin these</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-310446">
        <div id="dsq-comment-header-310446" class="dsq-comment-header">
            <cite id="dsq-cite-310446">
                <span id="dsq-author-user-310446">Sneaker&#8217;s2fresh</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310446" class="dsq-comment-body">
            <div id="dsq-comment-message-310446" class="dsq-comment-message"><p>What is wrong with all u idiots, hype beasts, retarded people. Always repeating what everyone else opinions were. Clearly you don&#8217;t know that this is still 8 months prior to the release. All of you saying &#8220;these ain&#8217;t worth copping&#8221; Just because your a 14 yr old that doesn&#8217;t have $220 to get these. These are clearly original colorways that people have been wanting since 2010. Bred-Space-Jam mix. I&#8217;m copping, these are too fresh.<br />
I could care less if you don&#8217;t cop them. The more people the harder it will be for me to cop. <img src="http://sneakerbardetroit.com/wp-includes/images/smilies/simple-smile.png" alt=":)" class="wp-smiley" style="height: 1em; max-height: 1em;" /></p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-310987">
        <div id="dsq-comment-header-310987" class="dsq-comment-header">
            <cite id="dsq-cite-310987">
                <span id="dsq-author-user-310987">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310987" class="dsq-comment-body">
            <div id="dsq-comment-message-310987" class="dsq-comment-message"><p>but if you could care less then it means that you do care and these aint worth copping</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-311118">
        <div id="dsq-comment-header-311118" class="dsq-comment-header">
            <cite id="dsq-cite-311118">
                <span id="dsq-author-user-311118">The struggle</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311118" class="dsq-comment-body">
            <div id="dsq-comment-message-311118" class="dsq-comment-message"><p>Um dude sorry to break it to you but I was never gonna copped these space jams and breds fuse together but if u going for it go for it nobody stopping u.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-310712">
        <div id="dsq-comment-header-310712" class="dsq-comment-header">
            <cite id="dsq-cite-310712">
                <span id="dsq-author-user-310712">JayC3</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310712" class="dsq-comment-body">
            <div id="dsq-comment-message-310712" class="dsq-comment-message"><p>Im about to cut through the GS sizes and have my toes showing to save me some money or maybe just drop some cut in that white girl LoL..</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-310988">
        <div id="dsq-comment-header-310988" class="dsq-comment-header">
            <cite id="dsq-cite-310988">
                <span id="dsq-author-user-310988">Malachi Stone</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310988" class="dsq-comment-body">
            <div id="dsq-comment-message-310988" class="dsq-comment-message"><p>These Shoes Are Just Awful This Nigga JB Expect Mfs To Buy Anything.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-310990">
        <div id="dsq-comment-header-310990" class="dsq-comment-header">
            <cite id="dsq-cite-310990">
                <span id="dsq-author-user-310990">Legendary</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310990" class="dsq-comment-body">
            <div id="dsq-comment-message-310990" class="dsq-comment-message"><p>Won&#8217;t be upset if I miss out on these, just like missing out on the gamma&#8217;s 2yrs ago. Would have loved to have gotten another Crack at the Space Jams especially for his 30th&#8230;&#8230;maybe we will get lucky and they will release a pack including the Space Jams and the XXX&#8217;s like they did last year with the pantones.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-310991">
        <div id="dsq-comment-header-310991" class="dsq-comment-header">
            <cite id="dsq-cite-310991">
                <span id="dsq-author-user-310991">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310991" class="dsq-comment-body">
            <div id="dsq-comment-message-310991" class="dsq-comment-message"><p>I was on the fence about these but not anymore. This is a def no cop for me.  Release actual 11&#8217;s and then I&#8217;ll buy them with the price increase but not paying that for made up colorways.   get a clue JB</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-311005">
        <div id="dsq-comment-header-311005" class="dsq-comment-header">
            <cite id="dsq-cite-311005">
                <span id="dsq-author-user-311005">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311005" class="dsq-comment-body">
            <div id="dsq-comment-message-311005" class="dsq-comment-message"><p>Yeah, fuck them for doing something different. Who the hell do they think they are?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-311020">
        <div id="dsq-comment-header-311020" class="dsq-comment-header">
            <cite id="dsq-cite-311020">
                <span id="dsq-author-user-311020">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311020" class="dsq-comment-body">
            <div id="dsq-comment-message-311020" class="dsq-comment-message"><p>Clowns like yourself most likely buy this made up trash and encourage JB to keep making these.  Ppl with these actually think they have a real Jordan shoe GTFOH</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-4" id="dsq-comment-311024">
        <div id="dsq-comment-header-311024" class="dsq-comment-header">
            <cite id="dsq-cite-311024">
                <span id="dsq-author-user-311024">Just asking</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311024" class="dsq-comment-body">
            <div id="dsq-comment-message-311024" class="dsq-comment-message"><p>I&#8217;m just wondering what &#8220;actual 11&#8217;s&#8221; and &#8220;real Jordan&#8217;s are?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-5" id="dsq-comment-311026">
        <div id="dsq-comment-header-311026" class="dsq-comment-header">
            <cite id="dsq-cite-311026">
                <span id="dsq-author-user-311026">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311026" class="dsq-comment-body">
            <div id="dsq-comment-message-311026" class="dsq-comment-message"><p>og colorways breh not made up colorways for the kids</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-5" id="dsq-comment-311028">
        <div id="dsq-comment-header-311028" class="dsq-comment-header">
            <cite id="dsq-cite-311028">
                <span id="dsq-author-user-311028">Just asking</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311028" class="dsq-comment-body">
            <div id="dsq-comment-message-311028" class="dsq-comment-message"><p>Oh..In that case i may have to disagree, I like those but I also like the Cool grey and Space Jams, which were not OG colors</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-311029">
        <div id="dsq-comment-header-311029" class="dsq-comment-header">
            <cite id="dsq-cite-311029">
                <span id="dsq-author-user-311029">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311029" class="dsq-comment-body">
            <div id="dsq-comment-message-311029" class="dsq-comment-message"><p>those may not be OGs but I&#8217;ll let those colorways pass.  Why?  1.) Both those shoes have been released multiple times and B.) Jordan actually wore them while playing</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-310994">
        <div id="dsq-comment-header-310994" class="dsq-comment-header">
            <cite id="dsq-cite-310994">
                <span id="dsq-author-user-310994">exel leon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310994" class="dsq-comment-body">
            <div id="dsq-comment-message-310994" class="dsq-comment-message"><p>I feel like they gona fuck up the 11s this year</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-310997">
        <div id="dsq-comment-header-310997" class="dsq-comment-header">
            <cite id="dsq-cite-310997">
                <span id="dsq-author-user-310997">Lodidodida</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310997" class="dsq-comment-body">
            <div id="dsq-comment-message-310997" class="dsq-comment-message"><p>How can yall already have an opinion on the shoe? all you&#8217;ve seen is freelance photoshop and old samples that don&#8217;t even apply to this release.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-310999">
        <div id="dsq-comment-header-310999" class="dsq-comment-header">
            <cite id="dsq-cite-310999">
                <span id="dsq-author-user-310999">GreenMeanie</span>
            </cite>
        </div>
        <div id="dsq-comment-body-310999" class="dsq-comment-body">
            <div id="dsq-comment-message-310999" class="dsq-comment-message"><p>lol&#8230;all the people on here saying &#8220;pass.&#8221;  Good.  Remember that come Dec.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-311023">
        <div id="dsq-comment-header-311023" class="dsq-comment-header">
            <cite id="dsq-cite-311023">
                <span id="dsq-author-user-311023">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311023" class="dsq-comment-body">
            <div id="dsq-comment-message-311023" class="dsq-comment-message"><p>Ppl saying pass are actually care about shoes.  Come Dec most ppl that will try and get these are only interested in resell and not the actual shoe</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-311051">
        <div id="dsq-comment-header-311051" class="dsq-comment-header">
            <cite id="dsq-cite-311051">
                <span id="dsq-author-user-311051">OnlyInNY</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311051" class="dsq-comment-body">
            <div id="dsq-comment-message-311051" class="dsq-comment-message"><p>I&#8217;ve said pass on many kicks and I didn&#8217;t care about them nor did I end up getting them? </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-311053">
        <div id="dsq-comment-header-311053" class="dsq-comment-header">
            <cite id="dsq-cite-311053">
                <span id="dsq-author-user-311053">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311053" class="dsq-comment-body">
            <div id="dsq-comment-message-311053" class="dsq-comment-message"><p>You didn&#8217;t understand my post but it&#8217;s ok.  It&#8217;s not that they care about that individual shoe but care about shoes overall and what they rock not just getting them to resell.  Read above at guest 23&#8217;s post and that pretty much sums up what I&#8217;m trying to say</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-311030">
        <div id="dsq-comment-header-311030" class="dsq-comment-header">
            <cite id="dsq-cite-311030">
                <span id="dsq-author-user-311030">guest 23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311030" class="dsq-comment-body">
            <div id="dsq-comment-message-311030" class="dsq-comment-message"><p>I gives two fucks about Jordans . I&#8217;m just happy that there&#8217;s stupid fucks out here that want to skip the lines and skip all that raffle shit and buy shoes off me . If these are 220 in December then I can easily pop them off for 340 . </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-311031">
        <div id="dsq-comment-header-311031" class="dsq-comment-header">
            <cite id="dsq-cite-311031">
                <span id="dsq-author-user-311031">The Truth OG</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311031" class="dsq-comment-body">
            <div id="dsq-comment-message-311031" class="dsq-comment-message"><p>Because of niggas like u is the reason the sneaker game is fucked up grow da fuck up it&#8217;s funny to me how u thinking u hustler cause u flipping kicks that sell themselves. Like my nigga J said &#8221; I can sell water to a well&#8221; but u can&#8217;t say that. I bet u could probably sell water bottles for $20 bucks on a real hot day in the summer where their ain&#8217;t any water any where else LMAO!!! Stop fucking up da game nigga u the reason nike is upping their prices and don&#8217;t give a fuck about what they put out anymore. Cause there are niggas that are true sneaker heads like a bunch of my niggas and myself. Don&#8217;t worry me about my peeps and me, but their is way to many young niggas getting shot over a pair of Js after making sacrifices 2 own a pair of Js. So don&#8217;t be so ignorant and nieve Buster!!!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-4" id="dsq-comment-311120">
        <div id="dsq-comment-header-311120" class="dsq-comment-header">
            <cite id="dsq-cite-311120">
                <span id="dsq-author-user-311120">The Struggle is Real</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311120" class="dsq-comment-body">
            <div id="dsq-comment-message-311120" class="dsq-comment-message"><p>I agree with this man all the stuff he said is true shit all of these fucking Hype beasts just buy shoes cause a friend told about and just want to be a wannabe I do agree . And many of them don&#8217;t even like them and use them as playing shoes or just fuck them up and they buy the ugliest color ways and that&#8217;s why JB rise the prices in these ugliest js </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311006">
        <div id="dsq-comment-header-311006" class="dsq-comment-header">
            <cite id="dsq-cite-311006">
                <span id="dsq-author-user-311006">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311006" class="dsq-comment-body">
            <div id="dsq-comment-message-311006" class="dsq-comment-message"><p>$350 for plastic XI&#8217;s from an ebay seller = hell yeah<br />
$220 for all leather XI&#8217;s directly from Nike = fuck that shit</p>
<p>I know there are a lot of borderline illiterate, stupid folks in the sneaker scene, but some of the shit is still comical.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311027">
        <div id="dsq-comment-header-311027" class="dsq-comment-header">
            <cite id="dsq-cite-311027">
                <span id="dsq-author-user-311027">machinegun</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311027" class="dsq-comment-body">
            <div id="dsq-comment-message-311027" class="dsq-comment-message"><p>Have to see actual photosof the shoe to judge them</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311033">
        <div id="dsq-comment-header-311033" class="dsq-comment-header">
            <cite id="dsq-cite-311033">
                <span id="dsq-author-user-311033">Flight Club Ky</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311033" class="dsq-comment-body">
            <div id="dsq-comment-message-311033" class="dsq-comment-message"><p>I&#8217;m copping just wish they had that black sole like the photo shop pics instead of the icey blue. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311036">
        <div id="dsq-comment-header-311036" class="dsq-comment-header">
            <cite id="dsq-cite-311036">
                <span id="dsq-author-user-311036">Kicks Giggles</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311036" class="dsq-comment-body">
            <div id="dsq-comment-message-311036" class="dsq-comment-message"><p>these shits trash let us get the cool greys or space jams</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-311060">
        <div id="dsq-comment-header-311060" class="dsq-comment-header">
            <cite id="dsq-cite-311060">
                <span id="dsq-author-user-311060">weatmatt</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311060" class="dsq-comment-body">
            <div id="dsq-comment-message-311060" class="dsq-comment-message"><p>tu tambien pendejo</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-311297">
        <div id="dsq-comment-header-311297" class="dsq-comment-header">
            <cite id="dsq-cite-311297">
                <span id="dsq-author-user-311297">weatmatt</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311297" class="dsq-comment-body">
            <div id="dsq-comment-message-311297" class="dsq-comment-message"><p>CONTESTA !!!!!!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311037">
        <div id="dsq-comment-header-311037" class="dsq-comment-header">
            <cite id="dsq-cite-311037">
                <span id="dsq-author-user-311037">ono</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311037" class="dsq-comment-body">
            <div id="dsq-comment-message-311037" class="dsq-comment-message"><p>They are giving us this shit so that the 11s next year are so hyped you wont be able to look without seeing a hypebeast</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311056">
        <div id="dsq-comment-header-311056" class="dsq-comment-header">
            <cite id="dsq-cite-311056">
                <span id="dsq-author-user-311056">Hank Trill</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311056" class="dsq-comment-body">
            <div id="dsq-comment-message-311056" class="dsq-comment-message"><p>Yall crying over $220 but you&#8217;ll still buy them lmao</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-311059">
        <div id="dsq-comment-header-311059" class="dsq-comment-header">
            <cite id="dsq-cite-311059">
                <span id="dsq-author-user-311059">jaghd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311059" class="dsq-comment-body">
            <div id="dsq-comment-message-311059" class="dsq-comment-message"><p>vete la verga</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311075">
        <div id="dsq-comment-header-311075" class="dsq-comment-header">
            <cite id="dsq-cite-311075">
                <span id="dsq-author-user-311075">whitemikenj</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311075" class="dsq-comment-body">
            <div id="dsq-comment-message-311075" class="dsq-comment-message"><p>JB need to hire me&#8230;i would have made the 72-10 a &#8220;Dirty Bred&#8221; with a gold keychain (much like the DMP 11) to signify their championship&#8230;what a winner that would have been&#8230;next year they need to drop the Space Jam &#8220;45&#8221; 11&#8217;s</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311113">
        <div id="dsq-comment-header-311113" class="dsq-comment-header">
            <cite id="dsq-cite-311113">
                <span id="dsq-author-user-311113">Noah Middlefingazalwayzup Siff</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311113" class="dsq-comment-body">
            <div id="dsq-comment-message-311113" class="dsq-comment-message"><p>all these haters on here need to chill if you not copping then fine but we all know just like with the yeezy boost when they drop 99.9% of the same dudes running off at the mouth were trying to cop outright or on the low..come december the same lames who talking now will be looking to cop these, also if ya&#8217;ll so unhappy with the price its simple DON&#8217;T BUY i think its b.s that nike is flat out waxing but thats why i only cop the must haves on my list instead of BUYING EVERY DAMN RELEASE LIKE A HYPEBEAST..</p>
<p>For all you &#8221;so called&#8221; sneakerheads who always wanna talk about the original release, colorways, materials &amp; whatever other little nick nacky shit makes you sound more like old school hypebeast than a REAL sneakerhead who simply has love &amp; passion for the shoe game PERIOD all your complaints are not invalid but many are just silly as hell ;)&gt;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311253">
        <div id="dsq-comment-header-311253" class="dsq-comment-header">
            <cite id="dsq-cite-311253">
                <span id="dsq-author-user-311253">daniel marban</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311253" class="dsq-comment-body">
            <div id="dsq-comment-message-311253" class="dsq-comment-message"><p>make the white red then these shoes would get hella hype</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311369">
        <div id="dsq-comment-header-311369" class="dsq-comment-header">
            <cite id="dsq-cite-311369">
                <span id="dsq-author-user-311369">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311369" class="dsq-comment-body">
            <div id="dsq-comment-message-311369" class="dsq-comment-message"><p>220 oh hell nah</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311371">
        <div id="dsq-comment-header-311371" class="dsq-comment-header">
            <cite id="dsq-cite-311371">
                <span id="dsq-author-user-311371">seth crutchfield</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311371" class="dsq-comment-body">
            <div id="dsq-comment-message-311371" class="dsq-comment-message"><p>A must cop. That leather on the upper with the 23 stitched in is fireeee  </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311373">
        <div id="dsq-comment-header-311373" class="dsq-comment-header">
            <cite id="dsq-cite-311373">
                <span id="dsq-author-user-311373">Mark</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311373" class="dsq-comment-body">
            <div id="dsq-comment-message-311373" class="dsq-comment-message"><p>Need to see more pics, but I really might just pass.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311377">
        <div id="dsq-comment-header-311377" class="dsq-comment-header">
            <cite id="dsq-cite-311377">
                <span id="dsq-author-user-311377">just that guy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311377" class="dsq-comment-body">
            <div id="dsq-comment-message-311377" class="dsq-comment-message"><p>Put it like this is this really going threw all that drama and shit just to get a pair like I wasn&#8217;t going to cop Anyway but i know when they show the full shoe everyone will change there minds but is it worth it price and hype wise it&#8217;s a 11 nothing new but the colorway </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311380">
        <div id="dsq-comment-header-311380" class="dsq-comment-header">
            <cite id="dsq-cite-311380">
                <span id="dsq-author-user-311380">ChiRaqGoofies</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311380" class="dsq-comment-body">
            <div id="dsq-comment-message-311380" class="dsq-comment-message"><p>DESE REMIND ME OF MY BALLS&#8230;BLACK,LEATHERY &amp;ALL DA BITCHEZ WANT EM #OTF #getOffmynutz</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311381">
        <div id="dsq-comment-header-311381" class="dsq-comment-header">
            <cite id="dsq-cite-311381">
                <span id="dsq-author-user-311381">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311381" class="dsq-comment-body">
            <div id="dsq-comment-message-311381" class="dsq-comment-message"><p>def trying to get these for resell only.  I&#8217;d pay $140 max for these.  The rest of the shoe is not hard to figure out what it will look like</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311384">
        <div id="dsq-comment-header-311384" class="dsq-comment-header">
            <cite id="dsq-cite-311384">
                <span id="dsq-author-user-311384">FISHSTICKS</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311384" class="dsq-comment-body">
            <div id="dsq-comment-message-311384" class="dsq-comment-message"><p>Most Hype Shoe of 2015</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311389">
        <div id="dsq-comment-header-311389" class="dsq-comment-header">
            <cite id="dsq-cite-311389">
                <span id="dsq-author-user-311389">Flight Club Ky</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311389" class="dsq-comment-body">
            <div id="dsq-comment-message-311389" class="dsq-comment-message"><p>Must cop I passed on the gammas, Gone be a nice lil pick up for my archives</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311394">
        <div id="dsq-comment-header-311394" class="dsq-comment-header">
            <cite id="dsq-cite-311394">
                <span id="dsq-author-user-311394">willhoops</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311394" class="dsq-comment-body">
            <div id="dsq-comment-message-311394" class="dsq-comment-message"><p>That jordan sign look like in time it&#8217;ll fall 0ff </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-311397">
        <div id="dsq-comment-header-311397" class="dsq-comment-header">
            <cite id="dsq-cite-311397">
                <span id="dsq-author-user-311397">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311397" class="dsq-comment-body">
            <div id="dsq-comment-message-311397" class="dsq-comment-message"><p>It won&#8217;t matter. Only 1 out of every 10,000 pairs will actually be worn.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-311414">
        <div id="dsq-comment-header-311414" class="dsq-comment-header">
            <cite id="dsq-cite-311414">
                <span id="dsq-author-user-311414">1 KING</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311414" class="dsq-comment-body">
            <div id="dsq-comment-message-311414" class="dsq-comment-message"><p>1 out of every 10,000&#8230;lol, nice!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-3" id="dsq-comment-311422">
        <div id="dsq-comment-header-311422" class="dsq-comment-header">
            <cite id="dsq-cite-311422">
                <span id="dsq-author-user-311422">willhoops</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311422" class="dsq-comment-body">
            <div id="dsq-comment-message-311422" class="dsq-comment-message"><p>Lol rt but for the people who actually  wear em hopefully it&#8217;s stitched on pretty tight </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311395">
        <div id="dsq-comment-header-311395" class="dsq-comment-header">
            <cite id="dsq-cite-311395">
                <span id="dsq-author-user-311395">yooo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311395" class="dsq-comment-body">
            <div id="dsq-comment-message-311395" class="dsq-comment-message"><p>Wtf!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311398">
        <div id="dsq-comment-header-311398" class="dsq-comment-header">
            <cite id="dsq-cite-311398">
                <span id="dsq-author-user-311398">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311398" class="dsq-comment-body">
            <div id="dsq-comment-message-311398" class="dsq-comment-message"><p>these gon &#8220;grow on&#8221; everyone&#8230;and sell out in minutes&#8230;.and cost $400 on ebay&#8230;just like all XI&#8217;s.</p>
<p>nothing new to see here.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-311399">
        <div id="dsq-comment-header-311399" class="dsq-comment-header">
            <cite id="dsq-cite-311399">
                <span id="dsq-author-user-311399">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311399" class="dsq-comment-body">
            <div id="dsq-comment-message-311399" class="dsq-comment-message"><p>Materials look nice on these, too. The first non-plastic XI&#8217;s since the 90&#8217;s.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311401">
        <div id="dsq-comment-header-311401" class="dsq-comment-header">
            <cite id="dsq-cite-311401">
                <span id="dsq-author-user-311401">ChiRaqGoofies</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311401" class="dsq-comment-body">
            <div id="dsq-comment-message-311401" class="dsq-comment-message"><p>these gon &#8220;grow on&#8221; everyone&#8230;and sell out in minutes&#8230;.and cost $400 on ebay&#8230;just like all XI&#8217;s.</p>
<p>nothing new to see here. #OTF #SameOlShit</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311407">
        <div id="dsq-comment-header-311407" class="dsq-comment-header">
            <cite id="dsq-cite-311407">
                <span id="dsq-author-user-311407">Swaggy p</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311407" class="dsq-comment-body">
            <div id="dsq-comment-message-311407" class="dsq-comment-message"><p>I didn&#8217;t like these at first but the quality looks dope and they&#8217;re growing on me. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-311415">
        <div id="dsq-comment-header-311415" class="dsq-comment-header">
            <cite id="dsq-cite-311415">
                <span id="dsq-author-user-311415">Michael</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311415" class="dsq-comment-body">
            <div id="dsq-comment-message-311415" class="dsq-comment-message"><p>I guess i&#8217;ll be waiting till one of these chinese site get them&#8230;.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-311417">
        <div id="dsq-comment-header-311417" class="dsq-comment-header">
            <cite id="dsq-cite-311417">
                <span id="dsq-author-user-311417">ben</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311417" class="dsq-comment-body">
            <div id="dsq-comment-message-311417" class="dsq-comment-message"><p>dino hatfield himself said these were not real</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-311493">
        <div id="dsq-comment-header-311493" class="dsq-comment-header">
            <cite id="dsq-cite-311493">
                <span id="dsq-author-user-311493">j&#8217;s 4 days</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311493" class="dsq-comment-body">
            <div id="dsq-comment-message-311493" class="dsq-comment-message"><p>yeah the real pair is supposed to have &#8220;72-10&#8243; not 23 SBD</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311418">
        <div id="dsq-comment-header-311418" class="dsq-comment-header">
            <cite id="dsq-cite-311418">
                <span id="dsq-author-user-311418">Michael</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311418" class="dsq-comment-body">
            <div id="dsq-comment-message-311418" class="dsq-comment-message"><p>You have a better chance at winning a scratch off lottery ticket then copping a pair of j&#8217;s now days&#8230;&#8230;..</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311424">
        <div id="dsq-comment-header-311424" class="dsq-comment-header">
            <cite id="dsq-cite-311424">
                <span id="dsq-author-user-311424">ShoeEtiquette</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311424" class="dsq-comment-body">
            <div id="dsq-comment-message-311424" class="dsq-comment-message"><p>Jb/Nike fucking crazy $220 that is an insult to the sneaker community as a whole, you monopolizing  motherfuckers are out of line and disrespectful something serious</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311433">
        <div id="dsq-comment-header-311433" class="dsq-comment-header">
            <cite id="dsq-cite-311433">
                <span id="dsq-author-user-311433">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311433" class="dsq-comment-body">
            <div id="dsq-comment-message-311433" class="dsq-comment-message"><p>nah fam</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311436">
        <div id="dsq-comment-header-311436" class="dsq-comment-header">
            <cite id="dsq-cite-311436">
                <span id="dsq-author-user-311436">Kicks Giggles</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311436" class="dsq-comment-body">
            <div id="dsq-comment-message-311436" class="dsq-comment-message"><p>trash &#8230;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311445">
        <div id="dsq-comment-header-311445" class="dsq-comment-header">
            <cite id="dsq-cite-311445">
                <span id="dsq-author-user-311445">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311445" class="dsq-comment-body">
            <div id="dsq-comment-message-311445" class="dsq-comment-message"><p>I can see Nike not giving away to much info for these until it&#8217;s close to release date. Nike fucked up all the bootleg sites that were selling fake bred 11 lows with black insole when the legit ones had red insoles. Be patient folks&#8230;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311474">
        <div id="dsq-comment-header-311474" class="dsq-comment-header">
            <cite id="dsq-cite-311474">
                <span id="dsq-author-user-311474">face</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311474" class="dsq-comment-body">
            <div id="dsq-comment-message-311474" class="dsq-comment-message"><p>These the only Jordan&#8217;s that don&#8217;t get old quick cuz they don&#8217;t come out every month like the rest of the J&#8217;s</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311497">
        <div id="dsq-comment-header-311497" class="dsq-comment-header">
            <cite id="dsq-cite-311497">
                <span id="dsq-author-user-311497">JustaGuy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311497" class="dsq-comment-body">
            <div id="dsq-comment-message-311497" class="dsq-comment-message"><p>Quality looks good on those pictures. Still gotta get more before I make my mind.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311508">
        <div id="dsq-comment-header-311508" class="dsq-comment-header">
            <cite id="dsq-cite-311508">
                <span id="dsq-author-user-311508">Julio Guerra</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311508" class="dsq-comment-body">
            <div id="dsq-comment-message-311508" class="dsq-comment-message"><p>So far it has the same quality as a AJ 12 </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-311655">
        <div id="dsq-comment-header-311655" class="dsq-comment-header">
            <cite id="dsq-cite-311655">
                <span id="dsq-author-user-311655">23Finest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311655" class="dsq-comment-body">
            <div id="dsq-comment-message-311655" class="dsq-comment-message"><p>just hit up a few connects and I&#8217;m getting a sample pair this week!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-311772">
        <div id="dsq-comment-header-311772" class="dsq-comment-header">
            <cite id="dsq-cite-311772">
                <span id="dsq-author-user-311772">3847LawrenceAv</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311772" class="dsq-comment-body">
            <div id="dsq-comment-message-311772" class="dsq-comment-message"><p>How much would they charge for a pair of size 13 samples?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-312389">
        <div id="dsq-comment-header-312389" class="dsq-comment-header">
            <cite id="dsq-cite-312389">
                <span id="dsq-author-user-312389">yankeesfn2</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312389" class="dsq-comment-body">
            <div id="dsq-comment-message-312389" class="dsq-comment-message"><p>$500</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-4" id="dsq-comment-312390">
        <div id="dsq-comment-header-312390" class="dsq-comment-header">
            <cite id="dsq-cite-312390">
                <span id="dsq-author-user-312390">3847LawrenceAv</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312390" class="dsq-comment-body">
            <div id="dsq-comment-message-312390" class="dsq-comment-message"><p>Woah I live in Toronto man that means I&#8217;m paying like $600 basically forget that</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-313802">
        <div id="dsq-comment-header-313802" class="dsq-comment-header">
            <cite id="dsq-cite-313802">
                <span id="dsq-author-user-313802">David Sadek</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313802" class="dsq-comment-body">
            <div id="dsq-comment-message-313802" class="dsq-comment-message"><p>Price &amp; time-frame for samples in a mens size 9 to <a href="mailto:davidsadek@gmail.com">davidsadek@gmail.com</a><br />
Thanks!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-311844">
        <div id="dsq-comment-header-311844" class="dsq-comment-header">
            <cite id="dsq-cite-311844">
                <span id="dsq-author-user-311844">Disqus_deez</span>
            </cite>
        </div>
        <div id="dsq-comment-body-311844" class="dsq-comment-body">
            <div id="dsq-comment-message-311844" class="dsq-comment-message"><p>Gotta see what the bottom gone look like. Don&#8217;t put that trashy ice bottom on em </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-312273">
        <div id="dsq-comment-header-312273" class="dsq-comment-header">
            <cite id="dsq-cite-312273">
                <span id="dsq-author-user-312273">JustaGuy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312273" class="dsq-comment-body">
            <div id="dsq-comment-message-312273" class="dsq-comment-message"><p>Man, what do people have against Icy bottoms? They look great IMO. Just take care of your outsoles and they won&#8217;t yellow much if at all.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-312309">
        <div id="dsq-comment-header-312309" class="dsq-comment-header">
            <cite id="dsq-cite-312309">
                <span id="dsq-author-user-312309">Disqus_deez</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312309" class="dsq-comment-body">
            <div id="dsq-comment-message-312309" class="dsq-comment-message"><p>They gone yellow regardless. I like the look of ice on certain shoes, but I don&#8217;t buy cuz of the fact that overtime they will yellow. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-312445">
        <div id="dsq-comment-header-312445" class="dsq-comment-header">
            <cite id="dsq-cite-312445">
                <span id="dsq-author-user-312445">Pete</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312445" class="dsq-comment-body">
            <div id="dsq-comment-message-312445" class="dsq-comment-message"><p>they&#8217;re icy. I&#8217;d dye it black if i was going to pick them up</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-5" id="dsq-comment-312452">
        <div id="dsq-comment-header-312452" class="dsq-comment-header">
            <cite id="dsq-cite-312452">
                <span id="dsq-author-user-312452">Disqus_deez</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312452" class="dsq-comment-body">
            <div id="dsq-comment-message-312452" class="dsq-comment-message"><p>Now if they had a black icy bottom that would be dope</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-312296">
        <div id="dsq-comment-header-312296" class="dsq-comment-header">
            <cite id="dsq-cite-312296">
                <span id="dsq-author-user-312296">shadowhawk672</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312296" class="dsq-comment-body">
            <div id="dsq-comment-message-312296" class="dsq-comment-message"><p>i wish the shoe looked like the sample they had that looked like the space jams now ii dont know what the hell thats all i gotta say idk if they are going to be sick or not i don&#8217;t know</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-312338">
        <div id="dsq-comment-header-312338" class="dsq-comment-header">
            <cite id="dsq-cite-312338">
                <span id="dsq-author-user-312338">bk718celebrityblack</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312338" class="dsq-comment-body">
            <div id="dsq-comment-message-312338" class="dsq-comment-message"><p>Not Fire! Shits look like patent leather orthopedic shoes, for the fashionable 80 year old. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-312391">
        <div id="dsq-comment-header-312391" class="dsq-comment-header">
            <cite id="dsq-cite-312391">
                <span id="dsq-author-user-312391">Petey</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312391" class="dsq-comment-body">
            <div id="dsq-comment-message-312391" class="dsq-comment-message"><p>and you&#8217;ll still buy so stfu</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-312430">
        <div id="dsq-comment-header-312430" class="dsq-comment-header">
            <cite id="dsq-cite-312430">
                <span id="dsq-author-user-312430">bk718celebrityblack</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312430" class="dsq-comment-body">
            <div id="dsq-comment-message-312430" class="dsq-comment-message"><p>I won&#8217;t so suck my dick!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-312344">
        <div id="dsq-comment-header-312344" class="dsq-comment-header">
            <cite id="dsq-cite-312344">
                <span id="dsq-author-user-312344">cat627h</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312344" class="dsq-comment-body">
            <div id="dsq-comment-message-312344" class="dsq-comment-message"><p>Waiting game now, wait n see, hopefully JB will produce a top quality pair of 11&#8217;s..</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-312347">
        <div id="dsq-comment-header-312347" class="dsq-comment-header">
            <cite id="dsq-cite-312347">
                <span id="dsq-author-user-312347">Lost123</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312347" class="dsq-comment-body">
            <div id="dsq-comment-message-312347" class="dsq-comment-message"><p>Spacejams wouldve been better</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-312350">
        <div id="dsq-comment-header-312350" class="dsq-comment-header">
            <cite id="dsq-cite-312350">
                <span id="dsq-author-user-312350">jon brandon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312350" class="dsq-comment-body">
            <div id="dsq-comment-message-312350" class="dsq-comment-message"><p>no matter what color the xmas 11 is everyone gonna buy..its a tradition, its fun, exciting..doesnt matter if its green and orange</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-312355">
        <div id="dsq-comment-header-312355" class="dsq-comment-header">
            <cite id="dsq-cite-312355">
                <span id="dsq-author-user-312355">Trent</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312355" class="dsq-comment-body">
            <div id="dsq-comment-message-312355" class="dsq-comment-message"><p>So ugly do not like the tumbled leather how could jb mess up a Jordan 11?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-312392">
        <div id="dsq-comment-header-312392" class="dsq-comment-header">
            <cite id="dsq-cite-312392">
                <span id="dsq-author-user-312392">fuck these 11s</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312392" class="dsq-comment-body">
            <div id="dsq-comment-message-312392" class="dsq-comment-message"><p>Never thought I&#8217;ll see a 11 that resembles a 12 with the materials they using</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-312449">
        <div id="dsq-comment-header-312449" class="dsq-comment-header">
            <cite id="dsq-cite-312449">
                <span id="dsq-author-user-312449">CJWatson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312449" class="dsq-comment-body">
            <div id="dsq-comment-message-312449" class="dsq-comment-message"><p>You&#8217;re rite on that bro but on the same note the 12 were the best built of all </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-312444">
        <div id="dsq-comment-header-312444" class="dsq-comment-header">
            <cite id="dsq-cite-312444">
                <span id="dsq-author-user-312444">Pete</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312444" class="dsq-comment-body">
            <div id="dsq-comment-message-312444" class="dsq-comment-message"><p>JB didn&#8217;t learn their lesson with the iridescent baron 13s? shits were ugly</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-313568">
        <div id="dsq-comment-header-313568" class="dsq-comment-header">
            <cite id="dsq-cite-313568">
                <span id="dsq-author-user-313568">Mike</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313568" class="dsq-comment-body">
            <div id="dsq-comment-message-313568" class="dsq-comment-message"><p>truuuuuu</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-312448">
        <div id="dsq-comment-header-312448" class="dsq-comment-header">
            <cite id="dsq-cite-312448">
                <span id="dsq-author-user-312448">Jayme C</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312448" class="dsq-comment-body">
            <div id="dsq-comment-message-312448" class="dsq-comment-message"><p>Fucking CLEAN</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-312485">
        <div id="dsq-comment-header-312485" class="dsq-comment-header">
            <cite id="dsq-cite-312485">
                <span id="dsq-author-user-312485">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312485" class="dsq-comment-body">
            <div id="dsq-comment-message-312485" class="dsq-comment-message"><p>wack</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-312514">
        <div id="dsq-comment-header-312514" class="dsq-comment-header">
            <cite id="dsq-cite-312514">
                <span id="dsq-author-user-312514">Juan</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312514" class="dsq-comment-body">
            <div id="dsq-comment-message-312514" class="dsq-comment-message"><p>I bet you still cop tho.  Wait till we see the whole shoe you gonna be like DAMNNNNNN (smokey voice)</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-312538">
        <div id="dsq-comment-header-312538" class="dsq-comment-header">
            <cite id="dsq-cite-312538">
                <span id="dsq-author-user-312538">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312538" class="dsq-comment-body">
            <div id="dsq-comment-message-312538" class="dsq-comment-message"><p>I don&#8217;t spend 200 on kicks</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-313523">
        <div id="dsq-comment-header-313523" class="dsq-comment-header">
            <cite id="dsq-cite-313523">
                <span id="dsq-author-user-313523">Jake E Torres</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313523" class="dsq-comment-body">
            <div id="dsq-comment-message-313523" class="dsq-comment-message"><p>I have a few Asics too.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-312564">
        <div id="dsq-comment-header-312564" class="dsq-comment-header">
            <cite id="dsq-cite-312564">
                <span id="dsq-author-user-312564">guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312564" class="dsq-comment-body">
            <div id="dsq-comment-message-312564" class="dsq-comment-message"><p>Is this general release?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-313555">
        <div id="dsq-comment-header-313555" class="dsq-comment-header">
            <cite id="dsq-cite-313555">
                <span id="dsq-author-user-313555">RockawayBully</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313555" class="dsq-comment-body">
            <div id="dsq-comment-message-313555" class="dsq-comment-message"><p>Yes GR like every 11 release </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-313656">
        <div id="dsq-comment-header-313656" class="dsq-comment-header">
            <cite id="dsq-cite-313656">
                <span id="dsq-author-user-313656">wolfxxx</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313656" class="dsq-comment-body">
            <div id="dsq-comment-message-313656" class="dsq-comment-message"><p>But expect to defend yourself against the hypebeasts that make camps, the resellers who will take your size run, and the &#8220;thugs&#8221; who will skip you, fight/jump you and last but not least kill you for this GR.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-312575">
        <div id="dsq-comment-header-312575" class="dsq-comment-header">
            <cite id="dsq-cite-312575">
                <span id="dsq-author-user-312575">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-312575" class="dsq-comment-body">
            <div id="dsq-comment-message-312575" class="dsq-comment-message"><p>Would rather have had the space jam 11s. Once I get my hands on those I think i&#8217;d be done buying Js.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-313522">
        <div id="dsq-comment-header-313522" class="dsq-comment-header">
            <cite id="dsq-cite-313522">
                <span id="dsq-author-user-313522">Jake E Torres</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313522" class="dsq-comment-body">
            <div id="dsq-comment-message-313522" class="dsq-comment-message"><p>What size are you? Be paypal ready.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313354">
        <div id="dsq-comment-header-313354" class="dsq-comment-header">
            <cite id="dsq-cite-313354">
                <span id="dsq-author-user-313354">Jeremy Jackson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313354" class="dsq-comment-body">
            <div id="dsq-comment-message-313354" class="dsq-comment-message"><p>Thought it was a red metalic jumpman, now its a embossed jumpman. Thought they was going to be dope at 1st, not so sure now.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-313655">
        <div id="dsq-comment-header-313655" class="dsq-comment-header">
            <cite id="dsq-cite-313655">
                <span id="dsq-author-user-313655">wolfxxx</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313655" class="dsq-comment-body">
            <div id="dsq-comment-message-313655" class="dsq-comment-message"><p>Don&#8217;t those peel off at some point? The embossed jumpman?</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-313501">
        <div id="dsq-comment-header-313501" class="dsq-comment-header">
            <cite id="dsq-cite-313501">
                <span id="dsq-author-user-313501">Reggie Hughes</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313501" class="dsq-comment-body">
            <div id="dsq-comment-message-313501" class="dsq-comment-message"><p>I just want a full pic of the shoe lol</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-313506">
        <div id="dsq-comment-header-313506" class="dsq-comment-header">
            <cite id="dsq-cite-313506">
http://www.SneakerAutoBot.com/                <span id="dsq-author-user-313506">SneakerAutoBot.com</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313506" class="dsq-comment-body">
            <div id="dsq-comment-message-313506" class="dsq-comment-message"><p>Why the GITD soles tho?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-313797">
        <div id="dsq-comment-header-313797" class="dsq-comment-header">
            <cite id="dsq-cite-313797">
                <span id="dsq-author-user-313797">No hype here</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313797" class="dsq-comment-body">
            <div id="dsq-comment-message-313797" class="dsq-comment-message"><p>Quick question bruh. You know if the 2009 space jams had the size sticker on the inside of the insole?? Any help would be much appreciated </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313516">
        <div id="dsq-comment-header-313516" class="dsq-comment-header">
            <cite id="dsq-cite-313516">
                <span id="dsq-author-user-313516">willhoops</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313516" class="dsq-comment-body">
            <div id="dsq-comment-message-313516" class="dsq-comment-message"><p>Man they better not that doesn&#8217;t even go together </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313517">
        <div id="dsq-comment-header-313517" class="dsq-comment-header">
            <cite id="dsq-cite-313517">
                <span id="dsq-author-user-313517">Lol</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313517" class="dsq-comment-body">
            <div id="dsq-comment-message-313517" class="dsq-comment-message"><p>I&#8217;m not falling for fucking mind games!! Stop talking shit about I don&#8217;t like and won&#8217;t cop. Come December everyone will cop if you&#8217;ve got the money!!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313557">
        <div id="dsq-comment-header-313557" class="dsq-comment-header">
            <cite id="dsq-cite-313557">
                <span id="dsq-author-user-313557">RetroKid823</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313557" class="dsq-comment-body">
            <div id="dsq-comment-message-313557" class="dsq-comment-message"><p>All these fuckin side pictures, where a pic of the whole shoe. Like If u going to show all the other pictures might as well show the full thing. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313570">
        <div id="dsq-comment-header-313570" class="dsq-comment-header">
            <cite id="dsq-cite-313570">
                <span id="dsq-author-user-313570">Charles</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313570" class="dsq-comment-body">
            <div id="dsq-comment-message-313570" class="dsq-comment-message"><p>Are they releasing GS sizes?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313582">
        <div id="dsq-comment-header-313582" class="dsq-comment-header">
            <cite id="dsq-cite-313582">
                <span id="dsq-author-user-313582">Gmf Fse</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313582" class="dsq-comment-body">
            <div id="dsq-comment-message-313582" class="dsq-comment-message"><p>I&#8217;m getting 2 pair of these so if yu need a size 12 hmu&#8230; IG gmf_archie_fse</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313609">
        <div id="dsq-comment-header-313609" class="dsq-comment-header">
            <cite id="dsq-cite-313609">
                <span id="dsq-author-user-313609">Lol</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313609" class="dsq-comment-body">
            <div id="dsq-comment-message-313609" class="dsq-comment-message"><p>funking hell!! You might as well have just repeated what I said first!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313620">
        <div id="dsq-comment-header-313620" class="dsq-comment-header">
            <cite id="dsq-cite-313620">
                <span id="dsq-author-user-313620">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313620" class="dsq-comment-body">
            <div id="dsq-comment-message-313620" class="dsq-comment-message"><p>Those can&#8217;t be authentic. Unless for some reason they took the Jordan tag off of the tongue.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313622">
        <div id="dsq-comment-header-313622" class="dsq-comment-header">
            <cite id="dsq-cite-313622">
                <span id="dsq-author-user-313622">Jake E Torres</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313622" class="dsq-comment-body">
            <div id="dsq-comment-message-313622" class="dsq-comment-message"><p>Sole looks clear and no blue tint = autocop</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313628">
        <div id="dsq-comment-header-313628" class="dsq-comment-header">
            <cite id="dsq-cite-313628">
                <span id="dsq-author-user-313628">Probably</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313628" class="dsq-comment-body">
            <div id="dsq-comment-message-313628" class="dsq-comment-message"><p>man these look clean AF</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313629">
        <div id="dsq-comment-header-313629" class="dsq-comment-header">
            <cite id="dsq-cite-313629">
                <span id="dsq-author-user-313629">SIZE11</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313629" class="dsq-comment-body">
            <div id="dsq-comment-message-313629" class="dsq-comment-message"><p>That on foot photo just sold me straight up.  They look 100x better than I expected.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313633">
        <div id="dsq-comment-header-313633" class="dsq-comment-header">
            <cite id="dsq-cite-313633">
                <span id="dsq-author-user-313633">exel leon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313633" class="dsq-comment-body">
            <div id="dsq-comment-message-313633" class="dsq-comment-message"><p>Sole is horrible </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313639">
        <div id="dsq-comment-header-313639" class="dsq-comment-header">
            <cite id="dsq-cite-313639">
                <span id="dsq-author-user-313639">510kidd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313639" class="dsq-comment-body">
            <div id="dsq-comment-message-313639" class="dsq-comment-message"><p>Yeah that white fucks it all up. Still passing</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313659">
        <div id="dsq-comment-header-313659" class="dsq-comment-header">
            <cite id="dsq-cite-313659">
                <span id="dsq-author-user-313659">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313659" class="dsq-comment-body">
            <div id="dsq-comment-message-313659" class="dsq-comment-message"><p>these are awful get real JB</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313660">
        <div id="dsq-comment-header-313660" class="dsq-comment-header">
            <cite id="dsq-cite-313660">
                <span id="dsq-author-user-313660">Hutch 19D</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313660" class="dsq-comment-body">
            <div id="dsq-comment-message-313660" class="dsq-comment-message"><p>The jumpman looks cheap like it&#8217;ll peel off</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313674">
        <div id="dsq-comment-header-313674" class="dsq-comment-header">
            <cite id="dsq-cite-313674">
                <span id="dsq-author-user-313674">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313674" class="dsq-comment-body">
            <div id="dsq-comment-message-313674" class="dsq-comment-message"><p>not impressed&#8230; release the info for motorsport 4s &amp; raging bulls 5.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-313682">
        <div id="dsq-comment-header-313682" class="dsq-comment-header">
            <cite id="dsq-cite-313682">
                <span id="dsq-author-user-313682">Khalid Perry</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313682" class="dsq-comment-body">
            <div id="dsq-comment-message-313682" class="dsq-comment-message"><p>Raging bull 5s not dropping this year bruh</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-313697">
        <div id="dsq-comment-header-313697" class="dsq-comment-header">
            <cite id="dsq-cite-313697">
                <span id="dsq-author-user-313697">ATL Hawks 2016 NBA Champions</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313697" class="dsq-comment-body">
            <div id="dsq-comment-message-313697" class="dsq-comment-message"><p>How do you know</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-313923">
        <div id="dsq-comment-header-313923" class="dsq-comment-header">
            <cite id="dsq-cite-313923">
                <span id="dsq-author-user-313923">J_Collector</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313923" class="dsq-comment-body">
            <div id="dsq-comment-message-313923" class="dsq-comment-message"><p>motorsport 4&#8217;s are never dropping to the general public those were a F&amp;F release</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-313675">
        <div id="dsq-comment-header-313675" class="dsq-comment-header">
            <cite id="dsq-cite-313675">
                <span id="dsq-author-user-313675">yup</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313675" class="dsq-comment-body">
            <div id="dsq-comment-message-313675" class="dsq-comment-message"><p>They&#8217;re good to go. I&#8217;ll take 3 pairs.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-313679">
        <div id="dsq-comment-header-313679" class="dsq-comment-header">
            <cite id="dsq-cite-313679">
                <span id="dsq-author-user-313679">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313679" class="dsq-comment-body">
            <div id="dsq-comment-message-313679" class="dsq-comment-message"><p>these arent wavy. the leather on the tongue looks dumb and the white bottom is wack</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-313681">
        <div id="dsq-comment-header-313681" class="dsq-comment-header">
            <cite id="dsq-cite-313681">
                <span id="dsq-author-user-313681">Chase Diego</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313681" class="dsq-comment-body">
            <div id="dsq-comment-message-313681" class="dsq-comment-message"><p>Hard af I&#8217;m so glad niggas is sleepin on these.. I&#8217;m boocoo hype for these</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-313690">
        <div id="dsq-comment-header-313690" class="dsq-comment-header">
            <cite id="dsq-cite-313690">
                <span id="dsq-author-user-313690">Johnny Robinson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313690" class="dsq-comment-body">
            <div id="dsq-comment-message-313690" class="dsq-comment-message"><p>Nobody sleeping bruh</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-314659">
        <div id="dsq-comment-header-314659" class="dsq-comment-header">
            <cite id="dsq-cite-314659">
                <span id="dsq-author-user-314659">Michael</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314659" class="dsq-comment-body">
            <div id="dsq-comment-message-314659" class="dsq-comment-message"><p>you&#8217;d be suprised how many dont like em!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-313688">
        <div id="dsq-comment-header-313688" class="dsq-comment-header">
            <cite id="dsq-cite-313688">
                <span id="dsq-author-user-313688">doobie Man</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313688" class="dsq-comment-body">
            <div id="dsq-comment-message-313688" class="dsq-comment-message"><p>These would make a good cup holder.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-313696">
        <div id="dsq-comment-header-313696" class="dsq-comment-header">
            <cite id="dsq-cite-313696">
                <span id="dsq-author-user-313696">ATL Hawks 2016 NBA Champions</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313696" class="dsq-comment-body">
            <div id="dsq-comment-message-313696" class="dsq-comment-message"><p>So they&#8217;re Breds without the red sole</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-313748">
        <div id="dsq-comment-header-313748" class="dsq-comment-header">
            <cite id="dsq-cite-313748">
                <span id="dsq-author-user-313748">skeen</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313748" class="dsq-comment-body">
            <div id="dsq-comment-message-313748" class="dsq-comment-message"><p>No they&#8217;re not&#8230; Reading is fundamental&#8230; the sole will glow in the dark and the upper is leather instead of mesh&#8230; looks nothing like the breads smh</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313706">
        <div id="dsq-comment-header-313706" class="dsq-comment-header">
            <cite id="dsq-cite-313706">
                <span id="dsq-author-user-313706">Kyle</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313706" class="dsq-comment-body">
            <div id="dsq-comment-message-313706" class="dsq-comment-message"><p>Good Coop I&#8217;ll Take Any High Top 11 with great qualify</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313727">
        <div id="dsq-comment-header-313727" class="dsq-comment-header">
            <cite id="dsq-cite-313727">
                <span id="dsq-author-user-313727">beatsbymarcus78</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313727" class="dsq-comment-body">
            <div id="dsq-comment-message-313727" class="dsq-comment-message"><p>i wonder how that sole will look after like a year.<br />
these dope.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313730">
        <div id="dsq-comment-header-313730" class="dsq-comment-header">
            <cite id="dsq-cite-313730">
                <span id="dsq-author-user-313730">InMy02&#8217;s</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313730" class="dsq-comment-body">
            <div id="dsq-comment-message-313730" class="dsq-comment-message"><p>Phuck all the nonsense all I know is that aggressive isn&#8217;t aggressive enough to explain these..</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313734">
        <div id="dsq-comment-header-313734" class="dsq-comment-header">
            <cite id="dsq-cite-313734">
                <span id="dsq-author-user-313734">MC GUSTO</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313734" class="dsq-comment-body">
            <div id="dsq-comment-message-313734" class="dsq-comment-message"><p>These are ass! Easy pass</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313743">
        <div id="dsq-comment-header-313743" class="dsq-comment-header">
            <cite id="dsq-cite-313743">
                <span id="dsq-author-user-313743">willhoops</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313743" class="dsq-comment-body">
            <div id="dsq-comment-message-313743" class="dsq-comment-message"><p>These would of been better as lows</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313753">
        <div id="dsq-comment-header-313753" class="dsq-comment-header">
            <cite id="dsq-cite-313753">
                <span id="dsq-author-user-313753">q</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313753" class="dsq-comment-body">
            <div id="dsq-comment-message-313753" class="dsq-comment-message"><p>These will not glow despite what you read</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313754">
        <div id="dsq-comment-header-313754" class="dsq-comment-header">
            <cite id="dsq-cite-313754">
                <span id="dsq-author-user-313754">guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313754" class="dsq-comment-body">
            <div id="dsq-comment-message-313754" class="dsq-comment-message"><p>Might be the first 11 to not sell out as fast lol. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313760">
        <div id="dsq-comment-header-313760" class="dsq-comment-header">
            <cite id="dsq-cite-313760">
                <span id="dsq-author-user-313760">T$ .</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313760" class="dsq-comment-body">
            <div id="dsq-comment-message-313760" class="dsq-comment-message"><p>I haven&#8217;t got a holiday 11 since the concords and Im loving these </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313764">
        <div id="dsq-comment-header-313764" class="dsq-comment-header">
            <cite id="dsq-cite-313764">
                <span id="dsq-author-user-313764">Reggie Hughes</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313764" class="dsq-comment-body">
            <div id="dsq-comment-message-313764" class="dsq-comment-message"><p>Better than expected, but still passing</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313765">
        <div id="dsq-comment-header-313765" class="dsq-comment-header">
            <cite id="dsq-cite-313765">
                <span id="dsq-author-user-313765">Hutch 19D</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313765" class="dsq-comment-body">
            <div id="dsq-comment-message-313765" class="dsq-comment-message"><p>They need a better design cause these are lacking and with a $220 price tag I&#8217;m unimpressed </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313768">
        <div id="dsq-comment-header-313768" class="dsq-comment-header">
            <cite id="dsq-cite-313768">
                <span id="dsq-author-user-313768">Louisianna Hot Boy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313768" class="dsq-comment-body">
            <div id="dsq-comment-message-313768" class="dsq-comment-message"><p>Cop city allday </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313769">
        <div id="dsq-comment-header-313769" class="dsq-comment-header">
            <cite id="dsq-cite-313769">
                <span id="dsq-author-user-313769">DOPE</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313769" class="dsq-comment-body">
            <div id="dsq-comment-message-313769" class="dsq-comment-message"><p>im in love</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313770">
        <div id="dsq-comment-header-313770" class="dsq-comment-header">
            <cite id="dsq-cite-313770">
                <span id="dsq-author-user-313770">CranMystery</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313770" class="dsq-comment-body">
            <div id="dsq-comment-message-313770" class="dsq-comment-message"><p>once someone paints that midsole black these gonna be fire</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313776">
        <div id="dsq-comment-header-313776" class="dsq-comment-header">
            <cite id="dsq-cite-313776">
                <span id="dsq-author-user-313776">kj osd</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313776" class="dsq-comment-body">
            <div id="dsq-comment-message-313776" class="dsq-comment-message"><p>These look great, I hope I can get a pair.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313790">
        <div id="dsq-comment-header-313790" class="dsq-comment-header">
            <cite id="dsq-cite-313790">
                <span id="dsq-author-user-313790">Poop Johnson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313790" class="dsq-comment-body">
            <div id="dsq-comment-message-313790" class="dsq-comment-message"><p>honestly they r kinda boring&#8230;i like them and i had the urges to get them but you gotta face reality sometime&#8230;my breds r enough :/</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313791">
        <div id="dsq-comment-header-313791" class="dsq-comment-header">
            <cite id="dsq-cite-313791">
                <span id="dsq-author-user-313791">CJWatson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313791" class="dsq-comment-body">
            <div id="dsq-comment-message-313791" class="dsq-comment-message"><p>Loving the leather and the texture changes just want a different colorway with this remastered treatment but oh so.fresh for the 11&#8217;s</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-313792">
        <div id="dsq-comment-header-313792" class="dsq-comment-header">
            <cite id="dsq-cite-313792">
                <span id="dsq-author-user-313792">Juteius Jasper</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313792" class="dsq-comment-body">
            <div id="dsq-comment-message-313792" class="dsq-comment-message"><p>Reminds me much of the Jordan Futures</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-313830">
        <div id="dsq-comment-header-313830" class="dsq-comment-header">
            <cite id="dsq-cite-313830">
                <span id="dsq-author-user-313830">sneakerhead23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313830" class="dsq-comment-body">
            <div id="dsq-comment-message-313830" class="dsq-comment-message"><p>if they got rid of the white bottom, and instead of that cheap looking jump man, make a stitched one like all other 11s, it would look a lot better.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-313938">
        <div id="dsq-comment-header-313938" class="dsq-comment-header">
            <cite id="dsq-cite-313938">
                <span id="dsq-author-user-313938">Will</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313938" class="dsq-comment-body">
            <div id="dsq-comment-message-313938" class="dsq-comment-message"><p>I agree about the stitched jumpmam 100%. I like the white bottoms tho, different. This pair will be my only cop this Holidays Season.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-313924">
        <div id="dsq-comment-header-313924" class="dsq-comment-header">
            <cite id="dsq-cite-313924">
                <span id="dsq-author-user-313924">Juteius Jasper</span>
            </cite>
        </div>
        <div id="dsq-comment-body-313924" class="dsq-comment-body">
            <div id="dsq-comment-message-313924" class="dsq-comment-message"><p>Particularly this 11 though</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314090">
        <div id="dsq-comment-header-314090" class="dsq-comment-header">
            <cite id="dsq-cite-314090">
                <span id="dsq-author-user-314090">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314090" class="dsq-comment-body">
            <div id="dsq-comment-message-314090" class="dsq-comment-message"><p>weak</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-314104">
        <div id="dsq-comment-header-314104" class="dsq-comment-header">
            <cite id="dsq-cite-314104">
                <span id="dsq-author-user-314104">yooo</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314104" class="dsq-comment-body">
            <div id="dsq-comment-message-314104" class="dsq-comment-message"><p>Ay I love the bottoms</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314115">
        <div id="dsq-comment-header-314115" class="dsq-comment-header">
            <cite id="dsq-cite-314115">
                <span id="dsq-author-user-314115">Michael</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314115" class="dsq-comment-body">
            <div id="dsq-comment-message-314115" class="dsq-comment-message"><p>Okay! I can fuck with this i didnt like the other pics they released but these are nice! i wish the jumpman writing on the tounge was there instead of the logo</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-314116">
        <div id="dsq-comment-header-314116" class="dsq-comment-header">
            <cite id="dsq-cite-314116">
                <span id="dsq-author-user-314116">Michael</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314116" class="dsq-comment-body">
            <div id="dsq-comment-message-314116" class="dsq-comment-message"><p>okay i just looked back its there! just not in red</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-314125">
        <div id="dsq-comment-header-314125" class="dsq-comment-header">
            <cite id="dsq-cite-314125">
                <span id="dsq-author-user-314125">Julio Guerra</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314125" class="dsq-comment-body">
            <div id="dsq-comment-message-314125" class="dsq-comment-message"><p>The white bottom like that make them look like boots lbs. None the less they some heat for the winter.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-314205">
        <div id="dsq-comment-header-314205" class="dsq-comment-header">
            <cite id="dsq-cite-314205">
                <span id="dsq-author-user-314205">BensonStanley88</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314205" class="dsq-comment-body">
            <div id="dsq-comment-message-314205" class="dsq-comment-message"><p>Quality looks exelent. For me they&#8217;d be perfect with a transparent charcoal sole.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-314372">
        <div id="dsq-comment-header-314372" class="dsq-comment-header">
            <cite id="dsq-cite-314372">
                <span id="dsq-author-user-314372">Michael</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314372" class="dsq-comment-body">
            <div id="dsq-comment-message-314372" class="dsq-comment-message"><p>These are not the final version, and also these will be in a pack like the pantone of last year</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-314624">
        <div id="dsq-comment-header-314624" class="dsq-comment-header">
            <cite id="dsq-cite-314624">
                <span id="dsq-author-user-314624">Prince Akeem</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314624" class="dsq-comment-body">
            <div id="dsq-comment-message-314624" class="dsq-comment-message"><p>Probably a stand alone like the Columbias of last year.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314461">
        <div id="dsq-comment-header-314461" class="dsq-comment-header">
            <cite id="dsq-cite-314461">
                <span id="dsq-author-user-314461">KJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314461" class="dsq-comment-body">
            <div id="dsq-comment-message-314461" class="dsq-comment-message"><p>sole collector officially said that the Melo PE 11s will be dropping on December 1st. They do not have a picture on the website. But if you look the image up, best 1s of all time hands down.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-314656">
        <div id="dsq-comment-header-314656" class="dsq-comment-header">
            <cite id="dsq-cite-314656">
                <span id="dsq-author-user-314656">Ezy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314656" class="dsq-comment-body">
            <div id="dsq-comment-message-314656" class="dsq-comment-message"><p>wait on it. if they were releasing they would be all over social media. everyone would have that same info by now.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-314462">
        <div id="dsq-comment-header-314462" class="dsq-comment-header">
            <cite id="dsq-cite-314462">
                <span id="dsq-author-user-314462">KJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314462" class="dsq-comment-body">
            <div id="dsq-comment-message-314462" class="dsq-comment-message"><p>Sole collector just announced the Melo PE 11s will drop in December. Best 11 ever hands down when you see the image.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-314614">
        <div id="dsq-comment-header-314614" class="dsq-comment-header">
            <cite id="dsq-cite-314614">
                <span id="dsq-author-user-314614">Smee</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314614" class="dsq-comment-body">
            <div id="dsq-comment-message-314614" class="dsq-comment-message"><p>yeap, i&#8217;ll pay 250 for those all day&#8230;fire right there.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-314616">
        <div id="dsq-comment-header-314616" class="dsq-comment-header">
            <cite id="dsq-cite-314616">
                <span id="dsq-author-user-314616">E</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314616" class="dsq-comment-body">
            <div id="dsq-comment-message-314616" class="dsq-comment-message"><p>But you know they gonna throw it in a pack with the air jordan 30 got a feeling so won&#8217;t pay 250 more like 500 again  </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-314623">
        <div id="dsq-comment-header-314623" class="dsq-comment-header">
            <cite id="dsq-cite-314623">
                <span id="dsq-author-user-314623">Prince Akeem</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314623" class="dsq-comment-body">
            <div id="dsq-comment-message-314623" class="dsq-comment-message"><p>Why are you fucking with us ?</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314635">
        <div id="dsq-comment-header-314635" class="dsq-comment-header">
            <cite id="dsq-cite-314635">
                <span id="dsq-author-user-314635">exel leon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314635" class="dsq-comment-body">
            <div id="dsq-comment-message-314635" class="dsq-comment-message"><p>They look better off feet</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-314662">
        <div id="dsq-comment-header-314662" class="dsq-comment-header">
            <cite id="dsq-cite-314662">
                <span id="dsq-author-user-314662">StephanieCCarr</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314662" class="dsq-comment-body">
            <div id="dsq-comment-message-314662" class="dsq-comment-message"><p>dsddd</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314671">
        <div id="dsq-comment-header-314671" class="dsq-comment-header">
            <cite id="dsq-cite-314671">
                <span id="dsq-author-user-314671">??</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314671" class="dsq-comment-body">
            <div id="dsq-comment-message-314671" class="dsq-comment-message"><p>So do the soles Glow in the dark or not???</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-314675">
        <div id="dsq-comment-header-314675" class="dsq-comment-header">
            <cite id="dsq-cite-314675">
                <span id="dsq-author-user-314675">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314675" class="dsq-comment-body">
            <div id="dsq-comment-message-314675" class="dsq-comment-message"><p>they look even dumber on feet</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314696">
        <div id="dsq-comment-header-314696" class="dsq-comment-header">
            <cite id="dsq-cite-314696">
                <span id="dsq-author-user-314696">tirevaro</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314696" class="dsq-comment-body">
            <div id="dsq-comment-message-314696" class="dsq-comment-message"><p>dfsdfsdf</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-314697">
        <div id="dsq-comment-header-314697" class="dsq-comment-header">
            <cite id="dsq-cite-314697">
                <span id="dsq-author-user-314697">guest 23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314697" class="dsq-comment-body">
            <div id="dsq-comment-message-314697" class="dsq-comment-message"><p>They look like boots . No actual creativity . I never thought JB could actually mess up a pair of 11s. I personally would along with the whole world would like to see them in person cause these pics make them look crazy ugly. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314702">
        <div id="dsq-comment-header-314702" class="dsq-comment-header">
            <cite id="dsq-cite-314702">
                <span id="dsq-author-user-314702">ShoeEtiquette</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314702" class="dsq-comment-body">
            <div id="dsq-comment-message-314702" class="dsq-comment-message"><p>I love 11&#8217;s like the majority of the sneaker community but im having a hard time feeling this particular pair ever since i seen pics, i understand the theme and all that but the leather upper is not looking all that flattering on this shoe imo, or maybe its the glittery patent leather and the iron on patch like jumpman smh shit just looks like a cheap knock-off version to me. Where is the quality jb? Now i do love the true icey sole if jb can use it on this pair just go head and put it on all the shoes that sport icey soles im sure most heads would definitely be happier. Yes i understand why the icey blue sole is being use i just dont care give me the crystal clear ice. The 11 is perfect in ever way why go and mess it up smh</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-314859">
        <div id="dsq-comment-header-314859" class="dsq-comment-header">
            <cite id="dsq-cite-314859">
                <span id="dsq-author-user-314859">T Bon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314859" class="dsq-comment-body">
            <div id="dsq-comment-message-314859" class="dsq-comment-message"><p>The sole is the reason I hate these muthafuckas. &#8230; Why couldn&#8217;t they have a black sole like the Gammas???</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-314878">
        <div id="dsq-comment-header-314878" class="dsq-comment-header">
            <cite id="dsq-cite-314878">
                <span id="dsq-author-user-314878">jjamesonthekid</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314878" class="dsq-comment-body">
            <div id="dsq-comment-message-314878" class="dsq-comment-message"><p>Gammas had a gamma blue outsole though&#8230;</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-314912">
        <div id="dsq-comment-header-314912" class="dsq-comment-header">
            <cite id="dsq-cite-314912">
                <span id="dsq-author-user-314912">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314912" class="dsq-comment-body">
            <div id="dsq-comment-message-314912" class="dsq-comment-message"><p>Not in this dude&#8217;s world. smfh</p>
<p>&#8230;and again, someone wants these to be more like a recent XI&#8230;.that was pleather and looks like shit after 1 wear. These motherfuckers are so used to buying garbage they can&#8217;t see something decent when it&#8217;s put in front of them&#8230;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-4" id="dsq-comment-314935">
        <div id="dsq-comment-header-314935" class="dsq-comment-header">
            <cite id="dsq-cite-314935">
                <span id="dsq-author-user-314935">T Bon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314935" class="dsq-comment-body">
            <div id="dsq-comment-message-314935" class="dsq-comment-message"><p>Yeah I mean the black mid sole</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-4" id="dsq-comment-314936">
        <div id="dsq-comment-header-314936" class="dsq-comment-header">
            <cite id="dsq-cite-314936">
                <span id="dsq-author-user-314936">T Bon</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314936" class="dsq-comment-body">
            <div id="dsq-comment-message-314936" class="dsq-comment-message"><p>Yeah I mean the black mid sole</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-314911">
        <div id="dsq-comment-header-314911" class="dsq-comment-header">
            <cite id="dsq-cite-314911">
                <span id="dsq-author-user-314911">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314911" class="dsq-comment-body">
            <div id="dsq-comment-message-314911" class="dsq-comment-message"><p>You look at this shoe and say &#8220;where is the quality at, JB&#8221;?</p>
<p>Have you not seen pretty much any Jordan XI that has released in the past 5 years or more? THOSE are garbage. This is the first XI in eons to NOT be shit in terms of quality. Seriously, you don&#8217;t have to like the shoe. You can hate them if you want, even. I couldn&#8217;t care less either way. &#8230;but to look at these and say &#8220;Where is the quality?&#8221; just makes you look stupid as fuck. </p>
<p>Bring this man some plastic Legend Blue XI&#8217;s. lol&#8230;</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-314945">
        <div id="dsq-comment-header-314945" class="dsq-comment-header">
            <cite id="dsq-cite-314945">
                <span id="dsq-author-user-314945">ShoeEtiquette</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314945" class="dsq-comment-body">
            <div id="dsq-comment-message-314945" class="dsq-comment-message"><p>Dizzle 1119 or whatever the fuck your calling yourself at this present moment in time ima let you make it my man, but the next time you feel like you got something derogatory to say about what the fuck i comment on when it come to these or any shoe that your in love with or hyped up to purchase ima embarrass you homie, See bruh you just a consumer like the rest of us on this site your nobody special your no authority on kicks dont fool yourself fool cus thats just foolish for a grown man to think that way. See i peeps all the lil slick shit you be talking to everyone when they reply something contray to your beliefs system but look a here money the next time you feel compelled to try and belittle me with any type of ignorance falling out of your pussy ima stick my dick, nuts plus my foot and whole entire leg in yo shit, im the wrong one dude keep it 100 and keep it moving you dont want these troubles in paradise cus thats just gonna &#8220;make you look stupid as fuck&#8221; you really should mind your manners. Now can somebody please go pull that plastic dildo out of Mr. Dizzle 1119 ass, i would do it myself but i dont fuck with punks lol. Naw but seriously relax dizzle don&#8217;t get your panties all uptight bout these lil shoes that most of us find so insignificant in the grand scheme of life, but i understand if you need them to validate your worth in this world.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-4" id="dsq-comment-315389">
        <div id="dsq-comment-header-315389" class="dsq-comment-header">
            <cite id="dsq-cite-315389">
                <span id="dsq-author-user-315389">shadowhawk</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315389" class="dsq-comment-body">
            <div id="dsq-comment-message-315389" class="dsq-comment-message"><p>Dang bro, Dizzle1119 will probably never post a bogus or any comment after what you said hommie</p>
<p>i aint gonna lie that was one of the most serious, real, but funny AF</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-5" id="dsq-comment-315407">
        <div id="dsq-comment-header-315407" class="dsq-comment-header">
            <cite id="dsq-cite-315407">
                <span id="dsq-author-user-315407">ShoeEtiquette</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315407" class="dsq-comment-body">
            <div id="dsq-comment-message-315407" class="dsq-comment-message"><p>I ain&#8217;t really mean to get into him like that bruh ima peaceful dude most of the time but when you step on these mean streets popping that  reckless shit with total disrespect outcha face we gone have a ugly confrontational clash. Im justa opinionated lover of kicks like the majority but ima fully grown man, dont talk down to me i aint&#8217;cha bitch or your seed, if he didn&#8217;t know before he know now</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-314715">
        <div id="dsq-comment-header-314715" class="dsq-comment-header">
            <cite id="dsq-cite-314715">
                <span id="dsq-author-user-314715">Ry</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314715" class="dsq-comment-body">
            <div id="dsq-comment-message-314715" class="dsq-comment-message"><p>These are fire! I like these &amp; the space jams honestly. I&#8217;d like to have em both.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-314748">
        <div id="dsq-comment-header-314748" class="dsq-comment-header">
            <cite id="dsq-cite-314748">
                <span id="dsq-author-user-314748">InMy02&#8217;s</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314748" class="dsq-comment-body">
            <div id="dsq-comment-message-314748" class="dsq-comment-message"><p>On feet didn&#8217;t do these no justice smh</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-314764">
        <div id="dsq-comment-header-314764" class="dsq-comment-header">
            <cite id="dsq-cite-314764">
                <span id="dsq-author-user-314764">Josh</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314764" class="dsq-comment-body">
            <div id="dsq-comment-message-314764" class="dsq-comment-message"><p>Only thing that could make these look worse would be to wear them wit some gay-ass camos.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-314807">
        <div id="dsq-comment-header-314807" class="dsq-comment-header">
            <cite id="dsq-cite-314807">
                <span id="dsq-author-user-314807">face</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314807" class="dsq-comment-body">
            <div id="dsq-comment-message-314807" class="dsq-comment-message"><p>exactly camo don&#8217;t go wit everything I&#8217;m tired of seeing hype beast rock j&#8217;s and  camos smfh don&#8217;t even match</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314782">
        <div id="dsq-comment-header-314782" class="dsq-comment-header">
            <cite id="dsq-cite-314782">
                <span id="dsq-author-user-314782">LookalikeSNIPES</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314782" class="dsq-comment-body">
            <div id="dsq-comment-message-314782" class="dsq-comment-message"><p>This looks more like it could be a DB 11</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-314837">
        <div id="dsq-comment-header-314837" class="dsq-comment-header">
            <cite id="dsq-cite-314837">
                <span id="dsq-author-user-314837">Louisianna Hot Boy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314837" class="dsq-comment-body">
            <div id="dsq-comment-message-314837" class="dsq-comment-message"><p>Hype Hype And More Hype it&#8217;s only June </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314838">
        <div id="dsq-comment-header-314838" class="dsq-comment-header">
            <cite id="dsq-cite-314838">
                <span id="dsq-author-user-314838">Casey</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314838" class="dsq-comment-body">
            <div id="dsq-comment-message-314838" class="dsq-comment-message"><p>i dont think they are gonna put the tag on the tongue on the shoe with &#8220;the greatest to play the game&#8221; cause of the stupid person that says someone else is better</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-314849">
        <div id="dsq-comment-header-314849" class="dsq-comment-header">
            <cite id="dsq-cite-314849">
                <span id="dsq-author-user-314849">Big Papi</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314849" class="dsq-comment-body">
            <div id="dsq-comment-message-314849" class="dsq-comment-message"><p>Damn if only the breds came in that sexy box</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-314909">
        <div id="dsq-comment-header-314909" class="dsq-comment-header">
            <cite id="dsq-cite-314909">
                <span id="dsq-author-user-314909">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-314909" class="dsq-comment-body">
            <div id="dsq-comment-message-314909" class="dsq-comment-message"><p>need a black midsole.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-315026">
        <div id="dsq-comment-header-315026" class="dsq-comment-header">
            <cite id="dsq-cite-315026">
                <span id="dsq-author-user-315026">shadowhawk</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315026" class="dsq-comment-body">
            <div id="dsq-comment-message-315026" class="dsq-comment-message"><p>I like but i think they should have came out with the sample versions that looked like the space jams but with the all red accents</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-315027">
        <div id="dsq-comment-header-315027" class="dsq-comment-header">
            <cite id="dsq-cite-315027">
                <span id="dsq-author-user-315027">shadowhawk</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315027" class="dsq-comment-body">
            <div id="dsq-comment-message-315027" class="dsq-comment-message"><p>defintly a cop because i always wanted some 11s but never had none. sad story yeah i know.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-315052">
        <div id="dsq-comment-header-315052" class="dsq-comment-header">
            <cite id="dsq-cite-315052">
                <span id="dsq-author-user-315052">Guy</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315052" class="dsq-comment-body">
            <div id="dsq-comment-message-315052" class="dsq-comment-message"><p>I don&#8217;t think it&#8217;s sad my friend!! I&#8217;ve never had a pair myself, but These will be on my list come December!!!!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-315390">
        <div id="dsq-comment-header-315390" class="dsq-comment-header">
            <cite id="dsq-cite-315390">
                <span id="dsq-author-user-315390">shadowhawk</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315390" class="dsq-comment-body">
            <div id="dsq-comment-message-315390" class="dsq-comment-message"><p>yeah totally but i might be gettings some breds pretty soon any way</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-315149">
        <div id="dsq-comment-header-315149" class="dsq-comment-header">
            <cite id="dsq-cite-315149">
                <span id="dsq-author-user-315149">Nite</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315149" class="dsq-comment-body">
            <div id="dsq-comment-message-315149" class="dsq-comment-message"><p>same thing I just need some 11s</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315112">
        <div id="dsq-comment-header-315112" class="dsq-comment-header">
            <cite id="dsq-cite-315112">
                <span id="dsq-author-user-315112">Unknown123</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315112" class="dsq-comment-body">
            <div id="dsq-comment-message-315112" class="dsq-comment-message"><p>These are hype </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315113">
        <div id="dsq-comment-header-315113" class="dsq-comment-header">
            <cite id="dsq-cite-315113">
                <span id="dsq-author-user-315113">Unknown123</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315113" class="dsq-comment-body">
            <div id="dsq-comment-message-315113" class="dsq-comment-message"><p>I hate when people act like they have big ass nuts but they dont sum dude  up here cursing peple out he need to shut the fuck up </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315208">
        <div id="dsq-comment-header-315208" class="dsq-comment-header">
            <cite id="dsq-cite-315208">
                <span id="dsq-author-user-315208">Chase Tompkins</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315208" class="dsq-comment-body">
            <div id="dsq-comment-message-315208" class="dsq-comment-message"><p>Man, I hate how the overlay with the 23 and the red jordan is now a suede texture instead of a mesh fabric, look how much hair gets stuck to it! </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-315320">
        <div id="dsq-comment-header-315320" class="dsq-comment-header">
            <cite id="dsq-cite-315320">
                <span id="dsq-author-user-315320">11 72-10</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315320" class="dsq-comment-body">
            <div id="dsq-comment-message-315320" class="dsq-comment-message"><p>Dude don&#8217;t be OCD, pick the fucking hair off then</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-315391">
        <div id="dsq-comment-header-315391" class="dsq-comment-header">
            <cite id="dsq-cite-315391">
                <span id="dsq-author-user-315391">shadowhawk</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315391" class="dsq-comment-body">
            <div id="dsq-comment-message-315391" class="dsq-comment-message"><p>yeah u better brings some tape or lint roller, hell thats what ima do</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315209">
        <div id="dsq-comment-header-315209" class="dsq-comment-header">
            <cite id="dsq-cite-315209">
                <span id="dsq-author-user-315209">Lomebot</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315209" class="dsq-comment-body">
            <div id="dsq-comment-message-315209" class="dsq-comment-message"><p>Sharpie Penny 6 from the Foamposite Sharpie pack<br />
$290 shipped</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-315213">
        <div id="dsq-comment-header-315213" class="dsq-comment-header">
            <cite id="dsq-cite-315213">
                <span id="dsq-author-user-315213">Leirda</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315213" class="dsq-comment-body">
            <div id="dsq-comment-message-315213" class="dsq-comment-message"><p>What size I&#8217;ll give you that if it is my size email me</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-3" id="dsq-comment-315218">
        <div id="dsq-comment-header-315218" class="dsq-comment-header">
            <cite id="dsq-cite-315218">
                <span id="dsq-author-user-315218">Leirda</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315218" class="dsq-comment-body">
            <div id="dsq-comment-message-315218" class="dsq-comment-message"><p>Never mind</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315259">
        <div id="dsq-comment-header-315259" class="dsq-comment-header">
            <cite id="dsq-cite-315259">
                <span id="dsq-author-user-315259">Lomebot</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315259" class="dsq-comment-body">
            <div id="dsq-comment-message-315259" class="dsq-comment-message"><p>Sharpie Penny 6 from the Foamposite Sharpie pack<br />
size 10 $290 shipped</p>
<p>What the KD 7 $250 shipped</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-316059">
        <div id="dsq-comment-header-316059" class="dsq-comment-header">
            <cite id="dsq-cite-316059">
                <span id="dsq-author-user-316059">RockawayBully</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316059" class="dsq-comment-body">
            <div id="dsq-comment-message-316059" class="dsq-comment-message"><p>Ha ha $290 for penny 6&#8217;s smh they just sitting in my mall and I Live in ny,good luck man but those penny 6  are a hard sell even if it was at $90 you might have a chance,just chalk it up as a lost and just realize you brought a pair of foams for $500 plus,don&#8217;t worry we all make these bad  decisions,I myself had brought many crappy Jordan packs with shitty seconds smh,but I didn&#8217;t bite on the last few like the Brazil,year of the dragon,but I was close to getting the Pantone and the MTM pack but last min decided no to much money for one shoe you like and another pair you have no intention of wearing,sucks I know.but I can see those KD&#8217;s going quick.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-316143">
        <div id="dsq-comment-header-316143" class="dsq-comment-header">
            <cite id="dsq-cite-316143">
                <span id="dsq-author-user-316143">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316143" class="dsq-comment-body">
            <div id="dsq-comment-message-316143" class="dsq-comment-message"><p>$300 for some Penny VI&#8217;s.</p>
<p>You crazy&#8230;or just stupid?</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315263">
        <div id="dsq-comment-header-315263" class="dsq-comment-header">
            <cite id="dsq-cite-315263">
                <span id="dsq-author-user-315263">Casey</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315263" class="dsq-comment-body">
            <div id="dsq-comment-message-315263" class="dsq-comment-message"><p>melos are better</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315283">
        <div id="dsq-comment-header-315283" class="dsq-comment-header">
            <cite id="dsq-cite-315283">
                <span id="dsq-author-user-315283">W_dubb</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315283" class="dsq-comment-body">
            <div id="dsq-comment-message-315283" class="dsq-comment-message"><p>the more i see these the more i hate them</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315392">
        <div id="dsq-comment-header-315392" class="dsq-comment-header">
            <cite id="dsq-cite-315392">
                <span id="dsq-author-user-315392">shadowhawk</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315392" class="dsq-comment-body">
            <div id="dsq-comment-message-315392" class="dsq-comment-message"><p>they look kinda like a &#8220;Fear Pack&#8221; part to but thats my opinion</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315522">
        <div id="dsq-comment-header-315522" class="dsq-comment-header">
            <cite id="dsq-cite-315522">
                <span id="dsq-author-user-315522">11 ID please!!!</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315522" class="dsq-comment-body">
            <div id="dsq-comment-message-315522" class="dsq-comment-message"><p>I know loads of people have said this on different sneaker websites and I 100% agree with them. Instead of all this rubbish they should have let us ID the 11 just for Christmas and if your ASS isn&#8217;t fast enough then you don&#8217;t get!!!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-315633">
        <div id="dsq-comment-header-315633" class="dsq-comment-header">
            <cite id="dsq-cite-315633">
                <span id="dsq-author-user-315633">Prince Akeem</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315633" class="dsq-comment-body">
            <div id="dsq-comment-message-315633" class="dsq-comment-message"><p>Good lord, ID the 11s ?  What the hell is wrong with you ?  How old are you kid ?  That&#8217;s just wrong on so many levels.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-315636">
        <div id="dsq-comment-header-315636" class="dsq-comment-header">
            <cite id="dsq-cite-315636">
                <span id="dsq-author-user-315636">DrakeDaSavage</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315636" class="dsq-comment-body">
            <div id="dsq-comment-message-315636" class="dsq-comment-message"><p>I flagged that nigga. He is a maniac</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-315637">
        <div id="dsq-comment-header-315637" class="dsq-comment-header">
            <cite id="dsq-cite-315637">
                <span id="dsq-author-user-315637">Prince Akeem</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315637" class="dsq-comment-body">
            <div id="dsq-comment-message-315637" class="dsq-comment-message"><p>lmao&#8230;..he&#8217;s one of those posers.  I see them everywhere.  Id the 11&#8217;s ?  What an embarrassment to sneaker culture.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-5" id="dsq-comment-315673">
        <div id="dsq-comment-header-315673" class="dsq-comment-header">
            <cite id="dsq-cite-315673">
                <span id="dsq-author-user-315673">Lol</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315673" class="dsq-comment-body">
            <div id="dsq-comment-message-315673" class="dsq-comment-message"><p>And as for you nigga, take DrakeDaSavages DICK outta your ASS and MOUTH for one second!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-5" id="dsq-comment-315694">
        <div id="dsq-comment-header-315694" class="dsq-comment-header">
            <cite id="dsq-cite-315694">
                <span id="dsq-author-user-315694">Prince Akeem</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315694" class="dsq-comment-body">
            <div id="dsq-comment-message-315694" class="dsq-comment-message"><p>Well that doesn&#8217;t make any sense&#8230;..he agreed with me..</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-4" id="dsq-comment-315672">
        <div id="dsq-comment-header-315672" class="dsq-comment-header">
            <cite id="dsq-cite-315672">
                <span id="dsq-author-user-315672">Lol</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315672" class="dsq-comment-body">
            <div id="dsq-comment-message-315672" class="dsq-comment-message"><p>Nigga don&#8217;t be silly now, if Nike put these on ID your DUMBASS would be one of the first ones on there!!! I mean you&#8217;ve got MJ&#8217;s DICK  in your mouth most of the time,  why wouldn&#8217;t your ASS want a pair??</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315596">
        <div id="dsq-comment-header-315596" class="dsq-comment-header">
            <cite id="dsq-cite-315596">
                <span id="dsq-author-user-315596">3847LawrenceAv</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315596" class="dsq-comment-body">
            <div id="dsq-comment-message-315596" class="dsq-comment-message"><p>I only want these so I can have a legit 11 box for my breds since they&#8217;re still ds</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315641">
        <div id="dsq-comment-header-315641" class="dsq-comment-header">
            <cite id="dsq-cite-315641">
                <span id="dsq-author-user-315641">sneakerhead23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315641" class="dsq-comment-body">
            <div id="dsq-comment-message-315641" class="dsq-comment-message"><p>WHERE CAN I GET THOSE PANTS AT THO?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315646">
        <div id="dsq-comment-header-315646" class="dsq-comment-header">
            <cite id="dsq-cite-315646">
                <span id="dsq-author-user-315646">OnlyInNY</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315646" class="dsq-comment-body">
            <div id="dsq-comment-message-315646" class="dsq-comment-message"><p>Is that a clear sole I see? Oh yes!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315647">
        <div id="dsq-comment-header-315647" class="dsq-comment-header">
            <cite id="dsq-cite-315647">
                <span id="dsq-author-user-315647">CutTheShit</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315647" class="dsq-comment-body">
            <div id="dsq-comment-message-315647" class="dsq-comment-message"><p>Most hyped up shoe of all time good god stop posting pics. We get it!!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315664">
        <div id="dsq-comment-header-315664" class="dsq-comment-header">
            <cite id="dsq-cite-315664">
                <span id="dsq-author-user-315664">1 KING</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315664" class="dsq-comment-body">
            <div id="dsq-comment-message-315664" class="dsq-comment-message"><p>I am going to hate this shoe come December.I wonder if Mario will say, &#8220;hold up, let&#8217;s let posting this shoe rest for a while&#8221; instead of every damn week until release day. </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-316084">
        <div id="dsq-comment-header-316084" class="dsq-comment-header">
            <cite id="dsq-cite-316084">
                <span id="dsq-author-user-316084">High_Influence</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316084" class="dsq-comment-body">
            <div id="dsq-comment-message-316084" class="dsq-comment-message"><p>AMEN, like holy shit </p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-315668">
        <div id="dsq-comment-header-315668" class="dsq-comment-header">
            <cite id="dsq-cite-315668">
http://instagram.com/the_true_great1                <span id="dsq-author-user-315668">Mr.Amazing</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315668" class="dsq-comment-body">
            <div id="dsq-comment-message-315668" class="dsq-comment-message"><p>I want to see them in person.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-315681">
        <div id="dsq-comment-header-315681" class="dsq-comment-header">
            <cite id="dsq-cite-315681">
                <span id="dsq-author-user-315681">Benny Bills</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315681" class="dsq-comment-body">
            <div id="dsq-comment-message-315681" class="dsq-comment-message"><p>these are str8 gaaaahbage!  the kiddies in the hood and fat joe will go crazy for em tho</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-315684">
        <div id="dsq-comment-header-315684" class="dsq-comment-header">
            <cite id="dsq-cite-315684">
                <span id="dsq-author-user-315684">Bruh</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315684" class="dsq-comment-body">
            <div id="dsq-comment-message-315684" class="dsq-comment-message"><p>CP3 11s &gt;&gt;&gt;&gt;&gt;&gt;&gt; this trash</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-315685">
        <div id="dsq-comment-header-315685" class="dsq-comment-header">
            <cite id="dsq-cite-315685">
                <span id="dsq-author-user-315685">no creativity on these 11s</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315685" class="dsq-comment-body">
            <div id="dsq-comment-message-315685" class="dsq-comment-message"><p>These not all that . They lack creativity they look like the breds which are in fact the real 72-10 smdh . I&#8217;m glad I still have a DS pair of the gammas . I&#8217;m more happy about that then these.  Last Yeats legend blue wasn&#8217;t all that ether . What&#8217;s next year a pair of concords with a red bottom or a all white bottom ? </p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-316214">
        <div id="dsq-comment-header-316214" class="dsq-comment-header">
            <cite id="dsq-cite-316214">
                <span id="dsq-author-user-316214">Robert Cardwell</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316214" class="dsq-comment-body">
            <div id="dsq-comment-message-316214" class="dsq-comment-message"><p>Its just commemoration, why hate?</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-315721">
        <div id="dsq-comment-header-315721" class="dsq-comment-header">
            <cite id="dsq-cite-315721">
                <span id="dsq-author-user-315721">space jam #23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315721" class="dsq-comment-body">
            <div id="dsq-comment-message-315721" class="dsq-comment-message"><p>Can&#8217;t wait for these to come out !! #excited</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-315758">
        <div id="dsq-comment-header-315758" class="dsq-comment-header">
            <cite id="dsq-cite-315758">
                <span id="dsq-author-user-315758">Beatriz Serrano</span>
            </cite>
        </div>
        <div id="dsq-comment-body-315758" class="dsq-comment-body">
            <div id="dsq-comment-message-315758" class="dsq-comment-message"><p>Are they going to come in youth sizes ? </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-316078">
        <div id="dsq-comment-header-316078" class="dsq-comment-header">
            <cite id="dsq-cite-316078">
                <span id="dsq-author-user-316078">OG_KiCKS_NJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316078" class="dsq-comment-body">
            <div id="dsq-comment-message-316078" class="dsq-comment-message"><p>Hopefully not, let men have these. Give us youth sizes something better for that damn $150 price increase this December</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-316166">
        <div id="dsq-comment-header-316166" class="dsq-comment-header">
            <cite id="dsq-cite-316166">
                <span id="dsq-author-user-316166">BeRealHomie</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316166" class="dsq-comment-body">
            <div id="dsq-comment-message-316166" class="dsq-comment-message"><p>which 11 hasnt?</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316049">
        <div id="dsq-comment-header-316049" class="dsq-comment-header">
            <cite id="dsq-cite-316049">
                <span id="dsq-author-user-316049">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316049" class="dsq-comment-body">
            <div id="dsq-comment-message-316049" class="dsq-comment-message"><p>I still don&#8217;t like these, but everyone has different tastes.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-316051">
        <div id="dsq-comment-header-316051" class="dsq-comment-header">
            <cite id="dsq-cite-316051">
                <span id="dsq-author-user-316051">Kicks Giggles</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316051" class="dsq-comment-body">
            <div id="dsq-comment-message-316051" class="dsq-comment-message"><p>they still aint nothing hot to me they just lack umph for me but maybe ill cop for collection sake</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316087">
        <div id="dsq-comment-header-316087" class="dsq-comment-header">
            <cite id="dsq-cite-316087">
                <span id="dsq-author-user-316087">Sneakerkenny</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316087" class="dsq-comment-body">
            <div id="dsq-comment-message-316087" class="dsq-comment-message"><p>Anyone know where there&#8217;s not Gona be raffles at ? GS sizes</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-316093">
        <div id="dsq-comment-header-316093" class="dsq-comment-header">
            <cite id="dsq-cite-316093">
                <span id="dsq-author-user-316093">b thumper</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316093" class="dsq-comment-body">
            <div id="dsq-comment-message-316093" class="dsq-comment-message"><p>Its like someone had a really good idea, then just said fuck this when they got to the sole. Just looks unfinished. Loved the ballistic mesh on the ogs, but the leather could be sweet. Just the damn sole. Shiulda been murdered out, straight black with red jump man. Black translucent sole on black midsole. That woulda been dope with the black leather and made the irredescent patent leather pop. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316102">
        <div id="dsq-comment-header-316102" class="dsq-comment-header">
            <cite id="dsq-cite-316102">
                <span id="dsq-author-user-316102">LookalikeSNIPES</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316102" class="dsq-comment-body">
            <div id="dsq-comment-message-316102" class="dsq-comment-message"><p>These are okay depending on the lighting </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-316139">
        <div id="dsq-comment-header-316139" class="dsq-comment-header">
            <cite id="dsq-cite-316139">
                <span id="dsq-author-user-316139">Fksd Hdekn</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316139" class="dsq-comment-body">
            <div id="dsq-comment-message-316139" class="dsq-comment-message"><p>You guys will like them better on feet. I know i will if im able to cop. Hopefully. I dont love them. But the pics they took sold me. So hopefully i get them. </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316142">
        <div id="dsq-comment-header-316142" class="dsq-comment-header">
            <cite id="dsq-cite-316142">
                <span id="dsq-author-user-316142">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316142" class="dsq-comment-body">
            <div id="dsq-comment-message-316142" class="dsq-comment-message"><p>everyone saying they&#8217;re garbage&#8230;.but there will be riots en route to these joints selling out in seconds everywhere.</p>
<p>same shit&#8230;.different day.</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-316146">
        <div id="dsq-comment-header-316146" class="dsq-comment-header">
            <cite id="dsq-cite-316146">
                <span id="dsq-author-user-316146">dizzle1119</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316146" class="dsq-comment-body">
            <div id="dsq-comment-message-316146" class="dsq-comment-message"><p>&#8220;I thought these were fucking garbage at first, but now that my facebook got 845,000 pics from ppl who copped I need to hit up ebay and drop $450 on these joints. They growin&#8217; on me&#8221;</p>
<p>&#8211; about 1,000,000 &#8220;sneakerheads&#8221; come December 20th 2015.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-316169">
        <div id="dsq-comment-header-316169" class="dsq-comment-header">
            <cite id="dsq-cite-316169">
http://www.SneakerAutoBot.com/                <span id="dsq-author-user-316169">SneakerAutoBot.com</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316169" class="dsq-comment-body">
            <div id="dsq-comment-message-316169" class="dsq-comment-message"><p>The box/packaging that should&#8217;ve been for the Breds&#8230;</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-316213">
        <div id="dsq-comment-header-316213" class="dsq-comment-header">
            <cite id="dsq-cite-316213">
                <span id="dsq-author-user-316213">Robert Cardwell</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316213" class="dsq-comment-body">
            <div id="dsq-comment-message-316213" class="dsq-comment-message"><p>Does it really matter what the packaging is? In the end, we want the shoe</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-316217">
        <div id="dsq-comment-header-316217" class="dsq-comment-header">
            <cite id="dsq-cite-316217">
http://www.SneakerAutoBot.com/                <span id="dsq-author-user-316217">SneakerAutoBot.com</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316217" class="dsq-comment-body">
            <div id="dsq-comment-message-316217" class="dsq-comment-message"><p>&#8230;&#8230;&#8230;&#8230;..The title of the page is &#8220;Air Jordan 11 &#8217;72-10&#8242; Packaging&#8221;</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-4" id="dsq-comment-316218">
        <div id="dsq-comment-header-316218" class="dsq-comment-header">
            <cite id="dsq-cite-316218">
                <span id="dsq-author-user-316218">Robert Cardwell</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316218" class="dsq-comment-body">
            <div id="dsq-comment-message-316218" class="dsq-comment-message"><p>I mean in the overall scheme of things, the shoe is what matters, not the packaging</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even depth-2" id="dsq-comment-316233">
        <div id="dsq-comment-header-316233" class="dsq-comment-header">
            <cite id="dsq-cite-316233">
                <span id="dsq-author-user-316233">JordanFan&#8217;84</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316233" class="dsq-comment-body">
            <div id="dsq-comment-message-316233" class="dsq-comment-message"><p>I totally agree with you</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-316212">
        <div id="dsq-comment-header-316212" class="dsq-comment-header">
            <cite id="dsq-cite-316212">
                <span id="dsq-author-user-316212">Robert Cardwell</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316212" class="dsq-comment-body">
            <div id="dsq-comment-message-316212" class="dsq-comment-message"><p>These are lookin good&#8230;</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-316231">
        <div id="dsq-comment-header-316231" class="dsq-comment-header">
            <cite id="dsq-cite-316231">
                <span id="dsq-author-user-316231">Guest</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316231" class="dsq-comment-body">
            <div id="dsq-comment-message-316231" class="dsq-comment-message"><p>I live in the Chi so I feel like i&#8217;m supposed to be hyped about these, but I&#8217;d still rather have a fresh pair of breds. smh</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-316498">
        <div id="dsq-comment-header-316498" class="dsq-comment-header">
            <cite id="dsq-cite-316498">
                <span id="dsq-author-user-316498">Taye</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316498" class="dsq-comment-body">
            <div id="dsq-comment-message-316498" class="dsq-comment-message"><p>Agreed</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316232">
        <div id="dsq-comment-header-316232" class="dsq-comment-header">
            <cite id="dsq-cite-316232">
                <span id="dsq-author-user-316232">ShoeEtiquette</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316232" class="dsq-comment-body">
            <div id="dsq-comment-message-316232" class="dsq-comment-message"><p>For God Sakes Sneaker Bar no disrespect i understand this is a sneaker information site and i come to it strictly by choice but the horse is now glue so can yall please give the 72 &#8211; 10 a rest period, by time December gets here yall will have marketed this shoe for damn near a full year and counting its a Jordan 11 its gone do what it do when Santa Claus come to town </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-316246">
        <div id="dsq-comment-header-316246" class="dsq-comment-header">
            <cite id="dsq-cite-316246">
                <span id="dsq-author-user-316246">Rowan Greener</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316246" class="dsq-comment-body">
            <div id="dsq-comment-message-316246" class="dsq-comment-message"><p>I was just wondering, I&#8217;m new in the sneaker world and these will be my first jordan pick-up, when it say select Jordan retailers does that mean I can&#8217;t pick them online </p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-316247">
        <div id="dsq-comment-header-316247" class="dsq-comment-header">
            <cite id="dsq-cite-316247">
                <span id="dsq-author-user-316247">Slade</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316247" class="dsq-comment-body">
            <div id="dsq-comment-message-316247" class="dsq-comment-message"><p>Yes my friend you can!! But if I was you I&#8217;d be one of the first ones on there come December, because these will be hard to get but not if your quick!!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt depth-2" id="dsq-comment-316557">
        <div id="dsq-comment-header-316557" class="dsq-comment-header">
            <cite id="dsq-cite-316557">
                <span id="dsq-author-user-316557">Critless</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316557" class="dsq-comment-body">
            <div id="dsq-comment-message-316557" class="dsq-comment-message"><p>wait with your money for the 2016 releases.<br />
The White Cement 4 is a MUST have</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316248">
        <div id="dsq-comment-header-316248" class="dsq-comment-header">
            <cite id="dsq-cite-316248">
                <span id="dsq-author-user-316248">livegoodplayhard</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316248" class="dsq-comment-body">
            <div id="dsq-comment-message-316248" class="dsq-comment-message"><p>I&#8217;ve been a fan of the 11&#8217;s, wearing them when I played hoops, sport&#8217;n them off the court, etc&#8230; my fav Jordans! I&#8217;ve got a few different colorways that I wear from time to time and keep a backup set for collection.  BUT, I don&#8217;t know about these 72-10&#8217;s&#8230;I mean, I like the concept, but I feel that it&#8217;s missing something&#8230;..btw, I just looked on eBay and they are on preorder for as high as $440 and I&#8217;m sure they&#8217;ll go up&#8230;.geez&#8230;. I know I&#8217;d never give any money now to &#8216;somebody&#8217; on eBay when the shoe doesn&#8217;t drop until December&#8230;.too much hype to be paying over $400&#8230;.I&#8217;ll wait until December to decide&#8230;.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-316340">
        <div id="dsq-comment-header-316340" class="dsq-comment-header">
            <cite id="dsq-cite-316340">
                <span id="dsq-author-user-316340">The rese</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316340" class="dsq-comment-body">
            <div id="dsq-comment-message-316340" class="dsq-comment-message"><p>Well I guess people got to give their money to Nike instead of planning for the future or paying bills. Just saying but to each is on. I have been n da shoe game a long time and all I got to show for it is lots of used shoes I could resell to others. I&#8217;m not sounding negative but we could have been all rich like Jordan lol. $17,265 on shoes along no lie not including past shoes.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316353">
        <div id="dsq-comment-header-316353" class="dsq-comment-header">
            <cite id="dsq-cite-316353">
                <span id="dsq-author-user-316353">Dutch</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316353" class="dsq-comment-body">
            <div id="dsq-comment-message-316353" class="dsq-comment-message"><p>That iridescent giltter fucking kills this shoe!!! </p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-316524">
        <div id="dsq-comment-header-316524" class="dsq-comment-header">
            <cite id="dsq-cite-316524">
                <span id="dsq-author-user-316524">OG_KiCKS_NJ</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316524" class="dsq-comment-body">
            <div id="dsq-comment-message-316524" class="dsq-comment-message"><p>That&#8217;s actually the PS version not the GS, but these look nice in PS. Will probably look into a pair for myself</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-316587">
        <div id="dsq-comment-header-316587" class="dsq-comment-header">
            <cite id="dsq-cite-316587">
                <span id="dsq-author-user-316587">ONE FREAKY ASS NIGGA!!!</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316587" class="dsq-comment-body">
            <div id="dsq-comment-message-316587" class="dsq-comment-message"><p>I gonna have SEX with my pair!!!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment odd alt depth-2" id="dsq-comment-316606">
        <div id="dsq-comment-header-316606" class="dsq-comment-header">
            <cite id="dsq-cite-316606">
                <span id="dsq-author-user-316606">Julio guerra</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316606" class="dsq-comment-body">
            <div id="dsq-comment-message-316606" class="dsq-comment-message"><p>You not lying . i got a couple bitches lined up on tagged and im getting hella pussy with these on .</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-3" id="dsq-comment-316879">
        <div id="dsq-comment-header-316879" class="dsq-comment-header">
            <cite id="dsq-cite-316879">
                <span id="dsq-author-user-316879">STILL ONE FREAKY ASS NIGGA!!!!</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316879" class="dsq-comment-body">
            <div id="dsq-comment-message-316879" class="dsq-comment-message"><p>I second that my friend!!!!</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-316640">
        <div id="dsq-comment-header-316640" class="dsq-comment-header">
            <cite id="dsq-cite-316640">
                <span id="dsq-author-user-316640">elicia</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316640" class="dsq-comment-body">
            <div id="dsq-comment-message-316640" class="dsq-comment-message"><p>WILL THEY HAVE GRADE SCHOOL???? LIKE A 6.5?!</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-316681">
        <div id="dsq-comment-header-316681" class="dsq-comment-header">
            <cite id="dsq-cite-316681">
                <span id="dsq-author-user-316681">sneakerhead23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316681" class="dsq-comment-body">
            <div id="dsq-comment-message-316681" class="dsq-comment-message"><p>yessssss</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-316656">
        <div id="dsq-comment-header-316656" class="dsq-comment-header">
            <cite id="dsq-cite-316656">
                <span id="dsq-author-user-316656">Jebonies</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316656" class="dsq-comment-body">
            <div id="dsq-comment-message-316656" class="dsq-comment-message"><p>Why can&#8217;t I have 200 bucks?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-odd thread-alt depth-1" id="dsq-comment-316675">
        <div id="dsq-comment-header-316675" class="dsq-comment-header">
            <cite id="dsq-cite-316675">
                <span id="dsq-author-user-316675">J San</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316675" class="dsq-comment-body">
            <div id="dsq-comment-message-316675" class="dsq-comment-message"><p>they finally grew on me, these are nice i can&#8217;t wait. the new pix of them outside show the shoe much better</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-even depth-1" id="dsq-comment-316694">
        <div id="dsq-comment-header-316694" class="dsq-comment-header">
            <cite id="dsq-cite-316694">
                <span id="dsq-author-user-316694">Bryan Marchan</span>
            </cite>
        </div>
        <div id="dsq-comment-body-316694" class="dsq-comment-body">
            <div id="dsq-comment-message-316694" class="dsq-comment-message"><p>Is it true that the red 11&#8217;s are gonna come out this december?</p>
</div>
        </div>

    <ul class="children">
    <li class="comment even depth-2" id="dsq-comment-317250">
        <div id="dsq-comment-header-317250" class="dsq-comment-header">
            <cite id="dsq-cite-317250">
                <span id="dsq-author-user-317250">Plethoric</span>
            </cite>
        </div>
        <div id="dsq-comment-body-317250" class="dsq-comment-body">
            <div id="dsq-comment-message-317250" class="dsq-comment-message"><p>Yea. On December 19th I think.</p>
</div>
        </div>

    </li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-317039">
        <div id="dsq-comment-header-317039" class="dsq-comment-header">
            <cite id="dsq-cite-317039">
                <span id="dsq-author-user-317039">space jam #23</span>
            </cite>
        </div>
        <div id="dsq-comment-body-317039" class="dsq-comment-body">
            <div id="dsq-comment-message-317039" class="dsq-comment-message"><p>Ok who ur connect</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-317279">
        <div id="dsq-comment-header-317279" class="dsq-comment-header">
            <cite id="dsq-cite-317279">
                <span id="dsq-author-user-317279">Dustin James</span>
            </cite>
        </div>
        <div id="dsq-comment-body-317279" class="dsq-comment-body">
            <div id="dsq-comment-message-317279" class="dsq-comment-message"><p>these look so damn good.</p>
</div>
        </div>

    </li><!-- #comment-## -->
            </ul>


        </div>

    </div>

<script type="text/javascript">
var disqus_url = 'http://sneakerbardetroit.com/air-jordan-11-72-10-holiday-2015/';
var disqus_identifier = '209135 http://sneakerbardetroit.com/?p=209135';
var disqus_container_id = 'disqus_thread';
var disqus_shortname = 'sneakerbardetroit';
var disqus_title = "Air Jordan 11 &#8220;72-10&#8243; Release Details";
var disqus_config_custom = window.disqus_config;
var disqus_config = function () {
    /*
    All currently supported events:
    onReady: fires when everything is ready,
    onNewComment: fires when a new comment is posted,
    onIdentify: fires when user is authenticated
    */
    
    
    this.language = '';
        this.callbacks.onReady.push(function () {

        // sync comments in the background so we don't block the page
        var script = document.createElement('script');
        script.async = true;
        script.src = '?cf_action=sync_comments&post_id=209135';

        var firstScript = document.getElementsByTagName('script')[0];
        firstScript.parentNode.insertBefore(script, firstScript);
    });
    
    if (disqus_config_custom) {
        disqus_config_custom.call(this);
    }
};

(function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript';
    dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>

                                </div>
                            </div>
                            <div class="td-pb-span4 td-main-sidebar td-pb-border-top" role="complementary" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
                                <div class="td-ss-main-sidebar">
                                    
 <!-- A generated by theme --> 

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><div class="td-g-rec td-g-rec-id-sidebar">
<script type="text/javascript">
var td_screen_width = document.body.clientWidth;

                    if ( td_screen_width >= 1024 ) {
                        /* large monitors */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="4887559689"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
            
                    if ( td_screen_width >= 768  && td_screen_width < 1024 ) {
                        /* portrait tablets */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:200px;height:200px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="4887559689"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
                
                    if ( td_screen_width < 768 ) {
                        /* Phones */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="4887559689"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
                </script>
</div>

 <!-- end A --> 

<aside class="widget widget_text">			<div class="textwidget"><a href="http://sneakerbardetroit.com/air-jordan-release-dates/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/aj-release-dates.jpg" alt="aj-release-dates" width="332" height="125" class="aligncenter size-full wp-image-196601" /></a>

<a href="http://sneakerbardetroit.com/sneaker-release-dates/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/sn-release-dates.jpg" alt="sn-release-dates" width="332" height="125" class="aligncenter size-medium wp-image-196602" /></a>

<a href="http://sneakerbardetroit.com/category/air-jordans/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/aj-new-links-sbd.jpg" alt="aj new links sbd" width="332" height="100" class="aligncenter size-full wp-image-230515" /></a>

<a href="http://sneakerbardetroit.com/tag/adidas-yeezy-350-boost/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/yeezy-350-boost-low-news-link.jpg" alt="yeezy 350 boost low news link" width="332" height="100" class="aligncenter size-full wp-image-230517" /></a></div>
		</aside>
 <!-- A generated by theme --> 

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><div class="td-g-rec td-g-rec-id-custom_ad_2">
<script type="text/javascript">
var td_screen_width = document.body.clientWidth;

                    if ( td_screen_width >= 1024 ) {
                        /* large monitors */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:300px;height:600px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="5319834489"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
            
                    if ( td_screen_width >= 768  && td_screen_width < 1024 ) {
                        /* portrait tablets */
                        document.write('<ins class="adsbygoogle" style="display:inline-block;width:200px;height:200px" data-ad-client="ca-pub-2599275089753181" data-ad-slot="5319834489"></ins>');
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    }
                </script>
</div>

 <!-- end A --> 

<aside class="widget widget_text">			<div class="textwidget"><a href="http://sneakerbardetroit.com/tag/nike-lebron-12/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/lebron-12-news-updated.jpg" alt="lebron-12-news-updated" width="332" height="100" class="aligncenter size-medium wp-image-196583" /></a>

<a href="http://sneakerbardetroit.com/tag/nike-kobe-10/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/kobe-10-news.jpg" alt="kobe-10-news" width="332" height="100" class="aligncenter size-full wp-image-206497" /></a>

<a href="http://sneakerbardetroit.com/tag/nike-kd-8/" target="_blank"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/nike-kd-8-news-link-sbd.jpg" alt="nike kd 8 news link sbd" width="332" height="100" class="aligncenter size-full wp-image-230516" /></a>

<a href="http://sneakerbardetroit.com/tag/nike-kyrie-1/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/nike-kyrie-1-news.jpg" alt="nike-kyrie-1-news" width="332" height="100" class="aligncenter size-full wp-image-206498" /></a></div>
		</aside><aside class="widget widget_text">			<div class="textwidget"><a href="http://bit.ly/1IOAmFh" target="_blank"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/SneakerBarDetroit_321.jpg" alt="SneakerBarDetroit_32" width="300" height="300" class="aligncenter size-full wp-image-235220" /></a></div>
		</aside>                                </div>
                            </div>
                                    </div> <!-- /.td-pb-row -->
        </article> <!-- /.post -->
    </div>
</div> <!-- /.td-container --><!-- Footer -->
    <div class="td-footer-container td-container">
        <div class="td-pb-row">
            
                        <div class="td-pb-span4">
                                                        <aside class="widget widget_nav_menu"><div class="menu-footer-menu-container"><ul id="menu-footer-menu-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-first td-menu-item td-normal-menu menu-item-122781"><a href="http://sneakerbardetroit.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-122782"><a href="http://sneakerbardetroit.com/privacy-policy/">Privacy Policy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-122797"><a href="http://sneakerbardetroit.com/advertise/">Advertise</a></li>
</ul></div></aside>                        </div>

                        <div class="td-pb-span4">
                                                                                </div>

                        <div class="td-pb-span4">
                                                        <aside class="widget widget_text">			<div class="textwidget"><a href="http://sneakerbardetroit.com/"><img src="http://sneakerbardetroit.com/wp-content/uploads/2011/05/footer-sbd-logo.jpg" alt="footer-sbd-logo" width="300" height="70" class="aligncenter size-full wp-image-203368" /></a></div>
		</aside>                        </div>

                            </div>
    </div>

<!-- Sub Footer -->
    <div class="td-sub-footer-container td-container td-container-border ">
        <div class="td-pb-row">
            <div class="td-pb-span4 td-sub-footer-copy">
                <div class="td-pb-padding-side">
                    &copy; Sneaker Bar Detroit, LLC                </div>
            </div>

            <div class="td-pb-span8 td-sub-footer-menu">
                <div class="td-pb-padding-side">
                    <div class="menu-footer-menu-container"><ul id="menu-footer-menu-2" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-first td-menu-item td-normal-menu menu-item-122781"><a href="http://sneakerbardetroit.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-122782"><a href="http://sneakerbardetroit.com/privacy-policy/">Privacy Policy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page td-menu-item td-normal-menu menu-item-122797"><a href="http://sneakerbardetroit.com/advertise/">Advertise</a></li>
</ul></div>                </div>
            </div>
        </div>
    </div>
    </div><!--close content div-->
</div><!--close td-outer-wrap-->



        <script type="text/javascript">
        // <![CDATA[
        var disqus_shortname = 'sneakerbardetroit';
        (function () {
            var nodes = document.getElementsByTagName('span');
            for (var i = 0, url; i < nodes.length; i++) {
                if (nodes[i].className.indexOf('dsq-postid') != -1) {
                    nodes[i].parentNode.setAttribute('data-disqus-identifier', nodes[i].getAttribute('data-dsqidentifier'));
                    url = nodes[i].parentNode.href.split('#', 1);
                    if (url.length == 1) { url = url[0]; }
                    else { url = url[1]; }
                    nodes[i].parentNode.href = url + '#disqus_thread';
                }
            }
            var s = document.createElement('script'); 
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
        // ]]>
        </script>
        

    <!--

        Theme: Newsmag by tagDiv 2015
        Version: 1.5 (rara)
        Deploy mode: deploy
        
        uid: 55c97c633a420
    -->

    <script type="text/javascript"><script type="text/javascript">
  var vglnk = { key: 'e8d50d15796fe99d4b77c36972e0e219' };

  (function(d, t) {
    var s = d.createElement(t); s.type = 'text/javascript'; s.async = true;
    s.src = '//cdn.viglink.com/api/vglnk.js';
    var r = d.getElementsByTagName(t)[0]; r.parentNode.insertBefore(s, r);
  }(document, 'script'));
</script></script><script type='text/javascript'>
/* <![CDATA[ */
var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Download File":"Download File","Download Video":"Download Video","Play\/Pause":"Play\/Pause","Mute Toggle":"Mute Toggle","None":"None","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Unmute":"Unmute","Mute":"Mute","Captions\/Subtitles":"Captions\/Subtitles"}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Download File":"Download File","Download Video":"Download Video","Play\/Pause":"Play\/Pause","Mute Toggle":"Mute Toggle","None":"None","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Unmute":"Unmute","Mute":"Mute","Captions\/Subtitles":"Captions\/Subtitles"}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=2.16.2'></script>
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-includes/js/mediaelement/wp-mediaelement.js?ver=4.2.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pollsL10n = {"ajax_url":"http:\/\/sneakerbardetroit.com\/wp-admin\/admin-ajax.php","text_wait":"Your last request is still being processed. Please wait a while ...","text_valid":"Please choose a valid poll answer.","text_multiple":"Maximum number of choices allowed: ","show_loading":"1","show_fading":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-content/plugins/wp-polls/polls-js.js?ver=2.7'></script>
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-content/themes/Newsmag/js/td_site.js?ver=1.5'></script>
<script type='text/javascript' src='http://sneakerbardetroit.com/wp-includes/js/comment-reply.min.js?ver=4.2.4'></script>

<!-- JS generated by theme -->

<script>
    

                jQuery().ready(function jQuery_ready() {
                    td_ajax_count.td_get_views_counts_ajax("post","[209135]");
                });
            
</script>

</body>
</html>
<!-- Performance optimized by W3 Total Cache. Learn more: http://www.w3-edge.com/wordpress-plugins/

Page Caching using disk: enhanced

 Served from: sneakerbardetroit.com @ 2015-08-11 00:38:59 by W3 Total Cache -->