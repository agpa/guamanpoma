/* Browser Checking */
//if (parent != self)
//	top.location.href = location.href;

var ie,ns,vpass;
function chkBrowser(){
	browser = navigator.appName;
	version = navigator.appVersion;
	verValue = version.substring(0,1);
	if(browser == "Microsoft Internet Explorer"){ ie = true; }
	else if(browser == "Netscape"){ ns = true; }
	if(verValue >= 4){ vpass = true; }
}

/* Email Updates Form Functions */
function emailUpdates(action){
	if(action == "blur"){
		if(document.emailUpdateForm._email.value == ''){ document.emailUpdateForm._email.value = 'Ingrese E-mail'; }
	}
	else if(action == "focus"){
		if(document.emailUpdateForm._email.value == 'Ingrese E-mail'){ document.emailUpdateForm._email.value = ''; }
	}
}

function doValidate(chk_form){
	if((document.emailUpdateForm._email.value == "") || (document.emailUpdateForm._email.value == "ingrese su correo ")){
		alert('Usted debe entrar en una dirección de correo electrónico para subscribirse.');
		document.emailUpdateForm._email.focus();
		return false;
	}
	return true;
}


/*Site Search Submit Button Proceessing Function*/
var submitDone;
submitDone = 0;

function funcChangeSubmit(){
	if (document.all && document.frmSiteSearch.cmdSubmit){
		document.frmSiteSearch.cmdSubmit.disabled = true;
		document.frmSiteSearch.cmdSubmit.value = 'Procesando busqueda, espere por favor...';
	}

	if (submitDone == 0){
		submitDone = 1;
		return true;
	}
	else{
		if(!document.frmSiteSearch.cmdSubmit || !document.all){ alert("Procesando busqueda, espere por favor..."); }
		return false;
	}
}


function showPopup(val){
	pfilename = val + ".asp";
	popupWindow = window.open(pfilename,val,'left=100,top=100,screenX=100,screenY=100,width=475,height=575,scrollbars');
}

function addBookmark(){
	var url = 'http://www.guamanpoma.org/';
	var title = 'guaman poma';
	window.external.AddFavorite(url,title);
}

function chgStatus(val){
	window.status = val;
	return true;
}


/*This function is here for backwards compatibility reasons */
function init_external(){
		chgImg[0]		=	"oGetInvolved.gif";
		chgImg[1]		=	"yGetInvolved.gif";
		chgImg[2]		=	"oAdvocateWithCARE.gif";
		chgImg[3]		=	"yAdvocateWithCARE.gif";
		chgImg[4]		=	"oVolunteer.gif";
		chgImg[5]		=	"yVolunteer.gif";
		chgImg[6]		=	"oYouthInitiatives.gif";
		chgImg[7]		=	"yYouthInitiatives.gif";
		chgImg[8]		=	"oCAREinYourComm.gif";
		chgImg[9]		=	"yCAREinYourComm.gif";
		chgImg[10]	=	"PageRightofBtnYel.gif"; //little graphic to the right of links on
		chgImg[11]	=	"RightofBtnO.gif"; //little graphic to the right of links off
		chgImg[12]	=	"PageRightofBtnBotYel.gif"; //little graphic to the right of links, down one on
		chgImg[13]	=	"common/blankGif.gif"; //blank gif
		chgImg[14]	=	"PageRightofBtnYel2.gif"; //special

		for (i=0;i<=chgImg.length-1;i++){
			img[i] = new Image();
			img[i].src = chgImg[i];
		}
		chkBrowser();
}

function ipMsOver(val00,val01,val02,name00,name01,name02){
		document.images[name00].src = img[val00].src;
		document.images[name01].src = img[val01].src;
		document.images[name02].src = img[val02].src;
}

/* Thumbnail enlargment function */
var fullsize_path;
function open_fullsize(page_url,img_path,img_caption){
	window_name = "enlg_viewer";
	fullsize_path = img_path;
	if(img_caption == 'undefined' || !img_caption){ fullsize_caption = "contact <a HREF=\"mailto:ivanleonardomiranda@yahoo.com\" onClick=\"window.close();\">ilmc</a> para información"; }
	else{ fullsize_caption = img_caption; }
	wref = window.open(page_url,window_name,'width=550,height=650');
	if(!wref.opener){ wref.opener = this.window; }
//	wref.blur();
//	wref.close;
//	wref = window.open(page_url,window_name,'width=550,height=650');
//	if(!wref.opener){ wref.opener = this.window; }
	return false;
}

function submit_form(objform,objbutton){
	if(document.getElementById){
		button_obj = eval("document.getElementById(objbutton.id)");
		button_obj.disabled = true;
		button_obj.value = "disabled";
		objform.submit();
		return true;
	}
	else{
		alert("button is currently disabled.");
		objform.submit();
		return false;
	}
}

function obfuscate(email,t){
	output = "";
	for (i=0;(i<email.length);i++){ output += "&#" + email.charCodeAt(i) + ";"; }
	if(t==0){ return('<a HREF="mailto:' + output + '">' + output + '</a>'); }
	else{ return(output); }
}

function show_wc(country){
	filename = "/careers/wcfiles/show-wc.asp?c=" + country + "&s=true";
	windowname = "wc";
	options = "width=500,height=550,resizable=yes,scrollbars=yes";
	window.open(filename, windowname, options)
}
