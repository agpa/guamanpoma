<?php /**/ ?><?php // $Id: version.php,v 1.29.2.7 2008/08/17 22:47:47 skodak Exp $

////////////////////////////////////////////////////////////////////////////////
//  Code fragment to define the module version etc.
//  This fragment is called by /admin/index.php
////////////////////////////////////////////////////////////////////////////////

$module->version  = 2007101513;
$module->requires = 2007101509;  // Requires this Moodle version
$module->cron     = 60;

?>
