function P=producto(a,b)
if length(a)~=length(b)
    fprintf('\n ERROR !!! \n');
    fprintf('\n No puede efectuarse dicho producto, debido \n');
    fprintf(' a que el nro. de columnas de la 1ra. matriz');
    fprintf('\n es diferente al nro. de filas de la 2da matriz. \n');
else
for i=1:length(a)
     for j=1:length(b)
        C(i,j) = a(i,:)*b(:,j);
     end
   end
P=C;
end;