// Author: Pontificia Universidad Cat�lica del Per�/Creator: MapViewSVG Professional 6.2.9 for ArcGIS by uismedia (http://www.mapviewsvg.com); 10/11/2009 10:58:53
var SVGscalebar;
var theScalebarFlag = true;
var theMassGesamtLength = 30000;
var theRectCount = 3;
var ObjectLinkText = "Mayor informaci�n";
var BrowserSet = 'width=400,height=400,top=50,left=50,toolbar=no,menubar=no,location=no,hotkeys=no,resizable=yes,scrollbars=no,dependent=yes,status=no';
var CloseButText = "Cerrar consulta";
var ChartArray = new Array();
ChartArray['th6'] = new Array(3);
ChartArray['th6'][0] = new Array(229,347,30000,150);
ChartArray['th6'][1] = new Array('3','4');
ChartArray['th6'][2] = '0';
var WMSLayerArray = new Array(0);
var ShareGeomArray = new Array();
ShareGeomArray['th7'] = 'inline';
var ShareGeomId = 'th7_5_2_xml';
var theShowKoord = 'textbox';
var QueryHeadText = "Resultado de la consulta";
var QueryHelpTableText = "<p>Nota:<ul><li>Mueva el cursor sobre las filas para resaltar los objetos respectivos en el mapa.</li><li>Cierre la ventana con el bot�n 'Cerrar consulta'.</li></ul></p>";
var QueryStatusText1 = "La tabla se est� procesando, por favor espere...";
var QueryStatusText2 = "Tabla completada.";
var recNo = "N�m.";
var QueryClearText = "Cerrar consulta";
var QueryBackText = "Herramienta de consultas";
var theHiQueryColor = 'orangered';
var QueryRecText1 = " de ";
var QueryRecText2 = " Registros encontrados:";
var QueryRecText3 = "Registros";
var theHiColor = 'yellow';
var theBGColor = 'beige';
var theBGColor2 = 'tan';
var SVGSupportText = "Para mostrar esta p�gina necesita el programa gratuito Adobe SVG Viewer 3.0.\n(http://www.adobe.com/svg/viewer/install/main.html)";
var theLegMapKoeff = 170.638666666667;
var QueryNoRecText = "No se encontraron registros.";
var SVGover;
var SVGoverRect;
var theOverviewFlag = true;
var theLegendFlag = true;
var SVGlegend;
var ScrollArray = new Array(2);
ScrollArray[0] = new Array('gesamtLeg0',-152,0.469816272965879,25,229,0);
ScrollArray[1] = new Array('gesamtLeg1',-1,0.215384615384615,25,64,243);
var theGetscaleFlag = true;
var theGeneralScaleFlag = true;
var theScaleConvert = 3393;
var CheckStatus = 'w18eHL7';
var butZoomFlag = false;
var thePrecision = 1;
var theXOrigin = 785614.1;
var theYOrigin = 9189582.3;
var maxScaleFakt = 20;
var theProfVersion = 0;
var theProgNS = "http://www.mapviewsvg.com";

function OperaFlag() {
  var theFlag = false;
  if (document.implementation && document.implementation.createDocument) {
    if (navigator.appName.toLowerCase().indexOf('opera') != -1) theFlag = true;
  }
  return theFlag;
}
function embedHead() {
  if (OperaFlag()) {
    document.write('<div id="head"><object type="image/svg+xml" id="svgdocHead" data="embfiles/head.svgz" width="976px" height="55px"><param name="src" value="embfiles/head.svgz"></object></div>');
  } else {
    document.write('<div id="head"><embed type="image/svg+xml" id="svgdocHead" src="embfiles/head.svgz" width="976px" height="55px"/></div>');
  }
}
function embedLegend() {
  if (OperaFlag()) {
    document.write('<div id="legend"><object type="image/svg+xml"  id="svgdocLegend" data="embfiles/legend.svgz" width="256px" height="308px"><param name="src" value="embfiles/legend.svgz"></object></div>');
  } else {
    document.write('<div id="legend"><embed type="image/svg+xml"  id="svgdocLegend" src="embfiles/legend.svgz" width="256px" height="308px"/></div>');
  }
}
function embedMap() {
  if (OperaFlag()) {
    document.write('<div id="map"><object type="image/svg+xml"  id="svgdocMap" data="embfiles/map.svgz" width="448px" height="299px"><param name="src" value="embfiles/map.svgz"></object></div>');
  } else {
    document.write('<div id="map"><embed type="image/svg+xml"  id="svgdocMap" src="embfiles/map.svgz" width="448px" height="299px"/></div>');
  }
}
function embedOverview() {
  if (OperaFlag()) {
    document.write('<div id="overview"><object type="image/svg+xml"  id="svgdocOverview" data="embfiles/overview.svgz" width="80px" height="53px"><param name="src" value="embfiles/overview.svgz"></object></div>');
  } else {
    document.write('<div id="overview"><embed type="image/svg+xml"  id="svgdocOverview" src="embfiles/overview.svgz" width="80px" height="53px"/></div>');
  }
}
function embedSiteinfo() {
  if (OperaFlag()) {
    document.write('<div id="siteinfo"><object type="image/svg+xml"  id="svgdocSiteinfo" data="embfiles/siteinfo.svgz" width="225px" height="92px"><param name="src" value="embfiles/siteinfo.svgz"></object></div>');
  } else {
    document.write('<div id="siteinfo"><embed type="image/svg+xml"  id="svgdocSiteinfo" src="embfiles/siteinfo.svgz" width="225px" height="92px"/></div>');
  }
}
function embedScalebar() {
  if (OperaFlag()) {
    document.write('<div id="scalebar"><object type="image/svg+xml"  id="svgdocScalebar" data="embfiles/scalebar.svgz" width="210px" height="35px"><param name="src" value="embfiles/scalebar.svgz"></object></div>');
  } else {
    document.write('<div id="scalebar"><embed type="image/svg+xml"  id="svgdocScalebar" src="embfiles/scalebar.svgz" width="210px" height="35px"/></div>');
  }
}

function userInit() {
//This function is for your own scripts, it will be called on loading, do not delete it
//myAlert("function userInit");
}
