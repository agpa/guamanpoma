<?PHP // $Id: lang.php,v 1.106.2.14 2008/12/10 07:09:48 dongsheng Exp $
    /**
    * Display the admin/language menu and process strings translation.
    *
    * @param string $mode the mode of the script: null, "compare", "missing"
    * @param string $currentfile the filename of the English file to edit (if mode==compare)
    * @param bool $uselocal save translations into *_local pack?
    */

    $dbg = '';  // debug output;

    require_once('../config.php');
    require_once($CFG->libdir.'/adminlib.php');

    admin_externalpage_setup('langedit');

    $context = get_context_instance(CONTEXT_SYSTEM);

    define('LANG_SUBMIT_REPEAT', 1);            // repeat displaying submit button?
    define('LANG_SUBMIT_REPEAT_EVERY', 20);     // if so, after how many lines?
    define('LANG_DISPLAY_MISSING_LINKS', 1);    // display "go to first/next missing string" links?
    define('LANG_DEFAULT_FILE', '');            // default file to translate. Empty allowed
    define('LANG_DEFAULT_HELPFILE', '');        // default helpfile to translate. Empty allowed
    define('LANG_LINK_MISSING_STRINGS', 1);     // create links from "missing" page to "compare" page?
    define('LANG_DEFAULT_USELOCAL', 0);         // should *_utf8_local be used by default?
    define('LANG_MISSING_TEXT_MAX_LEN', 60);    // maximum length of the missing text to display
    define('LANG_KEEP_ORPHANS', 1);             // keep orphaned strings (i.e. strings w/o English reference)
    define('LANG_SEARCH_EXTRA', 1);             // search lang files in extra locations
    define('LANG_ALWAYS_TEXTAREA', 1);          // always use <textarea> even for short strings MDL-15738

    $mode = optional_param('mode', '', PARAM_ALPHA);
    if ($mode == 'helpfiles') {
        // use different PARAM_ options according to mode
        $currentfile = optional_param('currentfile', LANG_DEFAULT_HELPFILE, PARAM_PATH);
    } else {
        $currentfile = optional_param('currentfile', LANG_DEFAULT_FILE, PARAM_FILE);
    }
    $uselocal = optional_param('uselocal', -1, PARAM_INT);

    if ($uselocal == -1) {
        if (isset($SESSION->langtranslateintolocal)) {
            $uselocal = $SESSION->langtranslateintolocal;
        } else {
            $uselocal = LANG_DEFAULT_USELOCAL;
        }
    } else {
        $SESSION->langtranslateintolocal = $uselocal;
    }

    if (!has_capability('moodle/site:langeditmaster', $context, $USER->id, false)) {
        // Force using _local
        $uselocal = 1;
    }

    if (!has_capability('moodle/site:langeditmaster', $context, $USER->id, false) && (!$uselocal)) {
        print_error('cannoteditmasterlang');
    }

    if ((!has_capability('moodle/site:langeditlocal', $context, $USER->id, false)) && ($uselocal)) {
        print_error('cannotcustomizelocallang');
    }

    $strlanguage = get_string("language");
    $strcurrentlanguage = get_string("currentlanguage");
    $strmissingstrings = get_string("missingstrings");
    $streditstrings = get_string("editstrings", 'admin');
    $stredithelpdocs = get_string("edithelpdocs", 'admin');
    $strthislanguage = get_string("thislanguage");
    $strgotofirst = get_string('gotofirst','admin');
    $strfilestoredin = get_string('filestoredin', 'admin');
    $strfilestoredinhelp = get_string('filestoredinhelp', 'admin');
    $strswitchlang = get_string('switchlang', 'admin');
    $strchoosefiletoedit = get_string('choosefiletoedit', 'admin');
    $streditennotallowed = get_string('langnoeditenglish', 'admin');
    $strfilecreated = get_string('filecreated', 'admin');
    $strprev = get_string('previous');
    $strnext = get_string('next');
    $strlocalstringcustomization = get_string('localstringcustomization', 'admin');
    $strlangpackmaintaining = get_string('langpackmaintaining', 'admin');
    $strnomissingstrings = get_string('nomissingstrings', 'admin');
    $streditingnoncorelangfile = get_string('editingnoncorelangfile', 'admin');
    $strlanglocalpackage = get_string('langlocalpackage', 'admin');
    $strlangmasterpackage = get_string('langmasterpackage', 'admin');
    $strlangmasterenglish = get_string('langmasterenglish', 'admin');

    $currentlang = current_language();

    switch ($mode) {
        case "missing":
            // Missing array keys are not bugs here but missing strings
            error_reporting(E_ALL ^ E_NOTICE);
            $title = $strmissingstrings;
            break;
        case "compare":
            $title = $streditstrings;
            break;
        case "helpfiles":
            $title = $stredithelpdocs;
        default:
            $title = $strlanguage;
            break;
    }
    $navlinks[] = array('name' => $strlanguage, 'link' => "$CFG->wwwroot/$CFG->admin/lang.php", 'type' => 'misc');
    $navigation = build_navigation($navlinks);

    admin_externalpage_print_header();

    // Prepare and render menu tabs
    $firstrow = array();
    $secondrow = array();
    $inactive = NULL;
    $activated = NULL;
    $currenttab = $mode;
    if ($uselocal) {
        $inactive = array('uselocal');
        $activated = array('uselocal');
    } else {
        $inactive = array('usemaster');
        $activated = array('usemaster');
    }
    if (has_capability('moodle/site:langeditlocal', $context, $USER->id, false)) {
        $firstrow[] = new tabobject('uselocal',
            "$CFG->wwwroot/$CFG->admin/lang.php?mode=$mode&amp;currentfile=$currentfile&amp;uselocal=1",
            $strlocalstringcustomization );
    }
    if (has_capability('moodle/site:langeditmaster', $context, $USER->id, false)) {
        $firstrow[] = new tabobject('usemaster',
            "$CFG->wwwroot/$CFG->admin/lang.php?mode=$mode&amp;currentfile=$currentfile&amp;uselocal=0",
            $strlangpackmaintaining );
    }
    $secondrow[] = new tabobject('missing', "$CFG->wwwroot/$CFG->admin/lang.php?mode=missing", $strmissingstrings );
    $secondrow[] = new tabobject('compare', "$CFG->wwwroot/$CFG->admin/lang.php?mode=compare", $streditstrings );
    $secondrow[] = new tabobject('helpfiles', "$CFG->wwwroot/$CFG->admin/lang.php?mode=helpfiles", $stredithelpdocs );
    $tabs = array($firstrow, $secondrow);
    print_tabs($tabs, $currenttab, $inactive, $activated);


    if (!$mode) {
        // TODO this is a very nice place to put some translation statistics
        print_box_start();
        $currlang = current_language();
        $langs = get_list_of_languages(false, true);
        popup_form ("$CFG->wwwroot/$CFG->admin/lang.php?lang=", $langs, "chooselang", $currlang, "", "", "", false, 'self', $strcurrentlanguage.':');
        print_box_end();
        admin_externalpage_print_footer();
        exit;
    }

    // Get a list of all the root files in the English directory

    $langbase = $CFG->dataroot . '/lang';
    $enlangdir = "$CFG->dirroot/lang/en_utf8";
    if ($currentlang == 'en_utf8') {
        $langdir = $enlangdir;
    } else {
        $langdir = "$langbase/$currentlang";
    }
    $locallangdir = "$langbase/{$currentlang}_local";
    $saveto = $uselocal ? $locallangdir : $langdir;

    if (($mode == 'missing') || ($mode == 'compare')) {
        // get the list of all English stringfiles
        $stringfiles = lang_standard_locations();
        if (LANG_SEARCH_EXTRA) {
            $stringfiles += lang_extra_locations();
        }
        if (count($stringfiles) == 0) {
            error("Could not find English language pack!");
        }
    } elseif ($mode == 'helpfiles') {
        $helpfiles = lang_help_standard_locations();
        if (LANG_SEARCH_EXTRA) {
            $helpfiles += lang_help_extra_locations();
        }
        if (count($helpfiles) == 0) {
            error("Could not find help files in the English language pack!");
        }
    }



    if ($mode == 'missing') {
        if (!file_exists($langdir)) {
            error ('to edit this language pack, you need to put it in '.$CFG->dataroot.'/lang');
        }

        // Following variables store the HTML output to be echo-ed
        $m = '';
        $o = '';

        $m_x = false;

        // Total number of strings and missing strings
        $totalcounter->strings = 0;
        $totalcounter->missing = 0;       

        // For each file, check that a counterpart exists, then check all the strings
        foreach ($stringfiles as $stringfile) {
            $location = $stringfile['location'];
            $plugin = $stringfile['plugin'];
            $prefix = $stringfile['prefix'];
            $filename = $stringfile['filename'];
            unset($string);
            
            // Get some information about file locations:
            //  $enfilepath = the path to the English file distributed either in the core space or in plugin space
            //  $trfilepath = the path to the translated file distributed either in the lang pack or in plugin space
            //  $lcfilepath = the path to the _local customization
            //  $trfilename = the filename of the translated version of the file (including prefix for non-core files)
            if ($location || $plugin) {
                // non-core file in an extra location
                $enfilepath = "$CFG->dirroot/$location/$plugin/lang/en_utf8/$filename";
                $trfilepath = "$CFG->dirroot/$location/$plugin/lang/$currentlang/$filename";
                $lcfilepath = "$locallangdir/$filename";
                $trfilename = $filename;
                if (!$m_x) {
                    $m .= '<hr />';
                    $m_x = true;
                }
            } else {
                // core file in standard location
                $enfilepath = "$CFG->dirroot/lang/en_utf8/$filename";
                $trfilepath = "$langdir/$filename";
                $lcfilepath = "$locallangdir/$filename";
                $trfilename = $filename;
            }
            // $enstring = English strings distributed either in the core space or in plugin space
            include($enfilepath);
            $enstring = isset($string) ? $string : array();
            unset($string);
            ksort($enstring);
            
            //$lcstring = local customizations
            $lcstring = array();
            if (file_exists($lcfilepath)) {
                include($lcfilepath);
                $localfileismissing = 0;
                if (isset($string) && is_array($string)) {
                    $lcstring = $string;
                }
                unset($string);
                ksort($lcstring);
            } else {
                $localfileismissing = 1;
            }

            // $string = translated strings distibuted either in core lang pack or in plugin space
            $string = array();
            if (file_exists($trfilepath)) {
                include($trfilepath);
                $fileismissing = 0;
            } else {
                $fileismissing = 1;
                $o .= notify(get_string("filemissing", "", $trfilepath), "notifyproblem", "center", true);
            }

            $missingcounter = 0;

            $first = true; // first missing string found in the file
            // For all English strings in the current file check distributed translations and _local customizations
       