<?php /**/ ?><?php

header("Content-type: text/plain; charset=utf-8");

$chameleon_theme_root = explode('/', $_SERVER['PHP_SELF']);
array_pop($chameleon_theme_root);
array_pop($chameleon_theme_root);
$chameleon_theme_root = implode('/', $chameleon_theme_root);

?>

if (!window.Node) {
     var Node = {
         ELEMENT_NODE: 1,
         ATTRIBUTE_NODE: 2,
         TEXT_NODE: 3,
         CDATA_SECTION_NODE: 4,
         ENTITY_REFERENCE_NODE: 5,
         ENTITY_NODE: 6,
         PROCESSING_INSTRUCTIONS_NODE: 7,
         COMMENT_NODE: 8,
         DOCUMENT_NODE: 9,
         DOCUMENT_TYPE_NODE: 10,
         DOCUMENT_FRAGMENT_NODE: 11,
         NOTATION_NODE: 12
    };
}



String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
};


(function() {

    var struct = [];
    var hotspotMode = null;
        
    var Config = {
        THEME_ROOT: '<?php echo $chameleon_theme_root; ?>',
        REMOTE_URI: '<?php echo substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/')); ?>/css.php<?php echo (isset($_GET['id'])) ? '?id='.(int) $_GET['id'] : '?dummy=1'; ?>',
        FONTS_LIST: ['verdana, arial, helvetica, sans-serif', '"trebuchet ms", verdana, sans-serif', 'georgia, "trebuchet ms", times, serif', 'Other'],
        FONT_WEIGHTS: ['normal', 'bold'],
        FONT_STYLES: ['normal', 'italic'],
        TEXT_DECORATION: ['none', 'underline', 'overline', 'line-through'],
        TEXT_ALIGN: ['left', 'right', 'center', 'justify'],
        REPEAT_LIST: ['repeat', 'repeat-x', 'repeat-y', 'no-repeat'],
        POSITION_LIST: ['left top', 'left center', 'left bottom', 'center top', 'center center', 'center bottom', 'right top', 'right center', 'right bottom'],
        BORDER_LIST: ['solid', 'dotted', 'dashed', 'none'],
        UNITS: ['px', 'pt', 'em', '%'],
        PROPS_LIST: ['color', 'background-color', 'background-image', 'background-attachment', 'background-position', 'font-family', 'font-size', 'font-weight', 'font-style', 'line-height', 'margin', 'padding', 'border-top-width', 'border-right-width', 'border-bottom-width', 'border-left-width', 'border-top-style', 'border-right-style', 'border-bottom-style', 'border-left-style', 'border-top-color', 'border-right-color', 'border-bottom-color', 'border-left-color']
    };
      


    var Util = {
        __registry: {},
        __uniqueId: 0,

        createElement: function(tag, id) {
            if (!id) var id = 'chameleon-element-' + ++Util.__uniqueId;
            var obj = document.createElement(tag);
            obj.setAttribute('id', id);
            return obj;
        },
    
        removeElement: function(obj) {
            if (!obj || !obj.parentNode) return false;

            var kids = obj.getElementsByTagName('*');
            if (!kids.length && typeof obj.all != 'undefined') {
                kids = obj.all;
            }
            
            var n = kids.length;
            while (n--) {
                if (kids[n].id && Util.__registry[kids[n].id]) {
                    Util.__removeAllEvents(kids[n]);
                }
            }
            if (Util.__registry[obj.id]) {
                Util.__removeAllEvents(obj);
            }
            obj.parentNode.removeChild(obj); 
        },
        
        clearElement: function(obj) {
            while (obj.hasChildNodes()) {
                obj.removeChild(obj.firstChild);
            }
        }, 

        addEvent: function(obj, ev, fn) {
            if (!Util.__addToRegistry(obj, ev, fn)) return;
  
            if (obj.addEventListener) {
                obj.addEventListener(ev, fn, false);
            } else if (obj.attachEvent) {
                obj['e' + ev + fn] = fn;
                obj[ev + fn] = function() { 
                    obj['e' + ev + fn](window.event);
                };
                obj.attachEvent('on' + ev, obj[ev + fn]);
            }
        },
        removeEvent: function(obj, ev, fn) {
            if (!Util.__removeFromRegistry(obj, ev, fn)) return;

            if (obj.removeEventListener) {
                obj.removeEventListener(ev, fn, false);
            } else if (obj.detachEvent) {
                obj.detachEvent('on' + ev, obj[ev + fn]);
                obj[ev + fn] = null;     
            }
        },

        __getEventId: function(obj) {
            if (obj == document)  return 'chameleon-doc';
            if (obj == window) return 'chameleon-win';
            if (obj.id) return obj.id;
            return false;
        },
        __findEvent: function(id, ev, fn) {
            var i = Util.__registry[id][ev].length;
            while (i--) {
                if (Util.__registry[id][ev][i] == fn) {
                    return i;
                }
            }
            return -1;
        },
        __addToRegistry: function(obj, ev, fn) {
            var id = Util.__getEventId(obj);

            if (!id) return false;

            if (!Util.__registry[id]) {
                Util.__registry[id] = {};
            }
            if (!Util.__registry[id][ev]) {
                Util.__registry[id][ev] = [];
            }
            if (Util.__findEvent(id, ev, fn) == -1) {
                Util.__registry[id][ev].push(fn);
                return true;
            }
            return false;
        },
        __removeFromRegistry: function(obj, ev, fn) {
            var id = Util.__getEventId(obj);
     
            if (!id) return false;
 
            var pos = Util.__findEvent(id, ev, fn);
            if (pos != -1) {
                Util.__registry[id][ev].splice(pos, 1);
                return true;
            }
            return false;
        },
        __removeAllEvents: function(obj) {
            for (var event in Util.__registry[obj.id]) {
                var n = Util.__registry[obj.id][event].length;
                while (n--) {
                    Util.removeEvent(obj, event, Util.__registry[obj.id][event][n]);
                }
            }
        },

        cleanUp: function() {
            struct = null;
            UI.closeAllBoxes();
        }
    };





    var Pos = {
        getElement: function(obj) {
            var x = 0; var y = 0;
            if (obj.offsetParent) {
                while (obj.offsetParent) {
                    x += obj.offsetLeft;
                    y += obj.offsetTop;
                    obj = obj.offsetParent;
                }
            }
            return {x: x, y: y};
        },
        getMouse: function(e) {
            var x = 0; var y = 0;
            if (e.pageX || e.pageY) {
                x = e.pageX;
                y = e.pageY;
            } else if (e.clientX || e.clientY) {
                x = e.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
                y = e.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
            }
            return {x: x, y: y};
        }
    };
    
    
    




    var CSS = {
        
        __localCSS: {},
        __remoteCSS: {},
        
        __localSaveRequired: false,
        __remoteSaveRequired: false,
        
        
        requireRemoteSave: function() {
            CSS.__remoteSaveRequired = true;            
        },
        
        clearTheme: function() {
            /*var links = document.getElementsByTagName('link');
            var n = links.length;
            while (n--) {
                if (links[n].href && links[n].href.indexOf('<?php echo $chameleon_theme_root . "/styles.php"; ?>') != -1) {
                    links[n].parentNode.removeChild(links[n]);
                    break;
                }
            }*/
        },
        

        loadRemote: function(doSetup) {
            if (!Sarissa.IS_ENABLED_XMLHTTP) {
                return false;
            }
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.responseText.indexOf('CHAMELEON_ERROR') != -1) {
                        alert('There was an error loading from the server:\n' + xmlhttp.responseText.replace(/CHAMELEON_ERROR /, '') + '.');
                        return;
                    }

                    CSS.__remoteCSS = CSS.toObject(xmlhttp.responseText);
                    CSS.__localCSS = CSS.__clone(CSS.__remoteCSS);
                    CSS.preview();
                    if (doSetup) {
                        setup();
                    }
                    xmlhttp = null;
                }
            };
            xmlhttp.open('GET', Config.REMOTE_URI  + '&nc=' + new Date().getTime(), true);
            xmlhttp.send(null);
            return true;
        },
        
        
        updateTemp: function(e, reset) {
            if (!CSS.__localSaveRequired && !reset) {
                UI.statusMsg('There are no changes that need saving!', 'chameleon-notice');
                return;
            }
            
            if (!reset) {
                UI.statusMsg('Updating temporary styles on the server...', 'chameleon-working');
            } else {
                UI.statusMsg('Deleting temporary styles from the server...', 'chameleon-working');
            }
            
            var css = CSS.toString();
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.responseText.indexOf('CHAMELEON_ERROR') != -1) {
                        UI.statusMsg('There was an error saving to the server:\n' + xmlhttp.responseText.replace(/CHAMELEON_ERROR /, '') + '.', 'chameleon-error');
                        
                    } else {
                        CSS.__localSaveRequired = false;
                        if (!reset) {
                            UI.statusMsg('Temporary styles have been updated.', 'chameleon-ok');
                        } else {
                            UI.statusMsg('Temporary styles have been cleared.', 'chameleon-ok');
                        }        
                    }
                    xmlhttp = null;
                }
            };
            xmlhttp.open('POST', Config.REMOTE_URI + '&temp=1', true);
            xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xmlhttp.send('css=' + css);
        },
        

        updateRemote: function() {
            if (!CSS.__remoteSaveRequired) {
                UI.statusMsg('There are no changes that need saving!', 'chameleon-notice');
                return;
            }
        
            var css = CSS.toString(CSS.__localCSS);

            UI.statusMsg('Updating styles on the server...', 'chameleon-working');
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.responseText.indexOf('CHAMELEON_ERROR') != -1) {
                        UI.statusMsg('There was an error saving to the server:\n' + xmlhttp.responseText.replace(/CHAMELEON_ERROR /, '') + '.', 'chameleon-error');
                    } else {
                        CSS.__remoteCSS = CSS.toObject(css);
                        CSS.__localSaveRequired = false;
                        CSS.__remoteSaveRequired = false;
                        UI.statusMsg('Styles have been saved to the server.', 'chameleon-ok');
                    }
                    xmlhttp = null;
                }
            };
            xmlhttp.open('POST', Config.REMOTE_URI, true);
            xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xmlhttp.send('css=' + css);
        },
        
    
        
        
      
      
        hardReset: function(e, noPrompt) {
            if (noPrompt || confirm('Are you sure? This will erase all styles that have not been permanently saved to the server.')) {
                CSS.__localCSS = {};
                CSS.updateTemp(null, true);
                
                CSS.__localCSS = CSS.__clone(CSS.__remoteCSS);
                CSS.__localSaveRequired = false;
                CSS.__remoteSaveRequired = false;
                CSS.preview();
            }
        },
        
        
        
        setPropValue: function(prop, value, selector) {
            if (!selector) var selector = CSS.Selector.get();

            if (!CSS.__localCSS[selector]) {
                CSS.__localCSS[selector] = {};
            }
            
            var matches = prop.match(/^border\-([^\-]+)$/);
            if (value) {
                var func = CSS.__requiresFunction(prop);
                if (func && value != 'none') {
                    CSS.__localCSS[selector][prop] = func + '(' + value + ')';
                } else if (matches) {
                    CSS.__localCSS[selector]['border-left-' + matches[1]] = value;
                    CSS.__localCSS[selector]['border-right-' + matches[1]] = value;
                    CSS.__localCSS[selector]['border-top-' + matches[1]] = value;
                    CSS.__localCSS[selector]['border-bottom-' + matches[1]] = value;
                } else {
                    CSS.__localCSS[selector][prop] = value;
                }
            } else {
                if (matches) {
                    CSS.unsetProp('border-left-' + matches[1], selector);
                    CSS.unsetProp('border-right-' + matches[1], selector);
                    CSS.unsetProp('border-top-' + matches[1], selector);
                    CSS.unsetProp('border-bottom-' + matches[1], selector);
                } else {
                    CSS.unsetProp(prop, selector);
                }
            }
            
            CSS.__localSaveRequired = true;
            CSS.__remoteSaveRequired = true;
            CSS.preview(selector);
        },
        
        getPropValue: function(prop, selector) {
            if (!selector) var selector = CSS.Selector.get();

            if (!CSS.__localCSS[selector] || !CSS.__localCSS[selector][prop]) {
                return '';
            }
            return CSS.__cleanFunctions(CSS.__localCSS[selector][prop]);
        },

        unsetProp: function(prop, selector) {
            if (!selector) var selector = CSS.Selector.get();

            if (!CSS.__localCSS[selector] || !CSS.__localCSS[selector][prop]) return;

            CSS.__localCSS[selector][prop] = null;
            delete CSS.__localCSS[selector][prop];

            if (!CSS.__hasProps(selector)) {
                CSS.__localCSS[selector] = null;
                delete CSS.__localCSS[selector];
            }
        },
        
        
        __hasProps: function(selector) {
            for (var prop in CSS.__localCSS[selector]) {
                if (prop) {
                    return true;
                }
            }
            return false;
        },
        
        


        __cleanFunctions: function(val) {
            var toClean = ['url'];
            for (var i = 0; i < toClean.length; ++i) {
                var start = val.indexOf(toClean[i] + '(');
                var end = val.indexOf(')', start);
                if (start == -1 || end == -1) {
                    continue;
                }
                val = val.slice(start + toClean[i].length + 1, end);
            }
            return val;
        },

        __requiresFunction: function(prop) {
            var fnProps = {};
            fnProps['background-image'] = 'url';
            if (fnProps[prop]) {
                return fnProps[prop];
            }
            return false;
        },




        fixPath: function(val) {
            if (val == 'none') return val;
            
            var tmp = val.split('(');
            if (tmp.length > 1) {
                tmp[1] = Config.THEME_ROOT + '/' + tmp[1];
                return tmp.join('(');
            }
            return Config.THEME_ROOT + '/' + val;
        },
        
        
        
        preview: function(sel) {
            var styleId = 'chameleon-preview-styles';

            var h = document.getElementsByTagName('head')[0];
            var s = document.getElementById(styleId);
            
            if (!s) {
                var s = Util.createElement('style', styleId);
                s.setAttribute('type', 'text/css');
                h.appendChild(s);
            }
            
            if (navigator.userAgent.toLowerCase().indexOf('msie') != -1  && !window.opera && document.styleSheets && document.styleSheets.length > 0) {
                var lastStyle = document.styleSheets[document.styleSheets.length - 1];
                
                var ieCrashProtector = /[^a-z0-9 #_:\.\-\*]/i; // some characters appearing in a selector can cause addRule to crash IE in spectacular style - if the selector contains any character outside this list don't try to add to the preview
                var ieWarning = false;
                
                if (sel) {
                    var matchedSelectors = [];
                    if (typeof sel == 'string') {
                        sel = [sel];
                    }
                    var n = lastStyle.rules.length;
                    while (n--) {
                        var ns = sel.length;
                        if (ns == 0) {
                            break;
                        }
                        while (ns--) {
                            if (sel[ns].match(ieCrashProtector)) {
                                ieWarning = true;
                                sel.splice(ns, 1);
                                break;
                            }
                            
                            if (lastStyle.rules[n].selectorText.toLowerCase() == sel[ns].toLowerCase()) {
                                matchedSelectors.push(sel[ns]);
                                sel.splice(ns, 1);
                                lastStyle.removeRule(n);
                                break;
                            }
                        }
                    }
                    matchedSelectors = matchedSelectors.concat(sel);
                    var sl = matchedSelectors.length;
                    while (sl--) {
                        lastStyle.addRule(matchedSelectors[sl], CSS.__propsToString(CSS.__localCSS[matchedSelectors[sl]], true));
                    }
                } else {
                    var n = lastStyle.rules.length;
                    while (n--) {
                        lastStyle.removeRule(n);
                    }
                   
                    for (var sel in CSS.__localCSS) {
                        if (sel.match(ieCrashProtector)) {
                            ieWarning = true;
                            continue;
                        }
                        var dec = CSS.__propsToString(CSS.__localCSS[sel], true);
                        lastStyle.addRule(sel, dec);
                    }
                }
                
                if (ieWarning) {
                    UI.statusMsg('The edited CSS contains content that can not be previewed by Internet Explorer', 'chameleon-notice');
                }
                
            } else {
                Util.clearElement(s);
                s.appendChild(document.createTextNode(CSS.toString(CSS.__localCSS, true))); // I think innerHTML would be faster here, but it doesn't work in KHTML browsers (Safari etc)
            }
        },
        
        

        __merge: function() {
            var merged = {};
            for (var i = 0; i < arguments.length; ++i) {
                for (var sel in arguments[i]) {
                    var newSelector = false;
                    if (!merged[sel]) {
                        merged[sel] = {};
                        newSelector = true;
                    }
                    for (var prop in arguments[i][sel]) {
                        merged[sel][prop] = arguments[i][sel][prop];
                    }

                    if (i > 0 && !newSelector) {
                        for (var prop in merged[sel]) {
                            if (!arguments[i][sel][prop]) {
                                 merged[sel][prop] = null;
                                 delete merged[sel][prop];
                            }
                        }
                    }
                }
                if (i > 0) {
                    for (var sel in merged) {
                        if (!arguments[i][sel]) {
                            merged[sel] = null;
                            delete merged[sel];
                        }
                    }
                }
            }
            return merged;
        },
        
        __clone: function(src) {
            var cloned = {};
            for (var sel in src) {
                if (!cloned[sel]) {
                    cloned[sel] = {};
                }
                for (var prop in src[sel]) {
                    cloned[sel][prop] = src[sel][prop];
                }
            }
            return cloned;
        },
        
        
        toString: function(css, fixpath) {
            if (!css) var css = CSS.__localCSS;
            
            var dec = '';
            for (var sel in css) {
                dec += sel + ' ' + CSS.__propsToString(css[sel], fixpath, sel);
            }
            return dec;
        },
        
        __propsToString: function(css, fixpath) {
            CSS.__Shorthand.border = {};
            
            var hasBorder = false;
            var col = false;
            var importantBorders = [];

            var dec = '{\n';
            for (var prop in css) {
                
                var includeProp = true;
                
                if (prop.indexOf('border') != -1 && prop.indexOf('spacing') == -1 && prop.indexOf('collapse') == -1) {
                    if (css[prop].indexOf('!important') == -1) {
                        CSS.__Shorthand.recordBorder(prop, css[prop]);
                    } else {
                        importantBorders.push({prop: prop, css: css[prop]});
                    }
                    includeProp = false;
                    hasBorder = true;
                }
                
                if (prop == 'color') {
                    col = css[prop];
                }

                if (includeProp) {
                    if (fixpath && (CSS.__requiresFunction(prop) == 'url') && css[prop] != 'none') {
                        dec += '  ' + prop + ': ' + CSS.fixPath(css[prop]) + ';\n';
                    } else {
                        dec += '  ' + prop + ': ' + css[prop] + ';\n';
                    }
                }
            }
            
            if (hasBorder) {
                dec += CSS.__Shorthand.getBorderString(col);
            }
            var n;
            if (n = importantBorders.length) {
                while (n--) {
                    dec += '  ' + importantBorders[n].prop + ': ' + importantBorders[n].css + ';\n';
                }
            }
            
            dec += '}\n';
            return dec;
        },
        
                
        
        
        toObject: function(css) {
            var cssObj = {};
            var end;

            while (end = css.indexOf('}'), end != -1) {
                var cssRule = css.substr(0, end);
                var parts = cssRule.split('{');
                var selector = parts.shift()
                if (selector.indexOf(',') != -1) {
                    var selectorArr = selector.split(',');
                } else {
                    var selectorArr = [selector];
                }
                
                var rules = parts.pop().trim();
                rules = rules.split(';');
                for (var i = 0; i < rules.length; ++i) {
                    if (rules[i].indexOf(':') == -1) {
                        break;
                    }
                    var rule = rules[i].split(':');
                    var prop = rule.shift().trim();
                    var val = rule.pop().trim();
                    
                    for (var j = 0; j < selectorArr.length; ++j) {
                        var noFontPropReset = {};
                        
                        selector = selectorArr[j].trim();
                        if (!cssObj[selector]) {
                            cssObj[selector] = {};
                        }
                    
                        if (prop != 'font' && (prop.indexOf('font') != -1 || prop == 'line-height')) {
                            noFontPropReset[prop] = true;
                        }
                    
                        if (prop == 'background') {
                            CSS.__Shorthand.setBackground(cssObj, selector, val);
                        } else if (prop == 'font') {    
                            CSS.__Shorthand.setFont(cssObj, selector, val, noFontPropReset);
                        } else if ((prop == 'border' || prop.match(/^border\-([^-]+)$/)) && prop.indexOf('spacing') == -1 && prop.indexOf('collapse') == -1) {
                            CSS.__Shorthand.setBorder(cssObj, selector, val, prop);
                        } else {
                            cssObj[selector][prop] = val;
                        }
                    }
                }
                css = css.substring(end + 1);
            }
            return cssObj;
        },
        
        
        
        
        
        getSelectorCSS: function(selector, asObject) {
            if (!selector) var selector = CSS.Selector.get();

            var css = (CSS.__localCSS[selector]) ? CSS.__localCSS[selector] : {};
            if (asObject) {
                return css;
            }
            return selector + ' ' + CSS.__propsToString(css);
        },
        
        
        
        saveRequired: function() {
            return CSS.__localSaveRequired || CSS.__serverSaveRequired;
        },
        
        
        checkSpec: function(e, selector) {
            if (!selector) var selector = CSS.Selector.get();
            if (selector == '') {
                UI.statusMsg('First you have to choose which item to style!', 'chameleon-notice');
                return;
            }
            
            var splitSelector = function(selector) {
                var selectorEnd = selector.split(' ').pop();
                selectorEnd = selectorEnd.replace(/([\.:#])/g, '|$1');
                return selectorEnd.split('|');
            };
            
            var similar = [];
        
            var selectorBits = splitSelector(selector);
        
            for (var sel in CSS.__localCSS) {
                var selBits = splitSelector(sel);
        
                var n = selectorBits.length;
        
                while (n--) {
                    var match = selectorBits[n];
                    var m = selBits.length;
                    while (m--) {
                        if (selBits[m] == match) {
                            var l = similar.length;
                            var add = true;
                            while (l--) {
                                if (similar[l] == sel) {
                                    add = false;
                                    break;
                                }
                            }
                            if (add) {
                                similar.push(sel);
                            }
                            break;
                        }
                    }
                }
            }
            
            if (similar.length) {
                UI.Selector.__displayOverview(null, similar, selector);
            } else {
                UI.statusMsg('Your file currently contains no selectors that appear similar to "' + selector + '"', 'chameleon-notice');
            }  
        },
        
        
        unloadPrompt: function() {
            if (CSS.__localSaveRequired) {
                if (confirm('You have made changes to the CSS on this page since the last time it was saved, these changes will be lost unless you save them now. Select OK to save a temporary copy or Cancel to continue and discard the unsaved CSS.')) {
                    CSS.updateTemp();
                }
            }
            var cookieVal = (CSS.__remoteSaveRequired) ? 1 : 0;
            var crumb = new cookie('chameleon_server_save_required', cookieVal, 30, '/', null, null);
            crumb.set();
        }

    };
    
    
    
    CSS.Selector = {
        
        trimmed: [],
        full: [],
        selector: '',
        
        create: function() {
            CSS.Selector.trimmed = [];
 
            var n = struct.length;
            while (n--) {
                if (CSS.Selector.full[n]) {
                    CSS.Selector.trimmed.push(CSS.Selector.full[n].val);
                }
            }
            CSS.Selector.set(CSS.Selector.trimmed.join(' '));
        },
        
        modify: function(e) {
            var target = e.target || e.srcElement;
            var p = target.position;
            
            var sel = CSS.Selector.full;

            if (!sel[p]) {
                UI.Selector.highlight(target);
                sel[p] = {val: target.selectorValue, id: target.id};
            } else if (sel[p].val != target.selectorValue) {
                UI.Selector.highlight(target);
                UI.Selector.unhighlight(document.getElementById(sel[p].id));
                sel[p] = {val: target.selectorValue, id: target.id};
            } else {
                UI.Selector.unhighlight(target);
                sel[p] = null;
            }

            CSS.Selector.create();
            UI.Selector.displaySelector(CSS.Selector.trimmed);
        },
        
        set: function(sel) {
            CSS.Selector.selector = sel;
        },
        
        get: function() {
            return CSS.Selector.selector;  
        },

        reset: function() {
            CSS.Selector.trimmed = [];
            CSS.Selector.full = [];
            CSS.Selector.set('');
        }               
    };
    
    
    
    CSS.__Shorthand = {
        border: {},
        
        recordBorder: function(prop, value) {
            var pr = prop.split('-')
            var p = pr.pop();
            var s = pr.pop();
            if (!CSS.__Shorthand.border[p]) {
                CSS.__Shorthand.border[p] = [];
            }
            if (!CSS.__Shorthand.border[s]) {
                CSS.__Shorthand.border[s] = {};
            }
            if (!CSS.__Shorthand.border[s][p]) {
                CSS.__Shorthand.border[s][p] = [];
            }
            CSS.__Shorthand.border[p].push({prop: prop, value: value});
            CSS.__Shorthand.border[s][p] = value;
        },
        
        getBorderString: function(col) {
            var cb = CSS.__Shorthand.border;
            
            var useHowManyProps = function(prop) {
                if (!cb['top'] || !cb['right'] || !cb['bottom'] || !cb['left']) {
                    return false;
                }
                
                if (!(cb['top'][prop] && cb['right'][prop] && cb['bottom'][prop] && cb['left'][prop])) {
                    return false;
                }
                
                if (cb['top'][prop] == cb['right'][prop] && cb['top'][prop] == cb['bottom'][prop] && cb['top'][prop] == cb['left'][prop]) {
                    return 1;
                }
                if (cb['top'][prop] == cb['bottom'][prop] && cb['right'][prop] == cb['left'][prop]) {
                    return 2;
                }
                if (cb['right'][prop] == cb['left'][prop]) {
                    return 3;
                }
                return 4;
            };
            
            var getPropShorthand = function(prop) {
                var num = useHowManyProps(prop);
                if (!num) {
                    return '';
                }
                
                if (prop.indexOf('color') != -1) {
                    var l = inheritColor(cb['left