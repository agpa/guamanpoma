<?php /**/ ?><?php
    /**
     *	base include file for SimpleTest
     *	@package	SimpleTest
     *	@subpackage	MockObjects
     *	@version	$Id$
     */

    /**#@+
     * Lexer mode stack constants
     */
    if (! defined('LEXER_ENTER')) {
        define('LEXER_ENTER', 1);
    }
    if (! defined('LEXER_MATCHED')) {
        define('LEXER_MATCHED', 2);
    }
    if (! defined('LEXER_UNMATCHED')) {
        define('LEXER_UNMATCHED', 3);
    }
    if (! defined('LEXER_EXIT')) {
        define('LEXER_EXIT', 4);
    }
    if (! defined('LEXER_SPECIAL')) {
        define('LEXER_SPECIAL', 5);
    }
    /**#@-*/
    
    /**
     *    Compounded regular expression. Any of
     *    the contained patterns could match and
     *    when one does, it's label is returned.
	 *    @package SimpleTest
	 *    @subpackage WebTester
     */
    class ParallelRegex {
        var $_patterns;
        var $_labels;
        var $_regex;
        var $_case;
        
        /**
         *    Constructor. Starts with no patterns.
         *    @param boolean $case    True for case sensitive, false
         *                            for insensitive.
         *    @access public
         */
        function ParallelRegex($case) {
            $this->_case = $case;
            $this->_patterns = array();
            $this->_labels = array();
            $this->_regex = null;
        }
        
        /**
         *    Adds a pattern with an optional label.
         *    @param string $pattern      Perl style regex, but ( and )
         *                                lose the usual meaning.
         *    @param string $label        Label of regex to be returned
         *                                on a match.
         *    @access public
         */
        function addPattern($pattern, $label = true) {
            $count = count($this->_patterns);
            $this->_patterns[$count] = $pattern;
            $this->_labels[$count] = $label;
            $this->_regex = null;
        }
        
        /**
         *    Attempts to match all patterns at once against
         *    a string.
         *    @param string $subject      String to match against.
         *    @param string $match        First matched portion of
         *                                subject.
         *    @return boolean             True on success.
         *    @access public
         */
        function match($subject, &$match) {
            if (count($this->_patterns) == 0) {
                return false;
            }
            if (! preg_match($this->_getCompoundedRegex(), $subject, $matches)) {
                $match = '';
                return false;
            }
            $match = $matches[0];
            for ($i = 1; $i < count($matches); $i++) {
                if ($matches[$i]) {
                    return $this->_labels[$i - 1];
                }
            }
            return true;
        }
        
        /**
         *    Compounds the patterns into a single
         *    regular expression separated with the
         *    "or" operator. Caches the regex.
         *    Will automatically escape (, ) and / tokens.
         *    @param array $patterns    List of patterns in order.
         *    @access private
         */
        function _getCompoundedRegex() {
            if ($this->_regex == null) {
                for ($i = 0, $count = count($this->_patterns); $i < $count; $i++) {
                    $this->_patterns[$i] = '(' . str_replace(
                            array('/', '(', ')'),
                            array('\/', '\(', '\)'),
                            $this->_patterns[$i]) . ')';
                }
                $this->_regex = "/" . implode("|", $this->_patterns) . "/" . $this->_getPerlMatchingFlags();
            }
            return $this->_regex;
        }
        
        /**
         *    Accessor for perl regex mode flags to use.
         *    @return string       Perl regex flags.
         *    @access private
         */
        function _getPerlMatchingFlags() {
            return ($this->_case ? "msS" : "msSi");
        }
    }
    
    /**
     *    States for a stack machine.
	 *    @package SimpleTest
	 *    @subpackage WebTester
     */
    class SimpleStateStack {
        var $_stack;
        
        /**
         *    Constructor. Starts in named state.
         *    @param string $start        Starting state name.
         *    @access public
         */
        function SimpleStateStack($start) {
            $this->_stack = array($start);
        }
        
        /**
         *    Accessor for current state.
         *    @return string       State.
         *    @access public
         */
        function getCurrent() {
            return $this->_stack[count($this->_stack) - 1];
        }
        
        /**
         *    Adds a state to the stack and sets it
         *    to be the current state.
         *    @param string $state        New state.
         *    @access public
         */
        function enter($state) {
            array_push($this->_stack, $state);
        }
        
        /**
         *    Leaves the current state and reverts
         *    to the previous one.
         *    @return boolean    False if we drop off
         *                       the bottom of the list.
         *    @access public
         */
        function leave() {
            if (count($this->_stack) == 1) {
                return false;
            }
            array_pop($this->_stack);
            return true;
        }
    }
    
    /**
     *    Accepts text and breaks it into