<?php /**/ ?><?php
    /**
     *	base include file for SimpleTest
     *	@package	SimpleTest
     *	@subpackage	UnitTester
     *	@version	$Id$
     */

    /**#@+*/
    require_once(dirname(__FILE__) . '/invoker.php');
    /**#@-*/

    /**
     *    Can recieve test events and display them. Display
     *    is achieved by making display methods available
     *    and visiting the incoming event.
	 *	  @package SimpleTest
	 *	  @subpackage UnitTester
     *    @abstract
     */
    class SimpleScorer {
        var $_passes;
        var $_fails;
        var $_exceptions;
        var $_is_dry_run;

        /**
         *    Starts the test run with no results.
         *    @access public
         */
        function SimpleScorer() {
            $this->_passes = 0;
            $this->_fails = 0;
            $this->_exceptions = 0;
            $this->_is_dry_run = false;
        }

        /**
         *    Signals that the next evaluation will be a dry
         *    run. That is, the structure events will be
         *    recorded, but no tests will be run.
         *    @param boolean $is_dry        Dry run if true.
         *    @access public
         */
        function makeDry($is_dry = true) {
            $this->_is_dry_run = $is_dry;
        }

        /**
         *    The reporter has a veto on what should be run.
         *    @param string $test_case_name  name of test case.
         *    @param string $method          Name of test method.
         *    @access public
         */
        function shouldInvoke($test_case_name, $method) {
            return ! $this->_is_dry_run;
        }

        /**
         *    Can wrap the invoker in preperation for running
         *    a test.
         *    @param SimpleInvoker $invoker   Individual test runner.
         *    @return SimpleInvoker           Wrapped test runner.
         *    @access public
         */
        function &createInvoker(&$invoker) {
            return $invoker;
        }

        /**
         *    Accessor for current status. Will be false
         *    if there have been any failures or exceptions.
         *    Used for command line tools.
         *    @return boolean        True if no failures.
         *    @access public
         */
        function getStatus() {
            if ($this->_exceptions + $this->_fails > 0) {
                return false;
            }
            return true;
        }

        /**
         *    Paints the start of a group test.
         *    @param string $test_name     Name of test or other label.
         *    @param integer $size         Number of test cases starting.
         *    @access public
         */
        function paintGroupStart($test_name, $size) {
        }

        /**
         *    Paints the end of a group test.
         *    @param string $test_name     Name of test or other label.
         *    @access public
         */
        function paintGroupEnd($test_name) {
        }

        /**
         *    Paints the start of a test case.
         *    @param string $test_name     Name of test or other label.
         *    @access public
         */
        function paintCaseStart($test_name) {
        }

        /**
         *    Paints the end of a test case.
         *    @param string $test_name     Name of test or other label.
         *    @access public
         */
        function paintCaseEnd($test_name) {
        }

        /**
         *    Paints the start of a test method.
         *    @param string $test_name     Name of test or other label.
         *    @access public
         */
        function paintMethodStart($test_name) {
        }

        /**
         *    Paints the end of a test method.
         *    @param string $test_name     Name of test or other label.
         *    @access public
         */
        function paintMethodEnd($test_name) {
        }

        /**
         *    Increments the pass count.
         *    @param string $message        Message is ignored.
         *    @access public
         */
        function paintPass($message) {
            $this->_passes++;
        }

        /**
         *    Increments the fail count.
         *    @param string $message        Message is ignored.
         *    @access public
         */
        function paintFail($message) {
            $this->_fails++;
        }

        /**
         *    Deals with PHP 4 throwing an error.
         *    @param string $message    Text of error formatted by
         *                              the test case.
         *    @access public
         */
        function paintError($message) {
            $this->_exceptions++;
        }

        /**
         *    Deals with PHP 5 throwing an exception.
         *    @param Exception $exception    The actual exception thrown.
         *    @access public
         */
        function paintException($exception) {
            $this->_exceptions++;
        }
		
		/**
		 *    Prints the message for skipping tests.
         *    @param string $message    Text of skip condition.
         *    @access public
         */
		function paintSkip($message) {
		}

        /**
         *    Accessor for the number of passes so far.
         *    @return integer       Number of passes.
         *    @access public
         */
        function getPassCount() {
            return $this->_passes;
        }

        /**
         *    Accessor for the number of fails so far.
         *    @return integer       Number of fails.
         *    @access public
         */
        function getFailCount() {
            return $this->_fails;
   