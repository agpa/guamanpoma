<?php /**/ ?><?php
    /**
     *	base include file for SimpleTest
     *	@package	SimpleTest
     *	@subpackage	WebTester
     *	@version	$Id$
     */
     
    /**#@+
     *	include other SimpleTest class files
     */
    require_once(dirname(__FILE__) . '/socket.php');
    /**#@-*/

    /**
     *    Single post parameter.
	 *    @package SimpleTest
	 *    @subpackage WebTester
     */
    class SimpleEncodedPair {
        var $_key;
        var $_value;
        
        /**
         *    Stashes the data for rendering later.
         *    @param string $key       Form element name.
         *    @param string $value     Data to send.
         */
        function SimpleEncodedPair($key, $value) {
            $this->_key = $key;
            $this->_value = $value;
        }
        
        /**
         *    The pair as a single string.
         *    @return string        Encoded pair.
         *    @access public
         */
        function asRequest() {
            return urlencode($this->_key) . '=' . urlencode($this->_value);
        }
        
        /**
         *    The MIME part as a string.
         *    @return string        MIME part encoding.
         *    @access public
         */
        function asMime() {
            $part = 'Content-Disposition: form-data; ';
            $part .= "name=\"" . $this->_key . "\"\r\n";
            $part .= "\r\n" . $this->_value;
        