<?php /**/ ?><?php
/***************************************************************
*  Copyright notice
*
*  (c) 2003-2006 Kasper Skaarhoj (kasperYYYY@typo3.com)
*  All rights reserved
*
*  This script is part of the Typo3 project. The Typo3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Class for conversion between charsets.
 *
 *    Typo Id: class.t3lib_cs.php,v 1.56 2006/05/03 08:47:30 masi Exp $
 * Moodle $Id: class.t3lib_cs.php,v 1.7.14.2 2009/11/19 10:10:50 skodak Exp $
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @author	Martin Kutschker <martin.t.kutschker@blackbox.net>
 */
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *
 *
 *  136: class t3lib_cs
 *  488:     function parse_charset($charset)
 *  507:     function get_locale_charset($locale)
 *
 *              SECTION: Charset Conversion functions
 *  560:     function conv($str,$fromCS,$toCS,$useEntityForNoChar=0)
 *  600:     function convArray(&$array,$fromCS,$toCS,$useEntityForNoChar=0)
 *  617:     function utf8_encode($str,$charset)
 *  663:     function utf8_decode($str,$charset,$useEntityForNoChar=0)
 *  706:     function utf8_to_entities($str)
 *  739:     function entities_to_utf8($str,$alsoStdHtmlEnt=0)
 *  773:     function utf8_to_numberarray($str,$convEntities=0,$retChar=0)
 *  823:     function UnumberToChar($cbyte)
 *  868:     function utf8CharToUnumber($str,$hex=0)
 *
 *              SECTION: Init functions
 *  911:     function initCharset($charset)
 *  973:     function initUnicodeData($mode=null)
 * 1198:     function initCaseFolding($charset)
 * 1260:     function initToASCII($charset)
 *
 *              SECTION: String operation functions
 * 1331:     function substr($charset,$string,$start,$len=null)
 * 1384:     function strlen($charset,$string)
 * 1414:     function crop($charset,$string,$len,$crop='')
 * 1467:     function strtrunc($charset,$string,$len)
 * 1501:     function conv_case($charset,$string,$case)
 * 1527:     function specCharsToASCII($charset,$string)
 *
 *              SECTION: Internal string operation functions
 * 1567:     function sb_char_mapping($str,$charset,$mode,$opt='')
 *
 *              SECTION: Internal UTF-8 string operation functions
 * 1622:     function utf8_substr($str,$start,$len=null)
 * 1655:     function utf8_strlen($str)
 * 1676:     function utf8_strtrunc($str,$len)
 * 1698:     function utf8_strpos($haystack,$needle,$offset=0)
 * 1723:     function utf8_strrpos($haystack,$needle)
 * 1745:     function utf8_char2byte_pos($str,$pos)
 * 1786:     function utf8_byte2char_pos($str,$pos)
 * 1809:     function utf8_char_mapping($str,$mode,$opt='')
 *
 *              SECTION: Internal EUC string operation functions
 * 1885:     function euc_strtrunc($str,$len,$charset)
 * 1914:     function euc_substr($str,$start,$charset,$len=null)
 * 1939:     function euc_strlen($str,$charset)
 * 1966:     function euc_char2byte_pos($str,$pos,$charset)
 * 2007:     function euc_char_mapping($str,$charset,$mode,$opt='')
 *
 * TOTAL FUNCTIONS: 35
 * (This index is automatically created/updated by the extension "extdeveval")
 *
 */








/**
 * Notes on UTF-8
 *
 * Functions working on UTF-8 strings:
 *
 * - strchr/strstr
 * - strrchr
 * - substr_count
 * - implode/explode/join
 *
 * Functions nearly working on UTF-8 strings:
 *
 * - strlen: returns the length in BYTES, if you need the length in CHARACTERS use utf8_strlen
 * - trim/ltrim/rtrim: the second parameter 'charlist' won't work for characters not contained in 7-bit ASCII
 * - strpos/strrpos: they return the BYTE position, if you need the CHARACTER position use utf8_strpos/utf8_strrpos
 * - htmlentities: charset support for UTF-8 only since PHP 4.3.0
 *
 * Functions NOT working on UTF-8 strings:
 *
 * - str*cmp
 * - stristr
 * - stripos
 * - substr
 * - strrev
 * - ereg/eregi
 * - split/spliti
 * - preg_*
 * - ...
 *
 */
/**
 * Class for conversion between charsets
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @author	Martin Kutschker <martin.t.kutschker@blackbox.net>
 * @package TYPO3
 * @subpackage t3lib
 */
class t3lib_cs {
	var $noCharByteVal=63;		// ASCII Value for chars with no equivalent.

		// This is the array where parsed conversion tables are stored (cached)
	var $parsedCharsets=array();

		// An array where case folding data will be stored (cached)
	var $caseFolding=array();

		// An array where charset-to-ASCII mappings are stored (cached)
	var $toASCII=array();

		// This tells the converter which charsets has two bytes per char:
	var $twoByteSets=array(
		'ucs-2'=>1,	// 2-byte Unicode
	);

		// This tells the converter which charsets has four bytes per char:
	var $fourByteSets=array(
		'ucs-4'=>1,	// 4-byte Unicode
		'utf-32'=>1,	// 4-byte Unicode (limited to the 21-bits of UTF-16)
	);

		// This tells the converter which charsets use a scheme like the Extended Unix Code:
	var $eucBasedSets=array(
		'gb2312'=>1,		// Chinese, simplified.
		'big5'=>1,	