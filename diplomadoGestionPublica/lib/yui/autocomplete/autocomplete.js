/*
Copyright (c) 2008, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.6.0
*/
/////////////////////////////////////////////////////////////////////////////
//
// YAHOO.widget.DataSource Backwards Compatibility
//
/////////////////////////////////////////////////////////////////////////////

YAHOO.widget.DS_JSArray = YAHOO.util.LocalDataSource;

YAHOO.widget.DS_JSFunction = YAHOO.util.FunctionDataSource;

YAHOO.widget.DS_XHR = function(sScriptURI, aSchema, oConfigs) {
    var DS = new YAHOO.util.XHRDataSource(sScriptURI, oConfigs);
    DS._aDeprecatedSchema = aSchema;
    return DS;
};

YAHOO.widget.DS_ScriptNode = function(sScriptURI, aSchema, oConfigs) {
    var DS = new YAHOO.util.ScriptNodeDataSource(sScriptURI, oConfigs);
    DS._aDeprecatedSchema = aSchema;
    return DS;
};

YAHOO.widget.DS_XHR.TYPE_JSON = YAHOO.util.DataSourceBase.TYPE_JSON;
YAHOO.widget.DS_XHR.TYPE_XML = YAHOO.util.DataSourceBase.TYPE_XML;
YAHOO.widget.DS_XHR.TYPE_FLAT = YAHOO.util.DataSourceBase.TYPE_TEXT;

// TODO: widget.DS_ScriptNode.scriptCallbackParam



 /**
 * The AutoComplete control provides the front-end logic for text-entry suggestion and
 * completion functionality.
 *
 * @module autocomplete
 * @requires yahoo, dom, event, datasource
 * @optional animation
 * @namespace YAHOO.widget
 * @title AutoComplete Widget
 */

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/**
 * The AutoComplete class provides the customizable functionality of a plug-and-play DHTML
 * auto completion widget.  Some key features:
 * <ul>
 * <li>Navigate with up/down arrow keys and/or mouse to pick a selection</li>
 * <li>The drop down container can "roll down" or "fly out" via configurable
 * animation</li>
 * <li>UI look-and-feel customizable through CSS, including container
 * attributes, borders, position, fonts, etc</li>
 * </ul>
 *
 * @class AutoComplete
 * @constructor
 * @param elInput {HTMLElement} DOM element reference of an input field.
 * @param elInput {String} String ID of an input field.
 * @param elContainer {HTMLElement} DOM element reference of an existing DIV.
 * @param elContainer {String} String ID of an existing DIV.
 * @param oDataSource {YAHOO.widget.DataSource} DataSource instance.
 * @param oConfigs {Object} (optional) Object literal of configuration params.
 */
YAHOO.widget.AutoComplete = function(elInput,elContainer,oDataSource,oConfigs) {
    if(elInput && elContainer && oDataSource) {
        // Validate DataSource
        if(oDataSource instanceof YAHOO.util.DataSourceBase) {
            this.dataSource = oDataSource;
        }
        else {
            return;
        }

        // YAHOO.widget.DataSource schema backwards compatibility
        // Converted deprecated schema into supported schema
        // First assume key data is held in position 0 of results array
        this.key = 0;
        var schema = oDataSource.responseSchema;
        // An old school schema has been defined in the deprecated DataSource constructor
        if(oDataSource._aDeprecatedSchema) {
            var aDeprecatedSchema = oDataSource._aDeprecatedSchema;
            if(YAHOO.lang.isArray(aDeprecatedSchema)) {
                
                if((oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_JSON) || 
                (oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_UNKNOWN)) { // Used to default to unknown
                    // Store the resultsList
                    schema.resultsList = aDeprecatedSchema[0];
                    // Store the key
                    this.key = aDeprecatedSchema[1];
                    // Only resultsList and key are defined, so grab all the data
                    schema.fields = (aDeprecatedSchema.length < 3) ? null : aDeprecatedSchema.slice(1);
                }
                else if(oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_XML) {
                    schema.resultNode = aDeprecatedSchema[0];
                    this.key = aDeprecatedSchema[1];
                    schema.fields = aDeprecatedSchema.slice(1);
                }                
                else if(oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_TEXT) {
                    schema.recordDelim = aDeprecatedSchema[0];
                    schema.fieldDelim = aDeprecatedSchema[1];
                }                
                oDataSource.responseSchema = schema;
            }
        }
        
        // Validate input element
        if(YAHOO.util.Dom.inDocument(elInput)) {
            if(YAHOO.lang.isString(elInput)) {
                    this._sName = "instance" + YAHOO.widget.AutoComplete._nIndex + " " + elInput;
                    this._elTextbox = document.getElementById(elInput);
            }
            else {
                this._sName = (elInput.id) ?
                    "instance" + YAHOO.widget.AutoComplete._nIndex + " " + elInput.id:
                    "instance" + YAHOO.widget.AutoComplete._nIndex;
                this._elTextbox = elInput;
            }
            YAHOO.util.Dom.addClass(this._elTextbox, "yui-ac-input");
        }
        else {
            return;
        }

        // Validate container element
        if(YAHOO.util.Dom.inDocument(elContainer)) {
            if(YAHOO.lang.isString(elContainer)) {
                    this._elContainer = document.getElementById(elContainer);
            }
            else {
                this._elContainer = elContainer;
            }
            if(this._elContainer.style.display == "none") {
            }
            
            // For skinning
            var elParent = this._elContainer.parentNode;
            var elTag = elParent.tagName.toLowerCase();
            if(elTag == "div") {
                YAHOO.util.Dom.addClass(elParent, "yui-ac");
            }
            else {
            }
        }
        else {
            return;
        }

        // Default applyLocalFilter setting is to enable for local sources
        if(this.dataSource.dataType === YAHOO.util.DataSourceBase.TYPE_LOCAL) {
            this.applyLocalFilter = true;
        }
        
        // Set any config params passed in to override defaults
        if(oConfigs && (oConfigs.constructor == Object)) {
            for(var sConfig in oConfigs) {
                if(sConfig) {
                    this[sConfig] = oConfigs[sConfig];
                }
            }
        }

        // Initialization sequence
        this._initContainerEl();
        this._initProps();
        this._initListEl();
        this._initContainerHelperEls();

        // Set up events
        var oSelf = this;
        var elTextbox = this._elTextbox;

        // Dom events
        YAHOO.util.Event.addListener(elTextbox,"keyup",oSelf._onTextboxKeyUp,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"keydown",oSelf._onTextboxKeyDown,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"focus",oSelf._onTextboxFocus,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"blur",oSelf._onTextboxBlur,oSelf);
        YAHOO.util.Event.addListener(elContainer,"mouseover",oSelf._onContainerMouseover,oSelf);
        YAHOO.util.Event.addListener(elContainer,"mouseout",oSelf._onContainerMouseout,oSelf);
        YAHOO.util.Event.addListener(elContainer,"click",oSelf._onContainerClick,oSelf);
        YAHOO.util.Event.addListener(elContainer,"scroll",oSelf._onContainerScroll,oSelf);
        YAHOO.util.Event.addListener(elContainer,"resize",oSelf._onContainerResize,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"keypress",oSelf._onTextboxKeyPress,oSelf);
        YAHOO.util.Event.addListener(window,"unload",oSelf._onWindowUnload,oSelf);

        // Custom events
        this.textboxFocusEvent = new YAHOO.util.CustomEvent("textboxFocus", this);
        this.textboxKeyEvent = new YAHOO.util.CustomEvent("textboxKey", this);
        this.dataRequestEvent = new YAHOO.util.CustomEvent("dataRequest", this);
        this.dataReturnEvent = new YAHOO.util.CustomEvent("dataReturn", this);
        this.dataErrorEvent = new YAHOO.util.CustomEvent("dataError", this);
        this.containerPopulateEvent = new YAHOO.util.CustomEvent("containerPopulate", this);
        this.containerExpandEvent = new YAHOO.util.CustomEvent("containerExpand", this);
        this.typeAheadEvent = new YAHOO.util.CustomEvent("typeAhead", this);
        this.itemMouseOverEvent = new YAHOO.util.CustomEvent("itemMouseOver", this);
        this.itemMouseOutEvent = new YAHOO.util.CustomEvent("itemMouseOut", this);
        this.itemArrowToEvent = new YAHOO.util.CustomEvent("itemArrowTo", this);
        this.itemArrowFromEvent = new YAHOO.util.CustomEvent("itemArrowFrom", this);
        this.itemSelectEvent = new YAHOO.util.CustomEvent("itemSelect", this);
        this.unmatchedItemSelectEvent = new YAHOO.util.CustomEvent("unmatchedItemSelect", this);
        this.selectionEnforceEvent = new YAHOO.util.CustomEvent("selectionEnforce", this);
        this.containerCollapseEvent = new YAHOO.util.CustomEvent("containerCollapse", this);
        this.textboxBlurEvent = new YAHOO.util.CustomEvent("textboxBlur", this);
        this.textboxChangeEvent = new YAHOO.util.CustomEvent("textboxChange", this);
        
        // Finish up
        elTextbox.setAttribute("autocomplete","off");
        YAHOO.widget.AutoComplete._nIndex++;
    }
    // Required arguments were not found
    else {
    }
};

/////////////////////////////////////////////////////////////////////////////
//
// Public member variables
//
/////////////////////////////////////////////////////////////////////////////

/**
 * The DataSource object that encapsulates the data used for auto completion.
 * This object should be an inherited object from YAHOO.widget.DataSource.
 *
 * @property dataSource
 * @type YAHOO.widget.DataSource
 */
YAHOO.widget.AutoComplete.prototype.dataSource = null;

/**
 * By default, results from local DataSources will pass through the filterResults
 * method to apply a client-side matching algorithm. 
 * 
 * @property applyLocalFilter
 * @type Boolean
 * @default true for local arrays and json, otherwise false
 */
YAHOO.widget.AutoComplete.prototype.applyLocalFilter = null;

/**
 * When applyLocalFilter is true, the local filtering algorthim can have case sensitivity
 * enabled. 
 * 
 * @property queryMatchCase
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.queryMatchCase = false;

/**
 * When applyLocalFilter is true, results can  be locally filtered to return
 * matching strings that "contain" the query string rather than simply "start with"
 * the query string.
 * 
 * @property queryMatchContains
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.queryMatchContains = false;

/**
 * Enables query subset matching. When the DataSource's cache is enabled and queryMatchSubset is
 * true, substrings of queries will return matching cached results. For
 * instance, if the first query is for "abc" susequent queries that start with
 * "abc", like "abcd", will be queried against the cache, and not the live data
 * source. Recommended only for DataSources that return comprehensive results
 * for queries with very few characters.
 *
 * @property queryMatchSubset
 * @type Boolean
 * @default false
 *
 */
YAHOO.widget.AutoComplete.prototype.queryMatchSubset = false;

/**
 * Number of characters that must be entered before querying for results. A negative value
 * effectively turns off the widget. A value of 0 allows queries of null or empty string
 * values.
 *
 * @property minQueryLength
 * @type Number
 * @default 1
 */
YAHOO.widget.AutoComplete.prototype.minQueryLength = 1;

/**
 * Maximum number of results to display in results container.
 *
 * @property maxResultsDisplayed
 * @type Number
 * @default 10
 */
YAHOO.widget.AutoComplete.prototype.maxResultsDisplayed = 10;

/**
 * Number of seconds to delay before submitting a query request.  If a query
 * request is received before a previous one has completed its delay, the
 * previous request is cancelled and the new request is set to the delay. If 
 * typeAhead is also enabled, this value must always be less than the typeAheadDelay
 * in order to avoid certain race conditions. 
 *
 * @property queryDelay
 * @type Number
 * @default 0.2
 */
YAHOO.widget.AutoComplete.prototype.queryDelay = 0.2;

/**
 * If typeAhead is true, number of seconds to delay before updating input with
 * typeAhead value. In order to prevent certain race conditions, this value must
 * always be greater than the queryDelay.
 *
 * @property typeAheadDelay
 * @type Number
 * @default 0.5
 */
YAHOO.widget.AutoComplete.prototype.typeAheadDelay = 0.5;

/**
 * When IME usage is detected, AutoComplete will switch to querying the input
 * value at the given interval rather than per key event.
 *
 * @property queryInterval
 * @type Number
 * @default 500
 */
YAHOO.widget.AutoComplete.prototype.queryInterval = 500;

/**
 * Class name of a highlighted item within results container.
 *
 * @property highlightClassName
 * @type String
 * @default "yui-ac-highlight"
 */
YAHOO.widget.AutoComplete.prototype.highlightClassName = "yui-ac-highlight";

/**
 * Class name of a pre-highlighted item within results container.
 *
 * @property prehighlightClassName
 * @type String
 */
YAHOO.widget.AutoComplete.prototype.prehighlightClassName = null;

/**
 * Query delimiter. A single character separator for multiple delimited
 * selections. Multiple delimiter characteres may be defined as an array of
 * strings. A null value or empty string indicates that query results cannot
 * be delimited. This feature is not recommended if you need forceSelection to
 * be true.
 *
 * @property delimChar
 * @type String | String[]
 */
YAHOO.widget.AutoComplete.prototype.delimChar = null;

/**
 * Whether or not the first item in results container should be automatically highlighted
 * on expand.
 *
 * @property autoHighlight
 * @type Boolean
 * @default true
 */
YAHOO.widget.AutoComplete.prototype.autoHighlight = true;

/**
 * If autohighlight is enabled, whether or not the input field should be automatically updated
 * with the first query result as the user types, auto-selecting the substring portion
 * of the first result that the user has not yet typed.
 *
 * @property typeAhead
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.typeAhead = false;

/**
 * Whether or not to animate the expansion/collapse of the results container in the
 * horizontal direction.
 *
 * @property animHoriz
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.animHoriz = false;

/**
 * Whether or not to animate the expansion/collapse of the results container in the
 * vertical direction.
 *
 * @property animVert
 * @type Boolean
 * @default true
 */
YAHOO.widget.AutoComplete.prototype.animVert = true;

/**
 * Speed of container expand/collapse animation, in seconds..
 *
 * @property animSpeed
 * @type Number
 * @default 0.3
 */
YAHOO.widget.AutoComplete.prototype.animSpeed = 0.3;

/**
 * Whether or not to force the user's selection to match one of the query
 * results. Enabling this feature essentially transforms the input field into a
 * &lt;select&gt; field. This feature is not recommended with delimiter character(s)
 * defined.
 *
 * @property forceSelection
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.forceSelection = false;

/**
 * Whether or not to allow browsers to cache user-typed input in the input
 * field. Disabling this feature will prevent the widget from setting the
 * autocomplete="off" on the input field. When autocomplete="off"
 * and users click the back button after form submission, user-typed input can
 * be prefilled by the browser from its cache. This caching of user input may
 * not be desired for sensitive data, such as credit card numbers, in which
 * case, implementers should consider setting allowBrowserAutocomplete to false.
 *
 * @property allowBrowserAutocomplete
 * @type Boolean
 * @default true
 */
YAHOO.widget.AutoComplete.prototype.allowBrowserAutocomplete = true;

/**
 * Enabling this feature prevents the toggling of the container to a collapsed state.
 * Setting to true does not automatically trigger the opening of the container.
 * Implementers are advised to pre-load the container with an explicit "sendQuery()" call.   
 *
 * @property alwaysShowContainer
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.alwaysShowContainer = false;

/**
 * Whether or not to use an iFrame to layer over Windows form elements in
 * IE. Set to true only when the results container will be on top of a
 * &lt;select&gt; field in IE and thus exposed to the IE z-index bug (i.e.,
 * 5.5 < IE < 7).
 *
 * @property useIFrame
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.useIFrame = false;

/**
 * Whether or not the results container should have a shadow.
 *
 * @property useShadow
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.useShadow = false;

/**
 * Whether or not the input field should be updated with selections.
 *
 * @property supressInputUpdate
 * @type Boolean
 * @default false
 */
YAHOO.widget.AutoComplete.prototype.suppressInputUpdate = false;

/**
 * For backward compatibility to pre-2.6.0 formatResults() signatures, setting
 * resultsTypeList to true will take each object literal result returned by
 * DataSource and flatten into an array.  
 *
 * @property resultTypeList
 * @type Boolean
 * @default true
 */
YAHOO.widget.AutoComplete.prototype.resultTypeList = true;

/**
 * For XHR DataSources, AutoComplete will automatically insert a "?" between the server URI and 
 * the "query" param/value pair. To prevent this behavior, implementers should
 * set this value to false. To more fully customize the query syntax, implementers
 * should override the generateRequest() method. 
 *
 * @property queryQuestionMark
 * @type Boolean
 * @default true
 */
YAHOO.widget.AutoComplete.prototype.queryQuestionMark = true;

/////////////////////////////////////////////////////////////////////////////
//
// Public methods
//
/////////////////////////////////////////////////////////////////////////////

 /**
 * Public accessor to the unique name of the AutoComplete instance.
 *
 * @method toString
 * @return {String} Unique name of the AutoComplete instance.
 */
YAHOO.widget.AutoComplete.prototype.toString = function() {
    return "AutoComplete " + this._sName;
};

 /**
 * Returns DOM reference to input element.
 *
 * @method getInputEl
 * @return {HTMLELement} DOM reference to input element.
 */
YAHOO.widget.AutoComplete.prototype.getInputEl = function() {
    return this._elTextbox;
};

 /**
 * Returns DOM reference to container element.
 *
 * @method getContainerEl
 * @return {HTMLELement} DOM reference to container element.
 */
YAHOO.widget.AutoComplete.prototype.getContainerEl = function() {
    return this._elContainer;
};

 /**
 * Returns true if widget instance is currently focused.
 *
 * @method isFocused
 * @return {Boolean} Returns true if widget instance is currently focused.
 */
YAHOO.widget.AutoComplete.prototype.isFocused = function() {
    return (this._bFocused === null) ? false : this._bFocused;
};

 /**
 * Returns true if container is in an expanded state, false otherwise.
 *
 * @method isCo