/*
Copyright (c) 2008, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.6.0
*/
/////////////////////////////////////////////////////////////////////////////
//
// YAHOO.widget.DataSource Backwards Compatibility
//
/////////////////////////////////////////////////////////////////////////////

YAHOO.widget.DS_JSArray = YAHOO.util.LocalDataSource;

YAHOO.widget.DS_JSFunction = YAHOO.util.FunctionDataSource;

YAHOO.widget.DS_XHR = function(sScriptURI, aSchema, oConfigs) {
    var DS = new YAHOO.util.XHRDataSource(sScriptURI, oConfigs);
    DS._aDeprecatedSchema = aSchema;
    return DS;
};

YAHOO.widget.DS_ScriptNode = function(sScriptURI, aSchema, oConfigs) {
    var DS = new YAHOO.util.ScriptNodeDataSource(sScriptURI, oConfigs);
    DS._aDeprecatedSchema = aSchema;
    return DS;
};

YAHOO.widget.DS_XHR.TYPE_JSON = YAHOO.util.DataSourceBase.TYPE_JSON;
YAHOO.widget.DS_XHR.TYPE_XML = YAHOO.util.DataSourceBase.TYPE_XML;
YAHOO.widget.DS_XHR.TYPE_FLAT = YAHOO.util.DataSourceBase.TYPE_TEXT;

// TODO: widget.DS_ScriptNode.scriptCallbackParam



 /**
 * The AutoComplete control provides the front-end logic for text-entry suggestion and
 * completion functionality.
 *
 * @module autocomplete
 * @requires yahoo, dom, event, datasource
 * @optional animation
 * @namespace YAHOO.widget
 * @title AutoComplete Widget
 */

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

/**
 * The AutoComplete class provides the customizable functionality of a plug-and-play DHTML
 * auto completion widget.  Some key features:
 * <ul>
 * <li>Navigate with up/down arrow keys and/or mouse to pick a selection</li>
 * <li>The drop down container can "roll down" or "fly out" via configurable
 * animation</li>
 * <li>UI look-and-feel customizable through CSS, including container
 * attributes, borders, position, fonts, etc</li>
 * </ul>
 *
 * @class AutoComplete
 * @constructor
 * @param elInput {HTMLElement} DOM element reference of an input field.
 * @param elInput {String} String ID of an input field.
 * @param elContainer {HTMLElement} DOM element reference of an existing DIV.
 * @param elContainer {String} String ID of an existing DIV.
 * @param oDataSource {YAHOO.widget.DataSource} DataSource instance.
 * @param oConfigs {Object} (optional) Object literal of configuration params.
 */
YAHOO.widget.AutoComplete = function(elInput,elContainer,oDataSource,oConfigs) {
    if(elInput && elContainer && oDataSource) {
        // Validate DataSource
        if(oDataSource instanceof YAHOO.util.DataSourceBase) {
            this.dataSource = oDataSource;
        }
        else {
            YAHOO.log("Could not instantiate AutoComplete due to an invalid DataSource", "error", this.toString());
            return;
        }

        // YAHOO.widget.DataSource schema backwards compatibility
        // Converted deprecated schema into supported schema
        // First assume key data is held in position 0 of results array
        this.key = 0;
        var schema = oDataSource.responseSchema;
        // An old school schema has been defined in the deprecated DataSource constructor
        if(oDataSource._aDeprecatedSchema) {
            var aDeprecatedSchema = oDataSource._aDeprecatedSchema;
            if(YAHOO.lang.isArray(aDeprecatedSchema)) {
                
                if((oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_JSON) || 
                (oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_UNKNOWN)) { // Used to default to unknown
                    // Store the resultsList
                    schema.resultsList = aDeprecatedSchema[0];
                    // Store the key
                    this.key = aDeprecatedSchema[1];
                    // Only resultsList and key are defined, so grab all the data
                    schema.fields = (aDeprecatedSchema.length < 3) ? null : aDeprecatedSchema.slice(1);
                }
                else if(oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_XML) {
                    schema.resultNode = aDeprecatedSchema[0];
                    this.key = aDeprecatedSchema[1];
                    schema.fields = aDeprecatedSchema.slice(1);
                }                
                else if(oDataSource.responseType === YAHOO.util.DataSourceBase.TYPE_TEXT) {
                    schema.recordDelim = aDeprecatedSchema[0];
                    schema.fieldDelim = aDeprecatedSchema[1];
                }                
                oDataSource.responseSchema = schema;
            }
        }
        
        // Validate input element
        if(YAHOO.util.Dom.inDocument(elInput)) {
            if(YAHOO.lang.isString(elInput)) {
                    this._sName = "instance" + YAHOO.widget.AutoComplete._nIndex + " " + elInput;
                    this._elTextbox = document.getElementById(elInput);
            }
            else {
                this._sName = (elInput.id) ?
                    "instance" + YAHOO.widget.AutoComplete._nIndex + " " + elInput.id:
                    "instance" + YAHOO.widget.AutoComplete._nIndex;
                this._elTextbox = elInput;
            }
            YAHOO.util.Dom.addClass(this._elTextbox, "yui-ac-input");
        }
        else {
            YAHOO.log("Could not instantiate AutoComplete due to an invalid input element", "error", this.toString());
            return;
        }

        // Validate container element
        if(YAHOO.util.Dom.inDocument(elContainer)) {
            if(YAHOO.lang.isString(elContainer)) {
                    this._elContainer = document.getElementById(elContainer);
            }
            else {
                this._elContainer = elContainer;
            }
            if(this._elContainer.style.display == "none") {
                YAHOO.log("The container may not display properly if display is set to \"none\" in CSS", "warn", this.toString());
            }
            
            // For skinning
            var elParent = this._elContainer.parentNode;
            var elTag = elParent.tagName.toLowerCase();
            if(elTag == "div") {
                YAHOO.util.Dom.addClass(elParent, "yui-ac");
            }
            else {
                YAHOO.log("Could not find the wrapper element for skinning", "warn", this.toString());
            }
        }
        else {
            YAHOO.log("Could not instantiate AutoComplete due to an invalid container element", "error", this.toString());
            return;
        }

        // Default applyLocalFilter setting is to enable for local sources
        if(this.dataSource.dataType === YAHOO.util.DataSourceBase.TYPE_LOCAL) {
            this.applyLocalFilter = true;
        }
        
        // Set any config params passed in to override defaults
        if(oConfigs && (oConfigs.constructor == Object)) {
            for(var sConfig in oConfigs) {
                if(sConfig) {
                    this[sConfig] = oConfigs[sConfig];
                }
            }
        }

        // Initialization sequence
        this._initContainerEl();
        this._initProps();
        this._initListEl();
        this._initContainerHelperEls();

        // Set up events
        var oSelf = this;
        var elTextbox = this._elTextbox;

        // Dom events
        YAHOO.util.Event.addListener(elTextbox,"keyup",oSelf._onTextboxKeyUp,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"keydown",oSelf._onTextboxKeyDown,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"focus",oSelf._onTextboxFocus,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"blur",oSelf._onTextboxBlur,oSelf);
        YAHOO.util.Event.addListener(elContainer,"mouseover",oSelf._onContainerMouseover,oSelf);
        YAHOO.util.Event.addListener(elContainer,"mouseout",oSelf._onContainerMouseout,oSelf);
        YAHOO.util.Event.addListener(elContainer,"click",oSelf._onContainerClick,oSelf);
        YAHOO.util.Event.addListener(elContainer,"scroll",oSelf._onContainerScroll,oSelf);
        YAHOO.util.Event.addListener(elContainer,"resize",oSelf._onContainerResize,oSelf);
        YAHOO.util.Event.addListener(elTextbox,"keypress",oSelf._onTextboxKeyPress