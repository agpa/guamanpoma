/*
Copyright (c) 2008, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.6.0
*/
/**
 * @description <p>Creates a Image Cropper control.</p>
 * @namespace YAHOO.widget
 * @requires yahoo, dom, dragdrop, element, event, resize
 * @module imagecropper
 * @beta
 */
(function() {
var Dom = YAHOO.util.Dom,
    Event = YAHOO.util.Event,
    Lang = YAHOO.lang;

    /**
     * @constructor
     * @class ImageCropper
     * @description <p>Creates a Image Cropper control.</p>
     * @extends YAHOO.util.Element
     * @param {String/HTMLElement} el The image element to make croppable.
     * @param {Object} attrs Object liternal containing configuration parameters.
    */
    var Crop = function(el, config) {
        YAHOO.log('Initializing', 'log', 'ImageCropper');
        var oConfig = {
            element: el,
            attributes: config || {}
        };

        Crop.superclass.constructor.call(this, oConfig.element, oConfig.attributes);    
    };

    /**
    * @private
    * @static
    * @property _instances
    * @description Internal hash table for all ImageCropper instances
    * @type Object
    */ 
    Crop._instances = {};
    /**
    * @static
    * @method getCropperById 
    * @description Get's an ImageCropper object by the HTML id of the image associated with the ImageCropper object.
    * @return {Object} The ImageCropper Object
    */ 
    Crop.getCropperById = function(id) {
        if (Crop._instances[id]) {
            return Crop._instances[id];
        }
        return false;
    };

    YAHOO.extend(Crop, YAHOO.util.Element, {
        /**
        * @private
        * @property CSS_MAIN
        * @description The CSS class used to wrap the element 
        * @type String
        */
        CSS_MAIN: 'yui-crop',
        /**
        * @private
        * @property CSS_MASK
        * @description The CSS class for the mask element
        * @type String
        */
        CSS_MASK: 'yui-crop-mask',
        /**
        * @private
        * @property CSS_RESIZE_MASK
        * @description The CSS class for the mask inside the resize element
        * @type String
        */
        CSS_RESIZE_MASK: 'yui-crop-resize-mask',

        /**
        * @private
        * @property _image
        * @description The url of the image we are cropping
        * @type String
        */
        _image: null,
        /**
        * @private
        * @property _active
        * @description Flag to determine if the crop region is active
        * @type Boolean
        */
        _active: null,
        /**
        * @private
        * @property _resize
        * @description A reference to the Resize Utility used in this Cropper Instance
        * @type Object
        */
        _resize: null,
        /**
        * @private
        * @property _resizeEl
        * @description The HTML Element used to create the Resize Oject
        * @type HTMLElement
        */
        _resizeEl: null,
        /**
        * @private
        * @property _resizeMaskEl
        * @description The HTML Element used to create the Resize mask
        * @type HTMLElement
        */
        _resizeMaskEl: null,
        /**
        * @private
        * @property _wrap
        * @description The HTML Element created to wrap the image
        * @type HTMLElement
        */
        _wrap: null,
        /**
        * @private
        * @property _mask
        * @description The HTML Element created to "mask" the image being cropped
        * @type HTMLElement
        */
        _mask: null,
        /**
        * @private
        * @method _createWrap
        * @description Creates the wrapper element used to wrap the image
        */
        _createWrap: function() {
            YAHOO.log('Creating the wrap element', 'log', 'ImageCropper');
            this._wrap = document.createElement('div');
            this._wrap.id = this.get('element').id + '_wrap';
            this._wrap.className = this.CSS_MAIN;
            var el = this.get('element');
            this._wrap.style.width = el.width ? el.width + 'px' : Dom.getStyle(el, 'width');
            this._wrap.style.height = el.height ? el.height + 'px' : Dom.getStyle(el, 'height');
            
            var par = this.get('element').parentNode;
            par.replaceChild(this._wrap, this.get('element'));
            this._wrap.appendChild(this.get('element'));

            Event.on(this._wrap, 'mouseover', this._handleMouseOver, this, true);
            Event.on(this._wrap, 'mouseout', this._handleMouseOut, this, true);

            Event.on(this._wrap, 'click', function(ev) { Event.stopEvent(ev); }, this, true);
        },

        /**
        * @private
        * @method _createMask
        * @description Creates the mask element used to mask the image
        */
        _createMask: function() {
            YAHOO.log('Creating the Mask', 'log', 'ImageCropper');
            this._mask = document.createElement('div');
            this._mask.className = this.CSS_MASK;
            this._wrap.appendChild(this._mask);
        },

        /**
        * @private
        * @method _createResize
        * @description Creates the resize element and the instance of the Resize Utility
        */
        _createResize: function() {
            YAHOO.log('Creating the Resize element', 'log', 'ImageCropper');
            this._resizeEl = document.createElement('div');
            this._resizeEl.className = YAHOO.util.Resize.prototype.CSS_RESIZE;
            this._resizeEl.style.position = 'absolute';
            
            this._resizeEl.innerHTML = '<div class="' + this.CSS_RESIZE_MASK + '"></div>';
            this._resizeMaskEl = this._resizeEl.firstChild;
            this._wrap.appendChild(this._resizeEl);
            this._resizeEl.style.top = this.get('initialXY')[1] + 'px';
            this._resizeEl.style.left = this.get('initialXY')[0] + 'px';
            this._resizeMaskEl.style.height = Math.floor(this.get('initHeight')) + 'px';
            this._resizeMaskEl.style.width = Math.floor(this.get('initWidth')) + 'px';

            this._resize = new YAHOO.util.Resize(this._resizeEl, {
                knobHandles: true,
                handles: 'all',
                draggable: true,
                status: this.get('status'),
                minWidth: this.get('minWidth'),
                minHeight: this.get('minHeight'),
                ratio: this.get('ratio'),
                autoRatio: this.get('autoRatio'),
                height: this.get('initHeight'),
                width: this.get('initWidth')
            });

            this._setBackgroundImage(this.get('element').getAttribute('src', 2));
            this._setBackgroundPosition(-(this.get('initialXY')[0]),  -(this.get('initialXY')[1]));

            this._resize.on('startResize', this._handleStartResizeEvent, this, true);
            this._resize.on('endResize', this._handleEndResizeEvent, this, true);
            this._resize.on('dragEvent', this._handleDragEvent, this, true);
            this._resize.on('beforeResize', this._handleBeforeResizeEvent, this, true);
            this._resize.on('resize', this._handleResizeEvent, this, true);
            this._resize.dd.on('b4StartDragEvent', this._handleB4DragEvent, this, true);
        },

        /**
        * @private
        * @method _handleMouseOver
        * @description Handles the mouseover event
        */
        _handleMouseOver: function(ev) {
            var evType = 'keydown';
            if (YAHOO.env.ua.gecko || YAHOO.env.ua.opera) {
                evType = 'keypress';
            }
            if (!this._active) {
                this._active = true;
                if (this.get('useKeys')) {
                    Event.on(document, evType, this._handleKeyPress, this, true);
                }
            }
        },
        /**
        * @private
        * @method _handleMouseOut
        * @description Handles the mouseout event
        */
        _handleMouseOut: function(ev) {
            var evType = 'keydown';
            if (YAHOO.env.ua.gecko || YAHOO.env.ua.opera) {
                evType = 'keypress';
            }
            this._active = false;
            if (this.get('useKeys')) {
                Event.removeListener(document, evType, this._handleKeyPress);
            }
        },

        /**
        * @private
        * @method _moveEl
        * @description Moves the resize element based on the arrow keys
        */
        _moveEl: function(dir, inc) {
            YAHOO.log('Moving the element', 'log', 'ImageCropper');
            var t = 0, l = 0,
                region = this._setConstraints(),
                resize = true;

            switch (dir) {
                case 'down':
                    t = -(inc);
                    if ((region.bottom - inc) < 0) {
                        resize = false;
                        this._resizeEl.style.top = (region.top + region.bottom) + 'px';
                    }
                    break;
                case 'up':
                    t = (inc);
                    if ((region.top - inc) < 0) {
                        resize = false;
                        this._resizeEl.style.top = '0px';
                    }
                    break;
                case 'right':
                    l = -(inc);
                    if ((region.right - inc) < 0) {
                        resize = false;
                        this._resizeEl.style.left = (region.left + region.right) + 'px';
                    }
                    break;
                case 'left':
                    l = inc;
                    if ((region.left - inc) < 0) {
                        resize = false;
                        this._resizeEl.style.left = '0p