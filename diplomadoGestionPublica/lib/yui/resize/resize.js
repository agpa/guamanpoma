/*
Copyright (c) 2008, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.6.0
*/
/**
 * @description <p>Makes an element resizable</p>
 * @namespace YAHOO.util
 * @requires yahoo, dom, dragdrop, element, event
 * @optional animation
 * @module resize
 * @beta
 */
(function() {
var D = YAHOO.util.Dom,
    Event = YAHOO.util.Event,
    Lang = YAHOO.lang;

    /**
     * @constructor
     * @class Resize
     * @extends YAHOO.util.Element
     * @description <p>Makes an element resizable</p>
     * @param {String/HTMLElement} el The element to make resizable.
     * @param {Object} attrs Object liternal containing configuration parameters.
    */

    var Resize = function(el, config) {
        var oConfig = {
            element: el,
            attributes: config || {}
        };

        Resize.superclass.constructor.call(this, oConfig.element, oConfig.attributes);    
    };

    /**
    * @private
    * @static
    * @property _instances
    * @description Internal hash table for all resize instances
    * @type Object
    */ 
    Resize._instances = {};
    /**
    * @static
    * @method getResizeById 
    * @description Get's a resize object by the HTML id of the element associated with the Resize object.
    * @return {Object} The Resize Object
    */ 
    Resize.getResizeById = function(id) {
        if (Resize._instances[id]) {
            return Resize._instances[id];
        }
        return false;
    };

    YAHOO.extend(Resize, YAHOO.util.Element, {
        /**
        * @private
        * @property CSS_RESIZE
        * @description Base CSS class name
        * @type String
        */ 
        CSS_RESIZE: 'yui-resize',
        /**
        * @private
        * @property CSS_DRAG
        * @description Class name added when dragging is enabled
        * @type String
        */ 
        CSS_DRAG: 'yui-draggable',
        /**
        * @private
        * @property CSS_HOVER
        * @description Class name used for hover only handles
        * @type String
        */ 
        CSS_HOVER: 'yui-resize-hover',
        /**
        * @private
        * @property CSS_PROXY
        * @description Class name given to the proxy element
        * @type String
        */ 
        CSS_PROXY: 'yui-resize-proxy',
        /**
        * @private
        * @property CSS_WRAP
        * @description Class name given to the wrap element
        * @type String
        */ 
        CSS_WRAP: 'yui-resize-wrap',
        /**
        * @private
        * @property CSS_KNOB
        * @description Class name used to make the knob style handles
        * @type String
        */ 
        CSS_KNOB: 'yui-resize-knob',
        /**
        * @private
        * @property CSS_HIDDEN
        * @description Class name given to the wrap element to make all handles hidden
        * @type String
        */ 
        CSS_HIDDEN: 'yui-resize-hidden',
        /**
        * @private
        * @property CSS_HANDLE
        * @description Class name given to all handles, used as a base for single handle names as well.. Handle "t" will get this.CSS_HANDLE + '-t' as well as this.CSS_HANDLE
        * @type String
        */ 
        CSS_HANDLE: 'yui-resize-handle',
        /**
        * @private
        * @property CSS_STATUS
        * @description Class name given to the status element
        * @type String
        */ 
        CSS_STATUS: 'yui-resize-status',
        /**
        * @private
        * @property CSS_GHOST
        * @description Class name given to the wrap element when the ghost property is active
        * @type String
        */ 
        CSS_GHOST: 'yui-resize-ghost',
        /**
        * @private
        * @property CSS_RESIZING
        * @description Class name given to the wrap element when a resize action is taking place.
        * @type String
        */ 
        CSS_RESIZING: 'yui-resize-resizing',
        /**
        * @private
        * @property _resizeEvent
        * @description The mouse event used to resize with
        * @type Event
        */ 
        _resizeEvent: null,
        /**
        * @private
        * @property dd
        * @description The <a href="YAHOO.util.DragDrop.html">YAHOO.util.DragDrop</a> instance used if draggable is true
        * @type Object
        */ 
        dd: null,
        /** 
        * @private
        * @property browser
        * @description A copy of the YAHOO.env.ua property
        * @type Object
        */
        browser: YAHOO.env.ua,
        /** 
        * @private
        * @property _locked
        * @description A flag to show if the resize is locked
        * @type Boolean
        */
        _locked: null,
        /** 
        * @private
        * @property _positioned
        * @description A flag to show if the element is absolutely positioned
        * @type Boolean
        */
        _positioned: null,
        /** 
        * @private
        * @property _dds
        * @description An Object containing references to all of the <a href="YAHOO.util.DragDrop.html">YAHOO.util.DragDrop</a> instances used for the resize handles
        * @type Object
        */
        _dds: null,
        /** 
        * @private
        * @property _wrap
        * @description The HTML reference of the element wrapper
        * @type HTMLElement
        */
        _wrap: null,
        /** 
        * @private
        * @property _proxy
        * @description The HTML reference of the element proxy
        * @type HTMLElement
        */
        _proxy: null,
        /** 
        * @private
        * @property _handles
        * @description An object containing references to all of the resize handles.
        * @type Object
        */
        _handles: null,
        /** 
        * @private
        * @property _currentHandle
        * @description The string identifier of the currently active handle. e.g. 'r', 'br', 'tl'
        * @type String
        */
        _currentHandle: null,
        /** 
        * @private
        * @property _currentDD
        * @description A link to the currently active DD object
        * @type Object
        */
        _currentDD: null,
        /** 
        * @private
        * @property _cache
        * @description An lookup table containing key information for the element being resized. e.g. height, width, x position, y position, etc..
        * @type Object
        */
        _cache: null,
        /** 
        * @private
        * @property _active
        * @description Flag to show if the resize is active. Used for events.
        * @type Boolean
        */
        _active: null,
        /** 
        * @private
        * @method _createProxy
        * @description Creates the proxy element if the proxy config is true
        */
        _createProxy: function() {
            if (this.get('proxy')) {
                this._proxy = document.createElement('div');
                this._proxy.className = this.CSS_PROXY;
                this._proxy.style.height = this.get('element').clientHeight + 'px';
                this._proxy.style.width = this.get('element').clientWidth + 'px';
                this._wrap.parentNode.appendChild(this._proxy);
            } else {
                this.set('animate', false);
            }
        },
        /** 
        * @private
        * @method _createWrap
        * @description Creates the wrap element if the wrap config is true. It will auto wrap the following element types: img, textarea, input, iframe, select
        */
        _createWrap: function() {
            this._positioned = false;
            //Force wrap for elements that can't have children 
            switch (this.get('element').tagName.toLowerCase()) {
                case 'img':
                case 'textarea':
                case 'input':
                case 'iframe':
                case 'select':
                    this.set('wrap', true);
                    break;
            }
            if (this.get('wrap') === true) {
                this._wrap = document.createElement('div');
                this._wrap.id = this.get('element').id + '_wrap';
                this._wrap.className = this.CSS_WRAP;
                D.setStyle(this._wrap, 'width', this.get('width') + 'px');
                D.setStyle(this._wrap, 'height', this.get('height') + 'px');
                D.setStyle(this._wrap, 'z-index', this.getStyle('z-index'));
                this.setStyle('z-index', 0);
                var pos = D.getStyle(this.get('element'), 'position');
                D.setStyle(this._wrap, 'position', ((pos == 'static') ? 'relative' : pos));
                D.setStyle(this._wrap, 'top', D.getStyle(this.get('element'), 'top'));
                D.setStyle(this._wrap, 'left', D.getStyle(this.get('element'), 'left'));
                if (D.getStyle(this.get('element'), 'position') == 'absolute') {
                    this._positioned = true;
                    D.setStyle(this.get('element'), 'position', 'relative');
                    D.setStyle(this.get('element'), 'top', '0');
                    D.setStyle(this.get('element'), 'left', '0');
                }
                var par = this.get('element').parentNode;
                par.replaceChild(this._wrap, this.get('element'));
                this._wrap.appendChild(this.get('element'));
            } else {
                this._wrap = this.get('element');
                if (D.getStyle(this._wrap, 'position') == 'absolute') {
                    this._positioned = true;
                }
            }
            if (this.get('draggable')) {
                this._setupDragDrop();
            }
            if (this.get('hover')) {
                D.addClass(this._wrap, this.CSS_HOVER);
            }
            if (this.get('knobHandles')) {
                D.addClass(this._wrap, this.CSS_KNOB);
            }
            if (this.get('hiddenHandles')) {
                D.addClass(this._wrap, this.CSS_HIDDEN);
            }
            D.addClass(this._wrap, this.CSS_RESIZE);
        },
        /** 
        * @private
        * @method _setupDragDrop
        * @description Setup the <a href="YAHOO.util.DragDrop.html">YAHOO.util.DragDrop</a> instance on the element
        */
        _setupDragDrop: function() {
            D.addClass(this._wrap, this.CSS_DRAG);
            this.dd = new YAHOO.util.DD(this._wrap, this.get('id') + '-resize', { dragOnly: true, useShim: this.get('useShim') });
            this.dd.on('dragEvent', function() {
                this.fireEvent('dragEvent', arguments);
            }, this, true);
        },
        /** 
        * @private
        * @method _createHandles
        * @description Creates the handles as specified in the config
        */
        _createHandles: function() {
            this._handles = {};
            this._dds = {};
            var h = this.get('handles');
            for (var i = 0; i < h.length; i++) {
                this._handles[h[i]] = document.createElement('div');
                this._handles[h[i]].id = D.generateId(this._handles[h[i]]);
                this._handles[h[i]].className = this.CSS_HANDLE + ' ' + this.CSS_HANDLE + '-' + h[i];
                var k = document.createElement('div');
                k.className = this.CSS_HANDLE + '-inner-' + h[i];
                this._handles[h[i]].appendChild(k);
                this._wrap.appendChild(this._handles[h[i]]);
                Event.on(this._handles[h[i]], 'mouseover', this._handleMouseOver, this, true);
                Event.on(this._handles[h[i]], 'mouseout', this._handleMouseOut, this, true);
                this._dds[h[i]] = new YAHOO.util.DragDrop(this._handles[h[i]], this.get('id') + '-handle-' + h, { useShim: this.get('useShim') });
                this._dds[h[i]].setPadding(15, 15, 15, 15);
                this._dds[h[i]].on('startDragEvent', this._handleStartDrag, this._dds[h[i]], this);
                this._dds[h[i]].on('mouseDownEvent', this._handleMouseDown, this._dds[h[i]], this);
            }
            this._status = document.createElement('span');
            this._status.className = this.CSS_STATUS;
            document.body.insertBefore(this._status, document.body.firstChild);
        },
        /** 
        * @private
        * @method _ieSelectFix
        * @description The function we use as the onselectstart handler when we start a drag in Internet Explorer
        */
        _ieSelectFix: function() {
            return false;
        },
        /** 
        * @private
        * @property _ieSelectBack
        * @description We will hold a copy of the current "onselectstart" method on this property, and reset it after we are done using it.
        */
        _ieSelectBack: null,
        /** 
        * @private
        * @method _setAutoRatio
        * @param {Event} ev A mouse event.
        * @description This method checks to see if the "autoRatio" config is set. If it is, we will check to see if the "Shift Key" is pressed. If so, we will set the config ratio to true.
        */
        _setAutoRatio: function(ev) {
            if (this.get('autoRatio')) {
                if (ev && ev.shiftKey) {
                    //Shift Pressed
                    this.set('ratio', true);
                } else {
                    this.set('ratio', this._configs.ratio._initialConfig.value);
                }
            }
        },
        /** 
        * @private
        * @method _handleMouseDown
        * @param {Event} ev A mouse event.
        * @description This method preps the autoRatio on MouseDown.
        */
        _handleMouseDown: function(ev) {
            if (this._locked) {
                return false;
            }
            if (D.getStyle(this._wrap, 'position') == 'absolute') {
                this._positioned = true;
            }
            if (ev) {
                this._setAutoRatio(ev);
            }
            if (this.browser.ie) {
                this._ieSelectBack = document.body.onselectstart;
                document.body.onselectstart = this._ieSelectFix;
            }
        },
        /** 
        * @private
        * @method _handleMouseOver
        * @param {Event} ev A mouse event.
        * @description Adds CSS class names to the handles
        */
        _handleMouseOver: function(ev) {
            if (this._locked) {
                return false;
            }
            //Internet Explorer needs this
            D.removeClass(this._wrap, this.CSS_RESIZE);
            if (this.get('hover')) {
                D.removeClass(this._wrap, this.CSS_HOVER);
            }
            var tar = Event.getTarget(ev);
            if (!D.hasClass(tar, this.CSS_HANDLE)) {
                tar = tar.parentNode;
            }
            if (D.hasClass(tar, this.CSS_HANDLE) && !this._active) {
                D.addClass(tar, this.CSS_HANDLE + '-active');
                for (var i in this._handles) {
                    if (Lang.hasOwnProperty(this._handles, i)) {
                        if (this._handles[i] == tar) {
                            D.addClass(tar, this.CSS_HANDLE + '-' + i + '-active');
                            break;
                        }
                    }
                }
            }

            //Internet Explorer needs this
            D.addClass(this._wrap, this.CSS_RESIZE);
        },
        /** 
        * @private
        * @method _handleMouseOut
        * @param {Event} ev A mouse event.
        * @description Removes CSS class names to the handles
        */
        _handleMouseOut: function(ev) {
            //Internet Explorer needs this
            D.removeClass(this._wrap, this.CSS_RESIZE);
            if (this.get('hover') && !this._active) {
                D.addClass(this._wrap, this.CSS_HOVER);
            }
            var tar = Event.getTarget(ev);
            if (!D.hasClass(tar, this.CSS_HANDLE)) {
                tar = tar.parentNode;
            }
            if (D.hasClass(tar, this.CSS_HANDLE) && !this._active) {
                D.removeClass(tar, this.CSS_HANDLE + '-active');
                for (var i in this._handles) {
                    if (Lang.hasOwnProperty(this._handles, i)) {
                        if (this._handles[i] == tar) {
                            D.removeClass(tar, this.CSS_HANDLE + '-' + i + '-active');
                            break;
                        }
                    }
                }
            }
            //Internet Explorer needs this
            D.addClass(this._wrap, this.CSS_RESIZE);
        },
        /** 
        * @private
        * @method _handleStartDrag
        * @param {Object} args The args passed from the CustomEvent.
        * @param {Object} dd The <a href="YAHOO.util.DragDrop.html">YAHOO.util.DragDrop</a> object we are worki