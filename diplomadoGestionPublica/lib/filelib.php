<?php /**/ ?><?php //$Id: filelib.php,v 1.50.2.32 2009/11/24 08:44:43 moodler Exp $

define('BYTESERVING_BOUNDARY', 's1k2o3d4a5k6s7'); //unique string constant

function get_file_url($path, $options=null, $type='coursefile') {
    global $CFG, $HTTPSPAGEREQUIRED;

    $path = str_replace('//', '/', $path);  
    $path = trim($path, '/'); // no leading and trailing slashes

    // type of file
    switch ($type) {
       case 'questionfile':
            $url = $CFG->wwwroot."/question/exportfile.php";
            break;
       case 'rssfile':
            $url = $CFG->wwwroot."/rss/file.php";
            break;
        case 'user':
            if (!empty($HTTPSPAGEREQUIRED)) {
                $wwwroot = $CFG->httpswwwroot;
            }
            else {
                $wwwroot = $CFG->wwwroot;
            }
            $url = $wwwroot."/user/pix.php";
            break;
        case 'usergroup':
            $url = $CFG->wwwroot."/user/pixgroup.php";
            break;
        case 'httpscoursefile':
            $url = $CFG->httpswwwroot."/file.php";
            break;
         case 'coursefile':
        default:
            $url = $CFG->wwwroot."/file.php";
    }

    if ($CFG->slasharguments) {
        $parts = explode('/', $path);
        foreach ($parts as $key => $part) {
        /// anchor dash character should not be encoded
            $subparts = explode('#', $part);
            $subparts = array_map('rawurlencode', $subparts);
            $parts[$key] = implode('#', $subparts);
        }
        $path  = implode('/', $parts);
        $ffurl = $url.'/'.$path;
        $separator = '?';
    } else {
        $path = rawurlencode('/'.$path);
        $ffurl = $url.'?file='.$path;
        $separator = '&amp;';
    }

    if ($options) {
        foreach ($options as $name=>$value) {
            $ffurl = $ffurl.$separator.$name.'='.$value;
            $separator = '&amp;';
        }
    }

    return $ffurl;
}

/**
 * Fetches content of file from Internet (using proxy if defined). Uses cURL extension if present.
 * Due to security concerns only downloads from http(s) sources are supported.
 *
 * @param string $url file url starting with http(s)://
 * @param array $headers http headers, null if none. If set, should be an
 *   associative array of header name => value pairs.
 * @param array $postdata array means use POST request with given parameters
 * @param bool $fullresponse return headers, responses, etc in a similar way snoopy does
 *   (if false, just returns content)
 * @param int $timeout timeout for complete download process including all file transfer
 *   (default 5 minutes)
 * @param int $connecttimeout timeout for connection to server; this is the timeout that
 *   usually happens if the remote server is completely down (default 20 seconds);
 *   may not work when using proxy
 * @param bool $skipcertverify If true, the peer's SSL certificate will not be checked. Only use this when already in a trusted location.
 * @return mixed false if request failed or content of the file as string if ok.
 */
function download_file_content($url, $headers=null, $postdata=null, $fullresponse=false, $timeout=300, $connecttimeout=20, $skipcertverify=false) {
    global $CFG;

    // some extra security
    $newlines = array("\r", "\n");
    if (is_array($headers) ) {
        foreach ($headers as $key => $value) {
            $headers[$key] = str_replace($newlines, '', $value);
        }
    }
    $url = str_replace($newlines, '', $url);
    if (!preg_match('|^https?://|i', $url)) {
        if ($fullresponse) {
            $response = new object();
            $response->status        = 0;
            $response->headers       = array();
            $response->response_code = 'Invalid protocol specified in url';
            $response->results       = '';
            $response->error         = 'Invalid protocol specified in url';
            return $response;
        } else {
            return false;
        }
    }


    if (!extension_loaded('curl') or ($ch = curl_init($url)) === false) {
        require_once($CFG->libdir.'/snoopy/Snoopy.class.inc');
        $snoopy = new Snoopy();
        $snoopy->read_timeout = $timeout;
        $snoopy->_fp_timeout  = $connecttimeout;
        $snoopy->proxy_host   = $CFG->proxyhost;
        $snoopy->proxy_port   = $CFG->proxyport;
        if (!empty($CFG->proxyuser) and !empty($CFG->proxypassword)) {
            // this will probably fail, but let's try it anyway
            $snoopy->proxy_user     = $CFG->proxyuser;
            $snoopy->proxy_password = $CFG->proxypassword;
        }
        if (is_array($headers) ) {
            $client->rawheaders = $headers;
        }

        if (is_array($postdata)) {
            $fetch = @$snoopy->fetch($url, $postdata); // use more specific debug code bellow
        } else {
            $fetch = @$snoopy->fetch($url); // use more specific debug code bellow
        }

        if ($fetch) {
            if ($fullresponse) {
                //fix header line endings
                foreach ($snoopy->headers as $key=>$unused) {
                    $snoopy->headers[$key] = trim($snoopy->headers[$key]);
                }
                $response = new object();
                $response->status        = $snoopy->status;
                $response->headers       = $snoopy->headers;
                $response->response_code = trim($snoopy->response_code);
                $response->results       = $snoopy->results;
                $response->error         = $snoopy->error;
                return $response;

            } else if ($snoopy->status != 200) {
                debugging("Snoopy request for \"$url\" failed, http response code: ".$snoopy->response_code, DEBUG_ALL);
                return false;

            } else {
                return $snoopy->results;
            }
        } else {
            if ($fullresponse) {
                $response = new object();
                $response->status        = $snoopy->status;
                $response->headers       = array();
                $response->response_code = $snoopy->response_code;
                $response->results       = '';
                $response->error         = $snoopy->error;
                return $response;
            } else {
                debugging("Snoopy request for \"$url\" failed with: ".$snoopy->error, DEBUG_ALL);
                return false;
            }
        }
    }

    // set extra headers
    if (is_array($headers) ) {
        $headers2 = array();
        foreach ($headers as $key => $value) {
            $headers2[] = "$key: $value";
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers2);
    }

        
    if ($skipcertverify) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    }
    
    // use POST if requested
    if (is_array($postdata)) {
        foreach ($postdata as $k=>$v) {
            $postdata[$k] = urlencode($k).'='.urlencode($v);
        }
        $postdata = implode('&', $postdata);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connecttimeout);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    if (!ini_get('open_basedir') and !ini_get('safe_mode')) {
        // TODO: add version test for '7.10.5'
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
    }

    if (!empty($CFG->proxyhost)) {
        // SOCKS supported in PHP5 only
        if (!empty($CFG->proxytype) and ($CFG->proxytype == 'SOCKS5')) {
            if (defined('CURLPROXY_SOCKS5')) {
                curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
            } else {
                curl_close($ch);
                if ($fullresponse) {
                    $response = new object();
                    $response->status        = '0';
                    $response->headers       = array();
                    $response->response_code = 'SOCKS5 proxy is not supported in PHP4';
                    $response->results       = '';
                    $response->error         = 'SOCKS5 proxy is not supported in PHP4';
                    return $response;
                } else {
                    debugging("SOCKS5 proxy is not supported in 