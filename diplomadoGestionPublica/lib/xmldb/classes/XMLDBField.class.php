<?php /**/ ?><?php // $Id: XMLDBField.class.php,v 1.12.2.2 2008/01/02 16:47:34 stronk7 Exp $

///////////////////////////////////////////////////////////////////////////
//                                                                       //
// NOTICE OF COPYRIGHT                                                   //
//                                                                       //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//          http://moodle.com                                            //
//                                                                       //
// Copyright (C) 1999 onwards Martin Dougiamas        http://dougiamas.com  //
//           (C) 2001-3001 Eloy Lafuente (stronk7) http://contiento.com  //
//                                                                       //
// This program is free software; you can redistribute it and/or modify  //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// This program is distributed in the hope that it will be useful,       //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details:                          //
//                                                                       //
//          http://www.gnu.org/copyleft/gpl.html                         //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

/// This class represent one XMLDB Field

class XMLDBField extends XMLDBObject {

    var $type;
    var $length;
    var $unsigned;
    var $notnull;
    var $default;
    var $sequence;
    var $enum;
    var $enumvalues;
    var $decimals;

    /**
     * Creates one new XMLDBField
     */
    function XMLDBField($name) {
        parent::XMLDBObject($name);
        $this->type = NULL;
        $this->length = NULL;
        $this->unsigned = true;
        $this->notnull = false;
        $this->default = NULL;
        $this->sequence = false;
        $this->enum = false;
        $this->enumvalues = NULL;
        $this->decimals = NULL;
    }

    /**
     * Set all the attributes of one XMLDBField
     *
     * @param string type XMLDB_TYPE_INTEGER, XMLDB_TYPE_NUMBER, XMLDB_TYPE_CHAR, XMLDB_TYPE_TEXT, XMLDB_TYPE_BINARY
     * @param string precision length for integers and chars, two-comma separated numbers for numbers and 'small', 'medium', 'big' for texts and binaries
     * @param string unsigned XMLDB_UNSIGNED or null (or false)
     * @param string notnull XMLDB_NOTNULL or null (or false)
     * @param string sequence XMLDB_SEQUENCE or null (or false)
     * @param string enum XMLDB_ENUM or null (or false)
     * @param array enumvalues an array of possible values if XMLDB_ENUM is set
     * @param string default meaningful default o null (or false)
     */
    function setAttributes($type, $precision=null, $unsigned=null, $notnull=null, $sequence=null, $enum=null, $enumvalues=null, $default=null, $previous=null) {
        $this->type = $type;
    /// Try to split the precision into length and decimals and apply
    /// each one as needed
        $precisionarr = explode(',', $precision);
        if (isset($precisionarr[0])) {
            $this->length = trim($precisionarr[0]);
        }
        if (isset($precisionarr[1])) {
            $this->decimals = trim($precisionarr[1]);
        }
        $this->precision = $type;
        $this->unsigned = !empty($unsigned) ? true : false;
        $this->notnull = !empty($notnull) ? true : false;
        $this->sequence = !empty($sequence) ? true : false;
        $this->enum = !empty($enum) ? true : false;
    /// Accept both quoted and non-quoted vales (quoting them)a
        if (is_array($enumvalues)) {
            $this->enumvalues = array();
            foreach ($enumvalues as $value) {
            /// trim each value quotes
                $value = trim($value, "'");
            /// add them back
                $value = "'" . $value . "'";
                $this->enumvalues[] = $value;
            }
        }
        $this->setDefault($default);

        $this->previous = $previous;
    }

    /**
     * Get the type
     */
    function getType() {
        return $this->type;
    }

    /**
     * Get the length
     */
    function getLength() {
        return $this->length;
    }

    /**
     * Get the decimals
     */
    function getDecimals() {
        return $this->decimals;
    }

    /**
     * Get the notnull
     */
    function getNotNull() {
        return $this->notnull;
    }

    /**
     * Get the unsigned
     */
    function getUnsigned() {
        return $this->unsigned;
    }

    /**
     * Get the sequence
     */
    function getSequence() {
        return $this->sequence;
    }

    /**
     * Get the enum
     */
    function getEnum() {
        return $this->enum;
    }

    /**
     * Get the enumvalues
     */
    function getEnumValues() {
        return $this->enumvalues;
    }

    /**
     * Get the default
     */
    function getDefault() {
        return $this->default;
    }

    /**
     * Set the field type
     */
    function setType($type) {
        $this->type = $type;
    }

    /**
     * Set the field length
     */
    function setLength($length) {
        $this->length = $length;
    }

    /**
     * Set the field decimals
     */
    function setDecimals($decimals) {
        $this->decimals = $decimals;
    }

    /**
     * Set the field unsigned
     */
    function setUnsigned($unsigned=true) {
        $this->unsigned = $unsigned;
    }

    /**
     * Set the field notnull
     */
    function setNotNull($notnull=true) {
        $this->notnull = $notnull;
    }

    /**
     * Set the field sequence
     */
    function setSequence($sequence=true) {
        $this->sequence = $sequence;
    }

    /**
     * Set the field enum
     */
    function setEnum($enum=true) {
        $this->enum = $enum;
    }

    /**
     * Set the field enumvalues, quoting unquoted values
     */
    function setEnumValues($enumvalues) {
        if (is_array($enumvalues)) {
            $this->enumvalues = array();
            foreach ($enumvalues as $value) {
            /// trim each value quotes
                $value = trim($value, "'");
            /// add them back
                $value = "'" . $value . "'";
                $this->enumvalues[] = $value;
            }
        }
    }

    /**
     * Set the field default
     */
    function setDefault($default) {
    /// Check, warn and auto-fix '' (empty) defaults for CHAR NOT NULL columns, changing them
    /// to NULL so XMLDB will apply the proper default
        if ($this->type == XMLDB_TYPE_CHAR && $this->notnull && $default === '') {
            $this->errormsg = 'XMLDB has detected one CHAR NOT NULL column (' . $this->name . ") with '' (empty string) as DEFAULT value. This type of columns must have one meaningful DEFAULT declared or none (NULL). XMLDB have fixed it automatically changing it to none (NULL). The process will continue ok and proper defaults will be created accordingly with each DB requirements. Please fix it in source (XML and/or upgrade script) to avoid this message to be displayed.";
            $this->debug($this->errormsg);
            $default = NULL;
        }
        $this->default = $default;
    }

    /**
     * Load data from XML to the table
     */
    function arr2XMLDBField($xmlarr) {

        $result = true;

    /// Debug the table
    /// traverse_xmlize($xmlarr);                   //Debug
    /// print_object ($GLOBALS['traverse_array']);  //Debug
    /// $GLOBALS['traverse_array']="";              //Debug

    /// Process table attributes (name, type, length, unsigned,
    /// notnull, sequence, enum, enumvalues, decimals, comment,
    /// previous, next)
        if (isset($xmlarr['@']['NAME'])) {
            $this->name = trim($xmlarr['@']['NAME']);
        } else {
            $this->errormsg = 'Missing NAME attribute';
            $this->debug($this->errormsg);