<?php /**/ ?><?php //$Id: olson.php,v 1.25 2007/10/02 01:43:07 mattc-catalyst Exp $

/***
 *** olson_to_timezones ($filename)
 ***
 *** Parses the olson files for Zones and DST rules.
 *** It updates the Moodle database with the Zones/DST rules
 ***
 *** Returns true/false
 ***
 */
function olson_to_timezones ($filename) {

    $zones = olson_simple_zone_parser($filename);
    $rules = olson_simple_rule_parser($filename);

    $mdl_zones = array();

    /**
     *** To translate the combined Zone & Rule changes
     *** in the Olson files to the Moodle single ruleset
     *** format, we need to trasverse every year and see
     *** if either the Zone or the relevant Rule has a 
     *** change. It's yuck but it yields a rationalized
     *** set of data, which is arguably simpler.
     ***
     *** Also note that I am starting at the epoch (1970)
     *** because I don't think we'll see many events scheduled
     *** before that, anyway. 
     ***
     **/
    $maxyear = localtime(time(), true);
    $maxyear = $maxyear['tm_year'] + 1900 + 10;

    foreach ($zones as $zname => $zbyyear) { // loop over zones
        /**
         *** Loop over years, only adding a rule when zone or rule
         *** have changed. All loops preserver the last seen vars
         *** until there's an explicit decision to delete them
         *** 
         **/ 

        // clean the slate for a new zone
        $zone = NULL;
        $rule = NULL;
        
        //
        // Find the pre 1970 zone rule entries
        //
        for ($y = 1970 ; $y >= 0 ; $y--) {
            if (array_key_exists((string)$y, $zbyyear )) { // we have a zone entry for the year
                $zone = $zbyyear[$y];
                //print_object("Zone $zname pre1970 is in  $y\n");
                break; // Perl's last -- get outta here
            }
        }
        if (!empty($zone['rule']) && array_key_exists($zone['rule'], $rules)) {
            $rule = NULL;
            for ($y = 1970 ; $y > 0 ; $y--) {
                if (array_key_exists((string)$y, $rules[$zone['rule']] )) { // we have a rule entry for the year                    
                    $rule  =  $rules[$zone['rule']][$y];
                    //print_object("Rule $rule[name] pre1970 is $y\n");
                    break; // Perl's last -- get outta here
                }
                
            }  
            if (empty($rule)) {
                // Colombia and a few others refer to rules before they exist 
                // Perhaps we should comment out this warning... 
                // trigger_error("Cannot find rule in $zone[rule] <= 1970");
                $rule  = array();
            }
        } else {
            // no DST this year!
            $rule  = array();
        }
        
        // Prepare to insert the base 1970 zone+rule        
        if (!empty($rule) && array_key_exists($zone['rule'], $rules)) {
            // merge the two arrays into the moodle rule
            unset($rule['name']); // warning: $rule must NOT be a reference! 
            unset($rule['year']);
            $mdl_tz = array_merge($zone, $rule);

            //fix (de)activate_time (AT) field to be GMT
            $mdl_tz['dst_time'] = olson_parse_at($mdl_tz['dst_time'], 'set',   $mdl_tz['gmtoff']);
            $mdl_tz['std_time'] = olson_parse_at($mdl_tz['std_time'], 'reset', $mdl_tz['gmtoff']);
        } else {
            // just a simple zone
            $mdl_tz = $zone;
            // TODO: Add other default values here!
            $mdl_tz['dstoff'] = 0;
        }        

        // Fix the from year to 1970
        $mdl_tz['year'] = 1970;

        // add to the array
        $mdl_zones[] = $mdl_tz;
        //print_object("Zero entry for $zone[name] added");

        $lasttimezone = $mdl_tz;

        ///
        /// 1971 onwards
        /// 
        for ($y = 1971; $y < $maxyear ; $y++) {
            $changed = false;
            ///
            /// We create a "zonerule" entry if either zone or rule change...
            ///
            /// force $y to string to avoid PHP
            /// thinking of a positional array
            ///
            if (array_key_exists((string)$y, $zbyyear)) { // we have a 