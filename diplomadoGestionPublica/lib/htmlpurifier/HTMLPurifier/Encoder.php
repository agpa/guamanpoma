<?php /**/ ?><?php

HTMLPurifier_ConfigSchema::define(
    'Core', 'Encoding', 'utf-8', 'istring', 
    'If for some reason you are unable to convert all webpages to UTF-8, '. 
    'you can use this directive as a stop-gap compatibility change to '. 
    'let HTML Purifier deal with non UTF-8 input.  This technique has '. 
    'notable deficiencies: absolutely no characters outside of the selected '. 
    'character encoding will be preserved, not even the ones that have '. 
    'been ampersand escaped (this is due to a UTF-8 specific <em>feature</em> '.
    'that automatically resolves all entities), making it pretty useless '.
    'for anything except the most I18N-blind applications, although '.
    '%Core.EscapeNonASCIICharacters offers fixes this trouble with '.
    'another tradeoff. This directive '.
    'only accepts ISO-8859-1 if iconv is not enabled.'
);

HTMLPurifier_ConfigSchema::define(
    'Core', 'EscapeNonASCIICharacters', false, 'bool',
    'This directive overcomes a deficiency in %Core.Encoding by blindly '.
    'converting all non-ASCII characters into decimal numeric entities before '.
    'converting it to its native encoding. This means that even '.
    'characters that can be expressed in the non-UTF-8 encoding will '.
    'be entity-ized, which can be a real downer for encodings like Big5. '.
    'It also assumes that the ASCII repetoire is available, although '.
    'this is the case for almost all encodings. Anyway, use UTF-8! This '.
    'directive has been available since 1.4.0.'
);

if ( !function_exists('iconv') ) {
    // only encodings with native PHP support
    HTMLPurifier_ConfigSchema::defineAllowedValues(
        'Core', 'Encoding', array(
            'utf-8',
            'iso-8859-1'
        )
    );
    HTMLPurifier_ConfigSchema::defineValueAliases(
        'Core', 'Encoding', array(
            'iso8859-1' => 'iso-8859-1'
        )
    );
}

HTMLPurifier_ConfigSchema::define(
    'Test', 'ForceNoIconv', false, 'bool', 
    'When set to true, HTMLPurifier_Encoder will act as if iconv does not '.
    'exist and use only pure PHP implementations.'
);

/**
 * A UTF-8 specific character encoder that handles cleaning and transforming.
 * @note All functions in this class should be static.
 */
class HTMLPurifier_Encoder
{
    
    /**
     * Constructor throws fatal error if you attempt to instantiate class
     */
    function HTMLPurifier_Encoder() {
        trigger_error('Cannot instantiate encoder, call methods statically', E_USER_ERROR);
    }
    
    /**
     * Error-handler that mutes errors, alternative to shut-up operator.
     */
    function muteErrorHandler() {}
    
    /**
    /**
     * Cleans a UTF-8 string for well-formedness and SGML validity
     * 
     * It will parse according to UTF-8 and return a valid UTF8 string, with
     * non-SGML codepoints excluded.
     * 
     * @static
     * @note Just for reference, the non-SGML code points are 0 to 31 and
     *       127 to 159, inclusive.  However, we allow code points 9, 10
     *       and 13, which are the tab, line feed and carriage return
     *       respectively. 128 and above the code points map to multibyte
     *       UTF-8 representations.
     * 
     * @note Fallback code adapted from utf8ToUnicode by Henri Sivonen and
     *       hsivonen@iki.fi at <http://iki.fi/hsivonen/php-utf8/> under the
     *       LGPL license.  Notes on what changed are inside, but in general,
     *       the original code transformed UTF-8 text into an array of integer
     *       Unicode codepoints. Understandably, transforming that back to
     *       a string would be somewhat expensive, so the function was modded to
     *       directly operate on the string.  However, this discourages code
     *       reuse, and the logic enumerated here would be useful for any
     *       function that needs to be able to understand UTF-8 characters.
     *       As of right now, only smart lossless character encoding converters
     *       would need that, and I'm probably not going to implement them.
     *       Once again, PHP 6 should solve all our problems.
     */
    function cleanUTF8($str, $force_php = false) {
        
        // 