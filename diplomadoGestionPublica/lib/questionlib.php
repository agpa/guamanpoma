<?php /**/ ?><?php  // $Id: questionlib.php,v 1.119.2.28 2009/06/20 23:33:39 arborrow Exp $
/**
 * Code for handling and processing questions
 *
 * This is code that is module independent, i.e., can be used by any module that
 * uses questions, like quiz, lesson, ..
 * This script also loads the questiontype classes
 * Code for handling the editing of questions is in {@link question/editlib.php}
 *
 * TODO: separate those functions which form part of the API
 *       from the helper functions.
 *
 * @author Martin Dougiamas and many others. This has recently been completely
 *         rewritten by Alex Smith, Julian Sedding and Gustav Delius as part of
 *         the Serving Mathematics project
 *         {@link http://maths.york.ac.uk/serving_maths}
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package question
 */

/// CONSTANTS ///////////////////////////////////

/**#@+
 * The different types of events that can create question states
 */
define('QUESTION_EVENTOPEN', '0');      // The state was created by Moodle
define('QUESTION_EVENTNAVIGATE', '1');  // The responses were saved because the student navigated to another page (this is not currently used)
define('QUESTION_EVENTSAVE', '2');      // The student has requested that the responses should be saved but not submitted or validated
define('QUESTION_EVENTGRADE', '3');     // Moodle has graded the responses. A SUBMIT event can be changed to a GRADE event by Moodle.
define('QUESTION_EVENTDUPLICATE', '4'); // The responses submitted were the same as previously
define('QUESTION_EVENTVALIDATE', '5');  // The student has requested a validation. This causes the responses to be saved as well, but not graded.
define('QUESTION_EVENTCLOSEANDGRADE', '6'); // Moodle has graded the responses. A CLOSE event can be changed to a CLOSEANDGRADE event by Moodle.
define('QUESTION_EVENTSUBMIT', '7');    // The student response has been submitted but it has not yet been marked
define('QUESTION_EVENTCLOSE', '8');     // The response has been submitted and the session has been closed, either because the student requested it or because Moodle did it (e.g. because of a timelimit). The responses have not been graded.
define('QUESTION_EVENTMANUALGRADE', '9');   // Grade was entered by teacher

define('QUESTION_EVENTS_GRADED', QUESTION_EVENTGRADE.','.
                    QUESTION_EVENTCLOSEANDGRADE.','.
                    QUESTION_EVENTMANUALGRADE);
/**#@-*/

/**#@+
 * The core question types.
 */
define("SHORTANSWER",   "shortanswer");
define("TRUEFALSE",     "truefalse");
define("MULTICHOICE",   "multichoice");
define("RANDOM",        "random");
define("MATCH",         "match");
define("RANDOMSAMATCH", "randomsamatch");
define("DESCRIPTION",   "description");
define("NUMERICAL",     "numerical");
define("MULTIANSWER",   "multianswer");
define("CALCULATED",    "calculated");
define("ESSAY",         "essay");
/**#@-*/

/**
 * Constant determines the number of answer boxes supplied in the editing
 * form for multiple choice and similar question types.
 */
define("QUESTION_NUMANS", "10");

/**
 * Constant determines the number of answer boxes supplied in the editing
 * form for multiple choice and similar question types to start with, with
 * the option of adding QUESTION_NUMANS_ADD more answers.
 */
define("QUESTION_NUMANS_START", 3);

/**
 * Constant determines the number of answer boxes to add in the editing
 * form for multiple choice and similar question types when the user presses
 * 'add form fields button'.
 */
define("QUESTION_NUMANS_ADD", 3);

/**
 * The options used when popping up a question preview window in Javascript.
 */
define('QUESTION_PREVIEW_POPUP_OPTIONS', 'scrollbars=yes,resizable=yes,width=700,height=540');

/**#@+
 * Option flags for ->optionflags
 * The options are read out via bitwise operation using these constants
 */
/**
 * Whether the questions is to be run in adaptive mode. If this is not set then
 * a question closes immediately after the first submission of responses. This
 * is how question is Moodle always worked before version 1.5
 */
define('QUESTION_ADAPTIVE', 1);

/**
 * options used in forms that move files.
 *
 */
define('QUESTION_FILENOTHINGSELECTED', 0);
define('QUESTION_FILEDONOTHING', 1);
define('QUESTION_FILECOPY', 2);
define('QUESTION_FILEMOVE', 3);
define('QUESTION_FILEMOVELINKSONLY', 4);

/**#@-*/

/// QTYPES INITIATION //////////////////
// These variables get initialised via calls to question_register_questiontype
// as the question type classes are included.
global $QTYPES, $QTYPE_MANUAL, $QTYPE_EXCLUDE_FROM_RANDOM;
/**
 * Array holding question type objects
 */
$QTYPES = array();
/**
 * String in the format "'type1','type2'" that can be used in SQL clauses like
 * "WHERE q.type IN ($QTYPE_MANUAL)".
 */
$QTYPE_MANUAL = '';
/**
 * String in the format "'type1','type2'" that can be used in SQL clauses like
 * "WHERE q.type NOT IN ($QTYPE_EXCLUDE_FROM_RANDOM)".
 */
$QTYPE_EXCLUDE_FROM_RANDOM = '';

/**
 * Add a new question type to the various global arrays above.
 *
 * @param object $qtype An instance of the new question type class.
 */
function question_register_questiontype($qtype) {
    global $QTYPES, $QTYPE_MANUAL, $QTYPE_EXCLUDE_FROM_RANDOM;

    $name = $qtype->name();
    $QTYPES[$name] = $qtype;
    if ($qtype->is_manual_graded()) {
        if ($QTYPE_MANUAL) {
            $QTYPE_MANUAL .= ',';
        }
        $QTYPE_MANUAL .= "'$name'";
    }
    if (!$qtype->is_usable_by_random()) {
        if ($QTYPE_EXCLUDE_FROM_RANDOM) {
            $QTYPE_EXCLUDE_FROM_RANDOM .= ',';
        }
        $QTYPE_EXCLUDE_FROM_RANDOM .= "'$name'";
    }
}

require_once("$CFG->dirroot/question/type/questiontype.php");

// Load the questiontype.php file for each question type
// These files in turn call question_register_questiontype()
// with a new instance of each qtype class.
$qtypenames= get_list_of_plugins('question/type');
foreach($qtypenames as $qtypename) {
    // Instanciates all plug-in question types
    $qtypefilepath= "$CFG->dirroot/question/type/$qtypename/questiontype.php";

    // echo "Loading $qtypename<br/>"; // Uncomment for debugging
    if (is_readable($qtypefilepath)) {
        require_once($qtypefilepath);
    }
}

/**
 * An array of question type names translated to the user's language, suitable for use when
 * creating a drop-down menu of options.
 *
 * Long-time Moodle programmers will realise that this replaces the old $QTYPE_MENU array.
 * The array returned will only hold the names of all the question types that the user should
 * be able to create directly. Some internal question types like random questions are excluded.
 *
 * @return array an array of question type names translated to the user's language.
 */
function question_type_menu() {
    global $QTYPES;
    static $menu_options = null;
    if (is_null($menu_options)) {
        $menu_options = array();
        foreach ($QTYPES as $name => $qtype) {
            $menuname = $qtype->menu_name();
            if ($menuname) {
                $menu_options[$name] = $menuname;
            }
        }
    }
    return $menu_options;
}

/// OTHER CLASSES /////////////////////////////////////////////////////////

/**
 * This holds the options that are set by the course module
 */
class cmoptions {
    /**
    * Whether a new attempt should be based on the previous one. If true
    * then a new attempt will start in a state where all responses are set
    * to the last responses from the previous attempt.
    */
    var $attemptonlast = false;

    /**
    * Various option flags. The flags are accessed via bitwise operations
    * using the constants defined in the CONSTANTS section above.
    */
    var $optionflags = QUESTION_ADAPTIVE;

    /**
    * Determines whether in the calculation of the score for a question
    * penalties for earlier wrong responses within the same attempt will
    * be subtracted.
    */
    var $penaltyscheme = true;

    /**
    * The maximum time the user is allowed to answer the questions withing
    * an attempt. This is measured in minutes so needs to be multiplied by
    * 60 before compared to timestamps. If set to 0 no timelimit will be applied
    */
    var $timelimit = 0;

    /**
    * Timestamp for the closing time. Responses submitted after this time will
    * be saved but no credit will be given for them.
    */
    var $timeclose = 9999999999;

    /**
    * The id of the course from withing which the question is currently being used
    */
    var $course = SITEID;

    /**
    * Whether the answers in a multiple choice question should be randomly
    * shuffled when a new attempt is started.
    */
    var $shuffleanswers = true;

    /**
    * The number of decimals to be shown when scores are printed
    */
    var $decimalpoints = 2;
}


/// FUNCTIONS //////////////////////////////////////////////////////

/**
 * Returns an array of names of activity modules that use this question
 *
 * @param object $questionid
 * @return array of strings
 */
function question_list_instances($questionid) {
    global $CFG;
    $instances = array();
    $modules = get_records('modules');
    foreach ($modules as $module) {
        $fullmod = $CFG->dirroot . '/mod/' . $module->name;
        if (file_exists($fullmod . '/lib.php')) {
            include_once($fullmod . '/lib.php');
            $fn = $module->name.'_question_list_instances';
            if (function_exists($fn)) {
                $instances = $instances + $fn($questionid);
            }
        }
    }
    return $instances;
}

/**
 * Determine whether there arey any questions belonging to this context, that is whether any of its
 * question categories contain any questions. This will return true even if all the questions are
 * hidden.
 *
 * @param mixed $context either a context object, or a context id.
 * @return boolean whether any of the question categories beloning to this context have
 *         any questions in them.
 */
function question_context_has_any_questions($context) {
    global $CFG;
    if (is_object($context)) {
        $contextid = $context->id;
    } else if (is_numeric($context)) {
        $contextid = $context;
    } else {
        print_error('invalidcontextinhasanyquestions', 'question');
    }
    return record_exists_sql('SELECT * FROM ' . $CFG->prefix . 'question q ' .
            'JOIN ' . $CFG->prefix . 'question_categories qc ON qc.id = q.category ' .
            "WHERE qc.contextid = $contextid AND q.parent = 0");
}

/**
 * Returns list of 'allowed' grades for grade selection
 * formatted suitably for dropdown box function
 * @return object ->gradeoptionsfull full array ->gradeoptions +ve only
 */
function get_grade_options() {
    // define basic array of grades
    $grades = array(
        1.00,
        0.90,
        0.83333,
        0.80,
        0.75,
        0.70,
        0.66666,
        0.60,
        0.50,
        0.40,
        0.33333,
        0.30,
        0.25,
        0.20,
        0.16666,
        0.142857,
        0.125,
        0.11111,
        0.10,
        0.05,
        0);

    // iterate through grades generating full range of options
    $gradeoptionsfull = array();
    $gradeoptions = array();
    foreach ($grades as $grade) {
        $percentage = 100 * $grade;
        $neggrade = -$grade;
        $gradeoptions["$grade"] = "$percentage %";
        $gradeoptionsfull["$grade"] = "$percentage %";
        $gradeoptionsfull["$neggrade"] = -$percentage." %";
    }
    $gradeoptionsfull["0"] = $gradeoptions["0"] = get_string("none");

    // sort lists
    arsort($gradeoptions, SORT_NUMERIC);
    arsort($gradeoptionsfull, SORT_NUMERIC);

    // construct return object
    $grades = new stdClass;
    $grades->gradeoptions = $gradeoptions;
    $grades->gradeoptionsfull = $gradeoptionsfull;

    return $grades;
}

/**
 * match grade options
 * if no match return error or match nearest
 * @param array $gradeoptionsfull list of valid options
 * @param int $grade grade to be tested
 * @param string $matchgrades 'error' or 'nearest'
 * @return mixed either 'fixed' value or false if erro
 */
function match_grade_options($gradeoptionsfull, $grade, $matchgrades='error') {
    // if we just need an error...
    if ($matchgrades=='error') {
        foreach($gradeoptionsfull as $value => $option) {
            // slightly fuzzy test, never check floats for equality :-)
            if (abs($grade-$value)<0.00001) {
                return $grade;
            }
        }
        // didn't find a match so that's an error
        return false;
    }
    // work out nearest value
    else if ($matchgrades=='nearest') {
        $hownear = array();
        foreach($gradeoptionsfull as $value => $option) {
            if ($grade==$value) {
                return $grade;
            }
            $hownear[ $value ] = abs( $grade - $value );
        }
        // reverse sort list of deltas and grab the last (smallest)
        asort( $hownear, SORT_NUMERIC );
        reset( $hownear );
        return key( $hownear );
    }
    else {
        return false;
    }
}

/**
 * Tests whether a category is in use by any activity module
 *
 * @return boolean
 * @param integer $categoryid
 * @param boolean $recursive Whether to examine category children recursively
 */
function question_category_isused($categoryid, $recursive = false) {

    //Look at each question in the category
    if ($questions = get_records('question', 'category', $categoryid)) {
        foreach ($questions as $question) {
            if (count(question_list_instances($question->id))) {
                return true;
            }
        }
    }

    //Look under child categories recursively
    if ($recursive) {
        if ($children = get_records('question_categories', 'parent', $categoryid)) {
            foreach ($children as $child) {
                if (question_category_isused($child->id, $recursive)) {
                    return true;
                }
            }
        }
    }

    return false;
}

/**
 * Deletes all data associated to an attempt from the database
 *
 * @param integer $attemptid The id of the attempt being deleted
 */
function delete_attempt($attemptid) {
    global $QTYPES;

    $states = get_records('question_states', 'attempt', $attemptid);
    if ($states) {
        $stateslist = implode(',', array_keys($states));

        // delete question-type specific data
        foreach ($QTYPES as $qtype) {
            $qtype->delete_states($stateslist);
        }
    }

    // delete entries from all other question tables
    // It is important that this is done only after calling the questiontype functions
    delete_records("question_states", "attempt", $attemptid);
    delete_records("question_sessions", "attemptid", $attemptid);
    delete_records("question_attempts", "id", $attemptid);
}

/**
 * Deletes question and all associated data from the database
 *
 * It will not delete a question if it is used by an activity module
 * @param object $question  The question being deleted
 */
function delete_question($questionid) {
    global $QTYPES;

    if (!$question = get_record('question', 'id', $questionid)) {
        // In some situations, for example if this was a child of a
        // Cloze question that was previously deleted, the question may already
        // have gone. In this case, just do nothing.
        return;
    }

    // Do not delete a question if it is used by an activity module
    if (count(question_list_instances($questionid))) {
        return;
    }

    // delete questiontype-specific data
    question_require_capability_on($question, 'edit');
    if ($question) {
        if (isset($QTYPES[$question->qtype])) {
            $QTYPES[$question->qtype]->delete_question($questionid);
        }
    } else {
        echo "Question with id $questionid does not exist.<br />";
    }

    if ($states = get_records('question_states', 'question', $questionid)) {
        $stateslist = implode(',', array_keys($states));

        // delete questiontype-specific data
        foreach ($QTYPES as $qtype) {
            $qtype->delete_states($stateslist);
        }
    }

    // delete entries from all other question tables
    // It is important that this is done only after calling the questiontype functions
    delete_records("question_answers", "question", $questionid);
    delete_records("question_states", "question", $questionid);
    delete_records("question_sessions", "questionid", $questionid);

    // Now recursively delete all child questions
    if ($children = get_records('question', 'parent', $questionid)) {
        foreach ($children as $child) {
            if ($child->id != $questionid) {
                delete_question($child->id);
            }
        }
    }

    // Finally delete the question record itself
    delete_records('question', 'id', $questionid);

    return;
}

/**
 * All question categories and their questions are deleted for this course.
 *
 * @param object $mod an object representing the activity
 * @param boolean $feedback to specify if the process must output a summary of its work
 * @return boolean
 */
function question_delete_course($course, $feedback=true) {
    //To store feedback to be showed at the end of the process
    $feedbackdata   = array();

    //Cache some strings
    $strcatdeleted = get_string('unusedcategorydeleted', 'quiz');
    $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
    $categoriescourse = get_records('question_categories', 'contextid', $coursecontext->id, 'parent', 'id, parent, name');

    if ($categoriescourse) {

        //Sort categories following their tree (parent-child) relationships
        //this will make the feedback more readable
        $categoriescourse = sort_categories_by_tree($categoriescourse);

        foreach ($categoriescourse as $category) {

            //Delete it completely (questions and category itself)
            //deleting questions
            if ($questions = get_records("question", "category", $category->id)) {
                foreach ($questions as $question) {
                    delete_question($question->id);
                }
                delete_records("question", "category", $category->id);
            }
            //delete the category
            delete_records('question_categories', 'id', $category->id);

            //Fill feedback
            $feedbackdata[] = array($category->name, $strcatdeleted);
        }
        //Inform about changes performed if feedback is enabled
        if ($feedback) {
            $table = new stdClass;
            $table->head = array(get_string('category','quiz'), get_string('action'));
            $table->data = $feedbackdata;
            print_table($table);
        }
    }
    return true;
}

/**
 * Category is about to be deleted,
 * 1/ All question categories and their questions are deleted for this course category.
 * 2/ All questions are moved to new category
 *
 * @param object $category course category object
 * @param object $newcategory empty means everything deleted, otherwise id of category where content moved
 * @param boolean $feedback to specify if the process must output a summary of its work
 * @return boolean
 */
function question_delete_course_category($category, $newcategory, $feedback=true) {
    $context = get_context_instance(CONTEXT_COURSECAT, $category->id);
    if (empty($newcategory)) {
        $feedbackdata   = array(); // To store feedback to be showed at the end of the process
        $rescueqcategory = null; // See the code around the call to question_save_from_deletion.
        $strcatdeleted = get_string('unusedcategorydeleted', 'quiz');

        // Loop over question categories.
        if ($categories = get_records('question_categories', 'contextid', $context->id, 'parent', 'id, parent, name')) {
            foreach ($categories as $category) {

                // Deal with any questions in the category.
                if ($questions = get_records('question', 'category', $category->id)) {

                    // Try to delete each question.
                    foreach ($questions as $question) {
                        delete_question($question->id);
                    }

                    // Check to see if there were any questions that were kept because they are
                    // still in use somehow, even though quizzes in courses in this category will
                    // already have been deteted. This could happen, for example, if questions are
                    // added to a course, and then that course is moved to another category (MDL-14802).
                    $questionids = get_records_select_menu('question', 'category = ' . $category->id, '', 'id,1');
                    if (!empty($questionids)) {
                        if (!$rescueqcategory = question_save_from_deletion(implode(',', array_keys($questionids)),
                                get_parent_contextid($context), print_context_name($context), $rescueqcategory)) {
                            return false;
                       }
                       $feedbackdata[] = array($category->name, get_string('questionsmovedto', 'question', $rescueqcategory->name));
                    }
                }

                // Now delete the category.
                if (!delete_records('question_categories', 'id', $category->id)) {
                    return false;
                }
                $feedbackdata[] = array($category->name, $strcatdeleted);

            } // End loop over categories.
        }

        // Output feedback if requested.
        if ($feedback and $feedbackdata) {
            $table = new stdClass;
            $table->head = array(get_string('questioncategory','question'), get_string('action'));
            $table->data = $feedbackdata;
            print_table($table);
        }

    } else {
        // Move question categories ot the new context.
        if (!$newcontext = get_context_instance(CONTEXT_COURSECAT, $newcategory->id)) {
            return false;
        }
        if (!set_field('question_categories', 'contextid', $newcontext->id, 'contextid', $context->id)) {
            return false;
        }
        if ($feedback) {
            $a = new stdClass;
            $a->oldplace = print_context_name($context);
            $a->newplace = print_context_name($newcontext);
            notify(get_string('movedquestionsandcategories', 'question', $a), 'notifysuccess');
        }
    }

    return true;
}

/**
 * Enter description here...
 *
 * @param string $questionids list of questionids
 * @param object $newcontext the context to create the saved category in.
 * @param string $oldplace a textual description of the think being deleted, e.g. from get_context_name
 * @param object $newcategory
 * @return mixed false on
 */
function question_save_from_deletion($questionids, $newcontextid, $oldplace, $newcategory = null) {
    // Make a category in the parent context to move the questions to.
    if (is_null($newcategory)) {
        $newcategory = new object();
        $newcategory->parent = 0;
        $newcategory->contextid = $newcontextid;
        $newcategory->name = addslashes(get_string('questionsrescuedfrom', 'question', $oldplace));
        $newcategory->info = addslashes(get_string('questionsrescuedfrominfo', 'question', $oldplace));
        $newcategory->sortorder = 999;
        $newcategory->stamp = make_unique_id_code();
        if (!$newcategory->id = insert_record('question_categories', $newcategory)) {
            return false;
        }
    }

    // Move any remaining questions to the 'saved' category.
    if (!question_move_questions_to_category($questionids, $newcategory->id)) {
        return false;
    }
    return $newcategory;
}

/**
 * All question categories and their questions are deleted for this activity.
 *
 * @param object $cm the course module object representing the activity
 * @param boolean $feedback to specify if the process must output a summary of its work
 * @return boolean
 */
function question_delete_activity($cm, $feedback=true) {
    //To store feedback to be showed at the end of the process
    $feedbackdata   = array();

    //Cache some strings
    $strcatdeleted = get_string('unusedcategorydeleted', 'quiz');
    $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
    if ($categoriesmods = get_records('question_categories', 'contextid', $modcontext->id, 'parent', 'id, parent, name')){
        //Sort categories following their tree (parent-child) relationships
        //this will make the feedback more readable
        $categoriesmods = sort_categories_by_tree($categoriesmods);

        foreach ($categoriesmods as $category) {

            //Delete it completely (questions and category itself)
            //deleting questions
            if ($questions = get_records("question", "category", $category->id)) {
                foreach ($questions as $question) {
                    delete_question($question->id);
                }
                delete_records("question", "category", $category->id);
            }
            //delete the category
            delete_records('question_categories'