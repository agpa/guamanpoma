<?php /**/ ?><?php
/**
 * OO AJAX Implementation for PHP
 *
 * SVN Rev: $Id: AJAX.php,v 1.1.2.1 2008/10/03 07:09:50 nicolasconnault Exp $
 *
 * @category  HTML
 * @package   AJAX
 * @author    Joshua Eichorn <josh@bluga.net>
 * @author    Arpad Ray <arpad@php.net>
 * @author    David Coallier <davidc@php.net>
 * @author    Elizabeth Smith <auroraeosrose@gmail.com>
 * @copyright 2005-2008 Joshua Eichorn, Arpad Ray, David Coallier, Elizabeth Smith
 * @license   http://www.opensource.org/licenses/lgpl-license.php   LGPL
 * @version   Release: 0.5.6
 * @link      http://pear.php.net/package/HTML_AJAX
 */

/**
 * This is a quick hack, loading serializers as needed doesn't work in php5
 */
require_once "HTML/AJAX/Serializer/JSON.php";
require_once "HTML/AJAX/Serializer/Null.php";
require_once "HTML/AJAX/Serializer/Error.php";
require_once "HTML/AJAX/Serializer/XML.php";
require_once "HTML/AJAX/Serializer/PHP.php";
require_once 'HTML/AJAX/Debug.php';
    
/**
 * OO AJAX Implementation for PHP
 *
 * @category  HTML
 * @package   AJAX
 * @author    Joshua Eichorn <josh@bluga.net>
 * @author    Arpad Ray <arpad@php.net>
 * @author    David Coallier <davidc@php.net>
 * @author    Elizabeth Smith <auroraeosrose@gmail.com>
 * @copyright 2005-2008 Joshua Eichorn, Arpad Ray, David Coallier, Elizabeth Smith
 * @license   http://www.opensource.org/licenses/lgpl-license.php   LGPL
 * @version   Release: 0.5.6
 * @link      http://pear.php.net/package/HTML_AJAX
 */
class HTML_AJAX
{
    /**
     * An array holding the instances were exporting
     *
     * key is the exported name
     *
     * row format is 
     * <code>
     * array('className'=>'','exportedName'=>'','instance'=>'','exportedMethods=>'')
     * </code>
     *
     * @var object
     * @access private
     */    
    var $_exportedInstances = array();

    /**
     * Set the server url in the generated stubs to this value
     * If set to false, serverUrl will not be set
     * @var false|string
     */
    var $serverUrl = false;

    /**
     * What encoding your going to use for serializing data 
     * from php being sent to javascript.
     *
     * @var string  JSON|PHP|Null
     */
    var $serializer = 'JSON';

    /**
     * What encoding your going to use for unserializing data sent from javascript
     * @var string  JSON|PHP|Null
     */
    var $unserializer = 'JSON';

    /**
     * Option to use loose typing for JSON encoding
     * @var bool
     * @access public
     */
    var $jsonLooseType = true;

    /**
     * Content-type map
     *
     * Used in to automatically choose serializers as needed
     */
    var $contentTypeMap = array(
            'JSON'  => 'application/json',
            'XML'   => 'application/xml',
            'Null'  => 'text/plain',
            'Error' => 'application/error',
            'PHP'   => 'application/php-serialized',
            'Urlencoded' => 'application/x-www-form-urlencoded'
        );
    
    /**
     * This is the debug variable that we will be passing the
     * HTML_AJAX_Debug instance to.
     *
     * @param object HTML_AJAX_Debug
     */
    var $debug;

    /**
     * This is to tell if debug is enabled or not. If so, then
     * debug is called, instantiated then saves the file and such.
     */
    var $debugEnabled = false;
    
    /**
     * This puts the error into a session variable is set to true.
     * set to false by default.
     *
     * @access public
     */
    var $debugSession = false;

    /**
     * Boolean telling if the Content-Length header should be sent. 
     *
     * If your using a gzip handler on an output buffer, or run into 
     * any compatability problems, try disabling this.
     *
     * @access public
     * @var boolean
     */
    var $sendContentLength = true;

    /**
     * Make Generated code compatible with php4 by lowercasing all 
     * class/method names before exporting to JavaScript.
     *
     * If you have code that works on php4 but not on php5 then setting 
     * this flag can fix the problem. The recommended solution is too 
     * specify the class and method names when registering the class 
     * letting you have function case in php4 as well
     *
     * @access public
     * @var boolean
     */
    var $php4CompatCase = false;

    /**
     * Automatically pack all generated JavaScript making it smaller
     *
     * If your using output compression this might not make sense
     */
    var $packJavaScript = false;

    /**
     * Holds current payload info
     *
     * @access private
     * @var string
     */
    var $_payload;

    /**
     * Holds iframe id IF this is an iframe xmlhttprequest
     *
     * @access private
     * @var string
     */
    var $_iframe;

    /**
     * Holds the list of classes permitted to be unserialized
     *
     * @access private
     * @var array
     */
    var $_allowedClasses = array();
    
    /**
     * Holds serializer instances
     */
    var $_serializers = array();
    
    /**
     * PHP callbacks we're exporting
     */
    var $_validCallbacks = array();

    /**
     * Interceptor instance
     */
    var $_interceptor = false;

    /**
     * Set a class to handle requests
     *
     * @param object &$instance       An instance to export
     * @param mixed  $exportedName    Name used for the javascript class, 
     *                                if false the name of the php class is used
     * @param mixed  $exportedMethods If false all functions without a _ prefix 
     *                                are exported, if an array only the methods 
     *                                listed in the array are exported
     *
     * @return void
     */
    function registerClass(&$instance, $exportedName = false, 
        $exportedMethods = false)
    {
        $className = strtolower(get_class($instance));

        if ($exportedName === false) {
            $exportedName = get_class($instance);
            if ($this->php4CompatCase) {
                $exportedName = strtolower($exportedName);
            }
        }

        if ($exportedMethods === false) {
            $exportedMethods = $this->_getMethodsToExport($className);
        }


        $index                                               = strtolower($exportedName);
        $this->_exportedInstances[$index]                    = array();
        $this->_exportedInstances[$index]['className']       = $className;
        $this->_exportedInstances[$index]['exportedName']    = $exportedName;
        $this->_exportedInstances[$index]['instance']        =& $instance;
        $this->_exportedInstances[$index]['exportedMethods'] = $exportedMethods;
    }

    /**
     * Get a list of methods in a class to export
     *
     * This function uses get_class_methods to get a list of callable methods, 
     * so if you're on PHP5 extending this class with a class you want to export 
     * should export its protected methods, while normally only its public methods 
     * would be exported. 