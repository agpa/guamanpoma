/**
 * library for ajaxcourse formats, the classes and related functions for
 * sections and resources.
 *
 * This library requires a 'main' object created in calling document.
 *
 * Drag and drop notes:
 *
 *   Dropping an activity or resource on a section will always add the activity
 *   or resource at the end of that section.
 *
 *   Dropping an activity or resource on another activity or resource will
 *   always move the former just above the latter.
 *
 * $Id: section_classes.js,v 1.35.2.11 2009/10/16 09:37:05 moodler Exp $
 */


/**
 * section_class
 */
function section_class(id, group, config, isDraggable) {
    this.init_section(id, group, config, isDraggable);
}

YAHOO.extend(section_class, YAHOO.util.DDProxy);


section_class.prototype.debug = false;


section_class.prototype.init_section = function(id, group, config, isDraggable) {

    if (!id) {
        return;
    }

    this.is = 'section';
    this.sectionId = null; // Section number. This is NOT the section id from
                            // the database.

    if (!isDraggable) {
        this.initTarget(id, group, config);
        this.removeFromGroup('sections');
    } else {
        this.init(id, group, config);
        this.handle = null;
    }

    this.createFrame();
    this.isTarget = true;

    this.resources = [];
    this.numberDisplay = null; // Used to display the section number on the top left
                                // of the section. Not used in all course formats.
    this.summary = null;
    this.content_td = null;
    this.hidden = false;
    this.highlighted = false;
    this.showOnly = false;
    this.resources_ul = null;
    this.process_section();

    this.viewButton = null;
    this.highlightButton = null;
    this.showOnlyButton = null;
    this.init_buttons();

    if (isDraggable) {
        this.add_handle();
    }
    if (this.debug) {
        YAHOO.log("init_section "+id+" draggable="+isDraggable);
    }
    if (YAHOO.util.Dom.hasClass(this.getEl(),'hidden')) {
        this.toggle_hide(null,null,true);
    }
}


section_class.prototype.init_buttons = function() {
    var commandContainer = YAHOO.util.Dom.getElementsByClassName('right',null,this.getEl())[0];

    //clear all but show only button
    var commandContainerCount = commandContainer.childNodes.length;

    for (var i=(commandContainerCount-1); i>0; i--) {
        commandContainer.removeChild(commandContainer.childNodes[i])
    }

    if (main.getString('courseformat', this.sectionId) != "weeks" && this.sectionId > 0) {
        var highlightbutton = main.mk_button('div', '/i/marker.gif', main.getString('marker', this.sectionId));
        YAHOO.util.Event.addListener(highlightbutton, 'click', this.mk_marker, this, true);
        commandContainer.appendChild(highlightbutton);
        this.highlightButton = highlightbutton;
    }
    
    if (this.sectionId > 0 ) {        
        var viewbutton = main.mk_button('div', '/i/hide.gif', main.getString('hidesection', this.sectionId), [['title', main.portal.strings['hide'] ]]);
    YAHOO.util.Event.addListener(viewbutton, 'click', this.toggle_hide, this,true);
    commandContainer.appendChild(viewbutton);
    this.viewButton = viewbutton;
}

}

section_class.prototype.add_handle = function() {
    var handleRef = main.mk_button('a', '/i/move_2d.gif', main.getString('movesection', this.sectionId),
            [['title', main.portal.strings['move'] ], ['style','cursor:move']]);

    YAHOO.util.Dom.generateId(handleRef, 'sectionHandle');

    this.handle = handleRef;

    this.getEl().childNodes[0].appendChild(handleRef);
    this.setHandleElId(this.handle.id);
}


section_class.prototype.process_section = function() {
    this.content_td = this.getEl().childNodes[1];

    if (YAHOO.util.Dom.hasClass(this.getEl(),'current')) {
        this.highlighted = true;
        main.marker = this;
    }

    // Create holder for display number for access later

    this.numberDisplay = document.createElement('div');
    this.numberDisplay.innerHTML = this.getEl().childNodes[0].innerHTML;
    this.getEl().childNodes[0].innerHTML = '';
    this.getEl().childNodes[0].appendChild(this.numberDisplay);

    this.sectionId = this.id.