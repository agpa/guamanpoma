<?php /**/ ?><?php // $Id: iCalendar_properties.php,v 1.1 2006/01/13 15:06:25 defacer Exp $

/**
 *  BENNU - PHP iCalendar library
 *  (c) 2005-2006 Ioannis Papaioannou (pj@moodle.org). All rights reserved.
 *
 *  Released under the LGPL.
 *
 *  See http://bennu.sourceforge.net/ for more information and downloads.
 *
 * @author Ioannis Papaioannou 
 * @version $Id: iCalendar_properties.php,v 1.1 2006/01/13 15:06:25 defacer Exp $
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 */

class iCalendar_property {
    // Properties can have parameters, but cannot have other properties or components

    var $parent_component = NULL;
    var $value            = NULL;
    var $parameters       = NULL;
    var $valid_parameters = NULL;

    // These are common for 95% of properties, so define them here and override as necessary
    var $val_multi        = false;
    var $val_default      = NULL;

    function iCalendar_property() {
        $this->construct();
    }

    function construct() {
        $this->parameters = array();
    }

    // If some property needs extra care with its parameters, override this
    // IMPORTANT: the parameter name MUST BE CAPITALIZED!
    function is_valid_parameter($parameter, $value) {

        if(is_array($value)) {
            if(!iCalendar_parameter::multiple_values_allowed($parameter)) {
                return false;
            }
            foreach($value as $item) {
                if(!iCalendar_parameter::is_valid_value($this, $parameter, $item)) {
                    return false;
                }
            }
            return true;
        }

        return iCalendar_parameter::is_valid_value($this, $parameter, $value);
    }

    function invariant_holds() {
        return true;
    }

    // If some property is very picky about its values, it should do the work itself
    // Only data type validation is done here
    function is_valid_value($value) {
        if(is_array($value)) {
            if(!$this->val_multi) {
                return false;
            }
            else {
                foreach($value as $oneval) {
                    if(!rfc2445_is_valid_value($oneval, $this->val_type)) {
                        return false;
                    }
                }
            }
            return true;
        }
        return rfc2445_is_valid_value($value, $this->val_type);
    }

    function default_value() {
        return $this->val_default;
    }

    function set_parent_component($componentname) {
        if(class_exists('iCalendar_'.strtolower(substr($componentname, 1)))) {
            $this->parent_component = strtoupper($componentname);
            return true;
        }

        return false;
    }

    function set_value($value) {
        if($this->is_valid_value($value)) {
            // This transparently formats any value type according to the iCalendar specs
            if(is_array($value)) {
                foreach($value as $key => $item) {
                    $value[$key] = rfc2445_do_value_formatting($item, $this->val_type);
                }
                $this->value = implode(',', $value);
            }
            else {
                $this->value = rfc2445_do_value_formatting($value, $this->val_type);
            }
            
            return true;
        }
        return false;
    }

    function get_value() {
        // First of all, assume that we have multiple values
        $valarray = explode('\\,', $this->value);

        // Undo transparent formatting
        $replace_function = create_function('$a', 'return rfc2445_undo_value_formatting($a, '.$this->val_type.');');
        $valarray = array_map($replace_function, $valarray);

        // Now, if this property cannot have multiple values, don't return as an array
        if(!$this->val_multi) {
            return $valarray[0];
        }

        // Otherwise return an array even if it has one element, for uniformity
        return $valarray;

    }

    function set_parameter($name, $value) {

        // Uppercase
        $name = strtoupper($name);

        // Are we trying to add a valid parameter?
        $xname = false;
        if(!isset($this->valid_para