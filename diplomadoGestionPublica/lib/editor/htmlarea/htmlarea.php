<?php /**/ ?><?php
    include("../../../config.php");
    require_once($CFG->dirroot.'/lib/languages.php');

    $id            = optional_param('id', SITEID, PARAM_INT);
    $httpsrequired = optional_param('httpsrequired', 0, PARAM_BOOL); //flag indicating editor on page with required https

    require_course_login($id);

    $lastmodified = filemtime("htmlarea.php");
    $lifetime = 1800;

    // Commenting this out since it's creating problems
    // where solution seem to be hard to find...
    // http://moodle.org/mod/forum/discuss.php?d=34376
    //if ( function_exists('ob_gzhandler') ) {
    //    ob_start("ob_gzhandler");
    //}

    header("Content-type: application/x-javascript; charset: utf-8");  // Correct MIME type
    header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastmodified) . " GMT");
    header("Expires: " . gmdate("D, d M Y H:i:s", time() + $lifetime) . " GMT");
    header("Cache-control: max_age = $lifetime");
    header("Pragma: ");

    $lang = current_language();

    if (empty($lang)) {
        $lang = "en";
    }

    if ($httpsrequired or (!empty($_SERVER['HTTPS']) and $_SERVER['HTTPS'] != 'off')) {
        $url = preg_replace('|https?://[^/]+|', '', $CFG->wwwroot).'/lib/editor/htmlarea/';
    } else {
        $url = $CFG->wwwroot.'/lib/editor/htmlarea/';
    }

    $strheading = get_string("heading", "editor");
    $strnormal = get_string("normal", "editor");
    $straddress = get_string("address", "editor");
    $strpreformatted = get_string("preformatted", "editor");
    $strlang = get_string('lang', 'editor');
    $strmulti = get_string('multi', 'editor');
?>

// htmlArea v3.0 - Copyright (c) 2002, 2003 interactivetools.com, inc.
// This copyright notice MUST stay intact for use (see license.txt).
//
// Portions (c) dynarch.com, 2003-2004
//
// A free WYSIWYG editor replacement for <textarea> fields.
// For full source code and docs, visit http://www.interactivetools.com/
//
// Version 3.0 developed by Mihai Bazon.
//   http://dynarch.com/mishoo
//
// $Id: htmlarea.php,v 1.24.2.8 2009/08/27 03:21:33 jerome Exp $

if (typeof _editor_url == "string") {
    // Leave exactly one backslash at the end of _editor_url
    _editor_url = _editor_url.replace(/\x2f*$/, '/');
} else {
    //alert("WARNING: _editor_url is not set!  You should set this variable to the editor files path; it should preferably be an absolute path, like in '/htmlarea', but it can be relative if you prefer.  Further we will try to load the editor files correctly but we'll probably fail.");
    _editor_url = '<?php echo $url; ?>';// we need relative path to site root for editor in pages wit hrequired https
}

// make sure we have a language
if (typeof _editor_lang == "string") {
    _editor_lang = "en"; // should always be english in moodle.
} else {
    _editor_lang = "en";
}

// Creates a new HTMLArea object.  Tries to replace the textarea with the given
// ID with it.
function HTMLArea(textarea, config) {
    if (HTMLArea.checkSupportedBrowser()) {
        if (typeof config == "undefined") {
            this.config = new HTMLArea.Config();
        } else {
            this.config = config;
        }
        this._htmlArea = null;
        this._textArea = textarea;
        this._editMode = "wysiwyg";
        this.plugins = {};
        this._timerToolbar = null;
        this._timerUndo = null;
        this._undoQueue = new Array(this.config.undoSteps);
        this._undoPos = -1;
        this._customUndo = true;
        this._mdoc = document; // cache the document, we need it in plugins
        this.doctype = '';
        this.dropdowns = [];   // Array of select elements in the toolbar
    }
};

// load some scripts
(function() {
    var scripts = HTMLArea._scripts = [ _editor_url + "htmlarea.js",
                        _editor_url + "dialog.js",
                        _editor_url + "popupwin.js" ];
    var head = document.getElementsByTagName("head")[0];
    // start from 1, htmlarea.js is already loaded
    for (var i = 1; i < scripts.length; ++i) {
        var script = document.createElement("script");
        script.src = scripts[i];
        head.appendChild(script);
    }
})();

// cache some regexps
HTMLArea.RE_tagName = /(<\/|<)\s*([^ \t\n>]+)/ig;
HTMLArea.RE_doctype = /(<!doctype((.|\n)*?)>)\n?/i;
HTMLArea.RE_head    = /<head>((.|\n)*?)<\/head>/i;
HTMLArea.RE_body    = /<body>((.|\n)*?)<\/body>/i;
HTMLArea.RE_blocktag = /^(h1|h2|h3|h4|h5|h6|p|address|pre)$/i;
HTMLArea.RE_junktag = /^\/($|\/)/;
// Hopefully a complete list of tags that MSIEs parser will consider
// as possible content tags. Retrieved from
// http://www.echoecho.com/htmlreference.htm
HTMLArea.RE_msietag  = /^\/?(a|abbr|acronym|address|applet|area|b|base|basefont|bdo|bgsound|big|blink|blockquote|body|br|button|caption|center|cite|code|col|colgroup|comment|dd|del|dfn|dir|div|dl|dt|em|embed|fieldset|font|form|frame|frameset|h1|h2|h3|h4|h5|h6|head|hr|html|i|iframe|ilayer|img|input|ins|isindex|kbd|keygen|label|layer|legend|li|link|map|marquee|menu|meta|multicol|nobr|noembed|noframes|nolayer|noscript|object|ol|optgroup|option|p|param|plaintext|pre|q|s|samp|script|select|server|small|spacer|span|strike|strong|style|sub|sup|table|tbody|td|textarea|tfoot|th|thead|title|tr|tt|u|ul|var)$/i

HTMLArea.Config = function () {
    this.version = "3.0";

    this.width = "auto";
    this.height = "auto";

    // enable creation of a status bar?
    this.statusBar = true;

    // maximum size of the undo queue
    this.undoSteps = 20;

    // the time interval at which undo samples are taken
    this.undoTimeout = 500; // 1/2 sec.

    // the next parameter specifies whether the toolbar should be included
    // in the size or not.
    this.sizeIncludesToolbar = true;

    // if true then HTMLArea will retrieve the full HTML, starting with the
    // <HTML> tag.
    this.fullPage = false;

    // style included in the iframe document
    this.pageStyle = "body { background-color: #fff; font-family: 'Times New Roman', Times; } \n .lang { background-color: #dee; }";

    // set to true if you want Word code to be cleaned upon Paste
    this.killWordOnPaste = true;

    // BaseURL included in the iframe document
    this.baseURL = document.baseURI || document.URL;
    if (this.baseURL && this.baseURL.match(/(.*)\/([^\/]+)/))
        this.baseURL = RegExp.$1 + "/";

    // URL-s
    this.imgURL = "images/";
    this.popupURL = "popups/";

    this.toolbar = [
        [ "fontname", "space",
          "fontsize", "space",
          "formatblock", "space",
          "language", "space",
          "bold", "italic", "underline", "strikethrough", "separator",
          "subscript", "superscript", "separator",
          "clean", "separator", "undo", "redo" ],

        [ "justifyleft", "justifycenter", "justifyright", "justifyfull", "separator",
          "lefttoright", "righttoleft", "separator",
          "insertorderedlist", "insertunorderedlist", "outdent", "indent", "separator",
          "forecolor", "hilitecolor", "separator",
          "inserthorizontalrule", "createanchor", "createlink", "unlink", "nolink", "separator",
          "insertimage", "inserttable",
          "insertsmile", "insertchar", "search_replace",
          <?php if (!empty($CFG->aspellpath) && file_exists($CFG->aspellpath) && !empty($CFG->editorspelling)) {
              echo '"separator","spellcheck",';
            } ?>
          "separator", "htmlmode", "separator", "popupeditor"]
    ];

    this.fontname = {
        "Arial":       'arial,helvetica,sans-serif',
        "Courier New":     'courier new,courier,monospace',
        "Georgia":     'georgia,times new roman,times,serif',
        "Tahoma":      'tahoma,arial,helvetica,sans-serif',
        "Times New Roman": 'times new roman,times,serif',
        "Verdana":     'verdana,arial,helvetica,sans-serif',
        "Impact":           'impact',
        "WingDings":       'wingdings'
    };

    this.fontsize = {
        "1 (8 pt)":  "1",
        "2 (10 pt)": "2",
        "3 (12 pt)": "3",
        "4 (14 pt)": "4",
        "5 (18 pt)": "5",
        "6 (24 pt)": "6",
        "7 (36 pt)": "7"
    };

    this.formatblock = {
        "":"",
        "<?php echo $strheading ?> 1": "h1",
        "<?php echo $strheading ?> 2": "h2",
        "<?php echo $strheading ?> 3": "h3",
        "<?php echo $strheading ?> 4": "h4",
        "<?php echo $strheading ?> 5": "h5",
        "<?php echo $strheading ?> 6": "h6",
        "<?php echo $strnormal ?>": "p",
        "<?php echo $straddress ?>": "address",
        "<?php echo $strpreformatted ?>": "pre"
    };

    this.language = {
        "<?php echo $strlang; ?>":"",
        <?php
        $strlangarray = '';
        foreach ($LANGUAGES as $key => $name) {
            $key = str_replace('_', '-', $key);
            $strlangarray .= '"'.$key.'": "'.$key.'",';
        }
        $strlangarray .= '"'.$strmulti.'": "multi",';

        foreach ($LANGUAGES as $key => $name) {
            $key = str_replace('_', '-', $key);
            $strlangarray .= '"'.$key.' ": "'.$key.'_ML",';
        }
        $strlangarray = substr($strlangarray, 0, -1);
        echo $strlangarray;
        ?>
    };

    this.customSelects = {};

    function cut_copy_paste(e, cmd, obj) {
        e.execCommand(cmd);
    };

    this.btnList = {
        bold: [ "Bold", "ed_format_bold.gif", false, function(e) {e.execCommand("bold");} ],
        italic: [ "Italic", "ed_format_italic.gif", false, function(e) {e.execCommand("italic");} ],
        underline: [ "Underline", "ed_format_underline.gif", false, function(e) {e.execCommand("underline");} ],
        strikethrough: [ "Strikethrough", "ed_format_strike.gif", false, function(e) {e.execCommand("strikethrough");} ],
        subscript: [ "Subscript", "ed_format_sub.gif", false, function(e) {e.execCommand("subscript");} ],
        superscript: [ "Superscript", "ed_format_sup.gif", false, function(e) {e.execCommand("superscript");} ],
        justifyleft: [ "Justify Left", "ed_align_left.gif", false, function(e) {e.execCommand("justifyleft");} ],
        justifycenter: [ "Justify Center", "ed_align_center.gif", false, function(e) {e.execCommand("justifycenter");} ],
        justifyright: [ "Justify Right", "ed_align_right.gif", false, function(e) {e.execCommand("justifyright");} ],
        justifyfull: [ "Justify Full", "ed_align_justify.gif", false, function(e) {e.execCommand("justifyfull");} ],
        insertorderedlist: [ "Ordered List", "ed_list_num.gif", false, function(e) {e.execCommand("insertorderedlist");} ],
        insertunorderedlist: [ "Bulleted List", "ed_list_bullet.gif", false, function(e) {e.execCommand("insertunorderedlist");} ],
        outdent: [ "Decrease Indent", "ed_indent_less.gif", false, function(e) {e.execCommand("outdent");} ],
        indent: [ "Increase Indent", "ed_indent_more.gif", false, function(e) {e.execCommand("indent");} ],
        forecolor: [ "Font Color", "ed_color_fg.gif", false, function(e) {e.execCommand("forecolor");} ],
        hilitecolor: [ "Background Color", "ed_color_bg.gif", false, function(e) {e.execCommand("hilitecolor");} ],
        inserthorizontalrule: [ "Horizontal Rule", "ed_hr.gif", false, function(e) {e.execCommand("inserthorizontalrule");} ],
        createanchor: [ "Create anchor", "ed_anchor.gif", false, function(e) {e.execCommand("createanchor", true);} ],
        createlink: [ "Insert Web Link", "ed_link.gif", false, function(e) {e.execCommand("createlink", true);} ],
        unlink: [ "Remove Link", "ed_unlink.gif", false, function(e) {e.execCommand("unlink");} ],
        nolink: [ "No link", "ed_nolink.gif", false, function(e) {e.execCommand("nolink");} ],
        insertimage: [ "Insert/Modify Image", "ed_image.gif", false, function(e) {e.execCommand("insertimage");} ],
        inserttable: [ "Insert Table", "insert_table.gif", false, function(e) {e.execCommand("inserttable");} ],
        htmlmode: [ "Toggle HTML Source", "ed_html.gif", true, function(e) {e.execCommand("htmlmode");} ],
        popupeditor: [ "Enlarge Editor", "fullscreen_maximize.gif", true, function(e) {e.execCommand("popupeditor");} ],
        about: [ "About this editor", "ed_about.gif", true, function(e) {e.execCommand("about");} ],
        showhelp: [ "Help using editor", "ed_help.gif", true, function(e) {e.execCommand("showhelp");} ],
        undo: [ "Undoes your last action", "ed_undo.gif", false, function(e) {e.execCommand("undo");} ],
        redo: [ "Redoes your last action", "ed_redo.gif", false, function(e) {e.execCommand("redo");} ],
        clean: [ "Clean Word HTML", "ed_wordclean.gif", false, function(e) {e.execCommand("killword"); }],
        lefttoright: [ "Direction left to right", "ed_left_to_right.gif", false, function(e) {e.execCommand("lefttoright");} ],
        righttoleft: [ "Direction right to left", "ed_right_to_left.gif", false, function(e) {e.execCommand("righttoleft");} ],
        <?php if (!empty($CFG->aspellpath) && file_exists($CFG->aspellpath) && !empty($CFG->editorspelling)) {
            echo 'spellcheck: ["Spell-check", "spell-check.gif", false, spellClickHandler ],'."\n";
        }?>
        insertsmile: ["Insert Smiley", "em.icon.smile.gif", false, function(e) {e.execCommand("insertsmile");} ],
        insertchar: [ "Insert Char", "icon_ins_char.gif", false, function(e) {e.execCommand("insertchar");} ],
        search_replace: [ "Search and replace", "ed_replace.gif", false, function(e) {e.execCommand("searchandreplace");} ]
    };

    // initialize tooltips from the I18N module and generate correct image path
    for (var i in this.btnList) {
        var btn = this.btnList[i];
        btn[1] = _editor_url + this.imgURL + btn[1];
        if (typeof HTMLArea.I18N.tooltips[i] != "undefined") {
            btn[0] = HTMLArea.I18N.tooltips[i];
        }
    }
};

HTMLArea.Config.prototype.registerButton = function(id, tooltip, image, textMode, action, context) {
    var the_id;
    if (typeof id == "string") {
        the_id = id;
    } else if (typeof id == "object") {
        the_id = id.id;
    } else {
        alert("ERROR [HTMLArea.Config::registerButton]:\ninvalid arguments");
        return false;
    }
    // check for existing id
    if (typeof this.customSelects[the_id] != "undefined") {
        // alert("WARNING [HTMLArea.Config::registerDropdown]:\nA dropdown with the same ID already exists.");
    }
    if (typeof this.btnList[the_id] != "undefined") {
        // alert("WARNING [HTMLArea.Config::registerDropdown]:\nA button with the same ID already exists.");
    }
    switch (typeof id) {
        case "string": this.btnList[id] = [ tooltip, image, textMode, action, context ]; break;
        case "object": this.btnList[id.id] = [ id.tooltip, id.image, id.textMode, id.action, id.context ]; break;
    }
};

HTMLArea.Config.prototype.registerDropdown = function(object) {
    // check for existing id
    if (typeof this.customSelects[object.id] != "undefined") {
        // alert("WARNING [HTMLArea.Config::registerDropdown]:\nA dropdown with the same ID already exists.");
    }
    if (typeof this.btnList[object.id] != "undefined") {
        // alert("WARNING [HTMLArea.Config::registerDropdown]:\nA button with the same ID already exists.");
    }
    this.customSelects[object.id] = object;
};

HTMLArea.Config.prototype.hideSomeButtons = function(remove) {
    var toolbar = this.toolbar;
    for (var i in toolbar) {
        var line = toolbar[i];
        for (var j = line.length; --j >= 0; ) {
            if (remove.indexOf(" " + line[j] + " ") >= 0) {
                var len = 1;
                if (/separator|space/.test(line[j + 1])) {
                    len = 2;
                }
                line.splice(j, len);
            }
        }
    }
};

/** Helper function: replace all TEXTAREA-s in the document with HTMLArea-s. */
HTMLArea.replaceAll = function(config) {
    var tas = document.getElementsByTagName("textarea");
    for (var i = tas.length; i > 0; (new HTMLArea(tas[--i], config)).generate());
};

/** Helper function: replaces the TEXTAREA with the given ID with HTMLArea. */
HTMLArea.replace = function(id, config) {
    var ta = HTMLArea.getElementById("textarea", id);
    return ta ? (new HTMLArea(ta, config)).generate() : null;
};

// Creates the toolbar and appends it to the _htmlarea
HTMLArea.prototype._createToolbar = function () {
    var editor = this;  // to access this in nested functions

    var toolbar = document.createElement("div");
    this._toolbar = toolbar;
    toolbar.className = "toolbar";
    toolbar.unselectable = "1";
    var tb_row = null;
    var tb_objects = new Object();
    this._toolbarObjects = tb_objects;

    // creates a new line in the toolbar
    function newLine() {
        var table = document.createElement("table");
        table.border = "0px";
        table.cellSpacing = "0px";
        table.cellPadding = "0px";
        toolbar.appendChild(table);
        // TBODY is required for IE, otherwise you don't see anything
        // in the TABLE.
        var tb_body = document.createElement("tbody");
        table.appendChild(tb_body);
        tb_row = document.createElement("tr");
        tb_body.appendChild(tb_row);
    }; // END of function: newLine
    // init first line
    newLine();

    function setButtonStatus(id, newval) {
        var oldval = this[id];
        var el = this.element;
        if (oldval != newval) {
            switch (id) {
                case "enabled":
                if (newval) {
                    HTMLArea._removeClass(el, "buttonDisabled");
                    el.disabled = false;
                } else {
                    HTMLArea._addClass(el, "buttonDisabled");
                    el.disabled = true;
                }
                break;
                case "active":
                if (newval) {
                    HTMLArea._addClass(el, "buttonPressed");
                } else {
                    HTMLArea._removeClass(el, "buttonPressed");
                }
                break;
            }
            this[id] = newval;
        }
    }; // END of function: setButtonStatus

    function createSelect(txt) {
        var options = null;
        var el = null;
        var cmd = null;
        var customSelects = editor.config.customSelects;
        var context = null;
        switch (txt) {
            case "fontsize":
            case "fontname":
            case "formatblock":
            case "language":
            options = editor.config[txt];
            cmd = txt;
            break;
            default:
            // try to fetch it from the list of registered selects
            cmd = txt;
            var dropdown = customSelects[cmd];
            if (typeof dropdown != "undefined") {
                options = dropdown.options;
                context = dropdown.context;
            } else {
                alert("ERROR [createSelect]:\nCan't find the requested dropdown definition");
            }
            break;
        }
        if (options) {
            el = document.createElement("select");
            var obj = {
                name    : txt, // field name
                element : el,   // the UI element (SELECT)
                enabled : true, // is it enabled?
                text    : false, // enabled in text mode?
                cmd : cmd, // command ID
                state   : setButtonStatus, // for changing state
                context : context
            };
            tb_objects[txt] = obj;
            for (var i in options) {
                var op = document.createElement("option");
                op.appendChild(document.createTextNode(i));
                op.value = options[i];
                el.appendChild(op);
            }
            HTMLArea._addEvent(el, "change", function () {
                editor._comboSelected(el, txt);
            });
        }
        editor.dropdowns[txt] = el;  // Keep track of the element for keyboard
                                     // access later.
        return el;
    }; // END of function: createSelect

    // appends a new button to toolbar
    function createButton(txt) {
        // the element that will be created
        var el = null;
        var btn = null;
        switch (txt) {
            case "separator":
            el = document.createElement("div");
            el.className = "separator";
            break;
            case "space":
            el = document.createElement("div");
            el.className = "space";
            break;
            case "linebreak":
            newLine();
            return false;
            case "textindicator":
            el = document.createElement("div");
            el.appendChild(document.createTextNode("A"));
            el.className = "indicator";
            el.title = HTMLArea.I18N.tooltips.textindicator;
            var obj = {
                name    : txt, // the button name (i.e. 'bold')
                element : el, // the UI element (DIV)
                enabled : true, // is it enabled?
                active  : false, // is it pressed?
                text    : false, // enabled in text mode?
                cmd : "textindicator", // the command ID
                state   : setButtonStatus // for changing state
            };
            tb_objects[txt] = obj;
            break;
            default:
            btn = editor.config.btnList[txt];
        }
        if (!el && btn) {
            el = document.createElement("div");
            el.title = btn[0];
            el.className = "button";
            // let's just pretend we have a button object, and
            // assign all the needed information to it.
            var obj = {
                name    : txt, // the button name (i.e. 'bold')
                element : el, // the UI element (DIV)
                enabled : true, // is it enabled?
                active  : false, // is it pressed?
                text    : btn[2], // enabled in text mode?
                cmd : btn[3], // the command ID
                state   : setButtonStatus, // for changing state
                context : btn[4] || null // enabled in a certain context?
            };
            tb_objects[txt] = obj;
            // handlers to emulate nice flat toolbar buttons
            HTMLArea._addEvent(el, "mouseover", function () {
                if (obj.enabled) {
                    HTMLArea._addClass(el, "buttonHover");
                }
            });
            HTMLArea._addEvent(el, "mouseout", function () {
                if (obj.enabled) with (HTMLArea) {
                    _removeClass(el, "buttonHover");
                    _removeClass(el, "buttonActive");
                    (obj.active) && _addClass(el, "buttonPressed");
                }
            });
            HTMLArea._addEvent(el, "mousedown", function (ev) {
                if (obj.enabled) with (HTMLArea) {
                    _addClass(el, "buttonActive");
                    _removeClass(el, "buttonPressed");
                    _stopEvent(is_ie ? window.event : ev);
                }
            });
            // when clicked, do the following:
            HTMLArea._addEvent(el, "click", function (ev) {
                if (obj.enabled) with (HTMLArea) {
                    _removeClass(el, "buttonActive");
                    _removeClass(el, "buttonHover");
                    obj.cmd(editor, obj.name, obj);
                    _stopEvent(is_ie ? window.event : ev);
                }
            });
            var img = document.createElement("img");
            img.src = btn[1];
            img.style.width = "18px";
            img.style.height = "18px";
            el.appendChild(img);
        } else if (!el) {
            el = createSelect(txt);
        }
        if (el) {
            var tb_cell = document.createElement("td");
            tb_row.appendChild(tb_cell);
            tb_cell.appendChild(el);
        } else {
            alert("FIXME: Unknown toolbar item: " + txt);
        }
        return el;
    };

    var first = true;
    for (var i in this.config.toolbar) {
        if (this.config.toolbar.propertyIsEnumerable(i)) { // fix for prototype.js compatibility
        if (!first) {
            createButton("linebreak");
        } else {
            first = false;
        }
        var group = this.config.toolbar[i];
        for (var j in group) {
                if (group.propertyIsEnumerable(j)) { // fix for prototype.js compatibility
            var code = group[j];
            if (/^([IT])\[(.*?)\]/.test(code)) {
                // special case, create text label
                var l7ed = RegExp.$1 == "I"; // localized?
                var label = RegExp.$2;
                if (l7ed) {
                    label = HTMLArea.I18N.custom[label];
                }
                var tb_cell = document.createElement("td");
                tb_row.appendChild(tb_cell);
                tb_cell.className = "label";
                tb_cell.innerHTML = label;
            } else {
                createButton(code);
                    }
                }
            }
        }
    }

    this._htmlArea.appendChild(toolbar);
};

HTMLArea.prototype._createStatusBar = function() {
    var statusbar = document.createElement("div");
    statusbar.className = "statusBar";
    this._htmlArea.appendChild(statusbar);
    this._statusBar = statusbar;
    // statusbar.appendChild(document.createTextNode(HTMLArea.I18N.msg["Path"] + ": "));
    // creates a holder for the path view
    div = document.createElement("span");
    div.className = "statusBarTree";
    div.innerHTML = HTMLArea.I18N.msg["Path"] + ": ";
    this._statusBarTree = div;
    this._statusBar.appendChild(div);
    if (!this.config.statusBar) {
        // disable it...
        statusbar.style.display = "none";
    }
};

// Creates the HTMLArea object and replaces the textarea with it.
HTMLArea.prototype.generate = function () {
    var editor = this;  // we'll need "this" in some nested functions

    // get the textarea
    var textarea = this._textArea;
    if (typeof textarea == "string") {
        // it's not element but ID
        this._textArea = textarea = HTMLArea.getElementById("textarea", textarea);
    }
    // Fix for IE's sticky bug. Editor doesn't load
    // editing area.
    var height;
    if ( textarea.offsetHeight && textarea.offsetHeight > 0 ) {
        height = textarea.offsetHeight;
    } else {
        height = 300;
    }
    this._ta_size = {
        w: textarea.offsetWidth,
        h: height
    };
    textarea.style.display = "none";

    // create the editor