                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                .cellSpacing;
        var f_padding = table.cellPadding;
        var f_borders = table.border;
        var f_frames = table.frame;
        var f_rules = table.rules;

        function selected(val) {
            return val ? " selected" : "";
        };

        // dialog contents

        dialog.content.innerHTML = " \
<div class='title'\
 style='background: url(" + dialog.baseURL + dialog.editor.imgURL("table-prop.gif", "TableOperations") + ") #fff 98% 50% no-repeat'>" + i18n["Table Properties"] + "\
</div> \
<table style='width:100%'> \
  <tr> \
    <td> \
      <fieldset><legend>" + i18n["Description"] + "</legend> \
       <table style='width:100%'> \
        <tr> \
          <td class='label'>" + i18n["Caption"] + ":</td> \
          <td class='value'><input type='text' name='f_caption' value='" + f_caption + "'/></td> \
        </tr><tr> \
          <td class='label'>" + i18n["Summary"] + ":</td> \
          <td class='value'><input type='text' name='f_summary' value='" + f_summary + "'/></td> \
        </tr> \
       </table> \
      </fieldset> \
    </td> \
  </tr> \
  <tr><td id='--HA-layout'></td></tr> \
  <tr> \
    <td> \
      <fieldset><legend>" + i18n["Spacing and padding"] + "</legend> \
       <table style='width:100%'> \
"+//        <tr> \
//           <td class='label'>" + i18n["Width"] + ":</td> \
//           <td><input type='text' name='f_width' value='" + f_width + "' size='5' /> \
//             <select name='f_unit'> \
//               <option value='%'" + selected(f_unit == "percent") + ">" + i18n["percent"] + "</option> \
//               <option value='px'" + selected(f_unit == "pixels") + ">" + i18n["pixels"] + "</option> \
//             </select> &nbsp;&nbsp;" + i18n["Align"] + ": \
//             <select name='f_align'> \
//               <option value='left'" + selected(f_align == "left") + ">" + i18n["Left"] + "</option> \
//               <option value='center'" + selected(f_align == "center") + ">" + i18n["Center"] + "</option> \
//               <option value='right'" + selected(f_align == "right") + ">" + i18n["Right"] + "</option> \
//             </select> \
//           </td> \
//         </tr> \
"        <tr> \
          <td class='label'>" + i18n["Spacing"] + ":</td> \
          <td><input type='text' name='f_spacing' size='5' value='" + f_spacing + "' /> &nbsp;" + i18n["Padding"] + ":\
            <input type='text' name='f_padding' size='5' value='" + f_padding + "' /> &nbsp;&nbsp;" + i18n["pixels"] + "\
          </td> \
        </tr> \
       </table> \
      </fieldset> \
    </td> \
  </tr> \
  <tr> \
    <td> \
      <fieldset><legend>Frame and borders</legend> \
        <table width='100%'> \
          <tr> \
            <td class='label'>" + i18n["Borders"] + ":</td> \
            <td><input name='f_borders' type='text' size='5' value='" + f_borders + "' /> &nbsp;&nbsp;" + i18n["pixels"] + "</td> \
          </tr> \
          <tr> \
            <td class='label'>" + i18n["Frames"] + ":</td> \
            <td> \
              <select name='f_frames'> \
                <option value='void'" + selected(f_frames == "void") + ">" + i18n["No sides"] + "</option> \
                <option value='above'" + selected(f_frames == "above") + ">" + i18n["The top side only"] + "</option> \
                <option value='below'" + selected(f_frames == "below") + ">" + i18n["The bottom side only"] + "</option> \
                <option value='hsides'" + selected(f_frames == "hsides") + ">" + i18n["The top and bottom sides only"] + "</option> \
                <option value='vsides'" + selected(f_frames == "vsides") + ">" + i18n["The right and left sides only"] + "</option> \
                <option value='lhs'" + selected(f_frames == "lhs") + ">" + i18n["The left-hand side only"] + "</option> \
                <option value='rhs'" + selected(f_frames == "rhs") + ">" + i18n["The right-hand side only"] + "</option> \
                <option value='box'" + selected(f_frames == "box") + ">" + i18n["All four sides"] + "</option> \
              </select> \
            </td> \
          </tr> \
          <tr> \
            <td class='label'>" + i18n["Rules"] + ":</td> \
            <td> \
              <select name='f_rules'> \
                <option value='none'" + selected(f_rules == "none") + ">" + i18n["No rules"] + "</option> \
                <option value='rows'" + selected(f_rules == "rows") + ">" + i18n["Rules will appear between rows only"] + "</option> \
                <option value='cols'" + selected(f_rules == "cols") + ">" + i18n["Rules will appear between columns only"] + "</option> \
                <option value='all'" + selected(f_rules == "all") + ">" + i18n["Rules will appear between all rows and columns"] + "</option> \
              </select> \
            </td> \
          </tr> \
        </table> \
      </fieldset> \
    </td> \
  </tr> \
  <tr> \
    <td id='--HA-style'></td> \
  </tr> \
</table> \
";
        var st_prop = TableOperations.createStyleFieldset(dialog.doc, dialog.editor, table);
        var p = dialog.doc.getElementById("--HA-style");
        p.appendChild(st_prop);
        var st_layout = TableOperations.createStyleLayoutFieldset(dialog.doc, dialog.editor, table);
        p = dialog.doc.getElementById("--HA-layout");
        p.appendChild(st_layout);
        dialog.modal = true;
        dialog.addButtons("ok", "cancel");
        dialog.showAtElement(dialog.editor._iframe, "c");
        dialog.content.style.width = "400px";
        if (document.all) {
            dialog.content.style.height = dialog.content.clientHeight + 60 + 'px'; //moodlefix
        }
    });
};

// this function requires the file PopupDiv/PopupWin to be loaded from browser
TableOperations.prototype.dialogRowCellProperties = function(cell) {
    var i18n = TableOperations.I18N;
    // retrieve existing values
    var element = this.getClosest(cell ? "td" : "tr");
    var table = this.getClosest("table");
    // this.editor.selectNodeContents(element);
    // this.editor.updateToolbar();

    var dialog = new PopupWin(this.editor, i18n[cell ? "Cell Properties" : "Row Properties"], function(dialog, params) {
        TableOperations.processStyle(params, element);
        for (var i in params) {
            var val = params[i];
            switch (i) {
                case "f_align":
                element.align = val;
                break;
                case "f_char":
                element.ch = val;
                break;
                case "f_valign":
                element.vAlign = val;
                break;
            }
        }
        // various workarounds to refresh the table display (Gecko,
        // what's going on?! do not disappoint me!)
        dialog.editor.forceRedraw();
        dialog.editor.focusEditor();
        dialog.editor.updateToolbar();
        var save_collapse = table.style.borderCollapse;
        table.style.borderCollapse = "collapse";
        table.style.borderCollapse = "separate";
     