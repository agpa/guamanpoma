/** This file is derived from PopupDiv, developed by Mihai Bazon for
 * SamWare.net.  Modifications were needed to make it usable in HTMLArea.
 * HTMLArea is a free WYSIWYG online HTML editor from InteractiveTools.com.
 *
 * This file does not function standalone.  It is dependent of global functions
 * defined in HTMLArea-3.0 (htmlarea.js).
 *
 * Please see file htmlarea.js for further details.
 **/

var is_ie = ( (navigator.userAgent.toLowerCase().indexOf("msie") != -1) &&
	      (navigator.userAgent.toLowerCase().indexOf("opera") == -1) );
var is_compat = (document.compatMode == "BackCompat");

function PopupDiv(editor, titleText, handler, initFunction) {
	var self = this;

	this.editor = editor;
	this.doc = editor._mdoc;
	this.handler = handler;

	var el = this.doc.createElement("div");
	el.className = "content";

	var popup = this.doc.createElement("div");
	popup.className = "dialog popupdiv";
	this.element = popup;
	var s = popup.style;
	s.position = "absolute";
	s.left = "0px";
	s.top = "0px";

	var title = this.doc.createElement("div");
	title.className = "title";
	this.title = title;
	popup.appendChild(title);

	HTMLArea._addEvent(title, "mousedown", function(ev) {
		self._dragStart(is_ie ? window.event : ev);
	});

	var button = this.doc.createElement("div");
	button.className = "button";
	title.appendChild(button);
	button.innerHTML = "&#x00d7;";
	title.appendChild(this.doc.createTextNode(titleText));
	this.titleText = titleText;

	button.onmouseover = function() {
		this.className += " button-hilite";
	};
	button.onmouseout = function() {
		this.className = this.className.replace(/\s*button-hilite\s*/g, " ");
	};
	button.onclick = function() {
		this.className = this.className.replace(/\s*button-hilite\s*/g, " ");
		self.close();
	};

	popup.appendChild(el);
	this.content = el;

	this.doc.body.appendChild(popup);

	this.dragging = false;
	this.onShow = null;
	this.onClose = null;
	this.modal = false;

	initFunction(this);
};

PopupDiv.currentPopup = null;

PopupDiv.prototype.showAtElement = function(el, mode) {
	this.defaultSize();
	var pos, ew, eh;
	var popup = this.element;
	popup.style.display = "block";
	var w = popup.offsetWidth;
	var h = popup.offsetHeight;
	popup.style.display = "none";
	if (el != window) {
		pos = PopupDiv.getAbsolutePos(el);
		ew = el.offsetWidth;
		eh = el.offsetHeight;
	} else {
		pos = {x:0, y:0};
		var size = PopupDiv.getWindowSize();
		ew = size.x;
		eh = size.y;
	}
	var FX = false, FY = false;
	if (mode.indexOf("l") != -1) {
		pos.x -= w;
		FX = true;
	}
	if (mode.indexOf("r") != -1) {
		pos.x += ew;
		FX = true;
	}
	if (mode.indexOf("t") != -1) {
		pos.y -= h;
		FY = true;
	}
	if (mode.indexOf("b") != -1) {
		pos.y += eh;
		FY = true;
	}
	if (mode.indexOf("c") != -1) {
		FX || (pos.x += Math.round((ew - w) / 2));
		FY || (pos.y += Math.round((eh - h) / 2));
	}
	this.showAt(pos.x, pos.y);
};

PopupDiv.prototype.defaultSize = function() {
	var s = this.element.style;
	var cs = this.element.currentStyle;
	var addX = (is_ie && is_compat) ? (parseInt(cs.borderLeftWidth) +
					   parseInt(cs.borderRightWidth) +
					   parseInt(cs.paddingLeft) +
					   parseInt(cs.paddingRight)) : 0;
	var addY = (is_ie && is_compat) ? (parseInt(cs.borderTopWidth) +
					   parseInt(cs.borderBottomWidth) +
					   parseInt(cs.paddingTop) +
					   parseInt(cs.paddingBottom)) : 0;
	s.display = "block";
	s.width = (this.content.offsetWidth + addX) + "px";
	s.height = (this.content.offsetHeight + this.title.offsetHeight) + "px";
	s.display = "none";
};

PopupDiv.prototype.showAt = function(x, y) {
	this.defaultSize();
	var s = this.element.style;
	s.display = "block";
	s.left = x + "px";
	s.top = y + "px";
	this.hideShowCovered();

	PopupDiv.currentPopup = this;
	HTMLArea._addEvents(this.doc.body, ["mousedown", "click"], PopupDiv.checkPopup);
	HTMLArea._addEvents(this.editor._doc.body, ["mousedown", "click"], PopupDiv.checkPopup);
	if (is_ie && this.modal) {
		this.doc.body.setCapture(false);
		this.doc.body.onlosecapture = function() {
			(PopupDiv.currentPopup) && (this.doc.body.setCapture(false));
		};
	}
	window.event && HTMLArea._stopEvent(window.event);

	if (typeof this.onShow == "function") {
		this.onShow();
	} else if (typeof this.onShow == "string") {
		eval(this.onShow);
	}

	var field = this.element.getElementsByTagName("input")[0];
	if (!field) {
		field = this.element.getElementsByTagName("select")[0];
	}
	if (!field) {
		field = this.element.getElementsByTagName("textarea")[0];
	}
	if (field) {
		field.focus();
	}
};

PopupDiv.prototype.close = function() {
	this.element.style.display = "none";
	PopupDiv.currentPopup = null;
	this.hideShowCovered();
	HTMLArea._removeEvents(this.doc.body, ["mousedown", "click"], PopupDiv.checkPopup);
	HTMLArea._removeEvents(this.editor._doc.body, ["mousedown", "click"], PopupDiv.checkPopup);
	is_ie && this.modal && this.doc.body.releaseCapture();
	if (typeof this.onClose == "function") {
		this.onClose();
	} else if (typeof this.onClose == "string") {
		eval(this.onClose);
	}
	this.element.parentNode.removeChild(this.element);
};

PopupDiv.prototype.getForm = function() {
	var forms = this.content.getElementsByTagName("form");
	return (forms.length > 0) ? forms[0] : null;
};

PopupDiv.prototype.callHandler = function() {
	var tags = ["input", "textarea", "select"];
	var params = new Object();
	for (var ti in tags) {
		var tag = tags[ti];
		var els = this.content.getElementsByTagName(tag);
		for (var j = 0; j < els.length; ++j) {
			var el = els[j];
			params[el.name] = el.value;
		}
	}
	this.handler(this, params);
	return false