<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

class grade_edit_tree {
    var $columns = array();

    /**
     * @var object $gtree          @see grade/lib.php
     */
    var $gtree;

    /**
     * @var grade_plugin_return @see grade/lib.php
     */
    var $gpr;

    /**
     * @var string              $moving The eid of the category or item being moved
     */
    var $moving;

    var $deepest_level;

    var $uses_extra_credit = false;

    var $uses_weight = false;

    /**
     * Constructor
     */
    function grade_edit_tree($gtree, $moving=false, $gpr) {
        $this->gtree = $gtree;
        $this->moving = $moving;
        $this->gpr = $gpr;
        $this->deepest_level = $this->get_deepest_level($this->gtree->top_element);

        $this->columns = array(grade_edit_tree_column::factory('name', array('deepest_level' => $this->deepest_level)),
                               grade_edit_tree_column::factory('aggregation', array('flag' => true)));

        if ($this->uses_weight) {
            $this->columns[] = grade_edit_tree_column::factory('weight', array('adv' => 'aggregationcoef'));
        }
        if ($this->uses_extra_credit) {
            $this->columns[] = grade_edit_tree_column::factory('extracredit', array('adv' => 'aggregationcoef'));
        }

        $this->columns[] = grade_edit_tree_column::factory('range'); // This is not a setting... How do we deal with it?
        $this->columns[] = grade_edit_tree_column::factory('aggregateonlygraded', array('flag' => true));
        $this->columns[] = grade_edit_tree_column::factory('aggregatesubcats', array('flag' => true));
        $this->columns[] = grade_edit_tree_column::factory('aggregateoutcomes', array('flag' => true));
        $this->columns[] = grade_edit_tree_column::factory('droplow', array('flag' => true));
        $this->columns[] = grade_edit_tree_column::factory('keephigh', array('flag' => true));
        $this->columns[] = grade_edit_tree_column::factory('multfactor', array('adv' => true));
        $this->columns[] = grade_edit_tree_column::factory('plusfactor', array('adv' => true));
        $this->columns[] = grade_edit_tree_column::factory('actions');
        $this->columns[] = grade_edit_tree_column::factory('select');
    }

    /**
     * Recursive function for building the table holding the grade categories and items,
     * with CSS indentation and styles.
     *
     * @param array               $element The current tree element being rendered
     * @param boolean             $totals Whether or not to print category grade items (category totals)
     * @param array               $parents An array of parent categories for the current element (used for indentation and row classes)
     *
     * @return string HTML
     */
    function build_html_tree($element, $totals, $parents, &$categories, $level, &$row_count) {
        global $CFG, $COURSE, $USER;

        $object = $element['object'];
        $eid    = $element['eid'];
        $object->name = $this->gtree->get_element_header($element, true, true, false);
        $object->stripped_name = $this->gtree->get_element_header($element, false, false, false);

        $is_category_item = false;
        if ($element['type'] == 'categoryitem' || $element['type'] == 'courseitem') {
            $is_category_item = true;
        }

        $rowclasses = '';
        foreach ($parents as $parent_eid) {
            $rowclasses .= " $parent_eid ";
        }

        $actions = '';

        if (!$is_category_item) {
            $actions .= $this->gtree->get_edit_icon($element, $this->gpr);
        }

        $actions .= $this->gtree->get_calculation_icon($element, $this->gpr);

        if ($element['type'] == 'item' or ($element['type'] == 'category' and $element['depth'] > 1)) {
            if ($this->element_deletable($element)) {
                $actions .= '<a href="index.php?id='.$COURSE->id.'&amp;action=delete&amp;eid='
                         . $eid.'&amp;sesskey='.sesskey().'"><img src="'.$CFG->pixpath.'/t/delete.gif" class="iconsmall" alt="'
                         . get_string('delete').'" title="'.get_string('delete').'"/></a>';
            }
            $actions .= '<a href="index.php?id='.$COURSE->id.'&amp;action=moveselect&amp;eid='
                     . $eid.'&amp;sesskey='.sesskey().'"><img src="'.$CFG->pixpath.'/t/move.gif" class="iconsmall" alt="'
                     . get_string('move').'" title="'.get_string('move').'"/></a>';
        }

        $actions .= $this->gtree->get_hiding_icon($element, $this->gpr);
        $actions .= $this->gtree->get_locking_icon($element, $this->gpr);

        $mode = ($USER->gradeediting[$COURSE->id]) ? 'advanced' : 'simple';

        $html = '';
        $root = false;


        $id = required_param('id', PARAM_INT);

        /// prepare move target if needed
        $last = '';

        /// print the list items now
        if ($this->moving == $eid) {

            // do not diplay children
            return '<tr><td colspan="12" class="'.$element['type'].' moving">'.$object->name.' ('.get_string('move').')</td></tr>';

        }

        if ($element['type'] == 'category') {
            $level++;
            $categories[$object->id] = $object->stripped_name;
            $category = grade_category::fetch(array('id' => $object->id));
            $item = $category->get_grade_item();

            // Add aggregation coef input if not a course item and if parent category has correct aggregation type
            $dimmed = ($item->is_hidden()) ? " dimmed " : "";

            // Before we print the category's row, we must find out how many rows will appear below it (for the filler cell's rowspan)
            $aggregation_position = grade_get_setting($COURSE->id, 'aggregationposition', $CFG->grade_aggregationposition);
            $category_total_data = null; // Used if aggregationposition is set to "last", so we can print it last

            $html_children = '';

            $row_count = 0;

            foreach($element['children'] as $child_el) {
                $moveto = '';

                if (empty($child_el['object']->itemtype)) {
                    $child_el['object']->itemtype = false;
                }

                if (($child_el['object']->itemtype == 'course' || $child_el['object']->itemtype == 'category') && !$totals) {
                    continue;
                }

                $child_eid    = $child_el['eid'];
                $first = '';

                if ($child_el['object']->itemtype == 'course' || $child_el['object']->itemtype == 'category') {
                    $first = '&amp;first=1';
                    $child_eid = $eid;
                }

                if ($this->moving && $this->moving != $child_eid) {

                    $strmove     = get_string('move');
                    $strmovehere = get_string('movehere');
                    $actions = ''; // no action icons when moving

                    $moveto = '<tr><td colspan="12"><a href="index.php?id='.$COURSE->id.'&amp;action=move&amp;eid='.$this->moving.'&amp;moveafter='
                            . $child_eid.'&amp;sesskey='.sesskey().$first.'"><img class="movetarget" src="'.$CFG->wwwroot.'/pix/movehere.gif" alt="'
                            . $strmovehere.'" title="'.s($strmovehere).'" /></a></td></tr>';
                }

                $newparents = $parents;
                $newparents[] = $eid;

                $row_count++;
                $child_row_count = 0;

                // If moving, do not print course and category totals, but still print the moveto target box
                if ($this->moving && ($child_el['object']->itemtype == 'course' || $child_el['object']->itemtype == 'category')) {
                    $html_children .= $moveto;
                } elseif ($child_el['object']->itemtype == 'course' || $child_el['object']->itemtype == 'category') {
                    // We don't build the item yet because we first need to know the deepest level of categories (for category/name colspans)
                    $category_total_item = $this->build_html_tree($child_el, $totals, $newparents, $categories, $level, $child_row_count);
                    if (!$aggregation_position) {
                        $html_children .= $category_total_item;
                    }
                } else {
                    $html_children .= $this->build_html_tree($child_el, $totals, $newparents, $categories, $level, $child_row_count) . $moveto;

                    if ($this->moving) {
                        $row_count++;
                    }
                }

                $row_count += $child_row_count;

                // If the child is a category, increment row_count by one more (for the extra coloured row)
                if ($child_el['type'] == 'category') {
                    $row_count++;
                }
            }

            // Print category total at the end if aggregation position is "last" (1)
            if (!empty($category_total_item) && $aggregation_position) {
                $html_children .= $category_total_item;
            }

            // now build the header
            if (isset($element['object']->grade_item) && $element['object']->grade_item->is_course_item()) {
                // Reduce width if advanced elements are not shown
                $width_style = '';

                if ($mode == 'simple') {
                    $width_style = ' style="width:auto;" ';
                }

                $html .= '<table cellpadding="5" class="generaltable" '.$width_style.'>
                            <tr>';

                foreach ($this->columns as $column) {
                    if (!($this->moving && $column->hide_when_moving) && !$column->is_hidden($mode)) {
                        $html .= $column->get_header_cell();
                    }
                }

                $html .= '</tr>';
                $root = true;
            }

            $row_count_offset = 0;

            if (empty($category_total_item) && !$this->moving) {
                $row_count_offset = -1;
            }

            $levelclass = " level$level ";

            $html .= '
                    <tr class="category '.$dimmed.$rowclasses.'">
                      <th scope="row" title="'.s($object->stripped_name).'" class="cell rowspan '.$levelclass.'" rowspan="'.($row_count+1+$row_count_offset).'"></th>';

            foreach ($this->columns as $column) {
                if (!($this->moving && $column->hide_when_moving) && !$column->is_hidden($mode)) {
                    $html .= $column->get_category_cell($category, $levelclass, array('id' => $id, 'name' => $object->name, 'level' => $level, 'actions' => $actions, 'eid' => $eid));
                }
            }

            $html .= '</tr>';

            $html .= $html_children;

            // Print a coloured row to show the end of the category accross the table
            $html .= '<tr><td colspan="'.(19 - $level).'" class="colspan '.$levelclass.'"></td></tr>';

        } else { // Dealing with a grade item

            $item = grade_item::fetch(array('id' => $object->id));
            $element['type'] = 'item';
            $element['object'] = $item;

            // Determine aggregation coef element

            $dimmed = ($item->is_hidden()) ? " dimmed_text " : "";
            $html .= '<tr class="item'.$dimmed.$rowclasses.'">';

            foreach ($this->columns as $column) {
                if (!($this->moving && $column->hide_when_moving) && !$column->is_hidden($mode)) {
                    $html .= $column->get_item_cell($item, array('id' => $id, 'name' => $object->name, 'level' => $level, 'actions' => $actions,
                                                                 'element' => $element, 'eid' => $eid, 'itemtype' => $object->itemtype));
                }
            }

            $html .= '</tr>';
        }


        if ($root) {
            $html .= "</table>\n";
        }

        return $html;

    }

    /**
     * Given a grade_item object, returns a labelled input if an aggregation coefficient (weight or extra credit) applies to it.
     * @param grade_item $item
     * @param string type "extra" or "weight": the type of the column hosting the weight input
     * @return string HTML
     */
    function get_weight_input($item, $type) {
        if (!is_object($item) || get_class($item) !== 'grade_item') {
            error('grade_edit_tree::get_weight_input($item) was given a variable that is not of the required type (grade_item object)');
        }

        if ($item->is_course_item()) {
            return '';
        }

        $parent_category = $item->get_parent_category();
        $parent_category->apply_forced_settings();
        $aggcoef = $item->get_coefstring();

        if ((($aggcoef == 'aggregationcoefweight' || $aggcoef == 'aggregationcoef') && $type == 'weight') ||
            ($aggcoef == 'aggregationcoefextra' && $type == 'extra')) {
            return '<input type="text" size="6" id="aggregationcoef_'.$item->id.'" name="aggregationcoef_'.$item->id.'"
                value="'.format_float($item->aggregationcoef, 4).'" />';
        } elseif ($aggcoef == 'aggregationcoefextrasum' && $type == 'extra') {
            $checked = ($item->aggregationcoef > 0) ? 'checked="checked"' : '';
            return '<input type="hidden" name="extracredit_'.$item->id.'" value="0" />
                    <input type="checkbox" id="extracredit_'.$item->id.'" name="extracredit_'.$item->id.'" value="1" '."$checked />\n";
        } else {
            return '';
        }
    }

    /**
     * Given an element of the grade tree, returns whether it is deletable or not (only manual grade items are deletable)
     *
     * @param array $element
     * @return bool
     */
    function element_deletable($element) {
        global $COURSE;

        if ($element['type'] != 'item') {
            return true;
        }

        $grade_item = $element['object'];

        if ($grade_item->itemtype != 'mod' or $grade_item->is_outcome_item() or $grade_item->gradetype == GRADE_TYPE_NONE) {
            return true;
        }

        $modinfo = get_fast_modinfo($COURSE);
        if (!isset($modinfo->instances[$grade_item->itemmodule][$grade_item->iteminstance])) {
            // module does not exist
            return true;
        }

        return false;
    }

    /**
     * Given the grade tree and an array of element ids (e.g. c15, i42), and expecting the 'moveafter' URL param,
     * moves the selected items to the requested location. Then redirects the user to the given $returnurl
     *
     * @param object $gtree The grade tree (a recursive representation of the grade categories and grade items)
     * @param array $eids
     * @param string $returnurl
     */
    function move_elements($eids, $returnurl) {
        $moveafter = required_param('moveafter', PARAM_INT);

        if (!is_array($eids)) {
            $eids = array($eids);
        }

        if(!$after_el = $this->gtree->locate_element("c$moveafter")) {
            print_error('invalidelementid', '', $returnurl);
        }

        $after = $after_el['object'];
        $parent = $after;
        $sortorder = $after->get_sortorder();

        foreach ($eids as $eid) {
            if (!$element = $this->gtree->locate_element($eid)) {
                print_error('invalidelementid', '', $returnurl);
            }
            $object = $element['object'];

            $object->set_parent($parent->id);
            $object->move_after_sortorder($sortorder);
            $sortorder++;
        }

        redirect($returnurl, '', 0);
    }

    /**
     * Recurses through the entire grade tree to find and return the maximum depth of the tree.
     * This should be run only once from the root element (course category), and is used for the
     * indentation of the Name column's cells (colspan)
     *
     * @param array $element An array of values representing a grade tree's element (all grade items in this case)
     * @param int $level The level of the current recursion
     * @param int $deepest_level A value passed to each subsequent le