<?php /**/ ?><?php  // $Id: format.php,v 1.16.2.3 2009/11/19 10:46:39 skodak Exp $
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// WebCT FORMAT                                                          //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// NOTICE OF COPYRIGHT                                                   //
//                                                                       //
// Part of Moodle - Modular Object-Oriented Dynamic Learning Environment //
//                  http://moodle.com                                    //
//                                                                       //
// Copyright (C) 2004 ASP Consulting   http://www.asp-consulting.net     //
//                                                                       //
// This program is free software; you can redistribute it and/or modify  //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// This program is distributed in the hope that it will be useful,       //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details:                          //
//                                                                       //
//          http://www.gnu.org/copyleft/gpl.html                         //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

// Based on format.php, included by ../../import.php
/**
 * @package questionbank
 * @subpackage importexport
 */

function unhtmlentities($string){
    $search = array ("'<script[?>]*?>.*?</script>'si",  // remove javascript
                 "'<[\/\!]*?[^<?>]*?>'si",  // remove HTML tags
                 "'([\r\n])[\s]+'",  // remove spaces
                 "'&(quot|#34);'i",  // remove HTML entites
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'&#(\d+);'e");  // Evaluate like PHP
    $replace = array ("",
                  "",
                  "\\1",
                  "\"",
                  "&",
                  "<",
                  "?>",
                  " ",
                  chr(161),
                  chr(162),
                  chr(163),
                  chr(169),
                  "chr(\\1)");
    return preg_replace ($search, $replace, $string);
}



function qformat_webct_convert_formula($formula) {

    // Remove empty space, as it would cause problems otherwise:
    $formula = str_replace(' ', '', $formula);

    // Remove paranthesis after e,E and *10**:
    while (ereg('[0-9.](e|E|\\*10\\*\\*)\\([+-]?[0-9]+\\)', $formula, $regs)) {
        $formula = str_replace(
                $regs[0], ereg_replace('[)(]', '', $regs[0]), $formula);
    }

    // Replace *10** with e where possible
    while (ereg(
            '(^[+-]?|[^eE][+-]|[^0-9eE+-])[0-9.]+\\*10\\*\\*[+-]?[0-9]+([^0-9.eE]|$)',
            $formula, $regs)) {
        $formula = str_replace(
                $regs[0], str_replace('*10**', 'e', $regs[0]), $formula);
    }

    // Replace other 10** with 1e where possible
    while (ereg('(^|[^0-9.eE])10\\*\\*[+-]?[0-9]+([^0-9.eE]|$)', $formula, $regs)) {
        $formula = str_replace(
                $regs[0], str_replace('10**', '1e', $regs[0]), $formula);
    }

    // Replace all other base**exp with the PHP equivalent function pow(base,exp)
    // (Pretty tricky to exchange an operator with a function)
    while (2 == count($splits = explode('**', $formula, 2))) {

        // Find $base
        if (ereg('^(.*[^0-9.eE])?(([0-9]+(\\.[0-9]*)?|\\.[0-9]+)([eE][+-]?[0-9]+)?|\\{[^}]*\\})$',
                $splits[0], $regs)) {
            // The simple cases
            $base = $regs[2];
            $splits[0] = $regs[1];

        } else if (ereg('\\)$', $splits[0])) {
            // Find the start of this parenthesis
            $deep = 1;
            for ($i = 1 ; $deep ; ++$i) {
                if (!ereg('^(.*[^[:alnum:]_])?([[:alnum:]_]*([)(])([^)(]*[)(]){'.$i.'})$',
                        $splits[0], $regs)) {
                    error("Parenthesis before ** is not properly started in $splits[0]**");
                }
                if ('(' == $regs[3]) {
                    --$deep;
                } else if (')' == $regs[3]) {
                    ++$deep;
                } else {
                    error("Impossible character $regs[3] detected as parenthesis character");
                }
            }
            $base = $regs[2];
            $splits[0] = $regs[1];

        } else {
            error("Bad base before **: $splits[0]**");
        }

        // Find $exp (similar to above but a little easier)
        if (ereg('^([+-]?(\\{[^}]\\}|([0-9]+(\\.[0-9]*)?|\\.[0-9]+)([eE][+-]?[0-9]+)?))(.*)