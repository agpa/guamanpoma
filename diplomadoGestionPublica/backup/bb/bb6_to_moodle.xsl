<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="UTF-8" />
<xsl:template match="/">
  <MOODLE_BACKUP>
   <INFO>
    <NAME>backup-from-blackboard.zip</NAME>
    <MOODLE_VERSION>2004083100</MOODLE_VERSION>
    <MOODLE_RELEASE>1.4</MOODLE_RELEASE>
    <BACKUP_VERSION>2004083100</BACKUP_VERSION>
    <BACKUP_RELEASE>1.4</BACKUP_RELEASE>
    <DATE>1094240862</DATE>
    <ORIGINAL_WWWROOT>INSERT URL HERE</ORIGINAL_WWWROOT>
    <DETAILS>
      <MOD>
        <NAME>assignment</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>chat</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>choice</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>forum</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>glossary</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>journal</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>label</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>lesson</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>quiz</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>resource</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>scorm</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>survey</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>wiki</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <MOD>
        <NAME>workshop</NAME>
        <INCLUDED>true</INCLUDED>
        <USERINFO>true</USERINFO>
      </MOD>
      <USERS>course</USERS>
      <LOGS>false</LOGS>
      <USERFILES>true</USERFILES>
      <COURSEFILES>true</COURSEFILES>
    </DETAILS>
  </INFO>
  <COURSE>
    <!-- Get course specific information -->
    <xsl:apply-templates select="document('res00001.dat')//COURSE"/>

    <xsl:call-template name="modules" />
    
  <SECTIONS>
    <!-- Create a title section -->
    <xsl:for-each select="document('res00001.dat')" >
      <xsl:call-template name="title_section" />
    </xsl:for-each>


    <!-- Create a topic for each top level Bb item and add section modules ONE folder deep -->
    <xsl:for-each select="manifest/organizations/organization/item">
      <xsl:variable name="section_number" select="position()"/>
      <xsl:call-template name="sections">
            <xsl:with-param name="section_number" select="$section_number"/>
            <xsl:with-param name="recurse" >false</xsl:with-param>
          </xsl:call-template>
    </xsl:for-each>

    <!-- Create a topic for each second level Bb item which is a folder, recursively make section modules  -->
    <xsl:for-each select="manifest/organizations/organization/item/item">
      <xsl:sort order="descending" select="document(concat(@identifierref,'.dat'))/CONTENT/FLAGS/ISFOLDER/@value"/>
      <xsl:if test="document(concat(@identifierref,'.dat'))/CONTENT/FLAGS/ISFOLDER/@value = 'true'">
        <xsl:variable name="prev_sections" select="count(/manifest/organizations/tableofcontents/item)"/>
        <xsl:variable name="section_number" select="position()+$prev_sections"/>
        <xsl:call-template name="sections">
              <xsl:with-param name="section_number" select="$section_number"/>
              <xsl:with-param name="recurse" >true</xsl:with-param>
        </xsl:call-template>
      </xsl:if>
    </xsl:for-each>
  </SECTIONS>
    
  </COURSE>
  </MOODLE_BACKUP>
</xsl:template>

<xsl:template match="COURSE">
      <HEADER>
      <ID>2</ID>
      <CATEGORY>
        <ID></ID>
        <NAME><xsl:value-of select="CATEGORIES/CATEGORY/@value"/></NAME>
      </CATEGORY>
      <PASSWORD></PASSWORD>
      <IDNUMBER>4</IDNUMBER>
      <FORMAT>topics</FORMAT>
      <SHOWGRADES>1</SHOWGRADES>
      <BLOCKINFO>participants,activity_modules,search_forums,admin,course_list:news_items,calendar_upcoming,recent_activity</BLOCKINFO>
      <NEWSITEMS>5</NEWSITEMS>
      <TEACHER>Teacher</TEACHER>
      <TEACHERS>Teachers</TEACHERS>
      <STUDENT>Student</STUDENT>
      <STUDENTS>Students</STUDENTS>
      <GUEST>0</GUEST>
      <STARTDATE>1094270400</STARTDATE>
      <ENROLPERIOD>0</ENROLPERIOD>
      <NUMSECTIONS>10</NUMSECTIONS>
      <MAXBYTES>2097152</MAXBYTES>
      <SHOWREPORTS>0</SHOWREPORTS>
      <GROUPMODE>0</GROUPMODE>
      <GROUPMODEFORCE>0</GROUPMODEFORCE>
      <LANG></LANG>
      <COST></COST>
      <MARKER>0</MARKER>
      <VISIBLE>1</VISIBLE>
      <HIDDENSECTIONS>0</HIDDENSECTIONS>
      <TIMECREATED>1094240775</TIMECREATED>
      <TIMEMODIFIED>1094240775</TIMEMODIFIED>
      <SUMMARY><xsl:value-of select="DESCRIPTION"/></SUMMARY>
      <SHORTNAME><xsl:value-of select="COURSEID/@value"/></SHORTNAME>
      <FULLNAME><xsl:value-of select="TITLE/@value"/></FULLNAME>
      </HEADER>
</xsl:template>

<xsl:template name="title_section" match="resource">
    <SECTION>
      <ID>0</ID>
      <NUMBER>0</NUMBER>
      <SUMMARY>&lt;div style="text-align: center;"&gt;&lt;font size="5" style="font-family: arial,helvetica,sans-serif;"&gt;<xsl:value-of select="COURSE/TITLE/@value"/>&lt;/font&gt;&lt;/div&gt;
        <xsl:value-of select="COURSE/DESCRIPTION"/>
      </SUMMARY>
      <VISIBLE>1</VISIBLE>
      <MODS>
      </MODS>
    </SECTION>
</xsl:template>

<xsl:template name="sections" match="resource">
    <xsl:param name="section_number">1. </xsl:param>
    <xsl:param name="recurse"/>
    <SECTION>
      <ID><xsl:value-of select="$section_number"/></ID>
      <NUMBER><xsl:value-of select="$section_number"/></NUMBER>
      <SUMMARY>&lt;span style="font-weight: bold;"&gt;<xsl:value-of select="title"/>&lt;/span&gt;</SUMMARY>
      <VISIBLE>1</VISIBLE>
      <MODS>
        
        <xsl:if test="$recurse = 'true'">
          <xsl:variable name="mod_number" select="substring-after(@identifierref,'res')"/>
          <xsl:call-template name="item_recurse_files" >
                <xsl:with-param name="mod_number" select="$mod_number"/>
                <xsl:with-param name="indent" >0</xsl:with-param>
                <xsl:with-param name="recurse" select="$recurse" />
          </xsl:call-template>
        
        </xsl:if>

        <xsl:if test="$recurse = 'false'">
        <xsl:for-each select="item">
          <xsl:variable name="mod_number" select="substring-after(@identifierref,'res')"/>
          <xsl:call-template name="item" >
                <xsl:with-param name="mod_number" select="$mod_number"/>
                <xsl:with-param name="indent" >0</xsl:with-param>
              </xsl:call-template>
        </xsl:for-each> 
        </xsl:if>
  

      </MODS>
    </SECTION>
  </xsl:template>

<xsl:template name="item_recurse_files">
   <xsl:param name="mod_number">1. </xsl:param>
   <xsl:param name="indent">1. </xsl:param>
   <xsl:param name="recurse"/>

   
    <!-- Create one section-mod -->
    <xsl:for-each select="document(concat(@identifierref,'.dat'))">
      <xsl:call-template name="section_mod">
          <xsl:with-param name="mod_number" select="$mod_number"/>
          <xsl:with-param name="indent" select="$indent"/>
      </xsl:call-template>
    </xsl:for-each>
    
    <!-- Depth first recursion to preserve order -->
    <xsl:for-each select="item">
      <xsl:variable name="m_number" select="substring-after(@identifierref,'res')"/>
      <xsl:call-template name="item_recurse_files" >
            <xsl:with-param name="mod_number" select="$m_number"/>
            <xsl:with-param name="indent" select="$indent + 1"/>
      </xsl:call-template>
    </xsl:for-each>
    
</xsl:template>

<xsl:template name="item">
   <xsl:param name="mod_number">1. </xsl:param>
   <xsl:param name="indent">1. </xsl:param>

   <GETHERE></GETHERE> 
   <xsl:if test="document(concat(@identifierref,'.dat'))/CONTENT/FLAGS/ISFOLDER/@value != 'true' or document(concat(@identifierref,'.dat'))/EXTERNALLINK/DESCRIPTION/FLAGS/ISHTML/@value ='true'">
    <!-- Create one section-mod -->
    <xsl:for-each select="document(concat(@identifierref,'.dat'))">
      <xsl:call-template name="section_mod">
            <xsl:with-param name="mod_number" select="$mod_number"/>
            <xsl:with-param name="indent" select="$indent"/>
      </xsl:call-template>
    </xsl:for-each>
   </xsl:if>
   
    
</xsl:template>

<!-- Determines the type of section mod entry and calls the appropriate creation template -->
<xsl:template name="section_mod" >
   <xsl:param name="mod_number">1. </xsl:param>
   <xsl:param name="contenttype" />
   <xsl:param name="indent">1. </xsl:param>

  <!-- Every file will have a label module describing it -->
  <xsl:choose>
      <!-- Detected a file -->
      <xsl:when test="CONTENT/FILE/@id != '' or CONTENT/FILES/FILE/NAME != ''">
        <!-- Create a label -->
        <xsl:call-template name="section_mod_label">
              <xsl:with-param name="mod_number" select="$mod_number"/>
              <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>

        <!-- Create a resource -->
        <xsl:call-template name="section_mod_resource">
              <xsl:with-param name="mod_number" select="$mod_number"/>
              <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>

      </xsl:when>

      <!-- Detected a folder -->
      <xsl:when test="CONTENT/FLAGS/ISFOLDER/@value = 'true'">
        <MAKINGLABEL></MAKINGLABEL>
        <!-- Create a label -->
        <xsl:call-template name="section_mod_label">
              <xsl:with-param name="mod_number" select="$mod_number"/>
              <xsl:with-param name="indent" select="$indent"/>
            </xsl:call-template>
      </xsl:when>

      <!-- Detected text -->
      <xsl:when test="CONTENT/MAINDATA/FLAGS/ISHTML/@value = 'true' or CONTENT/BODY/TYPE/@value = 'H' ">
        <MAKINGTEXT></MAKINGTEXT>
        <!-- Create a resource -->
        <xsl:call-template name="section_mod_resource">
              <xsl:with-param name="mod_number" select="$mod_number"/>
              <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
      </xsl:when>

      <!-- Detected external link -->
      <xsl:when test="EXTERNALLINK/TITLE/@value != '' ">
         <!-- Create a label -->
        <xsl:call-template name="section_mod_label">
              <xsl:with-param name="mod_number" select="$mod_number"/>
              <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>

        <!-- Create a resource -->
        <xsl:call-template name="section_mod_externallink">
              <xsl:with-param name="mod_number" select="$mod_number"/>
              <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <UNKNOWN>
        </UNKNOWN>
      </xsl:otherwise>

  </xsl:choose>


</xsl:template>

<!-- Creates one section-mod-label -->
<xsl:template name="section_mod_label" >
   <xsl:param name="mod_number">1. </xsl:param>
   <xsl:param name="indent">1. </xsl:param>
  <MOD>
    <ID>1<xsl:value-of select="$mod_number"/>0</ID>
	  <ZIBA_NAME>
      <!-- BB5.5 -->
      <xsl:value-of select="CONTENT/TITLE"/>
      <!-- BB6 -->
      <xsl:value-of select="CONTENT/TITLE/@value"/>
	  </ZIBA_NAME>
    <TYPE>label</TYPE>
    <INSTANCE><xsl:value-of select="$mod_number"/></INSTANCE>
    <ADDED>1094240775</ADDED>
    <DELETED>0</DELETED>
    <SCORE>0</SCORE>
    <INDENT><xsl:value-of select="$indent"/></INDENT>
    <VISIBLE>1</VISIBLE>
    <GROUPMODE>0</GROUPMODE>
  </MOD>
   
</xsl:template>

<!-- Creates one section-mod-resource -->
<xsl:template name="section_mod_resource" >
   <xsl:param name="mod_number">1. </xsl:param>
   <xsl:param name="indent">1. </xsl:param>
  <MOD>
    <ID><xsl:value-of select="$mod_number"/>0</ID>
	  <ZIBA_NAME>
      <!-- BB5.5 -->
      <xsl:value-of select="CONTENT/TITLE"/>
      <!-- BB6 -->
      <xsl:value-of select="CONTENT/TITLE/@value"/>
	  </ZIBA_NAME>
    <TYPE>resource</TYPE>
    <INSTANCE><xsl:value-of select="$m