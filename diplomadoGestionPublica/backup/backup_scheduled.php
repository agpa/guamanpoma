<?php //$Id: backup_scheduled.php,v 1.38.2.4 2008/12/15 21:28:02 stronk7 Exp $
    //This file contains all the code needed to execute scheduled backups

//This function is executed via moodle cron
//It prepares all the info and execute backups as necessary
function schedule_backup_cron() {

    global $CFG;

    $status = true;

    $emailpending = false;

    //Check for required functions...
    if(!function_exists('utf8_encode')) {
        mtrace("        ERROR: You need to add XML support to your PHP installation!");
        return true;
    }

    //Get now
    $now = time();

    //First of all, we have to see if the scheduled is active and detect
    //that there isn't another cron running
    mtrace("    Checking backup status",'...');
    $backup_config = backup_get_config();
    if(!isset($backup_config->backup_sche_active) || !$backup_config->backup_sche_active) {
        mtrace("INACTIVE");
        return true;
    } else if (isset($backup_config->backup_sche_running) && $backup_config->backup_sche_running) {
        mtrace("RUNNING");
        //Now check if it's a really running task or something very old looking
        //for info in backup_logs to unlock status as necessary
        $timetosee = 1800;   //Half an hour looking for activity
        $timeafter = time() - $timetosee;
        $numofrec = count_records_select ("backup_log","time > $timeafter");
        if (!$numofrec) {
            $timetoseemin = $timetosee/60;
            mtrace("    No activity in last ".$timetoseemin." minutes. Unlocking status");
        } else {
            mtrace("    Scheduled backup seems to be running. Execution delayed");
            return true;
        }
    } else {
        mtrace("OK");
        //Mark backup_sche_running
        backup_set_config("backup_sche_running","1");
    }

    //Now we get the main admin user (we'll use his timezone, mail...)
    mtrace("    Getting admin info");
    $admin = get_admin();
    if (!$admin) {
        $status = false;
    }

    //Delete old_entries from backup tables
    if ($status) {
        mtrace("    Deleting old data");
        $status = backup_delete_old_data();
    }

    //Now we get a list of courses in the server
    if ($status) {
        mtrace("    Checking courses");
        //First of all, we delete everything from backup tables related to deleted courses
        mtrace("        Skipping deleted courses");
        $skipped = 0;
        if ($bckcourses = get_records('backup_courses')) {
            foreach($bckcourses as $bckcourse) {
                //Search if it exists
                if (!$exists = get_record('course', 'id', "$bckcourse->courseid")) {
                    //Doesn't exist, so delete from backup tables
                    delete_records('backup_courses', 'courseid', "$bckcourse->courseid");
                    delete_records('backup_log', 'courseid', "$bckcourse->courseid");
                    $skipped++;
                }
            }
        }
        mtrace("            $skipped courses");
        //Now process existing courses
        $courses = get_records("course");
        //For each course, we check (insert, update) the backup_course table
        //with needed data
        foreach ($courses as $course) {
            if ($status) {
                mtrace("        $course->fullname");
                //We check if the course exists in backup_course
                $backup_course = get_record("backup_courses","courseid",$course->id);
                //If it doesn't exist, create
                if (!$backup_course) {
                    $temp_backup_course->courseid = $course->id;
                    $newid = insert_record("backup_courses",$temp_backup_course);
                    //And get it from db
                    $backup_course = get_record("backup_courses","id",$newid);
                }
                //If it doesn't exist now, error
                if (!$backup_course) {
                    mtrace("            ERROR (in backup_courses detection)");
                    $status = false;
                    continue;
                }
                // Skip backup of unavailable courses that have remained unmodified in a month
                $skipped = false;
                if (!$course->visible && ($now - $course->timemodified) > 31*24*60*60) {  //Hidden + unmodified last month
                    mtrace("            SKIPPING - hidden+unmodified");
                    set_field("backup_courses","laststatus","3","courseid",$backup_course->courseid);
                    $skipped = true;
                }
                //Now we backup every non skipped course with nextstarttime < now
                if (!$skipped  && $backup_course->nextstarttime > 0 && $backup_course->nextstarttime < $now) {
                    //We have to send a email because we have included at least one backup
                    $emailpending = true;
                    //Only make the backup if laststatus isn't 2-UNFINISHED (uncontrolled error)
                    if ($backup_course->laststatus != 2) {
                        //Set laststarttime
                        $starttime = time();
                        set_field("backup_courses","laststarttime",$starttime,"courseid",$backup_course->courseid);
                        //Set course status to unfinished, the process will reset it
                        set_field("backup_courses","laststatus","2","courseid",$backup_course->courseid);
                        //Launch backup
                        $course_status = schedule_backup_launch_backup($course,$starttime);
                        //Set lastendtime
                        set_field("backup_courses","lastendtime",time(),"courseid",$backup_course->courseid);
                        //Set laststatus
                        if ($course_status) {
                            set_field("backup_courses","laststatus","1","courseid",$backup_course->courseid);
                        } else {
                            set_field("backup_courses","laststatus","0","courseid",$backup_course->courseid);
                        }
                    }
                }

                //Now, calculate next execution of the course
                $nextstarttime = schedule_backup_next_execution ($backup_course,$backup_config,$now,$admin->timezone);
                //Save it to db
                set_field("backup_courses","nextstarttime",$nextstarttime,"courseid",$backup_course->courseid);
                //Print it to screen as necessary
                $showtime = "undefined";
                if ($nextstarttime > 0) {
                    $showtime = userdate($nextstarttime,"",$admin->timezone);
                }
                mtrace("            Next execution: $showtime");
            }
        }
    }

    //Delete old logs
    if (!empty($CFG->loglifetime)) {
        mtrace("    Deleting old logs");
        $loglifetime = $now - ($CFG->loglifetime * 86400);
        delete_records_select("backup_log", "laststarttime < '$loglifetime'");
    }

    //Send email to admin if necessary
    if ($emailpending) {
        mtrace("    Sending email to admin");
        $message = "";

        //Get info about the status of courses
        $count_all = count_records('backup_courses');
        $count_ok = count_records('backup_courses','laststatus','1');
        $count_error = count_records('backup_courses','laststatus','0');
        $count_unfinished = count_records('backup_courses','laststatus','2');
        $count_skipped = count_records('backup_courses','laststatus','3');

        //Build the message text
        //Summary
        $message .= get_string('summary')."\n";
        $message .= "==================================================\n";
        $message .= "  ".get_string('courses').": ".$count_all."\n";
        $message .= "  ".get_string('ok').": ".$count_ok."\n";
        $message .= "  ".get_string('skipped').": ".$count_skipped."\n";
        $message .= "  ".get_string('error').": ".$count_error."\n";
        $message .= "  ".get_string('unfinished').": ".$count_unfinished."\n\n";

        //Reference
        if ($count_error != 0 || $count_unfinished != 0) {
            $message .= "  ".get_string('backupfailed')."\n\n";
            $dest_url = "$CFG->wwwroot/$CFG->admin/report/backups/index.php";
            $message .= "  ".get_string('backuptakealook','',$dest_url)."\n\n";
            //Set message priority
            $admin->priority = 1;
            //Reset unfinished to error
            set_field('backup_courses','laststatus','0','laststatus','2');
        } else {
            $message .= "  ".get_string('backupfinished')."\n";
        }

        //Build the message subject
        $site = get_site();
        $prefix = $site->shortname.": ";
        if ($count_error != 0 || $count_unfinished != 0) {
            $prefix .= "[".strtoupper(get_string('error'))."] ";
        }
        $subject = $prefix.get_string("scheduledbackupstatus");

        //Send the message
        email_to_user($admin,$admin,$subject,$message);
    }


    //Everything is finished stop backup_sche_running
    backup_set_config("backup_sche_running","0");

    return $status;
}

//This function executes the ENTIRE backup of a course (passed as parameter)
//using all the scheduled backup preferences
function schedule_backup_launch_backup($course,$starttime = 0) {

    $preferences = false;
    $status = false;

    mtrace("            Executing backup");
    schedule_backup_log($starttime,$course->id,"Start backup course $course->fullname");
    schedule_backup_log($starttime,$course->id,"  Phase 1: Checking and counting:");
    $preferences = schedule_backup_course_configure($course,$starttime);

    if ($preferences) {
        schedule_backup_log($starttime,$course->id,"  Phase 2: Executing and copying:");
        $status = schedule_backup_course_execute($preferences,$starttime);
    }

    if ($status && $preferences) {
        //Only if the backup_sche_keep is set
        if ($preferences->backup_keep) {
            schedule_backup_log($starttime,$course->id,"  Phase 3: Deleting old backup files:");
            $status = schedule_backup_course_delete_old_files($preferences,$starttime);
        }
    }

    if ($status && $preferences) {
        mtrace("            End backup OK");
        schedule_backup_log($starttime,$course->id,"End backup course $course->fullname - OK");
    } else {
        mtrace("            End backup with ERROR");
        schedule_backup_log($starttime,$course->id,"End backup course $course->fullname - ERROR!!");
    }

    return $status && $preferences;
}

//This function saves to backup_log all the needed process info
//to use it later.  NOTE: If $starttime = 0 no info in saved
function schedule_backup_log($starttime,$courseid,$message) {

    if ($starttime) {
        $log->courseid = $courseid;
        $log->time = time();
        $log->laststarttime = $starttime;
        $log->info = addslashes($message);

        insert_record ("backup_log",$log);
    }

}

//This function returns the next future GMT time to execute the course based in the
//configuration of the scheduled backups
function schedule_backup_next_execution ($backup_course,$backup_config,$now,$timezone) {

    $result = -1;

    //Get today's midnight GMT
    $midnight = usergetmidnight($now,$timezone);

    //Get today's day of week (0=Sunday...6=Saturday)
    $date = usergetdate($now,$timezone);
    $dayofweek = $date['wday'];

    //Get number of days (from today) to execute backups
    $scheduled_days = substr($backup_config->backup_sche_weekdays,$dayofweek).
                      $backup_config->backup_sche_weekdays;
    $daysfromtoday = strpos($scheduled_days, "1");

    //If some day has been found
    if ($daysfromtoday !== false) {
        //Calculate distance
        $dist = ($daysfromtoday * 86400) +                     //Days distance
                ($backup_config->backup_sche_hour*3600) +      //Hours distance
                ($backup_config->backup_sche_minute*60);       //Minutes distance
        $result = $midnight + $dist;
    }

    //If that time is past, call the function recursively to obtain the next valid day
    if ($result > 0 && $result < time()) {
        $result = schedule_backup_next_execution ($backup_course,$backup_config,$now + 86400,$timezone);
    }

    return $result;
}



//This function implements all the needed code to prepare a course
//to be in backup (insert temp info into backup temp tables).
function schedule_backup_course_configure($course,$starttime = 0) {

    global $CFG;

    $status = true;

    schedule_backup_log($starttime,$course->id,"    checking parameters");

    //Check the required variable
    if (empty($course->id)) {
        $status = false;
    }
    //Get scheduled backup preferences
    $backup_config =  backup_get_config();

    //Checks backup_config pairs exist
    if ($status) {
        if (!isset($backup_config->backup_sche_modules)) {
            $backup_config->backup_sche_modules = 1;
        }
        if (!isset($backup_config->backup_sche_withuserdata)) {
            $backup_config->backup_sche_withuserdata = 1;
        }
        if (!isset($backup_config->backup_sche_metacourse)) {
            $backup_config->backup_sche_metacourse = 1;
        }
        if (!isset($backup_config->backup_sche_users)) {
            $backup_config->backup_sche_users = 1;
        }
        if (!isset($backup_config->backup_sche_logs)) {
            $backup_config->backup_sche_logs = 0;
        }
        if (!isset($backup_config->backup_sche_userfiles)) {
            $backup_config->backup_sche_userfiles = 1;
        }
        if (!isset($backup_config->backup_sche_coursefiles)) {
            $backup_config->backup_sche_coursefiles = 1;
        }
        if (!isset($backup_config->backup_sche_sitefiles)) {
            $backup_config->backup_sche_sitefiles = 1;
        }
        if (!isset($backup_config->backup_sche_gradebook_history)) {
            $backup_config->backup_sche_gradebook_history = 0;
        }
        if (!isset($backup_config->backup_sche_messages)) {
            $backup_config->backup_sche_messages = 0;
        }
        if (!isset($backup_config->backup_sche_blogs)) {
            $backup_config->backup_sche_blogs = 0;
        }
        if (!isset($backup_config->backup_sche_active)) {
            $backup_config->backup_sche_active = 0;
        }
        if (!isset($backup_config->backup_sche_weekdays)) {
            $backup_config->backup_sche_weekdays = "0000000";
        }
        if (!isset($backup_config->backup_sche_hour)) {
            $backup_config->backup_sche_hour = 00;
        }
        if (!isset($backup_config->backup_sche_minute)) {
            $backup_config->backup_sche_minute = 00;
        }
        if (!isset($backup_config->backup_sche_destination)) {
            $backup_config->backup_sche_destination = "";
        }
        if (!isset($backup_config->backup_sche_keep)) {
            $backup_config->backup_sche_keep = 1;
        }
    }

    if ($status) {
       //Checks for the required files/functions to backup every mod
        //And check if there is data about it
        $count = 0;
        if ($allmods = get_records("modules") ) {
            foreach ($allmods as $mod) {
                $modname = $mod->name;
                $modfile = "$CFG->di