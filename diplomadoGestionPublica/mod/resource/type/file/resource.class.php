<?php /**/ ?><?php // $Id: resource.class.php,v 1.71.2.31 2009/11/25 06:36:19 dongsheng Exp $

/**
* Extend the base resource class for file resources
*/
class resource_file extends resource_base {

    function resource_file($cmid=0) {
        parent::resource_base($cmid);
    }

    var $parameters;
    var $maxparameters = 5;


    /**
    * Sets the parameters property of the extended class
    *
    * @param    USER  global object
    * @param    CFG   global object
    */
    function set_parameters() {
        global $USER, $CFG;

        $site = get_site();

        $littlecfg = new object;       // to avoid some notices later
        $littlecfg->wwwroot = $CFG->wwwroot;


        $this->parameters = array(
                'label2'          => array('langstr' => "",
                                           'value'   =>'/optgroup'),
                'label3'          => array('langstr' => get_string('course'),
                                           'value'   => 'optgroup'),

                'courseid'        => array('langstr' => 'id',
                                           'value'   => $this->course->id),
                'coursefullname'  => array('langstr' => get_string('fullname'),
                                           'value'   => $this->course->fullname),
                'courseshortname' => array('langstr' => get_string('shortname'),
                                           'value'   => $this->course->shortname),
                'courseidnumber'  => array('langstr' => get_string('idnumbercourse'),
                                           'value'   => $this->course->idnumber),
                'coursesummary'   => array('langstr' => get_string('summary'),
                                           'value'   => $this->course->summary),
                'courseformat'    => array('langstr' => get_string('format'),
                                           'value'   => $this->course->format),
                'courseteacher'   => array('langstr' => get_string('wordforteacher'),
                                           'value'   => $this->course->teacher),
                'courseteachers'  => array('langstr' => get_string('wordforteachers'),
                                           'value'   => $this->course->teachers),
                'coursestudent'   => array('langstr' => get_string('wordforstudent'),
                                           'value'   => $this->course->student),
                'coursestudents'  => array('langstr' => get_string('wordforstudents'),
                                           'value'   => $this->course->students),

                'label4'          => array('langstr' => "",
                                           'value'   =>'/optgroup'),
                'label5'          => array('langstr' => get_string('miscellaneous'),
                                           'value'   => 'optgroup'),

                'lang'            => array('langstr' => get_string('preferredlanguage'),
                                           'value'   => current_language()),
                'sitename'        => array('langstr' => get_string('fullsitename'),
                                           'value'   => format_string($site->fullname)),
                'serverurl'       => array('langstr' => get_string('serverurl', 'resource', $littlecfg),
                                           'value'   => $littlecfg->wwwroot),
                'currenttime'     => array('langstr' => get_string('time'),
                                           'value'   => time()),
                'encryptedcode'   => array('langstr' => get_string('encryptedcode'),
                                           'value'   => $this->set_encrypted_parameter()),

                'label6'          => array('langstr' => "",
                                           'value'   =>'/optgroup')
        );

        if (!empty($USER->id)) {

            $userparameters = array(

                'label1'          => array('langstr' => get_string('user'),
                                           'value'   => 'optgroup'),

                'userid'          => array('langstr' => 'id',
                                           'value'   => $USER->id),
                'userusername'    => array('langstr' => get_string('username'),
                                           'value'   => $USER->username),
                'useridnumber'    => array('langstr' => get_string('idnumber'),
                                           'value'   => $USER->idnumber),
                'userfirstname'   => array('langstr' => get_string('firstname'),
                                           'value'   => $USER->firstname),
                'userlastname'    => array('langstr' => get_string('lastname'),
                                           'value'   => $USER->lastname),
                'userfullname'    => array('langstr' => get_string('fullname'),
                                           'value'   => fullname($USER)),
                'useremail'       => array('langstr' => get_string('email'),
                                           'value'   => $USER->email),
                'usericq'         => array('langstr' => get_string('icqnumber'),
                                           'value'   => $USER->icq),
                'userphone1'      => array('langstr' => get_string('phone').' 1',
                                           'value'   => $USER->phone1),
                'userphone2'      => array('langstr' => get_string('phone2').' 2',
                                           'value'   => $USER->phone2),
                'userinstitution' => array('langstr' => get_string('institution'),
                                           'value'   => $USER->institution),
                'userdepartment'  => array('langstr' => get_string('department'),
                                           'value'   => $USER->department),
                'useraddress'     => array('langstr' => get_string('address'),
                                           'value'   => $USER->address),
                'usercity'        => array('langstr' => get_string('city'),
                                           'value'   => $USER->city),
                'usertimezone'    => array('langstr' => get_string('timezone'),
                                           'value'   => get_user_timezone_offset()),
                'userurl'         => array('langstr' => get_string('webpage'),
                                           'value'   => $USER->url)
             );

             $this->parameters = $userparameters + $this->parameters;
        }
    }

    function add_instance($resource) {
        $this->_postprocess($resource);
        return parent::add_instance($resource);
    }


    function update_instance($resource) {
        $this->_postprocess($resource);
        return parent::update_instance($resource);
    }

    function _postprocess(&$resource) {
        global $RESOURCE_WINDOW_OPTIONS;
        $alloptions = $RESOURCE_WINDOW_OPTIONS;

        if (!empty($resource->forcedownload)) {
            $resource->popup = '';
            $resource->options = 'forcedownload';

        } else if ($resource->windowpopup) {
            $optionlist = array();
            foreach ($alloptions as $option) {
                $optionlist[] = $option."=".$resource->$option;
                unset($resource->$option);
            }
            $resource->popup = implode(',', $optionlist);
            unset($resource->windowpopup);
            $resource->options = '';

        } else {            
            if (empty($resource->framepage)) {
                $resource->options = '';
            } else {
                switch ($resource->framepage) {
                    case 1:
                        $resource->options = 'frame';
                        break;
                    case 2:
                        $resource->options = 'objectframe';
                        break;
                    default:
                        $resource->options = '';
                        break;
                }
            }
            unset($resource->framepage);
            $resource->popup = '';
        }

        $optionlist = array();
        for ($i = 0; $i < $this->maxparameters; $i++) {
            $parametername = "parameter$i";
            $parsename = "parse$i";
            if (!empty($resource->$parsename) and $resource->$parametername != "-") {
                $optionlist[] = $resource->$parametername."=".$resource->$parsename;
            }
            unset($resource->$parsename);
            unset($resource->$parametername);
        }

        $resource->alltext = implode(',', $optionlist);
    }


    /**
    * Display the file resource
    *
    * Displays a file resource embedded, in a frame, or in a popup.
    * Output depends on type of file resource.
    *
    * @param    CFG     global object
    */
    function display() {
        global $CFG, $THEME, $USER;

    /// Set up generic stuff first, including checking for access
        parent::display();

    /// Set up some shorthand variables
        $cm = $this->cm;
        $course = $this->course;
        $resource = $this->resource;


        $this->set_parameters(); // set the parameters array

    ///////////////////////////////////////////////

        /// Possible display modes are:
        /// File displayed embedded in a normal page
        /// File displayed in a popup window
        /// File displayed embedded in a popup window
        /// File not displayed at all, but downloaded


        /// First, find out what sort of file we are dealing with.
        require_once($CFG->libdir.'/filelib.php');

        $querystring = '';
        $resourcetype = '';
        $embedded = false;
        $mimetype = mimeinfo("type", $resource->reference);
        $pagetitle = strip_tags($course->shortname.': '.format_string($resource->name));

        $formatoptions = new object();
        $formatoptions->noclean = true;

        if ($resource->options != "forcedownload") { // TODO nicolasconnault 14-03-07: This option should be renamed "embed"
            if (in_array($mimetype, array('image/gif','image/jpeg','image/png'))) {  // It's an image
                $resourcetype = "image";
                    $embedded = true;

            } else if ($mimetype == "audio/mp3") {    // It's an MP3 audio file
                $resourcetype = "mp3";
                    $embedded = true;

            } else if ($mimetype == "video/x-flv") {    // It's a Flash video file
                $resourcetype = "flv";
                    $embedded = true;

            } else if (substr($mimetype, 0, 10) == "video/x-ms") {   // It's a Media Player file
                $resourcetype = "mediaplayer";
                    $embedded = true;

            } else if ($mimetype == "video/quicktime") {   // It's a Quicktime file
                $resourcetype = "quicktime";
                    $embedded = true;

            } else if ($mimetype == "application/x-shockwave-flash") {   // It's a Flash file
                $resourcetype = "flash";
                    $embedded = true;

            } else if ($mimetype == "video/mpeg") {   // It's a Mpeg file
                $resourcetype = "mpeg";
                    $embedded = true;

            } else if ($mimetype == "text/html") {    // It's a web page
                $resourcetype = "html";

            } else if ($mimetype == "application/zip") {    // It's a zip archive
                $resourcetype = "zip";
                    $embedded = true;

            } else if ($mimetype == 'application/pdf' || $mimetype == 'application/x-pdf') {
                $resourcetype = "pdf";
                //no need embedded, html file types behave like unknown file type
                
            } else if ($mimetype == "audio/x-pn-realaudio-plugin") {   // It's a realmedia file
                $resourcetype = "rm";
                    $embedded = true;
            }
        } 

        $isteamspeak = (stripos($resource->reference, 'teamspeak://') === 0);

    /// Form the parse string
        $querys = array();
        if (!empty($resource->alltext)) {
            $parray = explode(',', $resource->alltext);
            foreach ($parray as $fieldstring) {
                list($moodleparam, $urlname) = explode('=', $fieldstring);
                $value = urlencode($this->parameters[$moodleparam]['value']);
                $querys[urlencode($urlname)] = $value;
                $querysbits[] = urlencode($urlname) . '=' . $value;
            }
            if ($isteamspeak) {
                $querystring = implode('?', $querysbits);
            } else {
                $querystring = implode('&amp;', $querysbits);
            }
        }


        /// Set up some variables

        $inpopup = optional_param('inpopup', 0, PARAM_BOOL);

        if (resource_is_url($resource->reference)) {
            $fullurl = $resource->reference;
            if (!empty($querystring)) {
                $urlpieces = parse_url($resource->reference);
                if (empty($urlpieces['query']) or $isteamspeak) {
                    $fullurl .= '?'.$querystring;
                } else {
                    $fullurl .= '&amp;'.$querystring;
                }
            }

        } else if ($CFG->resource_allowlocalfiles and (strpos($resource->reference, RESOURCE_LOCALPATH) === 0)) {  // Localpath
            $localpath = get_user_preferences('resource_localpath', 'D:');
            $relativeurl = str_replace(RESOURCE_LOCALPATH, $localpath, $resource->reference);

            if ($querystring) {
                $relativeurl .= '?'.$querystring;
            }

            $relativeurl = str_replace('\\', '/', $relativeurl);
            $relativeurl = str_replace(' ', '%20', $relativeurl);
            $fullurl = 'file:///'.htmlentities($relativeurl);
            $localpath = true;

        } else {   // Normal uploaded file
            $forcedownloadsep = '?';
            if ($resource->options == 'forcedownload') {
                $querys['forcedownload'] = '1';
            }
            $fullurl = get_file_url($course->id.'/'.$resource->reference, $querys);
        }

        /// Print a notice and redirect if we are trying to access a file on a local file system
        /// and the config setting has been disabled
        if (!$CFG->resource_allowlocalfiles and (strpos($resource->reference, RESOURCE_LOCALPATH) === 0)) {
            if ($inpopup) {
                print_header($pagetitle, $course->fullname);
            } else {
                $navigation = build_navigation($this->navlinks, $cm);
                print_header($pagetitle, $course->fullname, $navigation,
                        "", "", true, update_module_button($cm->id, $course->id, $this->strresource), navmenu($course, $cm));
            }
            notify(get_string('notallowedlocalfileaccess', 'resource', ''));
            if ($inpopup) {
                close_window_button();
            }
            print_footer('none');
            die;
        }


        /// Check whether this is supposed to be a popup, but was called directly
        if ($resource->popup and !$inpopup) {    /// Make a page and a pop-up window
            $navigation = build_navigation($this->navlinks, $cm);
            print_header($pagetitle, $course->fullname, $navigation,
                    "", "", true, update_module_button($cm->id, $course->id, $this->strresource), navmenu($course, $cm));

            echo "\n<script type=\"text/javascript\">";
            echo "\n<!--\n";
            echo "openpopup('/mod/resource/view.php?inpopup=true&id={$cm->id}','resource{$resource->id}','{$resource->popup}');\n";
            echo "\n-->\n";
            echo '</script>';

            if (trim(strip_tags($resource->summary))) {
                print_simple_box(format_text($resource->summary, FORMAT_MOODLE, $formatoptions), "center");
            }

            $link = "<a href=\"$CFG->wwwroot/mod/resource/view.php?inpopup=true&amp;id={$cm->id}\" "
                  . "onclick=\"this.target='resource{$resource->id}'; return openpopup('/mod/resource/view.php?inpopup=true&amp;id={$cm->id}', "
                  . "'resource{$resource->id}','{$resource->popup}');\">".format_string($resource->name,true)."</a>";

            echo '<div class="popupnotice">';
            print_string('popupresource', 'resource');
            echo '<br />';
            print_string('popupresourcelink', 'resource', $link);
            echo '</div>';
            print_footer($course);
            exit;
        }


        /// Now check whether we need to display a frameset

        $frameset = optional_param('frameset', '', PARAM_ALPHA);
        if (empty($frameset) and !$embedded and !$inpopup and ($resource->options == "frame" || $resource->options == "objectframe") and empty($USER->screenreader)) {
        /// display the resource into a object tag
            if ($resource->options == "objectframe") {
            /// Yahoo javascript libaries for updating embedded object size
                require_js(array('yui_utilities'));
                require_js(array('yui_container'));
                require_js(array('yui_dom-event'));
                require_js(array('yui_dom'));


            /// Moodle Header and navigation bar
                $navigation = build_navigation($this->navlinks, $cm);
                print_header($pagetitle, $course->fullname, $navigation, "", "", true, update_module_button($cm->id, $course->id, $this->strresource), navmenu($course, $cm, "parent"));
                $options = new object();
                $options->para = false;
                if (!empty($localpath)) {  // Show some help
                    echo '<div class="mdl-right helplink">';
                    link_to_popup_window ('/mod/resource/type/file/localpath.php', get_string('localfile', 'resource'), get_string('localfilehelp','resource'), 400, 500, get_string('localfilehelp', 'resource'));
                    echo '</div>';
                }
                echo '</div></div>';

            /// embedded file into iframe if the resource is on anothe