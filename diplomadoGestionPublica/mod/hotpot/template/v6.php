<?php /**/ ?><?PHP

class hotpot_xml_quiz_template extends hotpot_xml_template_default {
    // left and right items for JMatch
    var $l_items = array();
    var $r_items = array();

    // constructor function for this class
    function hotpot_xml_quiz_template(&$parent) {

        $this->parent = &$parent;

        $get_js = optional_param('js', false);
        $get_css = optional_param('css', false);

        if (!empty($get_css)) {
            // set $this->css
            $this->v6_expand_StyleSheet();

        } else if (!empty($get_js)) {
            // set $this->js
            $this->read_template($this->parent->draganddrop.$this->parent->quiztype.'6.js_', 'js');

        } else {
            // set $this->html
            $this->read_template($this->parent->draganddrop.$this->parent->quiztype.'6.ht_', 'html');
        }

        // expand special strings, if any
        $pattern = '';
        switch ($this->parent->quiztype) {
            case 'jcloze':
                $pattern = '/\[(PreloadImageList)\]/';
                break;
            case 'jcross':
                $pattern = '/\[(PreloadImageList|ShowHideClueList)\]/';
                break;
            case 'jmatch':
                $pattern = '/\[(PreloadImageList|QsToShow|FixedArray|DragArray)\]/';
                break;
            case 'jmix':
                $pattern = '/\[(PreloadImageList|SegmentArray|AnswerArray)\]/';
                break;
            case 'jquiz':
                $pattern = '/\[(PreloadImageList|QsToShow)\]/';
                break;
        }
        if (!empty($pattern)) {
            $this->expand_strings('html', $pattern);
        }
        // fix doctype (convert short dtd to long dtd)
        $this->html = preg_replace(
            '/<!DOCTYPE[^>]*>/',
            '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
            $this->html, 1
        );
    }

    // captions and messages

    function v6_expand_AlsoCorrect() {
        return $this->parent->xml_value('hotpot-config-file,'.$this->parent->quiztype.',also-correct');
    }
    function v6_expand_CapitalizeFirst() {
        return $this->bool_value('hotpot-config-file,'.$this->parent->quiztype.',capitalize-first-letter');
    }
    function v6_expand_CheckCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,check-caption');
    }
    function v6_expand_CorrectIndicator() {
        return $this->js_value('hotpot-config-file,global,correct-indicator');
    }
    function v6_expand_Back() {
        return $this->int_value('hotpot-config-file,global,include-back');
    }
    function v6_expand_BackCaption() {
        return str_replace('<=', '&lt;=', $this->parent->xml_value('hotpot-config-file,global,back-caption'));
    }
    function v6_expand_ClickToAdd() {
        return $this->parent->xml_value('hotpot-config-file,'.$this->parent->quiztype.',click-to-add');
    }
    function v6_expand_ClueCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,clue-caption');
    }
    function v6_expand_Clues() {
        return $this->int_value('hotpot-config-file,'.$this->parent->quiztype.',include-clues');
    }
    function v6_expand_Contents() {
        return $this->int_value('hotpot-config-file,global,include-contents');
    }
    function v6_expand_ContentsCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,contents-caption');
    }
    function v6_expand_GuessCorrect() {
        return $this->js_value('hotpot-config-file,'.$this->parent->quiztype.',guess-correct');
    }
    function v6_expand_GuessIncorrect() {
        return $this->js_value('hotpot-config-file,'.$this->parent->quiztype.',guess-incorrect');
    }
    function v6_expand_Hint() {
        return $this->int_value('hotpot-config-file,'.$this->parent->quiztype.',include-hint');
    }
    function v6_expand_HintCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,hint-caption');
    }
    function v6_expand_IncorrectIndicator() {
        return $this->js_value('hotpot-config-file,global,incorrect-indicator');
    }
    function v6_expand_LastQCaption() {
        $caption = $this->parent->xml_value('hotpot-config-file,global,last-q-caption');
        return ($caption=='<=' ? '&lt;=' : $caption);
    }
    function v6_expand_NextCorrect() {
        $value = $this->js_value('hotpot-config-file,'.$this->parent->quiztype.',next-correct-part');
        if (empty($value)) { // jquiz
            $value = $this->js_value('hotpot-config-file,'.$this->parent->quiztype.',next-correct-letter');
        }
        return $value;
    }
    function v6_expand_NextEx() {
        return $this->int_value('hotpot-config-file,global,include-next-ex');
    }
    function v6_expand_NextExCaption() {
        return str_replace('=>', '=&gt;', $this->parent->xml_value('hotpot-config-file,global,next-ex-caption'));
    }
    function v6_expand_NextQCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,next-q-caption');
    }
    function v6_expand_OKCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,ok-caption');
    }
    function v6_expand_Restart() {
        return $this->int_value('hotpot-config-file,'.$this->parent->quiztype.',include-restart');
    }
    function v6_expand_RestartCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,restart-caption');
    }
    function v6_expand_ShowAllQuestionsCaption() {
        return $this->js_value('hotpot-config-file,global,show-all-questions-caption');
    }
    function v6_expand_ShowOneByOneCaption() {
        return $this->js_value('hotpot-config-file,global,show-one-by-one-caption');
    }
    function v6_expand_TheseAnswersToo() {
        return $this->js_value('hotpot-config-file,'.$this->parent->quiztype.',also-correct');
    }
    function v6_expand_ThisMuch() {
        return $this->js_value('hotpot-config-file,'.$this->parent->quiztype.',this-much-correct');
    }
    function v6_expand_Undo() {
        return $this->int_value('hotpot-config-file,'.$this->parent->quiztype.',include-undo');
    }
    function v6_expand_UndoCaption() {
        return $this->parent->xml_value('hotpot-config-file,global,undo-caption');
    }
    function v6_expand_YourScoreIs() {
        return $this->js_value('hotpot-config-file,global,your-score-is');
    }

    // reading

    function v6_expand_Reading() {
        return $this->int_value('data,reading,include-reading');
    }
    function v6_expand_ReadingText() {
        $title = $this->v6_expand_ReadingTitle();
        $value = $this->parent->xml_value('data,reading,reading-text');
        $value = empty($value) ? '' : ('<div class="ReadingText">'.$value.'</div>');
        return $title.$value;
    }
    function v6_expand_ReadingTitle() {
        $value = $this->parent->xml_value('data,reading,reading-title');
        return empty($value) ? '' : ('<h3 class="ExerciseSubtitle">'.$value.'</h3>');
    }

    // timer

    function v6_expand_Timer() {
        return $this->int_value('data,timer,include-timer');
    }
    function v6_expand_JSTimer() {
        return $this->read_template('hp6timer.js_');
    }
    function v6_expand_Seconds() {
        return $this->parent->xml_value('data,timer,seconds');
    }

    // send results

    function v6_expand_SendResults() {
        return $this->parent->xml_value('hotpot-config-file,'.$this->parent->quiztype.',send-email');
    }
    function v6_expand_JSSendResults() {
        return $this->read_template('hp6sendresults.js_');
    }
    function v6_expand_FormMailURL() {
        return $this->parent->xml_value('hotpot-config-file,global,formmail-url');
    }
    function v6_expand_EMail() {
        return $this->parent->xml_value('hotpot-config-file,global,email');
    }
    function v6_expand_NamePlease() {
        return $this->js_value('hotpot-config-file,global,name-please');
    }

    // preload images

    function v6_expand_PreloadImages() {
        $value = $this->v6_expand_PreloadImageList();
        return empty($value) ? false : true;
    }
    function v6_expand_PreloadImageList() {

        // check it has not been set already
        if (!isset($this->PreloadImageList)) {

            // the list of image urls
            $list = array();

            // extract <img> tags
            $img_tag = htmlspecialchars('|&#x003C;img.*?src="(.*?)".*?&#x003E;|is');
            if (preg_match_all($img_tag, $this->parent->source, $matches)) {
                $list = $matches[1];

                // remove duplicates
                $list = array_unique($list);
            }

            // convert to comma delimited string
            $this->PreloadImageList = empty($list) ? '' : "'".implode("','", $list)."'";
        }
        return $this->PreloadImageList;
    }

    // html files (all quiz types)

    function v6_expand_PlainTitle() {
        return $this->parent->xml_value('data,title');
    }
    function v6_expand_ExerciseSubtitle() {
        return $this->parent->xml_value('hotpot-config-file,'.$this->parent->quiztype.',exercise-subtitle');
    }
    function v6_expand_Instructions() {
        return $this->parent->xml_value('hotpot-config-file,'.$this->parent->quiztype.',instructions');
    }
    function v6_expand_DublinCoreMetadata() {
        $dc = '<link rel="schema.DC" href="'.$this->parent->xml_value('', "['rdf:RDF'][0]['@']['xmlns:dc']").'" />'."\n";
        if (is_string($this->parent->xml_value('rdf:RDF,rdf:Description'))) {
            // do nothing (there is no more dc info)
        } else {
            $dc .= '<meta name="DC:Creator" content="'.$this->parent->xml_value('rdf:RDF,rdf:Description,dc:creator').'" />'."\n";
            $dc .= '<meta name="DC:Title" content="'.strip_tags($this->parent->xml_value('rdf:RDF,rdf:Description,dc:title')).'" />'."\n";
        }
        return $dc;
    }
    function v6_expand_FullVersionInfo() {
        global $CFG;
        require_once($CFG->hotpotroot.DIRECTORY_SEPARATOR.'version.php'); // set $module
        return $this->parent->xml_value('version').'.x (Moodle '.$CFG->release.', hotpot-module '.$this->parent->obj_value($module, 'release').')';
    }
    function v6_expand_HeaderCode() {
        return $this->parent->xml_value('hotpot-config-file,global,header-code');
    }
    function v6_expand_StyleSheet() {
        $this->read_template('hp6.cs_', 'css');
        $this->css = hotpot_convert_stylesheets_urls($this->parent->get_baseurl(), $this->parent->reference, $this->css);
        return $this->css;
    }

    // stylesheet (hp6.cs_)

    function v6_expand_PageBGColor() {
        return $this->parent->xml_value('hotpot-config-file,global,page-bg-color');
    }
    function v6_expand_GraphicURL() {
        return $this->parent->xml_value('hotpot-config-file,global,graphic-url');
    }
    function v6_expand_ExBGColor() {
        return $this->parent->xml_value('hotpot-config-file,global,ex-bg-color');
    }

    function v6_expand_FontFace() {
        return $this->parent->xml_value('hotpot-config-file,global,font-face');
    }
    function v6_expand_FontSize() {
        $value = $this->parent->xml_value('hotpot-config-file,global,font-size');
        return (empty($value) ? 'small' : $value);
    }
    function v6_expand_TextColor() {
        return $this->parent->xml_value('hotpot-config-file,global,text-color');
    }
    function v6_expand_TitleColor() {
        return $this->parent->xml_value('hotpot-config-file,global,title-color');
    }
    function v6_expand_LinkColor() {
        return $this->parent->xml_value('hotpot-config-file,global,link-color');
    }
    function v6_expand_VLinkColor() {
        return $this->parent->xml_value('hotpot-config-file,global,vlink-color');
    }

    function v6_expand_NavTextColor() {
        return $this->parent->xml_value('hotpot-config-file,global,page-bg-color');
    }
    function v6_expand_NavBarColor() {
        return $this->parent->xml_value('hotpot-config-file,global,nav-bar-color');
    }
    function v6_expand_NavLightColor() {
        $color = $this->parent->xml_value('hotpot-config-file,global,nav-bar-color');
        return $this->get_halfway_color($color, '#ffffff');
    }
    function v6_expand_NavShadeColor() {
        $color = $this->parent->xml_value('hotpot-config-file,global,nav-bar-color');
        return $this->get_halfway_color($color, '#000000');
    }

    function v6_expand_FuncLightColor() { // top-left of buttons
        $color = $this->parent->xml_value('hotpot-config-file,global,ex-bg-color');
        return $this->get_halfway_color($color, '#ffffff');
    }
    function v6_expand_FuncShadeColor() { // bottom right of buttons
        $color = $this->parent->xml_value('hotpot-config-file,global,ex-bg-color');
        return $this->get_halfway_color($color, '#000000');
    }

    // navigation buttons

    function v6_expand_NavButtons() {
        $back = $this->v6_expand_Back();
        $next_ex = $this->v6_expand_NextEx();
        $contents = $this->v6_expand_Contents();
        return (empty($back) && empty($next_ex) && empty($contents) ? false : true);
    }
    function v6_expand_NavBarJS() {
        return $this->v6_expand_NavButtons();
    }

    // switch off scorm
    function v6_expand_Scorm12() {
        return false;
    }

    // js files (all quiz types)

    function v6_expand_JSBrowserCheck() {
        return $this->read_template('hp6browsercheck.js_');
    }
    function v6_expand_JSButtons() {
        return $this->read_template('hp6buttons.js_');
    }
    function v6_expand_JSCard() {
        return $this->read_template('hp6card.js_');
    }
    function v6_expand_JSCheckShortAnswer() {
        return $this->read_template('hp6checkshortanswer.js_');
    }
    function v6_expand_JSHotPotNet() {
        return $this->read_template('hp6hotpotnet.js_');
    }
    function v6_expand_JSShowMessage() {
        return $this->read_template('hp6showmessage.js_');
    }
    function v6_expand_JSUtilities() {
        return $this->read_template('hp6utilities.js_');
    }

    // js files

    function v6_expand_JSJCloze6() {
        return $this->read_template('jcloze6.js_');
    }
    function v6_expand_JSJCross6() {
        return $this->read_template('jcross6.js_');
    }
    function v6_expand_JSJMatch6() {
        return $this->read_template('jmatch6.js_');
    }
    function v6_expand_JSJMix6() {
        return $this->read_template('jmix6.js_');
    }
    function v6_expand_JSJQuiz6() {
        return $this->read_template('jquiz6.js_');
    }

    // drag and drop

    function v6_expand_JSDJMatch6() {
        return $this->read_template('djmatch6.js_');
    }
    function v6_expand_JSDJMix6() {
        return $this->read_template('djmix6.js_');
    }

    // what are these for?

    function v6_expand_JSFJMatch6() {
        return $this->read_template('fjmatch6.js_');
    }
    function v6_expand_JSFJMix6() {
        return $this->read_template('fjmix6.js_');
    }

    // jmatch6.js_

    function v6_expand_ShuffleQs() {
        return $this->bool_value('hotpot-config-file,'.$this->parent->quiztype.',shuffle-questions');
    }
    function v6_expand_QsToShow() {
        $i = $this->parent->xml_value('hotpot-config-file,'.$this->parent->quiztype.',show-limited-questions');
        if ($i) {
            $i = $this->parent->xml_value('hotpot-config-file,'.$this->parent->quiztype.',questions-to-show');
        }
        if (empty($i)) {
            $i = 0;
            switch ($this->parent->quiztype) {
                case 'jmatch':
                    $values = $this->parent->xml_values('data,matching-exercise,pair');
                    $i = count($values);
                    break;
                case 'jquiz':
                    $tags = 'data,questions,question-record';
                    while (($question="[$i]['#']") && $this->parent->xml_value($tags, $question)) {
                        $i++;
                    }
                    break;
            } // end switch
        }
        return $i;
    }
    function v6_expand_MatchDivItems() {
        $this->set_jmatch_items();

        $l_keys = $this->shuffle_jmatch_items($this->l_items);
        $r_keys = $this->shuffle_jmatch_items($this->r_items);

        $options = '<option value="x">'.$this->parent->xml_value('data,matching-exercise,default-right-item').'</option>';
        foreach ($r_keys as $key) {
            if (! $this->r_items[$key]['fixed']) {
                $options .= '<option value="'.$key.'">'.$this->r_items[$key]['text'].'</option>'."\n";
            }
        }

        $str = '';
        foreach ($l_keys as $key) {
            $str .= '<tr><td class="LeftItem">'.$this->l_items[$key]['text'].'</td>';
            $str .= '<td class="RightItem">';
            if ($this->r_items[$key]['fixed']) {
                $str .= $this->r_items[$key]['text'];
            }  else {
                $str .= '<select id="s'.$key.'_'.$key.'">'.$options.'</select>';
            }
            $str .= '</td><td></td></tr>';
        }
        return $str;
    }

    // jmix6.js_

    function v6_expand_Punctuation() {
        $tags = 'data,jumbled-order-exercise';
        $chars = array_merge(
            $this->jmix_Punctuation("$tags,main-order,segment"),
            $this->jmix_Punctuation("$tags,alternate")
        );
        $chars = array_unique($chars);
        $chars = implode('', $chars);
        $chars = $this->js_safe($chars, true);
        return $chars;
    }
    function jmix_Punctuation($tags) {
        $chars = array();

        // all punctutation except '&#;' (because they are used in html entities)
        $ENTITIES = $this->jmix_encode_punctuation('!"$%'."'".'()*+,-./:<=>?@[\]^_`{|}~');
        $pattern = "/&#x([0-9A-F]+);/i";
        $i = 0;

        // get next segment (or alternate answer)
        while ($value = $this->parent->xml_value($tags, "[$i]['#']")) {

            // convert low-ascii punctuation to entities
            $value = strtr($value, $ENTITIES);

            // extract all hex HTML entities
            if (preg_match_all($pattern, $value, $matches)) {

                // loop through hex entities
                $m_max = count($matches[0]);
                for ($m=0; $m<$m_max; $m++) {

                    // convert to hex number
                    eval('$hex=0x'.$matches[1][$m].';');

                    // is this a punctuation character?
                    if (
                        ($hex>=0x0020 && $hex<=0x00BF) || // ascii punctuation
                        ($hex>=0x2000 && $hex<=0x206F) || // general punctuation
                        ($hex>=0x3000 && $hex<=0x303F) || // CJK punctuation
                        ($hex>=0xFE30 && $hex<=0xFE4F) || // CJK compatability
                        ($hex>=0xFE50 && $hex<=0xFE6F) || // small form variants
                        ($hex>=0xFF00 && $hex<=0xFF40) || // halfwidth and fullwidth forms (1)
                        ($hex>=0xFF5B && $hex<=0xFF65) || // halfwidth and fullwidth forms (2)
                        ($hex>=0xFFE0 && $hex<=0xFFEE)    // halfwidth and fullwidth forms (3)
                    ) {
                        // add this character
                        $chars[] = $matches[0][$m];
                    }
                }
            }
            $i++;
        }

        return $chars;
    }
    function v6_expand_OpenPunctuation() {
        $tags = 'data,jumbled-order-exercise';
        $chars = array_merge(
            $this->jmix_OpenPunctuation("$tags,main-order,segment"),
            $this->jmix_OpenPunctuation("$tags,alternate")
        );
        $chars = array_unique($chars);
        $chars = implode('', $chars);
        $chars = $this->js_safe($chars, true);
        return $chars;
    }
    function jmix_OpenPunctuation($tags) {
        $chars = array();

        // unicode punctuation designations (pi="initial quote", ps="open")
        //  http://www.sql-und-xml.de/unicode-database/pi.html
        //  http://www.sql-und-xml.de/unicode-database/ps.html
        $pi = '0022|0027|00AB|2018|201B|201C|201F|2039';
        $ps = '0028|005B|007B|0F3A|0F3C|169B|201A|201E|2045|207D|208D|2329|23B4|2768|276A|276C|276E|2770|2772|2774|27E6|27E8|27EA|2983|2985|2987|2989|298B|298D|298F|2991|2993|2995|2997|29D8|29DA|29FC|3008|300A|300C|300E|3010|3014|3016|3018|301A|301D|FD3E|FE35|FE37|FE39|FE3B|FE3D|FE3F|FE41|FE43|FE47|FE59|FE5B|FE5D|FF08|FF3B|FF5B|FF5F|FF62';
        $pattern = "/(&#x($pi|$ps);)/i";

        $ENTITIES = $this->jmix_encode_punctuation('"'."'".'(<[{');

        $i = 0;
        while ($value = $this->parent->xml_value($tags, "[$i]['#']")) {
            $value = strtr($value, $ENTITIES);
            if (preg_match_all($pattern, $value, $matches)) {
                $chars = array_merge($chars, $matches[0]);
            }
            $i++;
        }

        return $chars;
    }
    function jmix_encode_punctuation($str) {
        $ENTITIES = array();
        $i_max = strlen($str);
        for ($i=0; $i<$i_max; $i++) {
            $ENTITIES[$str{$i}] = '&#x'.sprintf('%04X', ord($str{$i})).';';
        }
        return $ENTITIES;
    }
    function v6_expand_ExerciseTitle() {
        return $this->parent->xml_value('data,title');
    }

    // Jmix specials

    function v6_expand_SegmentArray() {

        $segments = array();
        $values = array();
        $VALUES = array();

        // XML tags to the start of a segment
        $tags = 'data,jumbled-order-exercise,main-order,segment';

        $i = 0;
        while ($value = $this->parent->xml_value($tags, "[$i]['#']")) {
            $VALUE = strtoupper($value);
            $key = array_search($VALUE, $VALUES);
            if (is_numeric($key)) {
                $segments[] = $key;
            } else {
                $segments[] = $i;
                $values[$i] = $value;
                $VALUES[$i] = $VALUE;
            }
            $i++;
        }

        $this->seed_random_number_generator();
        $keys = array_keys($segments);
        shuffle($keys);

        $str = '';
        for($i=0; $i<count($keys); $i++) {
            $key = $segments[$keys[$i]];
            $str .= "Segments[$i] = new Array();\n";
            $str .= "Segments[$i][0] = '".$this->js_safe($values[$key], true)."';\n";
            $str .= "Segments[$i][1] = ".($key+1).";\n";
            $str .= "Segments[$i][2] = 0;\n";
        }
        return $str;
    }
    function v6_expand_AnswerArray() {

        $segments = array();
        $values = array();
        $VALUES = array();
        $escapedvalues = array();

        // XML tags to the start of a segment
        $tags = 'data,jumbled-order-exercise,main-order,segment';

        $i = 0;
        while ($value = $this->parent->xml_value($tags, "[$i]['#']")) {
            $VALUE = strtoupper($value);
            $key = array_search($VALUE, $VALUES);
            if (is_numeric($key)) {
                $segments[] = $key+1;
            } else {
                $segments[] = $i+1;
                $values[$i] = $value;
                $VALUES[$i] = $VALUE;
                $escapedvalues[] = preg_quote($value, '/');
            }
            $i++;
        }

        // start the answers array
        $a = 0;
        $str = 'Answers['.($a++).'] = new Array('.implode(',', $segments).");\n";

        // pattern to match the next part of an alternate answer
        $pattern = '/^('.implode('|', $escapedvalues).')\\s*/i';

        // XML tags to the start of an alternate answer
        $tags = 'data,jumbled-order-exercise,alternate';

        $i = 0;
        while ($value = $this->parent->xml_value($tags, "[$i]['#']")) {
            $segments = array();
            while (strlen($value) &