<!--
// PLEASE NOTE that this version is more recent than the incorrectly
// numbered v6.1, dated 2003.11.17. From now on, version numbers will
// follow those of Hot Potatoes.
/* hot-potatoes.js (v6.0.4.0 - 2005.02.18)
 * =======================================
 * by Gordon Bateson, February 2003
 * Copyright (c) 2003 Gordon Bateson. All Rights Reserved.
 *
 * You are hereby granted a royalty free license to use or modify this
 * software provided that this copyright notice appears on all copies.
 *
 * This software is provided "AS IS" without a warranty of any kind.
 *
 * Documentation and downloads may be available from:
 * http://www.kanazawa-gu.ac.jp/~gordon/research/hot-potatoes/
 */
// This JavaScript library modifies the SendResults and StartUp functions
// used by hotpot v5 and v6, so that more (or less!) details about the
// student can be input, and more details of a quiz's questions and answers
// can be submitted to the server when the quiz is finished
// If the arrays below (Login, DB, JBC, ...) are set BEFORE calling this
// script, they will NOT be overwritten. Any array that is not set, will
// use the defaults below. This is useful if you want to use different
// settings for different quizzes.
// ************
//  Login Screen
// ************
if (window.Login==null) {
	Login = new Array();
	Login[0] = true;	// Show prompt for user name
				// This can also be a string of user names ...
				// Login[0] = "Guest,Peter,Paul,Mary,Webmaster";
				// or an array of user names (and on-screen texts) (and passwords) ...
				// Login[0] = new Array("Guest", "001,Peter,xxxx", "002,Paul,yyyy", "003,Mary,zzzz", "Webmaster");
				// and can also be  written as ...
				// Login[0] = new Array(
				//	new Array("Guest"),
				//	new Array("001", "Peter", "xxxx"),
				//	new Array("002", "Paul", "yyyy"),
				//	new Array("003", "Mary", "zzzz"),
				//	new Array("Webmaster")
				// );
	Login[1] = true;	// Show prompt for student's UserID
				// If there is no password prompt (i.e. Logon[3] is false), this value
				// will be checked against the password information, if any, in Login[0]
	Login[2] = false;	// Show prompt for student's email
	Login[3] = false;	// Show prompt for quiz password, and check this value against
				// the password information, if any, in Login[0]
				// This can also be a string required to start the quiz ...
				// Login[3] = "password";
	Login[4] = true;	// Show prompt for the cookie expiry date
				// If false, cookies expire at the end of the current session
	Login[5] = "guest,webmaster"
				// guest user names (case insensitive) ...
				// Login[5] = "guest,webmaster";
				// These users do NOT need to fill in other login fields
				// and their quiz results are NOT added to the database
	// the Login prompts and error messages
	// are defined in the MSG array (see below)
}
// *********
//  Database (for use with BFormMail)
// *********
if (window.DB==null) {
	DB = new Array();
	DB[0] = true; // append form fields to database on server
			// If you are NOT using BFormMail's database feature,
			// set DB[0]=false, and you can then safely ignore DB[1 to 5]
	DB[1] = "/home/gordon/public_html/cgi/hot-potatoes-data";
			// append_db folder path (no trailing slash)
			// Can be either an absolute path  e.g. "/home/gordon/public_html/cgi/hot-potatoes-data"
			// or a relative (to CGI bin) path  e.g. "hot-potatoes-data"
	DB[2] = "hot-potatoes";
			// append_db file name (no extension)
			// If left blank, the quiz file name, without extension, will be used
			// i.e. each quiz will have its results stored in a different file.
			// If filled in, this file will store the results for ALL quizzes.
			// Database files and folders must be set up BEFORE running the quiz
			// must have appropriate access privileges (on Unix, use "chmod 666").
	DB[3] = ""; // append_db extension (if left blank, ".txt" will be used)
	DB[4] = ""; // db_fields (if left blank, ALL quiz fields will be sent)
	DB[5] = ""; // db_delimiter (if left blank, tab will be used)
	DB[6] = "REMOTE_ADDR,HTTP_USER_AGENT";
			// env_report ('REMOTE_ADDR','HTTP_USER_AGENT' and a few others)
	// for a complete description of these fields are, see ...
	// http://www.infosheet.com/stuff/BFormMail.readme
	// Switches DB[7] and DB[8] force the settings in the ResultForm
	// In v5 and v6 quizzes, these settings wil be override those in the original quiz
	// If the quiz results are to be sent to an LMS (via the "store" form)
	// then switches DB[7] and DB[8] are not used
	DB[7] = '';	// URL of form processing script
			// e.g. http://www.kanazawa-gu.ac.jp/~gordon/cgi/bformmail.cgi
	DB[8] = '';	// email address to which results should be sent
			// e.g. gordon@kanazawa-gu.ac.jp
}
// By default the quiz's question's scores will be returned.
// If you want more detailed information, set the flags below:
// ********
//  JBC
// ********
if (window.JBC==null) {
	JBC = new Array();
	JBC[0] = true;	// show separator line between answers on email
	JBC[1] = true;	// show number of attempts to answer question
	JBC[2] = true;	// show question texts
	JBC[3] = true;	// show right answer(s)
	JBC[4] = true;	// show wrong answer(s)
	JBC[5] = true;	// show ignored answer(s)
	JBC[6] = false;	// show answer as text (false) or number (true)
}
// JBC quizzes use the global variables 'I' and 'Status'
// I : an array of JBC_QUESTIONs (one for each question)
// JBC_QUESTION :
//	[0] : question text
//	[1] : array of JBC_ANSWERs (one for each answer)
//	[2] : single/multi flag
//		0 : single answer (using 'button')
//		1 : multiple answers (using '