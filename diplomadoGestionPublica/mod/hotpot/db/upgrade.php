<?php /**/ ?><?php  //$Id: upgrade.php,v 1.1.8.6 2009/03/16 01:46:54 gbateson Exp $

// This file keeps track of upgrades to the hotpot module

function xmldb_hotpot_upgrade($oldversion=0) {

    global $CFG, $THEME, $db;

    $result = true;

//===== 1.9.0 upgrade line ======//

    if ($result && $oldversion < 2007101512) {
        // save and disable setting to display debugging messages
        $debug = $db->debug;
        $db->debug = false;

        notify('Fixing hotpot grades, this may take a while if there are many hotpots...', 'notifysuccess');
        hotpot_fix_grades();

        // restore $db->debug
        $db->debug = $debug;
    }

    // update hotpot grades from sites earlier than Moodle 1.9, 27th March 2008
    if ($result && $oldversion < 2007101513) {

        // ensure "hotpot_update_grades" function is available
        require_once $CFG->dirroot.'/mod/hotpot/lib.php';

        // save and disable setting to display debugging messages
        $debug = $db->debug;
        $db->debug = false;

        notify('Processing hotpot grades, this may take a while if there are many hotpots...', 'notifysuccess');
        hotpot_update_grades();

        // restore $db->debug
        $db->debug = $debug;
    }

    return $result;
}

function hotpot_fix_grades($print=true, $usehotpotname=1) {
    // if hotpot name and grade are different ...
    //     $usehotpotname=0: set hotpot name equal to grade name
    //     $usehotpotname=1: set grade name equal to hotpot name
    global $CFG, $db;

    require_once($CFG->dirroot.'/lib/gradelib.php');

    if (! $module = get_record('modules', 'name', 'hotpot')) {
        if ($print) {
            print_error('error_nohotpot', 'hotpot');
        } else {
            debugging(get_string('error_nohotpot', 'hotpot'), DEBUG_DEVELOPER);
        }
    }

    if (! $hotpots = get_records('hotpot')) {
        $hotpots = array();
    }

    if(! $gradeitems = get_records_select('grade_items', "itemtype='mod' AND itemmodule='hotpot'")) {
        $gradeitems = array();
    }

    $success = '<font color="green">OK</font>'."\n";
    $failure = '<font color="red">FAILED</font>'."\n";
    $not = '<font color="red">NOT</font>'."\n";
    $new = get_string('newvalue', 'hotpot');
    $old = get_string('oldvalue', 'hotpot');

    $hotpots_no_grade = array(); // hotpots without a grade item
    $hotpots_no_weighting = array(); // hotpots with zero grade limit/weighting
    $gradeitems_wrong_name = array(); // grade items that have a different name from their hotpot
    $gradeitems_no_hotpot = array(); // grade items without a hotpot
    $gradeitems_no_idnumber = array(); // grade items without an idnumber (= course_modules id)

    foreach (array_keys($gradeitems) as $id) {
        $hotpotid = $gradeitems[$id]->iteminstance;
        if (array_key_exists($hotpotid, $hotpots)) {
            $hotpots[$hotpotid]->gradeitem = &$gradeitems[$id];
            if (empty($gradeitems[$id]->idnumber)) {
                $gradeitems_no_idnumber[$id] = &$gradeitems[$id];
            }
            if ($gradeitems[$id]->itemname != $hotpots[$hotpotid]->name) {
                $gradeitems_wrong_name[$id] = &$gradeitems[$id];
            }
        } else {
            $gradeitems_no_hotpot[$id] = &$gradeitems[$id];
        }
    }

    foreach ($hotpots as $id=>$hotpot) {
        if ($hotpot->grade==0) {
            // no grade item required, because grade is always 0
            // transfer this hotpot to "no_weighting" array
            $hotpots_no_weighting[$id] = &$hotpots[$id];
            if (isset($hotpot->gradeitem)) {
                // grade item not required
                $gradeitemid = $hotpot->gradeitem->id;
                $gradeitems_no_hotpot[$gradeitemid] = &$gradeitems[$gradeitemid];
                unset($hotpots[$id]->gradeitem);
            }
        } else {
            if (empty($hotpot->gradeitem)) {
                // grade item required, but missing
                $hotpots_no_grade[$id] = &$hotpots[$id];
            }
        }
    }

    $output = '';
    $start_list = false;
    $count_idnumber_updated = 0;
    $count_idnumber_notupdated = 0;
    foreach ($gradeitems_no_idnumber as $id=>$gradeitem) {
        $idnumber = get_field('course_modules', 'idnumber', 'module', $module->id, 'instance', $gradeitem->iteminstance);
        if (! $idnumber) {
            unset($gradeitems_no_idnumber[$id]);
            continue;
        }
        if (! $start_list) {
            $start_list = true;
            if ($print) {
                print '<ul>'."\n";
            }
        }
        if ($print) {
            $a = 'grade_item(id='.$id.').idnumber: '.$new.'='.$idnumber;
            print '<li>'.get_string('updatinga', '', $a).' ... ';
        }
        if (set_field('grade_items', 'idnumber', addslashes($idnumber), 'id', $id)) {
            $count_idnumber_updated++;
            if ($print) {
                print $success;
            }
        } else {
            $count_idnumber_notupdated++;
            if ($print) {
                print $failure;
            }
        }
        if ($print) {
            print '</li>'."\n";
        }
    }
    if ($start_list) {
        if ($print) {
            print '</ul>'."\n";
        }
    }

    $start_list = false;
    $count_name_updated = 0;
    $count_name_notupdated = 0;
    foreach ($gradeitems_wrong_name as $id=>$gradeitem) {
        $gradename = $gradeitem->itemname;
        $hotpotid = $gradeitem->iteminstance;
        $hotpotname = $hotpots[$hotpotid]->name;
        if (! $start_list) {
            $start_list = t