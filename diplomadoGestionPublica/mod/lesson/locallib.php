<?php /**/ ?><?php // $Id: locallib.php,v 1.71.2.5 2009/03/17 16:24:47 mark-nielsen Exp $
/**
 * Local library file for Lesson.  These are non-standard functions that are used
 * only by Lesson.
 *
 * @version $Id: locallib.php,v 1.71.2.5 2009/03/17 16:24:47 mark-nielsen Exp $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package lesson
 **/

/**
* Next page -> any page not seen before
*/    
if (!defined("LESSON_UNSEENPAGE")) {
    define("LESSON_UNSEENPAGE", 1); // Next page -> any page not seen before
}
/**
* Next page -> any page not answered correctly
*/
if (!defined("LESSON_UNANSWEREDPAGE")) {
    define("LESSON_UNANSWEREDPAGE", 2); // Next page -> any page not answered correctly
}

/**
* Define different lesson flows for next page
*/
$LESSON_NEXTPAGE_ACTION = array (0 => get_string("normal", "lesson"),
                          LESSON_UNSEENPAGE => get_string("showanunseenpage", "lesson"),
                          LESSON_UNANSWEREDPAGE => get_string("showanunansweredpage", "lesson") );

// Lesson jump types defined
//  TODO: instead of using define statements, create an array with all the jump values

/**
 * Jump to Next Page
 */
if (!defined("LESSON_NEXTPAGE")) {
    define("LESSON_NEXTPAGE", -1);
}
/**
 * End of Lesson
 */
if (!defined("LESSON_EOL")) {
    define("LESSON_EOL", -9);
}
/**
 * Jump to an unseen page within a branch and end of branch or end of lesson
 */
if (!defined("LESSON_UNSEENBRANCHPAGE")) {
    define("LESSON_UNSEENBRANCHPAGE", -50);
}
/**
 * Jump to Previous Page
 */
if (!defined("LESSON_PREVIOUSPAGE")) {
    define("LESSON_PREVIOUSPAGE", -40);
}
/**
 * Jump to a random page within a branch and end of branch or end of lesson
 */
if (!defined("LESSON_RANDOMPAGE")) {
    define("LESSON_RANDOMPAGE", -60);
}
/**
 * Jump to a random Branch
 */
if (!defined("LESSON_RANDOMBRANCH")) {
    define("LESSON_RANDOMBRANCH", -70);
}
/**
 * Cluster Jump
 */
if (!defined("LESSON_CLUSTERJUMP")) {
    define("LESSON_CLUSTERJUMP", -80);
}
/**
 * Undefined
 */    
if (!defined("LESSON_UNDEFINED")) {
    define("LESSON_UNDEFINED", -99);
}

// Lesson question types defined

/**
 * Short answer question type
 */
if (!defined("LESSON_SHORTANSWER")) {
    define("LESSON_SHORTANSWER",   "1");
}        
/**
 * True/False question type
 */
if (!defined("LESSON_TRUEFALSE")) {
    define("LESSON_TRUEFALSE",     "2");
}
/**
 * Multichoice question type
 *
 * If you change the value of this then you need 
 * to change it in restorelib.php as well.
 */
if (!defined("LESSON_MULTICHOICE")) {
    define("LESSON_MULTICHOICE",   "3");
}
/**
 * Random question type - not used
 */
if (!defined("LESSON_RANDOM")) {
    define("LESSON_RANDOM",        "4");
}
/**
 * Matching question type
 *
 * If you change the value of this then you need
 * to change it in restorelib.php, in mysql.php 
 * and postgres7.php as well.
 */
if (!defined("LESSON_MATCHING")) {
    define("LESSON_MATCHING",      "5");
}
/**
 * Not sure - not used
 */
if (!defined("LESSON_RANDOMSAMATCH")) {
    define("LESSON_RANDOMSAMATCH", "6");
}
/**
 * Not sure - not used
 */
if (!defined("LESSON_DESCRIPTION")) {
    define("LESSON_DESCRIPTION",   "7");
}
/**
 * Numerical question type
 */
if (!defined("LESSON_NUMERICAL")) {
    define("LESSON_NUMERICAL",     "8");
}
/**
 * Multichoice with multianswer question type
 */
if (!defined("LESSON_MULTIANSWER")) {
    define("LESSON_MULTIANSWER",   "9");
}
/**
 * Essay question type
 */
if (!defined("LESSON_ESSAY")) {
    define("LESSON_ESSAY", "10");
}

/**
 * Lesson question type array.
 * Contains all question types used
 */
$LESSON_QUESTION_TYPE = array ( LESSON_MULTICHOICE => get_string("multichoice", "quiz"),
                              LESSON_TRUEFALSE     => get_string("truefalse", "quiz"),
                              LESSON_SHORTANSWER   => get_string("shortanswer", "quiz"),
                              LESSON_NUMERICAL     => get_string("numerical", "quiz"),
                              LESSON_MATCHING      => get_string("match", "quiz"),
                              LESSON_ESSAY           => get_string("essay", "lesson")
//                            LESSON_DESCRIPTION   => get_string("description", "quiz"),
//                            LESSON_RANDOM        => get_string("random", "quiz"),
//                            LESSON_RANDOMSAMATCH => get_string("randomsamatch", "quiz"),
//                            LESSON_MULTIANSWER   => get_string("multianswer", "quiz"),
                              );

// Non-question page types

/**
 * Branch Table page
 */
if (!defined("LESSON_BRANCHTABLE")) {
    define("LESSON_BRANCHTABLE",   "20");
}
/**
 * End of Branch page
 */
if (!defined("LESSON_ENDOFBRANCH")) {
    define("LESSON_ENDOFBRANCH",   "21");
}
/**
 * Start of Cluster page
 */
if (!defined("LESSON_CLUSTER")) {
    define("LESSON_CLUSTER",   "30");
}
/**
 * End of Cluster page
 */
if (!defined("LESSON_ENDOFCLUSTER")) {
    define("LESSON_ENDOFCLUSTER",   "31");
}

// other variables...

/**
 * Flag for the editor for the answer textarea.
 */
if (!defined("LESSON_ANSWER_EDITOR")) {
    define("LESSON_ANSWER_EDITOR",   "1");
}
/**
 * Flag for the editor for the response textarea.
 */
if (!defined("LESSON_RESPONSE_EDITOR")) {
    define("LESSON_RESPONSE_EDITOR",   "2");
}

//////////////////////////////////////////////////////////////////////////////////////
/// Any other lesson functions go here.  Each of them must have a name that 
/// starts with lesson_

/**
 * Print the standard header for lesson module
 *
 * @uses $CFG
 * @uses $USER
 * @param object $cm Course module record object
 * @param object $course Couse record object
 * @param object $lesson Lesson module record object
 * @param string $currenttab Current tab for the lesson tabs
 * @return boolean
 **/
function lesson_print_header($cm, $course, $lesson, $currenttab = '') {
    global $CFG, $USER;

    $strlesson = get_string('modulename', 'lesson');
    $strname   = format_string($lesson->name, true, $course->id);

    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    if (has_capability('mod/lesson:edit', $context)) {
        $button = update_module_button($cm->id, $course->id, $strlesson);
    } else {
        $button = '';
    }

/// Header setup
    $navigation = build_navigation('', $cm);
    
/// Print header, heading, tabs and messages
    print_header("$course->shortname: $strname", $course->fullname, $navigation,
                  '', '', true, $button, navmenu($course, $cm));

    if (has_capability('mod/lesson:manage', $context)) {
        print_heading_with_help($strname, "overview", "lesson");

        if (!empty($currenttab)) {
            include($CFG->dirroot.'/mod/lesson/tabs.php');
        }
    } else {
        print_heading($strname);
    }

    lesson_print_messages();

    return true;
}

/**
 * Returns course module, course and module instance given 
 * either the course module ID or a lesson module ID.
 *
 * @param int $cmid Course Module ID
 * @param int $lessonid Lesson module instance ID
 * @return array array($cm, $course, $lesson)
 **/
function lesson_get_basics($cmid = 0, $lessonid = 0) {
    if ($cmid) {
        if (!$cm = get_coursemodule_from_id('lesson', $cmid)) {
            error('Course Module ID was incorrect');
        }
        if (!$course = get_record('course', 'id', $cm->course)) {
            error('Course is misconfigured');
        }
        if (!$lesson = get_record('lesson', 'id', $cm->instance)) {
            error('Course module is incorrect');
        }
    } else if ($lessonid) {
        if (!$lesson = get_record('lesson', 'id', $lessonid)) {
            error('Course module is incorrect');
        }
        if (!$course = get_record('course', 'id', $lesson->course)) {
            error('Course is misconfigured');
        }
        if (!$cm = get_coursemodule_from_instance('lesson', $lesson->id, $course->id)) {
            error('Course Module ID was incorrect');
        }
    } else {
        error('No course module ID or lesson ID were passed');
    }
    
    return array($cm, $course, $lesson);
}

/**
 * Sets a message to be printed.  Messages are printed
 * by calling {@link lesson_print_messages()}.
 *
 * @uses $SESSION
 * @param string $message The message to be printed
 * @param string $class Class to be passed to {@link notify()}.  Usually notifyproblem or notifysuccess.
 * @param string $align Alignment of the message
 * @return boolean
 **/
function lesson_set_message($message, $class="notifyproblem", $align='center') {
    global $SESSION;
    
    if (empty($SESSION->lesson_messages) or !is_array($SESSION->lesson_messages)) {
        $SESSION->lesson_messages = array();
    }
    
    $SESSION->lesson_messages[] = array($message, $class, $align);
    
    return true;
}

/**
 * Print all set messages.
 *
 * See {@link lesson_set_message()} for setting messages.
 *
 * Uses {@link notify()} to print the messages.
 *
 * @uses $SESSION
 * @return boolean
 **/
function lesson_print_messages() {
    global $SESSION;
    
    if (empty($SESSION->lesson_messages)) {
        // No messages to print
        return true;
    }
    
    foreach($SESSION->lesson_messages as $message) {
        notify($message[0], $message[1], $message[2]);
    }
    
    // Reset
    unset($SESSION->lesson_messages);
    
    return true;
}

/**
 * Prints a lesson link that submits a form.
 *
 * If Javascript is disabled, then a regular submit button is printed
 *
 * @param string $name Name of the link or button
 * @param string $form The name of the form to be submitted
 * @param string $align Alignment of the button
 * @param string $class Class names to add to the div wrapper
 * @param string $title Title for the link (Not used if javascript is disabled)
 * @param string $id ID tag
 * @param boolean $return Return flag
 * @return mixed boolean/html
 **/
function lesson_print_submit_link($name, $form, $align = 'center', $class='standardbutton', $title = '', $id = '', $return = false) {
    if (!empty($align)) {
        $align = " style=\"text-align:$align\"";
    }
    if (!empty($id)) {
        $id = " id=\"$id\"";
    }
    if (empty($title)) {
        $title = $name;
    }

    $output = "<div class=\"lessonbutton $class\" $align>\n";
    $output .= "<input type=\"submit\" value=\"$name\" $align $id />";
    $output .= "</div>\n";
    
    if ($return) {
        return $output;
    } else {
        echo $output;
        return true;
    }
}

/**
 * Prints a time remaining in the following format: H:MM:SS
 *
 * @param int $starttime Time when the lesson started
 * @param int $maxtime Length of the lesson
 * @param boolean $return Return output switch
 * @return mixed boolean/string
 **/
function lesson_print_time_remaining($starttime, $maxtime, $return = false) {
    // Calculate hours, minutes and seconds
    $timeleft = $starttime + $maxtime * 60 - time();
    $hours = floor($timeleft/3600);
    $timeleft = $timeleft - ($hours * 3600);
    $minutes = floor($timeleft/60);
    $secs = $timeleft - ($minutes * 60);
    
    if ($minutes < 10) {
        $minutes = "0$minutes";
    }
    if ($secs < 10) {
        $secs = "0$secs";
    }
    $output   = array();
    $output[] = $hours;
    $output[] = $minutes;
    $output[] = $secs;
    
    $output = implode(':', $output);
    
    if ($return) {
        return $output;
    } else {
        echo $output;
        return true;
    }
}

/**
 * Prints the page action buttons
 *
 * Move/Edit/Preview/Delete
 *
 * @uses $CFG
 * @param int $cmid Course Module ID
 * @param object $page Page record
 * @param boolean $printmove Flag to print the move button or not
 * @param boolean $printaddpage Flag to print the add page drop-down or not
 * @param boolean $return Return flag
 * @return mixed boolean/string
 **/
function lesson_print_page_actions($cmid, $page, $printmove, $printaddpage = false, $return = false) {
    global $CFG;
    
    $context = get_context_instance(CONTEXT_MODULE, $cmid);
    $actions = array();
    
    if (has_capability('mod/lesson:edit', $context)) {
        if ($printmove) {
            $actions[] = "<a title=\"".get_string('move')."\" href=\"$CFG->wwwroot/mod/lesson/lesson.php?id=$cmid&amp;action=move&amp;pageid=$page->id\">
                          <img src=\"$CFG->pixpath/t/move.gif\" class=\"iconsmall\" alt=\"".get_string('move')."\" /></a>\n";
        }
        $actions[] = "<a title=\"".get_string('update')."\" href=\"$CFG->wwwroot/mod/lesson/lesson.php?id=$cmid&amp;action=editpage&amp;pageid=$page->id\">
                      <img src=\"$CFG->pixpath/t/edit.gif\" class=\"iconsmall\" alt=\"".get_string('update')."\" /></a>\n";
        
        $actions[] = "<a title=\"".get_string('preview')."\" href=\"$CFG->wwwroot/mod/lesson/view.php?id=$cmid&amp;pageid=$page->id\">
                      <img src=\"$CFG->pixpath/t/preview.gif\" class=\"iconsmall\" alt=\"".get_string('preview')."\" /></a>\n";
        
        $actions[] = "<a title=\"".get_string('delete')."\" href=\"$CFG->wwwroot/mod/lesson/lesson.php?id=$cmid&amp;sesskey=".sesskey()."&amp;action=confirmdelete&amp;pageid=$page->id\">
                      <img src=\"$CFG->pixpath/t/delete.gif\" class=\"iconsmall\" alt=\"".get_string('delete')."\" /></a>\n";
        
        if ($printaddpage) {
            // Add page drop-down
            $options = array();
            $options['addcluster&amp;sesskey='.sesskey()]      = get_string('clustertitle', 'lesson');
            $options['addendofcluster&amp;sesskey='.sesskey()] = get_string('endofclustertitle', 'lesson');
            $options['addbranchtable']                         = get_string('branchtable', 'lesson');
            $options['addendofbranch&amp;sesskey='.sesskey()]  = get_string('endofbranch', 'lesson');
            $options['addpage']                                = get_string('question', 'lesson');
            // Base url
            $common = "$CFG->wwwroot/mod/lesson/lesson.php?id=$cmid&amp;pageid=$page->i