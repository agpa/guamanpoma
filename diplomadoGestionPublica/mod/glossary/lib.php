<?php /**/ ?><?php  // $Id: lib.php,v 1.193.2.22 2009/11/22 12:43:10 skodak Exp $

/// Library of functions and constants for module glossary
/// (replace glossary with the name of your module and delete this line)

require_once($CFG->libdir.'/filelib.php');

define("GLOSSARY_SHOW_ALL_CATEGORIES", 0);
define("GLOSSARY_SHOW_NOT_CATEGORISED", -1);

define("GLOSSARY_NO_VIEW", -1);
define("GLOSSARY_STANDARD_VIEW", 0);
define("GLOSSARY_CATEGORY_VIEW", 1);
define("GLOSSARY_DATE_VIEW", 2);
define("GLOSSARY_AUTHOR_VIEW", 3);
define("GLOSSARY_ADDENTRY_VIEW", 4);
define("GLOSSARY_IMPORT_VIEW", 5);
define("GLOSSARY_EXPORT_VIEW", 6);
define("GLOSSARY_APPROVAL_VIEW", 7);

/// STANDARD FUNCTIONS ///////////////////////////////////////////////////////////

function glossary_add_instance($glossary) {
/// Given an object containing all the necessary data,
/// (defined by the form in mod.html) this function
/// will create a new instance and return the id number
/// of the new instance.

    if (empty($glossary->userating)) {
        $glossary->assessed = 0;
    }

    if (empty($glossary->ratingtime) or empty($glossary->assessed)) {
        $glossary->assesstimestart  = 0;
        $glossary->assesstimefinish = 0;
    }

    if (empty($glossary->globalglossary) ) {
        $glossary->globalglossary = 0;
    }

    if (!has_capability('mod/glossary:manageentries', get_context_instance(CONTEXT_SYSTEM))) {
        $glossary->globalglossary = 0;
    }

    $glossary->timecreated  = time();
    $glossary->timemodified = $glossary->timecreated;

    //Check displayformat is a valid one
    $formats = get_list_of_plugins('mod/glossary/formats','TEMPLATE');
    if (!in_array($glossary->displayformat, $formats)) {
        error("This format doesn't exist!");
    }

    if ($returnid = insert_record("glossary", $glossary)) {
        $glossary->id = $returnid;
        $glossary = stripslashes_recursive($glossary);
        glossary_grade_item_update($glossary);
    }

    return $returnid;
}


function glossary_update_instance($glossary) {
/// Given an object containing all the necessary data,
/// (defined by the form in mod.html) this function
/// will update an existing instance with new data.
    global $CFG;

    if (empty($glossary->globalglossary)) {
        $glossary->globalglossary = 0;
    }

    if (!has_capability('mod/glossary:manageentries', get_context_instance(CONTEXT_SYSTEM))) {
        // keep previous
        unset($glossary->globalglossary);
    }

    $glossary->timemodified = time();
    $glossary->id           = $glossary->instance;

    if (empty($glossary->userating)) {
        $glossary->assessed = 0;
    }

    if (empty($glossary->ratingtime) or empty($glossary->assessed)) {
        $glossary->assesstimestart  = 0;
        $glossary->assesstimefinish = 0;
    }

    //Check displayformat is a valid one
    $formats = get_list_of_plugins('mod/glossary/formats','TEMPLATE');
    if (!in_array($glossary->displayformat, $formats)) {
        error("This format doesn't exist!");
    }

    if ($return = update_record("glossary", $glossary)) {
        if ($glossary->defaultapproval) {
            execute_sql("update {$CFG->prefix}glossary_entries SET approved = 1 where approved != 1 and glossaryid = " . $glossary->id,false);
        }
        $glossary = stripslashes_recursive($glossary);
        glossary_grade_item_update($glossary);
    }

    return $return;
}


function glossary_delete_instance($id) {
/// Given an ID of an instance of this module,
/// this function will permanently delete the instance
/// and any data that depends on it.

    if (! $glossary = get_record("glossary", "id", "$id")) {
        return false;
    }

    $result = true;

    # Delete any dependent records here #

    if (! delete_records("glossary", "id", "$glossary->id")) {
        $result = false;
    } else {
        if ($categories = get_records("glossary_categories","glossaryid",$glossary->id)) {
            $cats = "";
            foreach ( $categories as $cat ) {
                $cats .= "$cat->id,";
            }
            $cats = substr($cats,0,-1);
            if ($cats) {
                delete_records_select("glossary_entries_categories", "categoryid in ($cats)");
                delete_records("glossary_categories", "glossaryid", $glossary->id);
            }
        }
        if ( $entries = get_records("glossary_entries", "glossaryid", $glossary->id) ) {
            $ents = "";
            foreach ( $entries as $entry ) {
                if ( $entry->sourceglossaryid ) {
                    $entry->glossaryid = $entry->sourceglossaryid;
                    $entry->sourceglossaryid = 0;
                    update_record("glossary_entries",$entry);
                } else {
                    $ents .= "$entry->id,";
                }
            }
            $ents = substr($ents,0,-1);
            if ($ents) {
                delete_records_select("glossary_comments", "entryid in ($ents)");
                delete_records_select("glossary_alias", "entryid in ($ents)");
                delete_records_select("glossary_ratings", "entryid in ($ents)");
            }
        }
        glossary_delete_attachments($glossary);
        delete_records("glossary_entries", "glossaryid", "$glossary->id");
    }
    glossary_grade_item_delete($glossary);

    return $result;
}

function glossary_user_outline($course, $user, $mod, $glossary) {
    global $CFG;
/// Return a small object with summary information about what a
/// user has done with a given particular instance of this module
/// Used for user activity reports.
/// $return->time = the time they did it
/// $return->info = a short text description
    require_once("$CFG->libdir/gradelib.php");
    $grades = grade_get_grades($course->id, 'mod', 'glossary', $glossary->id, $user->id);
    if (empty($grades->items[0]->grades)) {
        $grade = false;
    } else {
        $grade = reset($grades->items[0]->grades);
    }

    if ($entries = glossary_get_user_entries($glossary->id, $user->id)) {
        $result = new object();
        $result->info = count($entries) . ' ' . get_string("entries", "glossary");

        $lastentry = array_pop($entries);
        $result->time = $lastentry->timemodified;

        if ($grade) {
            $result->info .= ', ' . get_string('grade') . ': ' . $grade->str_long_grade;
        }
        return $result;
    } else if ($grade) {
        $result = new object();
        $result->info = get_string('grade') . ': ' . $grade->str_long_grade;
        $result->time = $grade->dategraded;
        return $result;
    }

    return NULL;
}

function glossary_get_user_entries($glossaryid, $userid) {
/// Get all the entries for a user in a glossary
    global $CFG;

    return get_records_sql("SELECT e.*, u.firstname, u.lastname, u.email, u.picture
                              FROM {$CFG->prefix}glossary g,
                                   {$CFG->prefix}glossary_entries e,
                                   {$CFG->prefix}user u
                             WHERE g.id = '$glossaryid'
                               AND e.glossaryid = g.id
                               AND e.userid = '$userid'
                               AND e.userid = u.id
                          ORDER BY e.timemodified ASC");
}

function glossary_user_complete($course, $user, $mod, $glossary) {
/// Print a detailed representation of what a  user has done with
/// a given particular instance of this module, for user activity reports.
    global $CFG;
    require_once("$CFG->libdir/gradelib.php");

    $grades = grade_get_grades($course->id, 'mod', 'glossary', $glossary->id, $user->id);
    if (!empty($grades->items[0]->grades)) {
        $grade = reset($grades->items[0]->grades);
        echo '<p>'.get_string('grade').': '.$grade->str_long_grade.'</p>';
        if ($grade->str_feedback) {
            echo '<p>'.get_string('feedback').': '.$grade->str_feedback.'</p>';
        }
    }
    if ($entries = glossary_get_user_entries($glossary->id, $user->id)) {
        echo '<table width="95%" border="0"><tr><td>';
        foreach ($entries as $entry) {
            $cm = get_coursemodule_from_instance("glossary", $glossary->id, $course->id);
            glossary_print_entry($course, $cm, $glossary, $entry,"","",0);
            echo '<p>';
        }
        echo '</td></tr></table>';
    }
}

function glossary_print_recent_activity($course, $viewfullnames, $timestart) {
/// Given a course and a time, this module should find recent activity
/// that has occurred in glossary activities and print it out.
/// Return true if there was output, or false is there was none.

    global $CFG, $USER;

    //TODO: use timestamp in approved field instead of changing timemodified when approving in 2.0

    $modinfo = get_fast_modinfo($course);
    $ids = array();
    foreach ($modinfo->cms as $cm) {
        if ($cm->modname != 'glossary') {
            continue;
        }
        if (!$cm->uservisible) {
            continue;
        }
        $ids[$cm->instance] = $cm->instance;
    }

    if (!$ids) {
        return false;
    }

    $glist = implode(',', $ids); // there should not be hundreds of glossaries in one course, right?

    if (!$entries = get_records_sql("SELECT ge.id, ge.concept, ge.approved, ge.timemodified, ge.glossaryid,
                                            ge.userid, u.firstname, u.lastname, u.email, u.picture
                                       FROM {$CFG->prefix}glossary_entries ge
                                            JOIN {$CFG->prefix}user u ON u.id = ge.userid
                                      WHERE ge.glossaryid IN ($glist) AND ge.timemodified > $timestart
                                   ORDER BY ge.timemodified ASC")) {
        return false;
    }

    $editor  = array();

    foreach ($entries as $entryid=>$entry) {
        if ($entry->approved) {
            continue;
        }

        if (!isset($editor[$entry->glossaryid])) {
            $editor[$entry->glossaryid] = has_capability('mod/glossary:approve', get_context_instance(CONTEXT_MODULE, $modinfo->instances['glossary'][$entry->glossaryid]->id));
        }

        if (!$editor[$entry->glossaryid]) {
            unset($entries[$entryid]);
        }
    }

    if (!$entries) {
        return false;
    }
    print_headline(get_string('newentries', 'glossary').':');

    $strftimerecent = get_string('strftimerecent');
    foreach ($entries as $entry) {
        $link = $CFG->wwwroot.'/mod/glossary/view.php?g='.$entry->glossaryid.'&amp;mode=entry&amp;hook='.$entry->id;
        if ($entry->approved) {
            $dimmed = '';
        } else {
            $dimmed = ' dimmed_text';
        }
        echo '<div class="head'.$dimmed.'">';
        echo '<div class="date">'.userdate($entry->timemodified, $strftimerecent).'</div>';
        echo '<div class="name">'.fullname($entry, $viewfullnames).'</div>';
        echo '</div>';
        echo '<div class="info"><a href="'.$link.'">'.format_text($entry->concept, true).'</a></div>';
    }

    return true;
}


function glossary_log_info($log) {
    global $CFG;

    return get_record_sql("SELECT e.*, u.firstname, u.lastname
                             FROM {$CFG->prefix}glossary_entries e,
                                  {$CFG->prefix}user u
                            WHERE e.id = '$log->info'
                              AND u.id = '$log->userid'");
}

function glossary_cron () {
/// Function to be run periodically according to the moodle cron
/// This function searches for things that need to be done, such
/// as sending out mail, toggling flags etc ...

    global $CFG;

    return true;
}

/**
 * Return grade for given user or all users.
 *
 * @param int $glossaryid id of glossary
 * @param int $userid optional user id, 0 means all users
 * @return array array of grades, false if none
 */
function glossary_get_user_grades($glossary, $userid=0) {
    global $CFG;

    $user = $userid ? "AND u.id = $userid" : "";

    $sql = "SELECT u.id, u.id AS userid, avg(gr.rating) AS rawgrade
              FROM {$CFG->prefix}user u, {$CFG->prefix}glossary_entries ge,
                   {$CFG->prefix}glossary_ratings gr
             WHERE u.id = ge.userid AND ge.id = gr.entryid
                   AND gr.userid != u.id AND ge.glossaryid = $glossary->id
                   $user
          GROUP BY u.id";

    return get_records_sql($sql);
}

/**
 * Update grades by firing grade_updated event
 *
 * @param object $glossary null means all glossaries (with extra cmidnumber property)
 * @param int $userid specific user only, 0 mean all
 */
function glossary_update_grades($glossary=null, $userid=0, $nullifnone=true) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    if ($glossary != null) {
        if ($grades = glossary_get_user_grades($glossary, $userid)) {
            glossary_grade_item_update($glossary, $grades);

        } else if ($userid and $nullifnone) {
            $grade = new object();
            $grade->userid   = $userid;
            $grade->rawgrade = NULL;
            glossary_grade_item_update($glossary, $grade);

        } else {
            glossary_grade_item_update($glossary);
        }

    } else {
        $sql = "SELECT g.*, cm.idnumber as cmidnumber
                  FROM {$CFG->prefix}glossary g, {$CFG->prefix}course_modules cm, {$CFG->prefix}modules m
                 WHERE m.name='glossary' AND m.id=cm.module AND cm.instance=g.id";
        if ($rs = get_recordset_sql($sql)) {
            while ($glossary = rs_fetch_next_record($rs)) {
                if ($glossary->assessed) {
                    glossary_update_grades($glossary, 0, false);
                } else {
                    glossary_grade_item_update($glossary);
                }
            }
            rs_close($rs);
        }
    }
}

/**
 * Create/update grade item for given glossary
 *
 * @param object $glossary object with extra cmidnumber
 * @param mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int, 0 if ok, error code otherwise
 */
function glossary_grade_item_update($glossary, $grades=NULL) {
    global $CFG;
    if (!function_exists('grade_update')) { //workaround for buggy PHP versions
        require_once($CFG->libdir.'/gradelib.php');
    }

    $params = array('itemname'=>$glossary->name, 'idnumber'=>$glossary->cmidnumber);

    if (!$glossary->assessed or $glossary->scale == 0) {
        $params['gradetype'] = GRADE_TYPE_NONE;

    } else if ($glossary->scale > 0) {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax']  = $glossary->scale;
        $params['grademin']  = 0;

    } else if ($glossary->scale < 0) {
        $params['gradetype'] = GRADE_TYPE_SCALE;
        $params['scaleid']   = -$glossary->scale;
    }

    if ($grades  === 'reset') {
        $params['reset'] = true;
        $grades = NULL;
    }

    return grade_update('mod/glossary', $glossary->course, 'mod', 'glossary', $glossary->id, 0, $grades, $params);
}

/**
 * Delete grade item for given glossary
 *
 * @param object $glossary object
 */
function glossary_grade_item_delete($glossary) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    return grade_update('mod/glossary', $glossary->course, 'mod', 'glossary', $glossary->id, 0, NULL, array('deleted'=>1));
}

function glossary_get_participants($glossaryid) {
//Returns the users with data in one glossary
//(users with records in glossary_entries, students)

    global $CFG;

    //Get students
    $students = get_records_sql("SELECT DISTINCT u.id, u.id
                                 FROM {$CFG->prefix}user u,
                                      {$CFG->prefix}glossary_entries g
                                 WHERE g.glossaryid = '$glossaryid' and
                                       u.id = g.userid");

    //Return students array (it contains an array of unique users)
    return ($students);
}

function glossary_scale_used ($glossaryid,$scaleid) {
//This function returns if a scale is being used by one glossary

    $return = false;

    $rec = get_record("glossary","id","$glossaryid","scale","-$scaleid");

    if (!empty($rec)  && !empty($scaleid)) {
        $return = true;
    }

    return $return;
}

/**
 * Checks if scale is being used by any instance of glossary
 *
 * This is used to find out if scale used anywhere
 * @param $scaleid int
 * @return boolean True if the scale is used by any glossary
 */
function glossary_scale_used_anywhere($scaleid) {
    if ($scaleid and record_exists('glossary', 'scale', -$scaleid)) {
        return true;
    } else {
        return false;
    }
}

//////////////////////////////////////////////////////////////////////////////////////
/// Any other glossary functions go here.  Each of them must have a name that
/// starts with glossary_

//This function return an array of valid glossary_formats records
//Everytime it's called, every existing format is checked, new formats
//are included if detected and old formats are deleted and any glossary
//using an invalid format is updated to the default (dictionary).
function glossary_get_available_formats() {

    global $CFG;

    //Get available formats (plugin) and insert (if necessary) them into glossary_formats
    $formats = get_list_of_plugins('mod/glossary/formats', 'TEMPLATE');
    $pluginformats = array();
    foreach ($formats as $format) {
        //If the format file exists
        if (file_exists($CFG->dirroot.'/mod/glossary/formats/'.$format.'/'.$format.'_format.php')) {
            include_once($CFG->dirroot.'/mod/glossary/formats/'.$format.'/'.$format.'_format.php');
            //If the function exists
            if (function_exists('glossary_show_entry_'.$format)) {
                //Acummulate it as a valid format
                $pluginformats[] = $format;
                //If the format doesn't exist in the table
                if (!$rec = get_record('glossary_formats','name',$format)) {
                    //Insert the record in glossary_formats
                    $gf = new object();
                    $gf->name = $format;
                    $gf->popupformatname = $format;
             