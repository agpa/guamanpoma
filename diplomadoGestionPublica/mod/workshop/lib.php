<?php /**/ ?><?php  // $Id: lib.php,v 1.98.2.7 2009/11/13 22:47:07 mudrd8mz Exp $

// workshop constants and standard Moodle functions plus the workshop functions
// called by the standard functions

// see also locallib.php for other non-standard workshop functions

require_once($CFG->libdir.'/filelib.php');

/*** Constants **********************************/


$WORKSHOP_EWEIGHTS = array(  0 => -4.0, 1 => -2.0, 2 => -1.5, 3 => -1.0, 4 => -0.75, 5 => -0.5,  6 => -0.25,
                             7 => 0.0, 8 => 0.25, 9 => 0.5, 10 => 0.75, 11=> 1.0, 12 => 1.5, 13=> 2.0,
                             14 => 4.0);

$WORKSHOP_FWEIGHTS = array(  0 => 0, 1 => 0.1, 2 => 0.25, 3 => 0.5, 4 => 0.75, 5 => 1.0,  6 => 1.5,
                             7 => 2.0, 8 => 3.0, 9 => 5.0, 10 => 7.5, 11=> 10.0, 12=>50.0);


$WORKSHOP_ASSESSMENT_COMPS = array (
                          0 => array('name' => get_string('verylax', 'workshop'), 'value' => 1),
                          1 => array('name' => get_string('lax', 'workshop'), 'value' => 0.6),
                          2 => array('name' => get_string('fair', 'workshop'), 'value' => 0.4),
                          3 => array('name' => get_string('strict', 'workshop'), 'value' => 0.33),
                          4 => array('name' => get_string('verystrict', 'workshop'), 'value' => 0.2) );


/*** Moodle 1.7 compatibility functions *****
 *
 ********************************************/
function workshop_context($workshop) {
    //TODO: add some $cm caching if needed
    if (is_object($workshop)) {
        $workshop = $workshop->id;
    }
    if (! $cm = get_coursemodule_from_instance('workshop', $workshop)) {
        error('Course Module ID was incorrect');
    }

    return get_context_instance(CONTEXT_MODULE, $cm->id);
}

function workshop_is_teacher($workshop, $userid=NULL) {
    return has_capability('mod/workshop:manage', workshop_context($workshop), $userid);
}

function workshop_is_teacheredit($workshop, $userid=NULL) {
    return has_capability('mod/workshop:manage', workshop_context($workshop), $userid)
       and has_capability('moodle/site:accessallgroups', workshop_context($workshop), $userid);
}

function workshop_is_student($workshop, $userid=NULL) {
    return has_capability('mod/workshop:participate', workshop_context($workshop), $userid);
}

function workshop_get_students($workshop, $sort='u.lastaccess', $fields='u.*') {
    return $users = get_users_by_capability(workshop_context($workshop), 'mod/workshop:participate', $fields, $sort);
}

function workshop_get_teachers($workshop, $sort='u.lastaccess', $fields='u.*') {
    return $users = get_users_by_capability(workshop_context($workshop), 'mod/workshop:manage', $fields, $sort);
}


/*** Standard Moodle functions ******************
workshop_add_instance($workshop)
workshop_check_dates($workshop)
workshop_cron ()
workshop_delete_instance($id)
workshop_grades($workshopid)
workshop_print_recent_activity(&$logs, $isteacher=false)
workshop_refresh_events($workshop)
workshop_update_instance($workshop)
workshop_user_complete($course, $user, $mod, $workshop)
workshop_user_outline($course, $user, $mod, $workshop)
**********************************************/

///////////////////////////////////////////////////////////////////////////////
function workshop_add_instance($workshop) {
// Given an object containing all the necessary data,
// (defined by the form in mod.html) this function
// will create a new instance and return the id number
// of the new instance.

    $workshop->timemodified = time();

    $workshop->submissionstart = make_timestamp($workshop->submissionstartyear,
            $workshop->submissionstartmonth, $workshop->submissionstartday, $workshop->submissionstarthour,
            $workshop->submissionstartminute);

    $workshop->assessmentstart = make_timestamp($workshop->assessmentstartyear,
            $workshop->assessmentstartmonth, $workshop->assessmentstartday, $workshop->assessmentstarthour,
            $workshop->assessmentstartminute);

    $workshop->submissionend = make_timestamp($workshop->submissionendyear,
            $workshop->submissionendmonth, $workshop->submissionendday, $workshop->submissionendhour,
            $workshop->submissionendminute);

    $workshop->assessmentend = make_timestamp($workshop->assessmentendyear,
            $workshop->assessmentendmonth, $workshop->assessmentendday, $workshop->assessmentendhour,
            $workshop->assessmentendminute);

    $workshop->releasegrades = make_timestamp($workshop->releaseyear,
            $workshop->releasemonth, $workshop->releaseday, $workshop->releasehour,
            $workshop->releaseminute);

    if (!workshop_check_dates($workshop)) {
        return get_string('invaliddates', 'workshop');
    }

    // set the workshop's type
    $wtype = 0; // 3 phases, no grading grades
    if ($workshop->includeself or $workshop->ntassessments) $wtype = 1; // 3 phases with grading grades
    if ($workshop->nsassessments) $wtype = 2; // 5 phases with grading grades
    $workshop->wtype = $wtype;

    if ($returnid = insert_record("workshop", $workshop)) {

        $event = NULL;
        $event->name        = get_string('submissionstartevent','workshop', $workshop->name);
        $event->description = $workshop->description;
        $event->courseid    = $workshop->course;
        $event->groupid     = 0;
        $event->userid      = 0;
        $event->modulename  = 'workshop';
        $event->instance    = $returnid;
        $event->eventtype   = 'submissionstart';
        $event->timestart   = $workshop->submissionstart;
        $event->timeduration = 0;
        add_event($event);

        $event->name        = get_string('submissionendevent','workshop', $workshop->name);
        $event->eventtype   = 'submissionend';
        $event->timestart   = $workshop->submissionend;
        add_event($event);

        $event->name        = get_string('assessmentstartevent','workshop', $workshop->name);
        $event->eventtype   = 'assessmentstart';
        $event->timestart   = $workshop->assessmentstart;
        add_event($event);

        $event->name        = get_string('assessmentendevent','workshop', $workshop->name);
        $event->eventtype   = 'assessmentend';
        $event->timestart   = $workshop->assessmentend;
        add_event($event);
    }

    return $returnid;
}

///////////////////////////////////////////////////////////////////////////////
// returns true if the dates are valid, false otherwise
function workshop_check_dates($workshop) {
    // allow submission and assessment to start on the same date and to end on the same date
    // but enforce non-empty submission period and non-empty assessment period.
    return ($workshop->submissionstart < $workshop->submissionend and
            $workshop->submissionstart <= $workshop->assessmentstart and
            $workshop->assessmentstart < $workshop->assessmentend and
            $workshop->submissionend <= $workshop->assessmentend);
}


///////////////////////////////////////////////////////////////////////////////
function workshop_cron () {
// Function to be run periodically according to the moodle cron

    global $CFG, $USER;

    // if there any ungraded assessments run the grading routine
    if ($workshops = get_records("workshop")) {
        foreach ($workshops as $workshop) {
            // automatically grade assessments if workshop has examples and/or peer assessments
            if ($workshop->gradingstrategy and ($workshop->ntassessments or $workshop->nsassessments)) {
                workshop_grade_assessments($workshop);
            }
        }
    }
    $timenow = time();

    // Find all workshop notifications that have yet to be mailed out, and mails them
    $cutofftime = $timenow - $CFG->maxeditingtime;

    // look for new assessments
    if ($assessments = workshop_get_unmailed_assessments($cutofftime)) {
        foreach ($assessments as $assessment) {

            echo "Processing workshop assessment $assessment->id\n";

            // only process the entry once
            if (! set_field("workshop_assessments", "mailed", "1", "id", "$assessment->id")) {
                echo "Could not update the mailed field for id $assessment->id\n";
            }

            if (! $submission = get_record("workshop_submissions", "id", "$assessment->submissionid")) {
                echo "Could not find submission $assessment->submissionid\n";
                continue;
            }
            if (! $workshop = get_record("workshop", "id", $submission->workshopid)) {
                echo "Could not find workshop id $submission->workshopid\n";
                continue;
            }
            if (! $course = get_record("course", "id", $workshop->course)) {
                error("Could not find course id $workshop->course");
                continue;
            }
            if (! $cm = get_coursemodule_from_instance("workshop", $workshop->id, $course->id)) {
                error("Course Module ID was incorrect");
                continue;
            }
            if (! $submissionowner = get_record("user", "id", "$submission->userid")) {
                echo "Could not find user $submission->userid\n";
                continue;
            }
            if (! $assessmentowner = get_record("user", "id", "$assessment->userid")) {
                echo "Could not find user $assessment->userid\n";
                continue;
            }
            if (! workshop_is_student($workshop, $submissionowner->id) and !workshop_is_teacher($workshop,
                        $submissionowner->id)) {
                continue;  // Not an active participant
            }
            if (! workshop_is_student($workshop, $assessmentowner->id) and !workshop_is_teacher($workshop,
                        $assessmentowner->id)) {
                continue;  // Not an active participant
            }
            // don't sent self assessment
            if ($submissionowner->id == $assessmentowner->id) {
                continue;
            }
            $strworkshops = get_string("modulenameplural", "workshop");
            $strworkshop  = get_string("modulename", "workshop");

            // it's an assessment, tell the submission owner
            $USER->lang = $submissionowner->lang;
            $sendto = $submissionowner;
            // "Your assignment \"$submission->title\" has been assessed by"
            if (workshop_is_student($workshop, $assessmentowner->id)) {
                $msg = get_string("mail1", "workshop", $submission->title)." a $course->student.\n";
            }
            else {
                $msg = get_string("mail1", "workshop", $submission->title).
                    " ".fullname($assessmentowner)."\n";
            }
            // "The comments and grade can be seen in the workshop assignment '$workshop->name'
            // I have taken the following line out because the info is repeated below.
            // $msg .= get_string("mail2", "workshop", $workshop->name)."\n\n";

            $postsubject = "$course->shortname: $strworkshops: ".format_string($workshop->name,true);
            $posttext  = "$course->shortname -> $strworkshops -> ".format_string($workshop->name,true)."\n";
            $posttext .= "---------------------------------------------------------------------\n";
            $posttext .= $msg;
            // "The comments and grade can be seen in ..."
            $posttext .= get_string("mail2", "workshop",
                format_string($workshop->name,true).",   $CFG->wwwroot/mod/workshop/view.php?id=$cm->id")."\n";
            $posttext .= "---------------------------------------------------------------------\n";
            if ($sendto->mailformat == 1) {  // HTML
                $posthtml = "<p><font face=\"sans-serif\">".
                    "<a href=\"$CFG->wwwroot/course/view.php?id=$course->id\">$course->shortname</a> ->".
                    "<a href=\"$CFG->wwwroot/mod/workshop/index.php?id=$course->id\">$strworkshops</a> ->".
                    "<a href=\"$CFG->wwwroot/mod/workshop/view.php?id=$cm->id\">".format_string($workshop->name,true)."</a></font></p>";
                $posthtml .= "<hr><font face=\"sans-serif\">";
                $posthtml .= "<p>$msg</p>";
                $posthtml .= "<p>".get_string("mail2", "workshop",
                    " <a href=\"$CFG->wwwroot/mod/workshop/view.php?id=$cm->id\">".format_string($workshop->name,true)."</a>")."</p></font><hr>";
            } else {
                $posthtml = "";
            }

            if (!$teacher = get_teacher(