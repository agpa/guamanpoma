<?php /**/ ?>// hopefully fool ie IE proof way of getting DOM element
function safeGetElement(doc, el) {
    return doc.ids ? doc.ids[el] : doc.getElementById ? doc.getElementById(el) : doc.all[el];
}

// Add in a JS controlled link for toggling the Debug logging
var logButton = document.createElement('a');
logButton.id = 'mod-scorm-log-toggle';
logButton.name = 'logToggle';
logButton.href = 'javascript:toggleLog();';
if (getLoggingActive() == "A") {
    logButton.innerHTML = '<?php print_string('scormloggingon','scorm') ?>';
} else {
    logButton.innerHTML = '<?php print_string('scormloggingoff','scorm') ?>';
}
var content = safeGetElement(document, 'content');
content.appendChild(logButton);

// retrieve cookie data
function getCookie (cookie_name){
    var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
    if ( results ) {
        return (unescape(results[2]));
    } else {
        return null;
    }
}

// retrieve the logging flag from a Cookie
function getLoggingActive () {
    var loggingActive = getCookie('SCORMLoggingActive');
    if (!loggingActive) {
        loggingActive = 'A';
    }
    return loggingActive;
}

// set the logging flag in a cookie
function setLoggingActive (flag) {
    new cookie("SCORMLoggingActive", flag, 365, "/").set();
}

// toggle the logging
function toggleLog () {
    if (getLoggingActive() == "A") {
        AppendToLog("Moodle Logging Deactivated", 0);
        setLoggingActive('N');
        logButton.innerHTML = '<?php print_string('scormloggingoff','scorm') ?>';
    } else {
        setLoggingActive('A');
        AppendToLog("Moodle Logging Activated", 0);
        logButton.innerHTML = '<?php print_string('scormloggingon','scorm') ?>';
        logPopUpWindow.focus();
    }
}

// globals for the log accumulation
var logString = "";
var logRow = 0;
var logPopUpWindow = "N";
var debugSCORMVersion = '<?php echo $scorm->version; ?>';
<?php
   $LMS_prefix = ($scorm->version == 'scorm_12' || $scorm->version == 'SCORM_1.2' || empty($scorm->version)) ? 'LMS' : '';
   $LMS_api = ($scorm->version == 'scorm_12' || $scorm->version == 'SCORM_1.2' || empty($scorm->version)) ? 'API' : 'API_1484_11';

   $LMS_elements = array();
   if ($scorm->version == 'scorm_12' || $scorm->version == 'SCORM_1.2' || empty($scorm->version)) {
       $LMS_elements = array(   'cmi.core._children',
                                'cmi.core.student_id',
                                'cmi.core.student_name',
                                'cmi.core.lesson_location',
                                'cmi.core.credit',
                                'cmi.core.lesson_status',
                                'cmi.core.entry',
                                'cmi.core._children',
                                'cmi.core.score.raw',
                                'cmi.core.score.max',
                                'cmi.core.score.min',
                                'cmi.core.total_time',
                                'cmi.core.lesson_mode',
                                'cmi.core.exit',
                                'cmi.core.session_time',
                                'cmi.suspend_data',
                                'cmi.launch_data',
                                'cmi.comments',
                                'cmi.comments_from_lms',
                                'cmi.objectives._count',
                                'cmi.objectives._children',
                                'cmi.objectives.n.id',
                                'cmi.objectives.n.score._children',
                                'cmi.objectives.n.score.raw',
                                'cmi.objectives.n.score.min',
                                'cmi.objectives.n.score.max',
                                'cmi.objectives.n.status',
                                'cmi.student_data._children',
                                'cmi.student_data.mastery_score',
                                'cmi.student_data.max_time_allowed',
                                'cmi.student_data.time_limit_action',
                                'cmi.student_preference._children',
                                'cmi.student_preference.audio',
                                'cmi.student_preference.language',
                                'cmi.student_preference.speed',
                                'cmi.student_preference.text',
                                'cmi.interactions._children',
                                'cmi.interactions._count',
                                'cmi.interactions.n.id',
                                'cmi.interactions.n.objectives._count',
                                'cmi.interactions.n.objectives.m.id',
                                'cmi.interactions.n.time',
                                'cmi.interactions.n.type',
                                'cmi.interactions.n.correct_responses._count',
                                'cmi.interactions.n.correct_responses.m.pattern',
                                'cmi.interactions.n.weighting',
                                'cmi.interactions.n.student_response',
                                'cmi.interactions.n.result',
                                'cmi.interactions.n.latency');
   } else {
       $LMS_elements = array(   'cmi._children',
                                'cmi._version',
                                'cmi.learner_id',
                                'cmi.learner_name',
                                'cmi.location',
                                'cmi.completion_status',
                                'cmi.completion_threshold',
                                'cmi.scaled_passing_score',
                                'cmi.progressive_measure',
                                'cmi.score._children',
                                'cmi.score.raw',
                                'cmi.score.max',
                                'cmi.score.min',
                                'cmi.score.scaled',
                                'cmi.total_time',
                                'cmi.time_limit_action',
                                'cmi.max_time_allowed',
                                'cmi.session_time',
                                'cmi.success_status',
                                'cmi.lesson_mode',
                                'cmi.entry',
                                'cmi.exit',
                                'cmi.credit',
                                'cmi.mode',
                                'cmi.suspend_data',
                                'cmi.launch_data',
                                'cmi.comments',
                                'cmi.comments_from_lms._children',
                                'cmi.comments_from_lms._count',
                                'cmi.comments_from_lms.n.comment',
                                'cmi.comments_from_lms.n.location',
                                'cmi.comments_from_lms.n.timestamp',
                                'cmi.comments_from_learner._children',
                                'cmi.comments_from_learner._count',
                                'cmi.comments_from_learner.n.comment',
                                'cmi.comments_from_learner.n.location',
                                'cmi.comments_from_learner.n.timestamp',
                                'cmi.objectives._count',
                                'cmi.objectives._children',
                                'cmi.objectives.n.id',
                                'cmi.objectives.n.score._children',
                                'cmi.objectives.n.score.raw',
                                'cmi.objectives.n.score.min',
                                'cmi.objectives.n.score.max',
                                'cmi.objectives.n.score.scaled',
                                'cmi.objectives.n.success_status',
                                'cmi.objectives.n.completion_status',
                                'cmi.objectives.n.progress_measure',
                                'cmi.objectives.n.description',
                                'cmi.student_data._children',
                                'cmi.student_data.mastery_score',
                                'cmi.student_data.max_time_allowed',
                                'cmi.student_data.time_limit_action',
                                'cmi.student_preference._children',
                                'cmi.student_preference.audio',
                                'cmi.student_preference.language',
                                'cmi.student_preference.speed',
                                'cmi.student_preference.text',
                                'cmi.interactions._children',
                                'cmi.interactions._count',
                                'cmi.interactions.n.id',
                                'cmi.interactions.n.objectives._count',
                                'cmi.interactions.n.objectives.m.id',
                                'cmi.interactions.n.time',
                                'cmi.interactions.n.type',
                                'cmi.interactions.n.correct_responses._count',
                                'cmi.interactions.n.correct_responses.m.pattern',
                                'cmi.interactions.n.weighting',
                                'cmi.interactions.n.learner_response',
                                'cmi.interactions.n.result',
                                'cmi.interactions.n.latency',
                                'cmi.interactions.n.description',
                                'adl.nav.request');
   }
?>

// add each entry to the log, or setup the log pane first time round
// The code written into the header is based on the ADL test suite API interaction code
// and various examples of test wrappers out in the community
function UpdateLog(s) {
    var s1 = '<html><head><style>\n'
        + 'body {font-family: Arial, Helvetica, Sans-Serif;font-size: xx-small;'
        + 'margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; '
        + 'background-color: ffffff;}\n'
        + '.even {background-color: ffffff; width: 100%;}\n'
        + '.odd {background-color: e8f2fe; width: 100%;}\n'
        + '.error {background-color: ffffff; color: red; width: 100%;}\n'
        + '<\/style>'
        + '<script>\n'
        + 'var LMSVersion = \'<?php echo $scorm->version; ?>\';\n'
        + ' \n'
        + 'function checkLMSVersion() {  \n'
        + '    if (this.document.body.childNodes.length > 0) { \n'
        + '        if (this.document.body.lastChild.id == LMSVersion) { \n'
        + '            return true; \n'
        + '        } \n'
        + '    }; \n'
        + '    alert(\'LMS Version: \' + this.document.body.lastChild.id + \n'
        + '          \' does not equal: \' + LMSVersion +  \n'
        + '          \' so API calls will fail - did navigate to another SCORM package?\'); \n'
        + '    return false; \n'
        + '} \n'
        + ' \n'
        + 'var saveElement = ""; \n'
        + 'function setAPIValue() {  \n'
        + '  document.elemForm.API_ELEMENT.value = document.elemForm.ELEMENT_LIST.value; \n'
        + '  saveElement = document.elemForm.API_ELEMENT.value; \n'
        + '} \n'
        + '  \n'
        + 'var _Debug = false;  // set this to false to turn debugging off  \n'
        + '  \n'
        + '// Define exception/error codes  \n'
        + 'var _NoError = 0;  \n'
        + 'var _GeneralException = 101;  \n'
        + 'var _ServerBusy = 102;  \n'
        + 'var _InvalidArgumentError = 201;  \n'
        + 'var _ElementCannotHaveChildren = 202;  \n'
        + 'var _ElementIsNotAnArray = 203;  \n'
        + 'var _NotInitialized = 301;  \n'
        + 'var _NotImplementedError = 401;  \n'
        + 'var _InvalidSetValue = 402;  \n'
        + 'var _ElementIsReadOnly = 403;  \n'
        + 'var _ElementIsWriteOnly = 404;  \n'
        + 'var _IncorrectDataType = 405;  \n'
        + '  \n'
        + '// local variable definitions  \n'
        + 'var apiHandle = null;  \n'
        + 'var API = null;  \n'
        + 'var findAPITries = 0;  \n'
        + '  \n'
        + '  \n'
        + 'function doLMSInitialize() { \n'
        + '   checkLMSVersion(); \n'
        + '   var api = getAPIHandle();  \n'
        + '   if (api == null) {  \n'
        + '      alert("Unable to locate the LMS\'s API Implementation.\\nLMSInitialize was not successful.");  \n'
        + '      return "false";  \n'
        + '   }  \n'
        + '   var result = api