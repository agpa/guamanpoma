<?php /**/ ?><?php // $Id: postgres7.php,v 1.110.4.1 2007/11/02 16:20:25 tjhunt Exp $

// THIS FILE IS DEPRECATED!  PLEASE DO NOT MAKE CHANGES TO IT!
//
// IT IS USED ONLY FOR UPGRADES FROM BEFORE MOODLE 1.7, ALL 
// LATER CHANGES SHOULD USE upgrade.php IN THIS DIRECTORY.

function quiz_upgrade($oldversion) {
// This function does anything necessary to upgrade
// older versions to match current functionality

    global $CFG, $db;
    $success = true;

    include_once("$CFG->dirroot/mod/quiz/locallib.php");

    if ($success && $oldversion < 2003010100) {
        $success = $success && execute_sql(" ALTER TABLE {$CFG->prefix}quiz ADD review integer DEFAULT '0' NOT NULL AFTER `grademethod` ");
    }

    if ($success && $oldversion < 2003010301) {
        $success = $success && table_column("quiz_truefalse", "true", "trueanswer", "INTEGER", "10", "UNSIGNED", "0", "NOT NULL", "");
        $success = $success && table_column("quiz_truefalse", "false", "falseanswer", "INTEGER", "10", "UNSIGNED", "0", "NOT NULL", "");
        $success = $success && table_column("quiz_questions", "type", "qtype", "INTEGER", "10", "UNSIGNED", "0", "NOT NULL", "");
    }

    if ($success && $oldversion < 2003022303) {
        $success = $success && modify_database ("", "CREATE TABLE prefix_quiz_randommatch (
                                  id SERIAL PRIMARY KEY,
                                  question integer NOT NULL default '0',
                                  choose integer NOT NULL default '4'
                              );");
    }
    if ($success && $oldversion < 2003030303) {
        $success = $success && table_column("quiz_questions", "", "defaultgrade", "INTEGER", "6", "UNSIGNED", "1", "NOT NULL", "image");
    }

    if ($success && $oldversion < 2003033100) {
        $success = $success && modify_database ("", "ALTER TABLE prefix_quiz_randommatch RENAME prefix_quiz_randomsamatch ");
        $success = $success && modify_database ("", "CREATE TABLE prefix_quiz_match_sub (
                                 id SERIAL PRIMARY KEY,
                                 question integer NOT NULL default '0',
                                 questiontext text NOT NULL default '',
                                 answertext varchar(255) NOT NULL default ''
                              );");
        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_match_sub_question_idx ON prefix_quiz_match_sub (question);");

        $success = $success && modify_database ("", "CREATE TABLE prefix_quiz_multichoice (
                                 id SERIAL PRIMARY KEY,
                                 question integer NOT NULL default '0',
                                 layout integer NOT NULL default '0',
                                 answers varchar(255) NOT NULL default '',
                                 single integer NOT NULL default '0'
                               );");
        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_multichoice_question_idx ON prefix_quiz_multichoice (question);");
    }

    if ($success && $oldversion < 2003040901) {
        $success = $success && table_column("quiz", "", "shufflequestions", "INTEGER", "5", "UNSIGNED", "0", "NOT NULL", "review");
        $success = $success && table_column("quiz", "", "shuffleanswers", "INTEGER", "4", "UNSIGNED", "0", "NOT NULL", "shufflequestions");
    }
    if ($success && $oldversion < 2003042702) {
        $success = $success && modify_database ("", "CREATE TABLE prefix_quiz_match (
                                 id SERIAL PRIMARY KEY,
                                 question integer NOT NULL default '0',
                                 subquestions varchar(255) NOT NULL default ''
                               );");
        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_match_question_idx ON prefix_quiz_match (question);");
    }
    if ($success && $oldversion < 2003071001) {
        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_numerical (
                               id SERIAL PRIMARY KEY,
                               question integer NOT NULL default '0',
                               answer integer NOT NULL default '0',
                               min varchar(255) NOT NULL default '',
                               max varchar(255) NOT NULL default ''
                               ); ");
        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_numerical_answer_idx ON prefix_quiz_numerical (answer);");
    }

    if ($success && $oldversion < 2003072400) {
        $success = $success && execute_sql(" INSERT INTO {$CFG->prefix}log_display (module, action, mtable, field) VALUES ('quiz', 'review', 'quiz', 'name') ");
    }

    if ($success && $oldversion < 2003082300) {
        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_multianswers (
                               id SERIAL PRIMARY KEY,
                               question integer NOT NULL default '0',
                               answers varchar(255) NOT NULL default '',
                               positionkey varchar(255) NOT NULL default '',
                               answertype integer NOT NULL default '0',
                               norm integer NOT NULL default '1'
                              ); ");
        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_multianswers_question_idx ON prefix_quiz_multianswers (question);");

        $success = $success && table_column("quiz", "", "attemptonlast", "INTEGER", "10", "UNSIGNED", "0", "NOT NULL", "attempts");

        $success = $success && table_column("quiz_questions", "", "stamp", "varchar", "255", "", "qtype");
    }

    if ($success && $oldversion < 2003082301) {
        $success = $success && table_column("quiz_questions", "", "version", "integer", "10", "", "1", "not null", "stamp");
        if ($questions = get_records("quiz_questions")) {
            foreach ($questions as $question) {
                $stamp = make_unique_id_code();
                if (!($success = $success && set_field("quiz_questions", "stamp", $stamp, "id", $question->id))) {
                    notify("Error while adding stamp to question id = $question->id");
                    break;
                }
            }
        }
    }

    if ($success && $oldversion < 2003082700) {
        table_column("quiz_categories", "", "stamp", "varchar", "255", "", "", "not null");
        if ($categories = get_records("quiz_categories")) {
            foreach ($categories as $category) {
                $stamp = make_unique_id_code();
                if (!($success = $success && set_field("quiz_categories", "stamp", $stamp, "id", $category->id))) {
                    notify("Error while adding stamp to category id = $category->id");
                    break;
                }
            }
        }
    }

    if ($success && $oldversion < 2003111100) {
        $duplicates = get_records_sql("SELECT stamp as id,count(*) as cuenta
                                       FROM {$CFG->prefix}quiz_questions
                                       GROUP BY stamp
                                       HAVING count(*)>1");

        if ($duplicates) {
            notify("You have some quiz questions with duplicate stamps IDs.  Cleaning these up.");
            foreach ($duplicates as $duplicate) {
                $questions = get_records("quiz_questions","stamp",$duplicate->id);
                $add = 1;
                foreach ($questions as $question) {
                    echo "Changing question id $question->id stamp to ".$duplicate->id.$add."<br />";
                    $success = $success && set_field("quiz_questions","stamp",$duplicate->id.$add,"id",$question->id);
                    $add++;
                }
            }
        } else {
            notify("Checked your quiz questions for stamp duplication errors, but no problems were found.", "green");
        }
    }

    if ($success && $oldversion < 2004021300) {
        $success = $success && table_column("quiz_questions", "", "questiontextformat", "integer", "2", "", "0", "not null", "questiontext");
    }

    if ($success && $oldversion < 2004021900) {
        $success = $success && modify_database("","INSERT INTO prefix_log_display (module, action, mtable, field) VALUES ('quiz', 'add', 'quiz', 'name');");
        $success = $success && modify_database("","INSERT INTO prefix_log_display (module, action, mtable, field) VALUES ('quiz', 'update', 'quiz', 'name');");
    }

    if ($success && $oldversion < 2004051700) {
        include_once("$CFG->dirroot/mod/quiz/lib.php");
        $success = $success && quiz_refresh_events();
    }
    if ($success && $oldversion < 2004060200) {
        $success = $success && table_column("quiz", "", "timelimit", "integer", "", "", "0", "NOT NULL", "");
    }

    if ($success && $oldversion < 2004070700) {
        $success = $success && table_column("quiz", "", "password", "varchar", "255", "", "", "not null", "");
        $success = $success && table_column("quiz", "", "subnet", "varchar", "255", "", "", "not null", "");
    }

    if ($success && $oldversion < 2004073001) {
        // Six new tables:


        $success = $success && modify_database ( "", "BEGIN;");

        // One table for handling units for numerical questions
        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_numerical_units (
                               id SERIAL8 PRIMARY KEY,
                               question INT8  NOT NULL default '0',
                               multiplier decimal(40,20) NOT NULL default '1.00000000000000000000',
                               unit varchar(50) NOT NULL default ''
                );" );


        // Four tables for handling distribution and storage of
        // individual data for dataset dependent question types
        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_attemptonlast_datasets (
                               id SERIAL8 PRIMARY KEY,
                               category INT8  NOT NULL default '0',
                               userid INT8  NOT NULL default '0',
                               datasetnumber INT8  NOT NULL default '0',
                               CONSTRAINT prefix_quiz_attemptonlast_datasets_category_userid UNIQUE (category,userid)
            ) ;");

        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_dataset_definitions (
                               id SERIAL8 PRIMARY KEY,
                               category INT8  NOT NULL default '0',
                               name varchar(255) NOT NULL default '',
                               type INT8 NOT NULL default '0',
                               options varchar(255) NOT NULL default '',
                               itemcount INT8  NOT NULL default '0'
            ) ; ");

        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_dataset_items (
                               id SERIAL8 PRIMARY KEY,
                               definition INT8  NOT NULL default '0',
                               number INT8  NOT NULL default '0',
                               value varchar(255) NOT NULL default ''
                             ) ; ");

        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_dataset_items_definition_idx ON prefix_quiz_dataset_items (definition);");

        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_question_datasets (
                               id SERIAL8 PRIMARY KEY,
                               question INT8  NOT NULL default '0',
                               datasetdefinition INT8  NOT NULL default '0'
            ) ; ");

        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_question_datasets_question_datasetdefinition_idx ON prefix_quiz_question_datasets (question,datasetdefinition);");

        // One table for new question type calculated
        //  - the first dataset dependent question type
        $success = $success && modify_database ("", " CREATE TABLE prefix_quiz_calculated (
                               id SERIAL8 PRIMARY KEY,
                               question INT8  NOT NULL default '0',
                               answer INT8  NOT NULL default '0',
                               tolerance varchar(20) NOT NULL default '0.0',
                               tolerancetype INT8 NOT NULL default '1',
                               correctanswerlength INT8 NOT NULL default '2'
                ) ; ");

        $success = $success && modify_database ("", "CREATE INDEX prefix_quiz_calculated_question_idx ON  prefix_quiz_calculated (question);");

        $success = $success && modify_database ( "", "COMMIT;");
    }

    if ($success && $oldversion < 2004111400) {
        $success = $success && table_column("quiz_responses", "answer", "answer", "text", "", "", "", "not null");
    }

    if ($success && $oldversion < 2004111700) {
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_course_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_answers_question_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_attempts_quiz_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_attempts_userid_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_calculated_answer_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_categories_course_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_dataset_definitions_category_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_grades_quiz_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_grades_userid_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_numerical_question_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_numerical_units_question_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_question_grades_quiz_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_question_grades_question_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_questions_category_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_randomsamatch_question_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_responses_attempt_idx;",false);
        $success = $success && execute_sql("DROP INDEX {$CFG->prefix}quiz_responses_question_idx;",false);

        $success = $success && modify_database('','CREATE INDEX prefix_quiz_course_idx ON prefix_quiz (course);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_answers_question_idx ON prefix_quiz_answers (question);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_attempts_quiz_idx ON prefix_quiz_attempts (quiz);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_attempts_userid_idx ON prefix_quiz_attempts (userid);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_calculated_answer_idx ON prefix_quiz_calculated (answer);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_categories_course_idx ON prefix_quiz_categories (course);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_dataset_definitions_category_idx ON prefix_quiz_dataset_definitions (category);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_grades_quiz_idx ON prefix_quiz_grades (quiz);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_grades_userid_idx ON prefix_quiz_grades (userid);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_numerical_question_idx ON prefix_quiz_numerical (question);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_numerical_units_question_idx ON prefix_quiz_numerical_units (question);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_question_grades_quiz_idx ON prefix_quiz_question_grades (quiz);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_question_grades_question_idx ON prefix_quiz_question_grades (question);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_questions_category_idx ON prefix_quiz_questions (category);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_randomsamatch_question_idx ON prefix_quiz_randomsamatch (question);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_responses_attempt_idx ON prefix_quiz_responses (attempt);');
        $success = $success && modify_database('','CREATE INDEX prefix_quiz_responses_question_idx ON prefix_quiz_responses (question);');
    }

    if ($success && $oldversion < 2004112300) { //try and clean up an old mistake - try and bring us up to what is in postgres7.sql today.
        $success = $success && execute_sql("ALTER TABLE {$CFG->prefix}quiz_attemptonlast_datasets DROP CONSTRAINT category;",false);
        $success = $success && execute_sql("ALTER TABLE {$CFG->prefix}quiz_attemptonlast_datasets DROP CONSTRAINT {$CFG->prefix}quiz_attemptonlast_datasets_category_userid;",false);
        $success = $success && execute_sql("ALTER TABLE {$CFG->prefix}quiz_attemptonlast_datasets DROP CONSTRAINT {$CFG->prefix}quiz_category_userid_unique;",false);
        $success = $success && modify_database('','ALTER TABLE prefix_quiz_attemptonlast_datasets ADD CONSTRAINT prefix_quiz_category_userid_unique UNIQUE (category,userid);');
    }

    if ($success && $oldversion < 2004120501) {
        $success = $success && table_column("quiz_calculated", "", "correctanswerformat", "integer", "10", "", "0", "not null", "correctanswerlength");
    }

    if ($success && $oldversion < 2004121400) {  // New field to determine popup window behaviour
        $success = $success && table_column("quiz", "", "popup", "integer", "4", "", "0", "not null", "subnet");
    }

    if ($success && $oldversion < 2005010201) {
        $success = $success && table_column('quiz_categories', '', 'parent');
        $success = $success && table_column('quiz_categories', '', 'sortorder', 'integer', '10', '', '999');
    }

    if ($success && $oldversion < 2005010300) {
        $success = $success && table_column("quiz", "", "questionsperpage", "integer", "10", "", "0", "not null", "review");
    }

    if ($success && $oldversion < 2005012700) {
        $success = $success && table_column('quiz_grades', 'grade', 'grade', 'real', 2, '');
    }

    if ($success && $oldversion < 2005021400) {
        $success = $success && table_column("quiz", "", "decimalpoints", "integer", "4", "", "2", "not null", "grademethod");
    }

    if($success && $oldversion < 2005022800) {
        $success = $success && table_column('quiz_questions', '', 'hidden', 'integer', '1', 'unsigned', '0', 'not null', 'version');
        $success = $success && table_column('quiz_responses', '', 'originalquestion', 'integer', '10', 'unsigned', '0', 'not null', 'question');
        $success = $suc