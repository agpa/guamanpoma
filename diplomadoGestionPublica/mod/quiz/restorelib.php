<?php /**/ ?><?php // $Id: restorelib.php,v 1.90.6.2 2007/12/21 15:38:38 tjhunt Exp $
    //This php script contains all the stuff to restore quiz mods

// Todo:

    // whereever it says "/// We have to recode the .... field" we should put in a check
    // to see if the recoding was successful and throw an appropriate error otherwise

//This is the "graphical" structure of the quiz mod:
    //To see, put your terminal to 160cc

    //
    //                           quiz
    //                        (CL,pk->id)
    //                            |
    //           -------------------------------------------------------------------
    //           |                    |                        |                    |
    //           |               quiz_grades                   |        quiz_question_versions
    //           |           (UL,pk->id,fk->quiz)              |         (CL,pk->id,fk->quiz)
    //           |                                             |
    //      quiz_attempts                          quiz_question_instances
    //  (UL,pk->id,fk->quiz)                    (CL,pk->id,fk->quiz,question)
    //
    // Meaning: pk->primary key field of the table
    //          fk->foreign key to link with parent
    //          nt->nested field (recursive data)
    //          SL->site level info
    //          CL->course level info
    //          UL->user level info
    //          files->table may have files
    //
    //-----------------------------------------------------------

    // When we restore a quiz we also need to restore the questions and possibly
    // the data about student interaction with the questions. The functions to do
    // that are included with the following library
    include_once("$CFG->dirroot/question/restorelib.php");

    function quiz_restore_mods($mod,$restore) {

        global $CFG;

        $status = true;

        //Hook to call Moodle < 1.5 Quiz Restore
        if ($restore->backup_version < 2005043000) {
            include_once("restorelibpre15.php");
            return quiz_restore_pre15_mods($mod,$restore);
        }

        //Get record from backup_ids
        $data = backup_getid($restore->backup_unique_code,$mod->modtype,$mod->id);

        if ($data) {
            //Now get completed xmlized object
            $info = $data->info;
            //if necessary, write to restorelog and adjust date/time fields
            if ($restore->course_startdateoffset) {
                restore_log_date_changes('Quiz', $restore, $info['MOD']['#'], array('TIMEOPEN', 'TIMECLOSE'));
            }            
            //traverse_xmlize($info);                                                                     //Debug
            //print_object ($GLOBALS['traverse_array']);                                                  //Debug
            //$GLOBALS['traverse_array']="";                                                              //Debug

            //Now, build the QUIZ record structure
            $quiz = new stdClass;
            $quiz->course = $restore->course_id;
            $quiz->name = backup_todb($info['MOD']['#']['NAME']['0']['#']);
            $quiz->intro = backup_todb($info['MOD']['#']['INTRO']['0']['#']); 
            $quiz->timeopen = backup_todb($info['MOD']['#']['TIMEOPEN']['0']['#']);
            $quiz->timeclose = backup_todb($info['MOD']['#']['TIMECLOSE']['0']['#']);
            $quiz->optionflags = backup_todb($info['MOD']['#']['OPTIONFLAGS']['0']['#']);
            $quiz->penaltyscheme = backup_todb($info['MOD']['#']['PENALTYSCHEME']['0']['#']);
            $quiz->attempts = backup_todb($info['MOD']['#']['ATTEMPTS_NUMBER']['0']['#']);
            $quiz->attemptonlast = backup_todb($info['MOD']['#']['ATTEMPTONLAST']['0']['#']);
            $quiz->grademethod = backup_todb($info['MOD']['#']['GRADEMETHOD']['0']['#']);
            $quiz->decimalpoints = backup_todb($info['MOD']['#']['DECIMALPOINTS']['0']['#']);
            $quiz->review = backup_todb($info['MOD']['#']['REVIEW']['0']['#']);
            $quiz->questionsperpage = backup_todb($info['MOD']['#']['QUESTIONSPERPAGE']['0']['#']);
            $quiz->shufflequestions = backup_todb($info['MOD']['#']['SHUFFLE