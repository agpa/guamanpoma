<?php /**/ ?><?php  // $Id: report.php,v 1.41.2.8 2008/11/27 04:48:11 tjhunt Exp $

    require_once($CFG->libdir.'/tablelib.php');

/// Item analysis displays a table of quiz questions and their performance
class quiz_report extends quiz_default_report {

    function display($quiz, $cm, $course) {     /// This function just displays the report
        global $CFG, $SESSION, $QTYPES;
        $strnoattempts = get_string('noattempts','quiz');
    /// Only print headers if not asked to download data
        $download = optional_param('download', NULL);
        if (!$download) {
            $this->print_header_and_tabs($cm, $course, $quiz, $reportmode="analysis");
        }
    /// Construct the table for this particular report

        if (!$quiz->questions) {
            print_heading($strnoattempts);
            return true;
        }

    /// Check to see if groups are being used in this quiz
        $currentgroup = groups_get_activity_group($cm, true);
        
        if ($groupmode = groups_get_activity_groupmode($cm)) {   // Groups are being used
            if (!$download) {
                groups_print_activity_menu($cm, "report.php?id=$cm->id&amp;mode=analysis");
            }
        }

        // set Table and Analysis stats options
        if(!isset($SESSION->quiz_analysis_table)) {
            $SESSION->quiz_analysis_table = array('attemptselection' => 0, 'lowmarklimit' => 0, 'pagesize' => QUIZ_REPORT_DEFAULT_PAGE_SIZE);
        }

        foreach($SESSION->quiz_analysis_table as $option => $value) {
            $urlparam = optional_param($option, NULL, PARAM_INT);
            if($urlparam === NULL) {
                $$option = $value;
            } else {
                $$option = $SESSION->quiz_analysis_table[$option] = $urlparam;
            }
        }
        if (!isset($pagesize) || ((int)$pagesize < 1) ){
            $pagesize = QUIZ_REPORT_DEFAULT_PAGE_SIZE;
        }


        $scorelimit = $quiz->sumgrades * $lowmarklimit/ 100;

        // ULPGC ecastro DEBUG this is here to allow for different SQL to select attempts
        switch ($attemptselection) {
        case QUIZ_ALLATTEMPTS :
            $limit = '';
            $group = '';
            break;
        case QUIZ_HIGHESTATTEMPT :
            $limit = ', max(qa.sumgrades) ';
            $group = ' GROUP BY qa.userid ';
            break;
        case QUIZ_FIRSTATTEMPT :
            $limit = ', min(qa.timemodified) ';
            $group = ' GROUP BY qa.userid ';
            break;
        case QUIZ_LASTATTEMPT :
            $limit = ', max(qa.timemodified) ';
            $group = ' GROUP BY qa.userid ';
            break;
        }

        if ($attemptselection != QUIZ_ALLATTEMPTS) {
            $sql = 'SELECT qa.userid '.$limit.
                    'FROM '.$CFG->prefix.'user u LEFT JOIN '.$CFG->prefix.'quiz_attempts qa ON u.id = qa.userid '.
                    'WHERE qa.quiz = '.$quiz->id.' AND qa.preview = 0 '.
                    $group;
            $usermax = get_records_sql_menu($sql);
        }

        $groupmembers = '';
        $groupwhere = '';

        //Add this to the SQL to show only group users
        if ($currentgroup) {
            $groupmembers = ", {$CFG->prefix}groups_members gm ";
            $groupwhere = "AND gm.groupid = '$currentgroup' AND u.id = gm.userid";
        }

        $sql = 'SELECT  qa.* FROM '.$CFG->prefix.'quiz_attempts qa, '.$CFG->prefix.'user u '.$groupmembers.
                 'WHERE u.id = qa.userid AND qa.quiz = '.$quiz->id.' AND qa.preview = 0 AND ( qa.sumgrades >= '.$scorelimit.' ) '.$groupwhere;

        // ^^^^^^ es posible seleccionar aqu TODOS los quizzes, como quiere Jussi,
        // pero haba que llevar la cuenta ed cada quiz para restaura las preguntas (quizquestions, states)

        /// Fetch the attempts
        $attempts = get_records_sql($sql);

        if(empty($attempts)) {
            print_heading(get_string('nothingtodisplay'));
            $this->print_options_form($quiz, $cm, $attemptselection, $lowmarklimit, $pagesize);
            return true;
        }

    /// Here we rewiew all attempts and record data to construct the table
        $questions = array();
        $statstable = array();
        $questionarray = array();
        foreach ($attempts as $attempt) {
            $questionarray[] = quiz_questions_in_quiz($attempt->layout);
        }
        $questionlist = quiz_questions_in_quiz(implode(",", $questionarray));
        $questionarray = array_unique(explode(",",$questionlist));
        $questionlist = implode(",", $questionarray);
        unset($questionarray);

        foreach ($attempts as $attempt) {
            switch ($attemptselection) {
            case QUIZ_ALLATTEMPTS :
                $userscore = 0;      // can be anything, not used
                break;
            case QUIZ_HIGHESTATTEMPT :
                $userscore = $attempt->sumgrades;
                break;
            case QUIZ_FIRSTATTEMPT :
                $userscore = $attempt->timemodified;
                break;
            case QUIZ_LASTATTEMPT :
                $userscore = $attempt->timemodified;
                break;
            }

            if ($attemptselection == QUIZ_ALLATTEMPTS || $userscore == $usermax[$attempt->userid]) {

            $sql = "SELECT q.*, i.grade AS maxgrade, i.id AS instance".
                   "  FROM {$CFG->prefix}question q,".
                   "       {$CFG->prefix}quiz_question_instances i".
                   " WHERE i.quiz = '$quiz->id' AND q.id = i.question".
                   "   AND q.id IN ($questionlist)";

            if (!$quizquesti