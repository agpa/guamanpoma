<?php /**/ ?><?php // $Id: mod_form.php,v 1.28.2.11 2009/10/05 03:05:38 jerome Exp $
require_once ($CFG->dirroot.'/course/moodleform_mod.php');

require_once("$CFG->dirroot/mod/quiz/locallib.php");

class mod_quiz_mod_form extends moodleform_mod {
    var $_feedbacks;

    function definition() {

        global $COURSE, $CFG;
        $mform    =& $this->_form;

//-------------------------------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name'), array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }
        $mform->addRule('name', null, 'required', null, 'client');

        $mform->addElement('htmleditor', 'intro', get_string("introduction", "quiz"));
        $mform->setType('intro', PARAM_RAW);
        $mform->setHelpButton('intro', array('richtext', get_string('helprichtext')));

//-------------------------------------------------------------------------------
        $mform->addElement('header', 'timinghdr', get_string('timing', 'form'));
        $mform->addElement('date_time_selector', 'timeopen', get_string('quizopen', 'quiz'), array('optional'=>true));
        $mform->setHelpButton('timeopen', array('timeopen', get_string('quizopen', 'quiz'), 'quiz'));

        $mform->addElement('date_time_selector', 'timeclose', get_string('quizclose', 'quiz'), array('optional'=>true));
        $mform->setHelpButton('timeclose', array('timeopen', get_string('quizclose', 'quiz'), 'quiz'));


        $timelimitgrp=array();
        $timelimitgrp[] = &$mform->createElement('text', 'timelimit');
        $timelimitgrp[] = &$mform->createElement('checkbox', 'timelimitenable', '', get_string('enable'));
        $mform->addGroup($timelimitgrp, 'timelimitgrp', get_string('timelimitmin', 'quiz'), array(' '), false);
        $mform->setType('timelimit', PARAM_TEXT);
        $timelimitgrprules = array();
        $timelimitgrprules['timelimit'][] = array(null, 'numeric', null, 'client');
        $mform->addGroupRule('timelimitgrp', $timelimitgrprules);
        $mform->disabledIf('timelimitgrp', 'timelimitenable');
        $mform->setAdvanced('timelimitgrp', $CFG->quiz_fix_timelimit);
        $mform->setHelpButton('timelimitgrp', array("timelimit", get_string("quiztimer","quiz"), "quiz"));
        $mform->setDefault('timelimit', $CFG->quiz_timelimit);
        $mform->setDefault('timelimitenable', !empty($CFG->quiz_timelimit));


        //enforced time delay between quiz attempts add-on
        $timedelayoptions = array();
        $timedelayoptions[0] = get_string('none');
        $timedelayoptions[1800] = get_string('numminutes', '', 30);
        $timedelayoptions[3600] = get_string('numminutes', '', 60);
        for($i=2; $i<=23; $i++) {
             $seconds  = $i*3600;
             $timedelayoptions[$seconds] = get_string('numhours', '', $i);
        }
        $timedelayoptions[86400] = get_string('numhours', '', 24);
        for($i=2; $i<=7; $i++) {
             $seconds = $i*86400;
             $timedelayoptions[$seconds] = get_string('numdays', '', $i);
        }
        $mform->addElement('select', 'delay1', get_string("delay1", "quiz"), $timedelayoptions);
        $mform->setHelpButton('delay1', array("timedelay1", get_string("delay1", "quiz"), "quiz"));
        $mform->setAdvanced('delay1', $CFG->quiz_fix_delay1);
        $mform->setDefault('delay1', $CFG->quiz_delay1);

        $mform->addElement('select', 'delay2', get_string("delay2", "quiz"), $timedelayoptions);
        $mform->setHelpButton('delay2', array("timedelay2", get_string("delay2", "quiz"), "quiz"));
        $mform->setAdvanced('delay2', $CFG->quiz_fix_delay2);
        $mform->setDefault('delay2', $CFG->quiz_delay2);

//-------------------------------------------------------------------------------
        $mform->addElement('header', 'displayhdr', get_string('display', 'form'));
        $perpage = array();
        for ($i = 0; $i <= 50; ++$i) {
            $perpage[$i] = $i;
        }
        $perpage[0] 