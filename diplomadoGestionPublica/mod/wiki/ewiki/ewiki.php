<?php /**/ ?><?php
@define("EWIKI_VERSION", "R1.01d");

/*

  ErfurtWiki - an embedable, fast and user-friendly wiki engine
  ---------
  This is Public Domain (no license, no warranty); but feel free
  to redistribute it under GPL or anything else you like.

  http://erfurtwiki.sourceforge.net/
  Mario Salzer <mario*erphesfurt·de> and many others(tm)


  Use it from inside yoursite.php like that:
  <html><body>...
  <?php
     include("ewiki.php");
     echo ewiki_page();
  ?>

*/

#-- you could also establish a mysql connection in here, of course:
// mysql_connect(":/var/run/mysqld/mysqld.sock", "user", "pw")
// and mysql_query("USE mydatabase");


        #-------------------------------------------------------- config ---

        #-- I'm sorry for that, but all the @ annoy me
        error_reporting(0x0000377 & error_reporting());
#   error_reporting(E_ALL^E_NOTICE);

    #-- the position of your ewiki-wrapper script
    define("EWIKI_SCRIPT", "?id=");     # relative/absolute to docroot
#   define("EWIKI_SCRIPT_URL", "http://...?id=");       # absolute URL

        #-- change to your needs (site lang)
    define("EWIKI_NAME", "ErfurtWiki");
    define("EWIKI_PAGE_INDEX", "ErfurtWiki");
    define("EWIKI_PAGE_NEWEST", "NewestPages");
    define("EWIKI_PAGE_SEARCH", "SearchPages");
    define("EWIKI_PAGE_HITS", "MostVisitedPages");
    define("EWIKI_PAGE_VERSIONS", "MostOftenChangedPages");
    define("EWIKI_PAGE_UPDATES", "UpdatedPages");

    #-- default settings are good settings - most often ;)
        #- look & feel
    define("EWIKI_PRINT_TITLE", 1);     # <h2>WikiPageName</h2> on top
    define("EWIKI_SPLIT_TITLE", 0);     # <h2>Wiki Page Name</h2>
    define("EWIKI_CONTROL_LINE", 1);    # EditThisPage-link at bottom
    define("EWIKI_LIST_LIMIT", 20);     # listing limit
        #- behaviour
    define("EWIKI_AUTO_EDIT", 1);       # edit box for non-existent pages
    define("EWIKI_EDIT_REDIRECT", 1);   # redirect after edit save
    define("EWIKI_DEFAULT_ACTION", "view"); # (keep!)
    define("EWIKI_CASE_INSENSITIVE", 1);    # wikilink case sensitivity
    define("EWIKI_HIT_COUNTING", 1);
    define("UNIX_MILLENNIUM", 1000000000);
        #- rendering
    define("EWIKI_ALLOW_HTML", 0);      # often a very bad idea
    define("EWIKI_HTML_CHARS", 1);      # allows for &#200;
    define("EWIKI_ESCAPE_AT", 1);       # "@" -> "&#x40;"
        #- http/urls
    define("EWIKI_HTTP_HEADERS", 1);    # most often a good thing
    define("EWIKI_NO_CACHE", 1);        # browser+proxy shall not cache
    define("EWIKI_URLENCODE", 1);       # disable when _USE_PATH_INFO
    define("EWIKI_URLDECODE", 1);
    define("EWIKI_USE_PATH_INFO", 1  &&!strstr($_SERVER["SERVER_SOFTWARE"],"Apache"));
    define("EWIKI_USE_ACTION_PARAM", 1);
    define("EWIKI_ACTION_SEP_CHAR", "/");
    define("EWIKI_UP_PAGENUM", "n");    # _UP_ means "url parameter"
    define("EWIKI_UP_PAGEEND", "e");
    define("EWIKI_UP_BINARY", "binary");
    define("EWIKI_UP_UPLOAD", "upload");
        #- other stuff
        define("EWIKI_DEFAULT_LANG", "en");
        define("EWIKI_CHARSET", "UTF-8");
    #- user permissions
    define("EWIKI_PROTECTED_MODE", 0);  # disable funcs + require auth
    define("EWIKI_PROTECTED_MODE_HIDING", 0);  # hides disallowed actions
    define("EWIKI_AUTH_DEFAULT_RING", 3);   # 0=root 1=priv 2=user 3=view
    define("EWIKI_AUTO_LOGIN", 1);      # [auth_query] on startup

    #-- allowed WikiPageNameCharacters

#### BEGIN MOODLE CHANGES - to remove auto-camelcase linking.   
    global $moodle_disable_camel_case;   
    if ($moodle_disable_camel_case) {
        define("EWIKI_CHARS_L", "");
        define("EWIKI_CHARS_U", "");
    }
    else {
#### END MOODLE CHANGES

    define("EWIKI_CHARS_L", "a-z_µ¤$\337-\377");
    define("EWIKI_CHARS_U", "A-Z0-9\300-\336");

#### BEGIN MOODLE CHANGES   
    }
#### END MOODLE CHANGES
   
    define("EWIKI_CHARS", EWIKI_CHARS_L.EWIKI_CHARS_U);

        #-- database
    define("EWIKI_DB_TABLE_NAME", "ewiki");      # MySQL / ADOdb
    define("EWIKI_DBFILES_DIRECTORY", "/tmp");   # see "db_flat_files.php"
    define("EWIKI_DBA", "/tmp/ewiki.dba");       # see "db_dba.php"
    define("EWIKI_DBQUERY_BUFFER", 512*1024);    # 512K
    define("EWIKI_INIT_PAGES", "./init-pages");  # for initialization

    define("EWIKI_DB_F_TEXT", 1<<0);
    define("EWIKI_DB_F_BINARY", 1<<1);
    define("EWIKI_DB_F_DISABLED", 1<<2);
    define("EWIKI_DB_F_HTML", 1<<3);
    define("EWIKI_DB_F_READONLY", 1<<4);
    define("EWIKI_DB_F_WRITEABLE", 1<<5);
    define("EWIKI_DB_F_APPENDONLY", 1<<6);  #nyi
    define("EWIKI_DB_F_SYSTEM", 1<<7);
    define("EWIKI_DB_F_PART", 1<<8);
    define("EWIKI_DB_F_TYPE", EWIKI_DB_F_TEXT | EWIKI_DB_F_BINARY | EWIKI_DB_F_DISABLED | EWIKI_DB_F_SYSTEM | EWIKI_DB_F_PART);
    define("EWIKI_DB_F_ACCESS", EWIKI_DB_F_READONLY | EWIKI_DB_F_WRITEABLE | EWIKI_DB_F_APPENDONLY);
    define("EWIKI_DB_F_COPYMASK", EWIKI_DB_F_TYPE | EWIKI_DB_F_ACCESS);

    define("EWIKI_DBFILES_NLR", '\\n');
    define("EWIKI_DBFILES_ENCODE", 0 || (DIRECTORY_SEPARATOR != "/"));
    define("EWIKI_DBFILES_GZLEVEL", "2");

    #-- internal
    define("EWIKI_ADDPARAMDELIM", (strstr(EWIKI_SCRIPT,"?") ? "&" : "?"));

    #-- binary content (images)
    define("EWIKI_SCRIPT_BINARY", /*"/binary.php?binary="*/  ltrim(strtok(" ".EWIKI_SCRIPT,"?"))."?".EWIKI_UP_BINARY."="  );
    define("EWIKI_CACHE_IMAGES", 1  &&!headers_sent());
    define("EWIKI_IMAGE_MAXSIZE", 64 *1024);
    define("EWIKI_IMAGE_MAXWIDTH", 3072);
    define("EWIKI_IMAGE_MAXHEIGHT", 2048);
    define("EWIKI_IMAGE_MAXALLOC", 1<<19);
    define("EWIKI_IMAGE_RESIZE", 1);
    define("EWIKI_IMAGE_ACCEPT", "image/jpeg,image/png,image/gif,application/x-shockwave-flash");
    define("EWIKI_IDF_INTERNAL", "internal://");
    define("EWIKI_ACCEPT_BINARY", 0);   # for arbitrary binary data files

    #-- misc
        define("EWIKI_TMP", $_SERVER["TEMP"] ? $_SERVER["TEMP"] : "/tmp");
    define("EWIKI_LOGLEVEL", -1);       # 0=error 1=warn 2=info 3=debug
    define("EWIKI_LOGFILE", "/tmp/ewiki.log");

    #-- plugins (tasks mapped to function names)
    $ewiki_plugins["database"][] = "ewiki_database_mysql";
    $ewiki_plugins["edit_preview"][] = "ewiki_page_edit_preview";
    $ewiki_plugins["render"][] = "ewiki_format";
    $ewiki_plugins["init"][-5] = "ewiki_localization";
    $ewiki_plugins["init"][-1] = "ewiki_binary";
        $ewiki_plugins["handler"][-105] = "ewiki_eventually_initialize";
        $ewiki_plugins["handler"][] = "ewiki_intermap_walking";
    $ewiki_plugins["view_append"][-1] = "ewiki_control_links";
        $ewiki_plugins["view_final"][-1] = "ewiki_add_title";
        $ewiki_plugins["page_final"][] = "ewiki_http_headers";
        $ewiki_plugins["page_final"][99115115] = "ewiki_page_css_container";
    $ewiki_plugins["edit_form_final"][] = "ewiki_page_edit_form_final_imgupload";
        $ewiki_plugins["format_block"]["pre"][] = "ewiki_format_pre";
        $ewiki_plugins["format_block"]["code"][] = "ewiki_format_pre";
        $ewiki_plugins["format_block"]["htm"][] = "ewiki_format_html";
        $ewiki_plugins["format_block"]["html"][] = "ewiki_format_html";
        $ewiki_plugins["format_block"]["comment"][] = "ewiki_format_comment";


    #-- internal pages
    $ewiki_plugins["page"][EWIKI_PAGE_NEWEST] = "ewiki_page_newest";
    $ewiki_plugins["page"][EWIKI_PAGE_SEARCH] = "ewiki_page_search";
    if (EWIKI_HIT_COUNTING) $ewiki_plugins["page"][EWIKI_PAGE_HITS] = "ewiki_page_hits";
    $ewiki_plugins["page"][EWIKI_PAGE_VERSIONS] = "ewiki_page_versions";
    $ewiki_plugins["page"][EWIKI_PAGE_UPDATES] = "ewiki_page_updates";

    #-- page actions
    $ewiki_plugins["action"]["edit"] = "ewiki_page_edit";
    $ewiki_plugins["action_always"]["links"] = "ewiki_page_links";
    $ewiki_plugins["action"]["info"] = "ewiki_page_info";
    $ewiki_plugins["action"]["view"] = "ewiki_page_view";

    #-- helper vars ---------------------------------------------------
    $ewiki_config["idf"]["url"] = array("http://", "mailto:", "internal://", "ftp://", "https://", "irc://", "telnet://", "news://", "chrome://", "file://", "gopher://", "httpz://");
    $ewiki_config["idf"]["img"] = array(".jpeg", ".png", ".jpg", ".gif", ".j2k");
    $ewiki_config["idf"]["obj"] = array(".swf", ".svg");

    #-- entitle actions
    $ewiki_config["action_links"]["view"] = @array_merge(array(
        "edit" => "EDITTHISPAGE",   # ewiki_t() is called on these
        "links" => "BACKLINKS",
        "info" => "PAGEHISTORY",
        "like" => "LIKEPAGES",
    ), @$ewiki_config["action_links"]["view"]
        );
    $ewiki_config["action_links"]["info"] = @array_merge(array(
        "view" => "browse",
        "edit" => "fetchback",
    ), @$ewiki_config["action_links"]["info"]
        );

        #-- variable configuration settings (go into '$ewiki_config')
        $ewiki_config_DEFAULTSTMP = array(
           "edit_thank_you" => 1,
           "edit_box_size" => "70x15",
           "print_title" => EWIKI_PRINT_TITLE,
           "split_title" => EWIKI_SPLIT_TITLE,
           "control_line" => EWIKI_CONTROL_LINE,
           "list_limit" => EWIKI_LIST_LIMIT,
           "script" => EWIKI_SCRIPT,
           "script_url" => (defined("EWIKI_SCRIPT_URL")?EWIKI_SCRIPT_URL:NULL),
           "script_binary" => EWIKI_SCRIPT_BINARY,
    #-- heart of the wiki -- don't try to read this! ;)

           "wiki_pre_scan_regex" => '/
        (?<![~!])
        ((?:(?:\w+:)*['.EWIKI_CHARS_U.']+['.EWIKI_CHARS_L.']+){2,}[\w\d]*)
        |\^([-'.EWIKI_CHARS_L.EWIKI_CHARS_U.']{3,})
        |\[ (?:"[^\]\"]+" | \s+ | [^:\]#]+\|)*  ([^\|\"\[\]\#]+)  (?:\s+ | "[^\]\"]+")* [\]\#] 
        |(\w{3,9}:\/\/[^?#\s\[\]\'\"\)\,<]+)    /x',

           "wiki_link_regex" => "\007 [!~]?(
        \#?\[[^<>\[\]\n]+\] |
        \^[-".EWIKI_CHARS_U.EWIKI_CHARS_L."]{3,} |
        \b([\w]{3,}:)*([".EWIKI_CHARS_U."]+[".EWIKI_CHARS_L."]+){2,}\#?[\w\d]* |
        ([a-z]{2,9}://|mailto:)[^\s\[\]\'\"\)\,<]+ |
        \w[-_.+\w]+@(\w[-_\w]+[.])+\w{2,}   ) \007x",

    #-- rendering ruleset
           "wm_indent" => '',
           "wm_table_defaults" => 'cellpadding="2" border="1" cellspacing="0"',
           "wm_whole_line" => array(),
           "htmlentities" => array(
        "&" => "&amp;",
        ">" => "&gt;",
        "<" => "&lt;",
           ),
           "wm_source" => array(
        "%%%" => "<br />",
        "\t" => "        ",
        "\n;:" => "\n      ",   # workaround, replaces the old ;:
           ),
           "wm_list" => array(
        "-" => array('ul type="square"', "", "li"),
        "*" => array('ul type="circle"', "", "li"),
        "#" => array("ol", "", "li"),
        ":" => array("dl", "dt", "dd"),
    #<out># ";" => array("dl", "dt", "dd"),
           ),
           "wm_style" => array(
        "'''''" => array("<b><i>", "</i></b>"),
        "'''" => array("<b>", "</b>"),
        "___" => array("<i><b>", "</b></i>"),
        "''" => array("<em>", "</em>"),
        "__" => array("<strong>", "</strong>"),
        "^^" => array("<sup>", "</sup>"),
        "==" => array("<tt>", "</tt>"),
    #<off># "***" => array("<b><i>", "</i></b>"),
    #<off># "###" => array("<big><b>", "</b></big>"),
        "**" => array("<b>", "</b>"),
        "##" => array("<big>", "</big>"),
        "µµ" => array("<small>", "</small>"),
           ),
           "wm_start_end" => array(
           ),
    #-- rendering plugins
           "format_block" => array(
        "html" => array("&lt;html&gt;", "&lt;/html&gt;", "html", 0x0000),
        "htm" => array("&lt;htm&gt;", "&lt;/htm&gt;", "html", 0x0003),
        "code" => array("&lt;code&gt;", "&lt;/code&gt;", false, 0x0000),
        "pre" => array("&lt;pre&gt;", "&lt;/pre&gt;", false, 0x003F),
        "comment" => array("\n&lt;!--", "--&gt;", false, 0x0030),
        #  "verbatim" => array("&lt;verbatim&gt;", "&lt;/verbatim&gt;", false, 0x0000),
           ),
           "format_params" => array(
        "scan_links" => 1,
        "html" => EWIKI_ALLOW_HTML,
        "mpi" => 1,
           ),
        );
        foreach ($ewiki_config_DEFAULTSTMP as $set => $val) {
           if (!isset($ewiki_config[$set])) {
              $ewiki_config[$set] = $val;
           }
           elseif (is_array($val)) foreach ($val as $vali=>$valv) {
              if (is_int($vali)) {
                 $ewiki_config[$set][] = $valv;
              }
              elseif (!isset($ewiki_config[$set][$vali])) {
                 $ewiki_config[$set][$vali] = $valv;
              }
           }
        }
        $ewiki_config_DEFAULTSTMP = $valv = $vali = $val = NULL;

    #-