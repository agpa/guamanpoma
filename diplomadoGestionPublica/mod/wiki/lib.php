<?php /**/ ?><?php  // $Id: lib.php,v 1.53.2.8 2009/11/19 10:42:58 skodak Exp $

/// Library of functions and constants for module wiki
/// (replace wiki with the name of your module and delete this line)


$wiki_CONSTANT = 7;     /// for example
$site = get_site();
$WIKI_TYPES = array ('teacher' =>   get_string('defaultcourseteacher'),
                     'group' =>     get_string('groups',"wiki"),
                     'student' =>   get_string('defaultcoursestudent') );
define("EWIKI_ESCAPE_AT", 0);       # For the algebraic filter

// How long locks stay around without being confirmed (seconds)
define("WIKI_LOCK_PERSISTENCE",120);

// How often to confirm that you still want a lock
define("WIKI_LOCK_RECONFIRM",60);

// Session variable used to store wiki locks
define('SESSION_WIKI_LOCKS','wikilocks');

/*** Moodle 1.7 compatibility functions *****
 *
 ********************************************/
function wiki_context($wiki) {
    //TODO: add some $cm caching if needed
    if (is_object($wiki)) {
        $wiki = $wiki->id;
    }
    if (! $cm = get_coursemodule_from_instance('wiki', $wiki)) {
        error('Course Module ID was incorrect');
    }

    return get_context_instance(CONTEXT_MODULE, $cm->id);
}

function wiki_is_teacher($wiki, $userid=NULL) {
    return has_capability('mod/wiki:manage', wiki_context($wiki), $userid);
}

function wiki_is_teacheredit($wiki, $userid=NULL) {
    return has_capability('mod/wiki:manage', wiki_context($wiki), $userid)
       and has_capability('moodle/site:accessallgroups', wiki_context($wiki), $userid);
}

function wiki_is_student($wiki, $userid=NULL) {
    return has_capability('mod/wiki:participate', wiki_context($wiki), $userid);
}

function wiki_get_students($wiki, $groups='', $sort='u.lastaccess', $fields='u.*') {
    return $users = get_users_by_capability(wiki_context($wiki), 'mod/wiki:participate', $fields, $sort, '', '', $groups);
}

/* end of compatibility functions */


function wiki_add_instance($wiki) {
/// Given an object containing all the necessary data,
/// (defined by the form in mod.html) this function
/// will create a new instance and return the id number
/// of the new instance.

    $wiki->timemodified = time();

    # May have to add extra stuff in here #

    /// Determine the pagename for this wiki and save.
    $wiki->pagename = wiki_page_name($wiki);

    return insert_record("wiki", $wiki);
}


function wiki_update_instance($wiki) {
/// Given an object containing all the necessary data,
/// (defined by the form in mod.html) this function
/// will update an existing instance with new data.

    /// Determine the pagename for this wiki.
    $wiki->pagename = wiki_page_name($wiki);

    $wiki->timemodified = time();
    $wiki->id = $wiki->instance;
    return update_record("wiki", $wiki);
}

/// Delete all Directories recursively
function wiki_rmdir($basedir) {
  $handle = @opendir($basedir);
  if($handle) {
    while (false!==($folder = readdir($handle))) {
       if($folder != "." && $folder != ".." && $folder != "CVS") {
          wiki_rmdir("$basedir/$folder");  // recursive
       }
    }
    closedir($handle);
  }
  @rmdir($basedir);
}

function wiki_delete_instance($id) {
/// Given an ID of an instance of this module,
/// this function will permanently delete the instance
/// and any data that depends on it.
    global $CFG;

    if (! $wiki = get_record("wiki", "id", $id)) {
        return false;
    }

    $result = true;

    #Delete Files
### Should probably check regardless of this setting in case its been changed...
    if($wiki->ewikiacceptbinary) {
      if ($basedir = $CFG->dataroot."/".$wiki->course."/".$CFG->moddata."/wiki/$id") {
          if ($files = get_directory_list($basedir)) {
              foreach ($files as $file) {
                  #if ($file != $exception) {
                      unlink("$basedir/$file");
                      notify("Existing file '$file' has been deleted!");
                  #}
              }
          }
          #if (!$exception) {  // Delete directory as well, if empty
              wiki_rmdir("$basedir");
          #}
      }
    }

    # Delete any dependent records here #
    if(!delete_records("wiki_locks","wikiid",$wiki->id)) {
        $result = false;
    }

    if (! delete_records("wiki", "id", $wiki->id)) {
        $result = false;
    }

    /// Delete all wiki_entries and wiki_pages.
    if (($wiki_entries = wiki_get_entries($wiki)) !== false) {
        foreach ($wiki_entries as $wiki_entry) {
            if (! delete_records("wiki_pages", "wiki", "$wiki_entry->id")) {
                $result = false;
            }
            if (! delete_records("wiki_entries", "id", "$wiki_entry->id")) {
                $result = false;
            }
        }
    }

    return $result;
}

function wiki_user_outline($course, $user, $mod, $wiki) {
/// Return a small object with summary information about what a
/// user has done with a given particular instance of this module
/// Used for user activity reports.
/// $return->time = the time they did it
/// $return->info = a short text description

    $return = NULL;
    return $return;
}

function wiki_user_complete($course, $user, $mod, $wiki) {
/// Print a detailed representation of what a  user has done with
/// a given particular instance of this module, for user activity reports.

    return true;
}

function wiki_print_recent_activity($course, $isteacher, $timestart) {
/// Given a course and a time, this module should find recent activity
/// that has occurred in wiki activities and print it out.
/// Return true if there was output, or false is there was none.

    global $CFG;
    
    $sql = "SELECT l.*, cm.instance FROM {$CFG->prefix}log l 
                INNER JOIN {$CFG->prefix}course_modules cm ON l.cmid = cm.id 
            WHERE l.time > '$timestart' AND l.course = {$course->id} 
                AND l.module = 'wiki' AND action LIKE 'edit%'
            ORDER BY l.time ASC";
            
    if (!$logs = get_records_sql($sql)){
        return false;
    }

    $modinfo = get_fast_modinfo($course);
    $wikis = array();

    foreach ($logs as $log) {
        $cm = $modinfo->instances['wiki'][$log->instance];
        if (!$cm->uservisible) {
            continue;
        }

    /// Process log->url and rebuild it here to properly clean the pagename - MDL-15896
        $extractedpage = preg_replace('/^.*&page=/', '', $log->url);
        $log->url = preg_replace('/page=.*$/', 'page='.urlencode($extractedpage), $log->url);

        $wikis[$log->info] = wiki_log_info($log);
        $wikis[$log->info]->pagename = $log->info;
        $wikis[$log->info]->time = $log->time;
        $wikis[$log->info]->url  = str_replace('&', '&amp;', $log->url);
    }

    if (!$wikis) {
        return false;
    }
    print_headline(get_string('updatedwikipages', 'wiki').':', 3);
    foreach ($wikis as $wiki) {
        print_recent_activity_note($wiki->time, $wiki, $wiki->pagename,
                                   $CFG->wwwroot.'/mod/wiki/'.$wiki->url);
    }

    return false;
}

function wiki_log_info($log) {
    global $CFG;
    return get_record_sql("SELECT u.firstname, u.lastname
                             FROM {$CFG->prefix}user u
                            WHERE u.id = '$log->userid'");
}

function wiki_cron () {
/// Function to be run periodically according to the moodle cron
/// This function searches for things that need to be done, such
/// as sending out mail, toggling flags etc ...

    // Delete expired locks
    $result=delete_records_select('wiki_locks','lockedseen < '.(time()-WIKI_LOCK_PERSISTENCE));

    return $result;
}

function wiki_get_participants($wikiid) {
//Returns the users with data in one wiki
//(users with records in wiki_pages and wiki_entries)

    global $CFG;

    //Get users from wiki_pages
    $st_pages = get_records_sql("SELECT DISTINCT u.id, u.id
                                 FROM {$CFG->prefix}user u,
                                      {$CFG->prefix}wiki_entries e,
                                      {$CFG->prefix}wiki_pages p
                                 WHERE e.wikiid = '$wikiid' and
                                       p.wiki = e.id and
                                       u.id = p.userid");

    //Get users from wiki_entries
    $st_entries = get_records_sql("SELECT DISTINCT u.id, u.id
                                   FROM {$CFG->prefix}user u,
                                        {$CFG->prefix}wiki_entries e
                                   WHERE e.wikiid = '$wikiid' and
                                         u.id = e.userid");

    //Add entries to pages
    if ($st_entries) {
        foreach ($st_entries as $st_entry) {
            $st_pages[$st_entry->id] = $st_entry;
        }
    }

    return $st_pages;
}


//////////////////////////////////////////////////////////////////////////////////////
/// Any other wiki functions go here.  Each of them must have a name that
/// starts with wiki_

function wiki_wiki_name($wikiname) {
/// Return the passed in string in Wiki name format.
/// Remove any leading and trailing whitespace, capitalize all the words
/// and then remove any internal whitespace.

    if (wiki_is_wiki_name($wikiname)) {
        return $wikiname;
    }
    else {
        /// Create uppercase words and remove whitespace.
        $wikiname = preg_replace("/(\w+)\s/", "$1", ucwords(trim($wikiname)));

        /// Check again - there may only be one word.
        if (wiki_is_wiki_name($wikiname)) {
            return $wikiname;
        }
        /// If there is only one word, append default wiki name to it.
        else {
            return $wikiname.get_string('wikidefaultpagename', 'wiki');
        }
    }
}

function wiki_is_wiki_name($wikiname) {
/// Check for correct wikiname syntax and return true or false.

    /// If there are spaces between the words, incorrect format.
    if (preg_match_all('/\w+/', $wikiname, $out) > 1) {
        return false;
    }
    /// If there isn't more than one group of uppercase letters separated by
    /// lowercase letters or '_', incorrect format.
    else if (preg_match_all('/[A-Z]+[a-z_]+/', $wikiname, $out) > 1) {
        return true;
    }
    else {
        return false;
    }
}

function wiki_page_name(&$wiki) {
/// Determines the wiki's page name and returns it.
    if (!empty($wiki->initialcontent)) {
        $ppos = strrpos($wiki->initialcontent, '/');
        if ($ppos === false) {
            $pagename = $wiki->initialcontent;
        }
        else {
            $pagename = substr($wiki->initialcontent, $ppos+1);
        }
    }
    else if (!empty($wiki->pagename)) {
        $pagename = $wiki->pagename;
    }
    else {
        $pagename = $wiki->name;
    }
    return $pagename;
}

function wiki_content_dir(&$wiki) {
/// Determines the wiki's default content directory (if there is one).
    global $CFG;

    if (!empty($wiki->initialcontent)) {
        $ppos = strrpos($wiki->initialcontent, '/');
        if ($ppos === false) {
            $subdir = '';
        }
        else {
            $subdir = substr($wiki->initialcontent, 0, $ppos+1);
        }
        $contentdir = $CFG->dataroot.'/'.$wiki->course.'/'.$subdir;
    }
    else {
        $contentdir = false;
    }
    return $contentdir;
}

function wiki_get_course_wikis($courseid, $wtype='*') {
/// Returns all wikis for the specified course and optionally of the specified type.

    $select = 'course = '.$courseid;
    if ($wtype != '*') {
        $select .= ' AND wtype = \''.$wtype.'\'';
    }
    return get_records_select('wiki', $select, 'id');
}

function wiki_has_entries(&$wiki) {
/// Returns true if wiki already has wiki entries; otherwise false.

    return record_exists('wiki_entries', 'wikiid', $wiki->id);
}

function wiki_get_entries(&$wiki, $byindex=NULL) {
/// Returns an array with all wiki entries indexed by entry id; false if there are none.
/// If the optional $byindex is specified, returns the entries indexed by that field.
/// Valid values for $byindex are 'student', 'group'.
    global $CFG;
    
    if ($byindex == 'student') {
        return get_records('wiki_entries', 'wikiid', $wiki->id, '',
                           'userid,id,wikiid,course,groupid,pagename,timemodified');
    }
    else if ($byindex == 'group') {
        return get_records('wiki_entries', 'wikiid', $wiki->id, '',
                           'groupid,id,wikiid,course,userid,pagename,timemodified');
    }
    else {
        return get_records('wiki_entries', 'wikiid', $wiki->id);
    }
}

function wiki_get_default_entry(&$wiki, &$course, $userid=0, $groupid=0) {
/// Returns the wiki entry according to the wiki type.
/// Optionally, will return wiki entry for $userid student wiki, or
/// $groupid group or teacher wiki.
/// Creates one if it needs to and it can.
    global $USER;
    /// If there is a groupmode, get the user's group id.
    $groupmode = groups_get_activity_groupmode($wiki);
    // if groups mode is in use and no group supplied, use the first one found
    if ($groupmode && !$groupid) {
        if(($mygroupids=mygroupid($course->id)) && count($mygroupids)>0) {
            // Use first group. They ought to be able to change later
            $groupid=$mygroupids[0];
        } else {
            // Whatever groups are in the course, pick one
            $coursegroups = groups_get_all_groups($course->id);
            if(!$coursegroups || count($coursegroups)==0) {
                error("Can't access wiki in group mode when no groups are configured for the course");
            }
            $unkeyed=array_values($coursegroups); // Make sure first item is index 0
            $groupid=$unkeyed[0]->id;
        }
    }

    /// If the wiki entry doesn't exist, can this user create it?
    if (($wiki_entry = wiki_get_entry($wiki, $course, $userid, $groupid)) === false) {
        if (wiki_can_add_entry($wiki, $USER, $course, $userid, $groupid)) {
            wiki_add_entry($wiki, $course, $userid, $groupid);
            if (($wiki_entry = wiki_get_entry($wiki, $course, $userid, $groupid)) === false) {
                error("Could not add wiki entry.");
            }
        }
    }
    //print_object($wiki_entry);
    return $wiki_entry;
}

function wiki_get_entry(&$wiki, &$course, $userid=0, $groupid=0) {
/// Returns the wiki entry according to the wiki type.
/// Optionally, will return wiki entry for $userid student wiki, or
/// $groupid group or teacher wiki.
    global $USER;

    switch ($wiki->wtype) {
    case 'student':
        /// If a specific user was requested, return it, if allowed.
        if ($userid and wiki_user_can_access_student_wiki($wiki, $userid, $course)) {
            $wentry = wiki_get_student_entry($wiki, $userid);
        }

        /// If there is no entry for this user, check if this user is a teacher.
        else if (!$wentry = wiki_get_student_entry($wiki, $USER->id)) {
/*            if (wiki_is_teacher($wiki, $USER->id)) {
                /// If this user is a teacher, return the first entry.
                if ($wentries = wiki_get_entries($wiki)) {
                    $wentry = current($wentries);
                }
            }*/
        }
        break;

    case 'group':
        /// If there is a groupmode, get the user's group id.
        $groupmode = groups_get_activity_groupmode($wiki);
        if($groupmode) {
            if(!$groupid) {
                if(($mygroupids=mygroupid($course->id)) && count($mygroupids)>0) {
                    // Use first group. They ought to be able to change later
                    $groupid=$mygroupids[0];
                } else {
                    // Whatever groups are in the course, pick one
                    $coursegroups = groups_get_all_groups($course->id);
                    if(!$coursegroups || count($coursegroups)==0) {
                        error("Can't access wiki in group mode when no groups are configured for the course");
                    }
                    $unkeyed=array_values($coursegroups); // Make sure first item is index 0
                    $groupid=$unkeyed[0]->id;
                }
            }

            //echo "groupid is in wiki_get_entry ".$groupid."<br />";
            /// If a specific group was requested, return it, if allowed.
            if ($groupid and wiki_user_can_access_group_wiki($wiki, $groupid, $course)) {
                $wentry = wiki_get_group_entry($wiki, $groupid);
            } else {
                error("Cannot access any groups for this wiki");
            }
        }
        /// If mode is 'nogroups', then groupid is zero.
        else {
            $wentry = wiki_get_group_entry($wiki, 0);
        }
        break;

    case 'teacher':
        /// If there is a groupmode, get the user's group id.
        if (groupmode($course, $wiki)) {
            $mygroupids = mygroupid($course->id);//same here, default to the first one
            $groupid = $groupid ? $groupid : $mygroupids[0]/*mygroupid($course->id)*/;
        }

        /// If a specific group was requested, return it, if allowed.
        if (wiki_user_can_access_teacher_wiki($wiki, $groupid, $course)) {
            $wentry = wiki_get_teacher_entry($wiki, $groupid);
        }
        break;
    }
    return $wentry;
}

function wiki_get_teacher_entry(&$wiki, $groupid=0) {
/// Returns the wiki entry for the wiki teacher type.
    return get_record('wiki_entries', 'wikiid', $wiki->id, 'course', $wiki->course, 'groupid', $groupid);
}

function wiki_get_group_entry(&$wiki, $groupid=null) {
/// Returns the wiki entry for the given group.
    return get_record('wiki_entries', 'wikiid', $wiki->id, 'groupid', $groupid);
}

function wiki_get_student_entry(&$wiki, $userid=null) {
/// Returns the wiki entry for the given student.
    global $USER;

    if (is_null($userid)) {
        $userid = $USER->id;
    }
    return get_record('wiki_entries', 'wikiid', $wiki->id, 'userid', $userid);
}

function wiki_get_other_wikis(&$wiki, &$user, &$course, $currentid=0) {
    /// Returns a list of other wikis to display, depending on the type, group and user.
    /// Returns the key containing the currently selected entry as well.

    global $CFG, $id;

    $wikis = false;

    $groupmode = groups_get_activity_groupmode($wiki);
    $mygroupid = mygroupid($course->id);
    $isteacher = wiki_is_teacher($wiki, $user->id);
    $isteacheredit = wiki_is_teacheredit($wiki, $user->id);

    $groupingid = null;
    $cm = new stdClass;
    $cm->id = $wiki->cmid;
    $cm->groupmode = $wiki->groupmode;
    $cm->groupingid = $wiki->groupingid;
    $cm->groupmembersonly = $wiki->groupmembersonly;
    if (!empty($CFG->enablegroupings) && !empty($cm->groupingid)) {
        $groupingid = $wiki->groupingid;
    }
    
    
    switch ($wiki->wtype) {

    case 'student':
        /// Get all the existing entries for this wiki.
        $wiki_entries = wiki_get_entries($wiki, 'student');
        
        if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid)) {
            $sql = "SELECT gm.userid FROM {$CFG->prefix}groups_members gm " .
                    "INNER JOIN {$CFG->prefix}groupings_groups gg ON gm.groupid = gg.groupid " .
                    "WHERE gg.groupingid = $wiki->groupingid ";
    
            $groupingmembers = get_records_sql($sql);
        }
        
        if ($isteacher and (SITEID != $course->id)) {

            /// If the user is an editing teacher, or a non-editing teacher not assigned to a group, show all student
            /// wikis, regardless of creation.
            if ((SITEID != $course->id) and ($isteacheredit or ($groupmode == NOGROUPS))) {

                if ($students = get_course_students($course->id)) {
                    /// Default pagename is dependent on the wiki settings.
                    $defpagename = empty($wiki->pagename) ? get_string('wikidefaultpagename', 'wiki') : $wiki->pagename;

                    foreach ($students as $student) {
                        if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid) && empty($groupingmembers[$student->id])) {
                            continue;
                        }
                        /// If this student already has an entry, use its pagename.
                        if ($wiki_entries[$student->id]) {
                            $pagename = $wiki_entries[$student->id]->pagename;
                        }
                        else {
                            $pagename = $defpagename;
                        }

                        $key = 'view.php?id='.$id.'&userid='.$student->id.'&page='.$pagename;
                        $wikis[$key] = fullname($student).':'.$pagename;
                    }
                }
            }
            else if ($groupmode == SEPARATEGROUPS) {

                if ($students = wiki_get_students($wiki, $mygroupid)) {
                    $defpagename = empty($wiki->pagename) ? get_string('wikidefaultpagename', 'wiki') : $wiki->pagename;
                    foreach ($students as $student) {
                        if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid) && empty($groupingmembers[$student->id])) {
                            continue;
                        }
                        /// If this student already has an entry, use its pagename.
                        if ($wiki_entries[$student->id]) {
                            $pagename = $wiki_entries[$student->id]->pagename;
                        }
                        else {
                            $pagename = $defpagename;
                        }

                        $key = 'view.php?id='.$id.'&userid='.$student->id.'&page='.$pagename;
                        $wikis[$key] = fullname($student).':'.$pagename;
                    }
                }
            }
            else if ($groupmode == VISIBLEGROUPS) {
                /// Get all students in your group.
                if ($students = wiki_get_students($wiki, $mygroupid)) {
                    $defpagename = empty($wiki->pagename) ? get_string('wikidefaultpagename', 'wiki') : $wiki->pagename;
                    foreach ($students as $student) {
                        if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid) && empty($groupingmembers[$student->id])) {
                            continue;
                        }
                        /// If this student already has an entry, use its pagename.
                        if ($wiki_entries[$student->id]) {
                            $pagename = $wiki_entries[$student->id]->pagename;
                        }
                        else {
                            $pagename = $defpagename;
                        }
                        $key = 'view.php?id='.$id.'&userid='.$student->id.'&page='.$pagename;
                        $wikis[$key] = fullname($student).':'.$pagename;
                    }
                }
                /// Get all student wikis created, regardless of group.
                if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid)) {
                    $sql = 'SELECT w.id, w.userid, w.pagename, u.firstname, u.lastname '
                          .'    FROM '.$CFG->prefix.'wiki_entries w '
                          .'    INNER JOIN '.$CFG->prefix.'user u ON w.userid = u.id '
                          .'    INNER JOIN '.$CFG->prefix.'groups_members gm ON gm.userid = u.id '
                          .'    INNER JOIN '.$CFG->prefix.'groupings_groups gg ON gm.groupid = gg.groupid '
                          .'    WHERE w.wikiid = '.$wiki->id.' AND gg.groupingid =  '.$wiki->groupingid
                          .'    ORDER BY w.id';
                } else {
                    $sql = 'SELECT w.id, w.userid, w.pagename, u.firstname, u.lastname '
                          .'    FROM '.$CFG->prefix.'wiki_entries w, '.$CFG->prefix.'user u '
                          .'    WHERE w.wikiid = '.$wiki->id.' AND u.id = w.userid '
                          .'    ORDER BY w.id';
                }
                $wiki_entries = get_records_sql($sql);
                $wiki_entries=is_array($wiki_entries)?$wiki_entries:array();
                foreach ($wiki_entries as $wiki_entry) {
                    $key = 'view.php?id='.$id.'&userid='.$wiki_entry->userid.'&page='.$wiki_entry->pagename;
                    $wikis[$key] = fullname($wiki_entry).':'.$wiki_entry->pagename;
                    if ($currentid == $wiki_entry->id) {
                        $wikis['selected'] = $key;
                    }
                }
            }
        }
        else {
            /// A user can see other student wikis if they are a member of the same
            /// group (for separate groups) or there are visible groups, or if this is
            /// a site-level wiki, and they are an administrator.
            if (($groupmode == VISIBLEGROUPS) or wiki_is_teacheredit($wiki)) {
                $viewall = true;
            }
            else if ($groupmode == SEPARATEGROUPS) {
                $viewall = mygroupid($course->id);
            }
            else {
                $viewall = false;
            }

            if ($viewall !== false) {
                if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid)) {
                    $sql = 'SELECT w.id, w.userid, w.pagename, u.firstname, u.lastname '
                          .'    FROM '.$CFG->prefix.'wiki_entries w '
                          .'    INNER JOIN '.$CFG->prefix.'user u ON w.userid = u.id '
                          .'    INNER JOIN '.$CFG->prefix.'groups_members gm ON gm.userid = u.id '
                          .'    INNER JOIN '.$CFG->prefix.'groupings_groups gg ON gm.groupid = gg.groupid '
                          .'    WHERE w.wikiid = '.$wiki->id.' AND gg.groupingid =  '.$wiki->groupingid
                          .'    ORDER BY w.id';
                } else {
                    $sql = 'SELECT w.id, w.userid, w.pagename, u.firstname, u.lastname '
                          .'    FROM '.$CFG->prefix.'wiki_entries w, '.$CFG->prefix.'user u '
                          .'    WHERE w.wikiid = '.$wiki->id.' AND u.id = w.userid '
                          .'    ORDER BY w.id';
                }
                $wiki_entries = get_records_sql($sql);
                $wiki_entries=is_array($wiki_entries)?$wiki_entries:array();
                foreach ($wiki_entries as $wiki_entry) {
                    if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid) && empty($groupingmembers[$wiki_entry->userid])) {
                        continue;
                    }
                
                    if (($viewall === true) or groups_is_member($viewall, $wiki_entry->userid)) {
                        $key = 'view.php?id='.$id.'&userid='.$wiki_entry->userid.'&page='.$wiki_entry->pagename;
                        $wikis[$key] = fullname($wiki_entry).':'.$wiki_entry->pagename;
                        if ($currentid == $wiki_entry->id) {
                            $wikis['selected'] = $key;
                        }
                    }
                }
            }
        }
        break;

    case 'group':
        /// If the user is an editing teacher, or a non-editing teacher not assigned to a group, show all group
        /// wikis, regardless of creation.

        /// If user is a member of multiple groups, need to show current group etc?

        /// Get all the existing entries for this wiki.
        $wiki_entries = wiki_get_entries($wiki, 'group');
        
        if ($groupmode and ($isteacheredit or ($isteacher and !$mygroupid))) {
            if ($groups = groups_get_all_groups($course->id, null, $groupingid)) {
                $defpagename = empty($wiki->pagename) ? get_string('wikidefaultpagename', 'wiki') : $wiki->pagename;
                foreach ($groups as $group) {

                    /// If this group already has an entry, use its pagename.
                    if (isset($wiki_entries[$group->id])) {
                        $pagename = $wiki_entries[$group->id]->pagename;
                    }
                    else {
                        $pagename = $defpagename;
                    }

                    $key = 'view.php?id='.$id.($group->id?"&groupid=".$group->id:"").'&page='.$pagename;
                    $wikis[$key] = $group->name.':'.$pagename;
                }
            }
        }
        //if a studnet with multiple groups in SPG
        else if ($groupmode == SEPARATEGROUPS){
            if ($groups = groups_get_all_groups($course->id, $user->id, $groupingid)){

                $defpagename = empty($wiki->pagename) ? get_string('wikidefaultpagename', 'wiki') : $wiki->pagename;
                foreach ($groups as $group) {
                    /// If this group already has an entry, use its pagename.
                    if (isset($wiki_entries[$group->id])) {
                        $pagename = $wiki_entries[$group->id]->pagename;
                    }
                    else {
                        $pagename = $defpagename;
                    }
                    $key = 'view.php?id='.$id.($group->id?"&groupid=".$group->id:"").'&page='.$pagename;
                    $wikis[$key] = $group->name.':'.$pagename;
                }

            }

        }
        /// A user can see other group wikis if there are visible groups.
        else if ($groupmode == VISIBLEGROUPS) {
            if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid)) {
                $sql = 'SELECT w.id, w.groupid, w.pagename, g.name as gname '
                      .'    FROM '.$CFG->prefix.'wiki_entries w '
                      .'    INNER JOIN '.$CFG->prefix.'groups g ON g.id = w.groupid '
                      .'    INNER JOIN '.$CFG->prefix.'groupings_groups gg ON g.id = gg.groupid '
                      .'    WHERE w.wikiid = '.$wiki->id.' AND gg.groupingid =  '.$wiki->groupingid
                      .'    ORDER BY w.groupid';
            } else {
                $sql = 'SELECT w.id, w.groupid, w.pagename, g.name as gname '
                      .'    FROM '.$CFG->prefix.'wiki_entries w, '.$CFG->prefix.'groups g '
                      .'    WHERE w.wikiid = '.$wiki->id.' AND g.id = w.groupid '
                      .'    ORDER BY w.groupid';
            }
            $wiki_entries = get_records_sql($sql);
            $wiki_entries=is_array($wiki_entries)?$wiki_entries:array();
            foreach ($wiki_entries as $wiki_entry) {
                $key = 'view.php?id='.$id.($wiki_entry->groupid?"&groupid=".$wiki_entry->groupid:"").'&page='.$wiki_entry->pagename;
                $wikis[$key] = $wiki_entry->gname.':'.$wiki_entry->pagename;
                if ($currentid == $wiki_entry->id) {
                    $wikis['selected'] = $key;
                }
            }
        }
        break;

    case 'teacher':
        if ($isteacher) {
            /// If the user is an editing teacher, or a non-editing teacher not assigned to a group, show all
            /// teacher wikis, regardless of creation.
            if ($groupmode and ($isteacheredit or ($isteacher and !$mygroupid))) {
                if ($groups = groups_get_all_groups($course->id, null, $groupingid)) {
                    $defpagename = empty($wiki->pagename) ? get_string('wikidefaultpagename', 'wiki') : $wiki->pagename;
                    foreach ($groups as $group) {
                        /// If this group already has an entry, use its pagename.
                        if ($wiki_entries[$group->id]) {
                            $pagename = $wiki_entries[$group->id]->pagename;
                        }
                        else {
                            $pagename = $defpagename;
                        }

                        $key = 'view.php?id='.$id.($group->id?"&groupid=".$group->id:"").'&page='.$pagename;
                        $wikis[$key] = $group->name.':'.$pagename;
                    }
                }
            }
            /// A teacher can see all other group teacher wikis.
            else if ($groupmode) {
            if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid)) {
                $sql = 'SELECT w.id, w.groupid, w.pagename, g.name as gname '
                      .'    FROM '.$CFG->prefix.'wiki_entries w '
                      .'    INNER JOIN '.$CFG->prefix.'groups g ON g.id = w.groupid '
                      .'    INNER JOIN '.$CFG->prefix.'groupings_groups gg ON g.id = gg.groupid '
                      .'    WHERE w.wikiid = '.$wiki->id.' AND gg.groupingid =  '.$wiki->groupingid
                      .'    ORDER BY w.groupid';
            } else {
                $sql = 'SELECT w.id, w.groupid, w.pagename, g.name as gname '
                      .'    FROM '.$CFG->prefix.'wiki_entries w, '.$CFG->prefix.'groups g '
                      .'    WHERE w.wikiid = '.$wiki->id.' AND g.id = w.groupid '
                      .'    ORDER BY w.groupid';
            }
                $wiki_entries = get_records_sql($sql);
                $wiki_entries=is_array($wiki_entries)?$wiki_entries:array();
                foreach ($wiki_entries as $wiki_entry) {
                    $key = 'view.php?id='.$id.($wiki_entry->groupid?"&groupid=".$wiki_entry->groupid:"").'&page='.$wiki_entry->pagename;
                    $wikis[$key] = $wiki_entry->gname.':'.$wiki_entry->pagename;
                    if ($currentid == $wiki_entry->id) {
                        $wikis['selected'] = $key;
                    }
                }
            }
        }
        else {
            /// A user can see other teacher wikis if they are a teacher, a member of the same
            /// group (for separate groups) or there are visible groups.
            if ($groupmode == VISIBLEGROUPS) {
                $viewall = true;
            }
            else if ($groupmode == SEPARATEGROUPS) {
                $viewall = $mygroupid;
            }
            else {
                $viewall = false;
            }
            if ($viewall !== false) {
                if (!empty($CFG->enablegroupings) && !empty($wiki->groupingid)) {
                    $sql = 'SELECT w.id, w.groupid, w.pagename, g.name as gname '
                          .'    FROM '.$CFG->prefix.'wiki_entries w '
                          .'    INNER JOIN '.$CFG->prefix.'groups g ON g.id = w.groupid '
                          .'    INNER JOIN '.$CFG->prefix.'groupings_groups gg ON g.id = gg.groupid '
                          .'    WHERE w.wikiid = '.$wiki->id.' AND gg.groupingid =  '.$wiki->groupingid
                          .'    ORDER BY w.groupid';
                } else {
                    $sql = 'SELECT w.id, w.groupid, w.pagename, g.name as gname '
                          .'    FROM '.$CFG->prefix.'wiki_entries w, '.$CFG->prefix.'groups g '
                          .'    WHERE w.wikiid = '.$wiki->id.' AND g.id = w.groupid '
                          .'    ORDER BY w.groupid';
                }
                $wiki_entries = get_records_sql($sql);
                $wiki_entries=is_array($wiki_entries)?$wiki_entries:array();


                foreach ($wiki_entries as $wiki_entry) {
                    if (($viewall === true) or @in_array($wiki_entry->groupid, $viewall)/*$viewall == $wiki_entry->groupid*/) {
                        $key = 'view.php?id='.$id.($wiki_entry->groupid?"&groupid=".$wiki_entry->groupid:"").'&page='.$wiki_entry->pagename;
                        $wikis[$key] = $wiki_entry->gname.':'.$wiki_entry->pagename;
                        if ($currentid == $wiki_entry->id) {
                            $wikis['selected'] = $key;
                        }
                    }
                }
            }
        }
        break;
    }
    
    return $wikis;
}

function wiki_add_entry(&$wiki, &$course, $userid=0, $groupid=0) {
/// Adds a new wiki entry of the specified type, unless already entered.
/// No checking is done here. It is assumed that the caller has the correct
/// privileges to add this entry.

    global $USER;

    /// If this wiki already has a wiki_type entry, return false.
    if (wiki_get_entry($wiki, $course, $userid, $groupid) !== false) {
        return false;
    }

    $wiki_entry = new Object();

    switch ($wiki->wtype) {

    case 'student':
        $wiki_entry->wikiid = $wiki->id;
        $wiki_entry->useri