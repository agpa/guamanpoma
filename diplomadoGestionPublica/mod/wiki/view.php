<?php /**/ ?><?php  // $Id: view.php,v 1.76.2.6 2008/09/29 22:37:06 stronk7 Exp $
/// Extended by Michael Schneider
/// This page prints a particular instance of wiki

    global $CFG,$USER;

    require_once("../../config.php");
    require_once("lib.php");
    #require_once("$CFG->dirroot/course/lib.php"); // For side-blocks    
    require_once($CFG->libdir . '/ajax/ajaxlib.php');
    require_js(array('yui_yahoo', 'yui_event', 'yui_connection'));

    $ewiki_action = optional_param('ewiki_action', '', PARAM_ALPHA);     // Action on Wiki-Page
    $id           = optional_param('id', 0, PARAM_INT);                  // Course Module ID, or
    $wid          = optional_param('wid', 0, PARAM_INT);                 // Wiki ID
    $page         = optional_param('page', false);                       // Wiki Page Name
    $q            = optional_param('q',"", PARAM_PATH);                  // Search Context
    $userid       = optional_param('userid', 0, PARAM_INT);              // User wiki.
    $groupid      = optional_param('groupid', 0, PARAM_INT);             // Group wiki.
    $canceledit   = optional_param('canceledit','', PARAM_ALPHA);        // Editing has been cancelled
    $cacheme      = optional_param('allowcache', 1, PARAM_INT);          // Set this to 0 to try and disable page caching.
    
    // Only want to add edit log entries if we have made some changes ie submitted a form
    $editsave = optional_param('thankyou', '');
    
    if($page) {
        // Split page command into action and page
        $actions = explode('/', $page,2);
        if(count($actions)==2) {
            $pagename=$actions[1];
        } else {
            $pagename=$actions[0];
        }
    } else {
        $actions=array('');
        $pagename='';
    }
    
    if ($id) {
        if (! $cm = get_coursemodule_from_id('wiki', $id)) {
            error("Course Module ID was incorrect");
        }

        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $wiki = get_record("wiki", "id", $cm->instance)) {
            error("Course module is incorrect");
        }

    } else {
        if (! $wiki = get_record("wiki", "id", $wid)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $wiki->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("wiki", $wiki->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_course_login($course, true, $cm);
    
    /// Add the course module info to the wiki object, for easy access.
    $wiki->groupmode = $cm->groupmode;
    $wiki->groupingid = $cm->groupingid;
    $wiki->groupmembersonly = $cm->groupmembersonly;
    $wiki->cmid = $cm->id;
    
    /// Default format:
    $moodle_format=FORMAT_MOODLE;

/// Globally disable CamelCase, if the option is selected for this wiki.
    $moodle_disable_camel_case = ($wiki->disablecamelcase == 1);
    
    if (($wiki_entry = wiki_get_default_entry($wiki, $course, $userid, $groupid))) {
        // OK, now we know the entry ID, we can do lock etc.
        
        // If true, we are 'really' on an editing page, not just on edit/something
        $reallyedit=$actions[0]=='edit' && !$canceledit && !$editsave;

        // Remove lock when we go to another wiki page (such as the cancel page)
        if(!$reallyedit) {
            wiki_release_lock($wiki_entry->id,$pagename);
        } else if(array_key_exists('content',$_POST)) {
            // Do not allow blank content because it causes problems (the wiki decides
            // the page should automatically go into edit mode, but Moodle doesn't realise
            // this and filters out the JS)
            if($_POST['content']=='') {
                $_POST['content']="\n";
                $_REQUEST['content']="\n";
            }
    
            // We must have the edit lock in order to be permitted to save    
            list($ok,$lock)=wiki_obtain_lock($wiki_entry->id,$pagename);
            if(!$ok) {
                $strsavenolock=get_string('savenolock','wiki');
                error($strsavenolock,$CFG->wwwroot.'/mod/wiki/view.php?id='.$cm->id.'&page=view/'.urlencode($pagename));
            }
        }
        
///     ################# EWIKI Part ###########################
///     The wiki_entry->pagename is set to the specified value of the wiki,
///     or the default value in the 'lang' file if the specified value was empty.
        define("EWIKI_PAGE_INDEX",$wiki_entry->pagename);

        /// If the page has a ' in it, it may have slashes added to it. Remove them if it does.
        $page = ($page === false) ?  stripslashes(EWIKI_PAGE_INDEX) : stripslashes($page);

///     # Prevent ewiki getting id as PageID...
        unset($_REQUEST["id"]);
        unset($_GET["id"]);
        unset($_POST["id"]);
        unset($_POST["id"]);
        unset($_SERVER["QUERY_STRING"]);
        if (isset($HTTP_GET_VARS)) {
            unset($HTTP_GET_VARS["id"]);
        }
        if (isset($HTTP_POST_VARS)) {
            unset($HTTP_POST_VARS["id"]);
        }
        global $ewiki_title;

///     #-- predefine some of the configuration constants


        /// EWIKI_NAME is defined in ewikimoodlelibs, so that also admin.php can use this
        #define("EWIKI_NAME", $wiki_entry->pagename);

        /// Search Hilighting
        if($ewiki_title=="SearchPages") {
            $qArgument="&amp;q=".urlencode($q);
        }

        /// Build the ewsiki script constant
        /// ewbase will also be needed by EWIKI_SCRIPT_BINARY
        $ewbase = 'view.php?id='.$cm->id;
        if (isset($userid) && $userid!=0) $ewbase .= '&amp;userid='.$userid;
        if (isset($groupid) && $groupid!=0) $ewbase .= '&amp;groupid='.$groupid;
        $ewscript = $ewbase.'&amp;page=';
        define("EWIKI_SCRIPT", $ewscript);
        define("EWIKI_SCRIPT_URL", $ewscript);

        /// # Settings for this specific Wiki
        define("EWIKI_PRINT_TITLE", $wiki->ewikiprinttitle);

        define("EWIKI_INIT_PAGES", wiki_content_dir($wiki));

///     # Moodle always addslashes to everything so we are going to strip them always
///     # to allow wiki itself to add them again. It's a triple add-strip-add but
///     # was the only way to solve the problem without modifying how the rest of
///     # the module works.
        include($CFG->dirroot."/mod/wiki/ewiki/fragments/strip_wonderful_slashes.php");

        if (ini_get("register_globals")) {
            #    include($CFG->dirroot."/mod/wiki/ewiki/fragments/strike_register_globals.php");
        }

        # Database Handler
        include_once($CFG->dirroot."/mod/wiki/ewikimoodlelib.php");
        # Plugins
        //include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/email_protect.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/patchsaving.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/notify.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/feature/imgresize_gd.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/moodle_highlight.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/f_fixhtml.php");
        #include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/wikinews.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/sitemap.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/moodle_wikidump.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/aview/backlinks.php");
        #include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/markup/css.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/markup/footnotes.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/diff.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/page/pageindex.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/page/orphanedpages.php");
        include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/wantedpages.php");

        # Binary Handling
        if($wiki->ewikiacceptbinary) {
            define("EWIKI_UPLOAD_MAXSIZE", get_max_upload_file_size());
            define("EWIKI_SCRIPT_BINARY", $ewbase."&binary=");
            define("EWIKI_ALLOW_BINARY",1);
            define("EWIKI_IMAGE_CACHING",1);
            #define("EWIKI_AUTOVIEW",1);
            include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/lib/mime_magic.php");
            include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/aview/downloads.php");
            include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/downloads.php");
            #include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/db/binary_store.php");
            include_once($CFG->dirroot."/mod/wiki/ewiki/plugins/moodle/moodle_binary_store.php");
        } else {
            define("EWIKI_SCRIPT_BINARY", 0);
            define("EWIKI_ALLOW_BINARY",0);
        }

        # The mighty Wiki itself
        include_once($CFG->dirroot."/mod/wiki/ewiki/ewiki.php");

        if($canceledit) {
          if ($delim = strpos($page, EWIKI_ACTION_SEP_CHAR)) {
            @$page = substr($page, $delim + 1);
          } else {
            @$page="";
          }
        }
        # Language-stuff: eWiki gets language from Browser. Lets correct it. Empty arrayelements do no harm
        $ewiki_t["languages"]=array(current_language(), $course->lang, $CFG->lang,"en","c");

        # Check Access Rights
        $canedit = wiki_can_edit_entry($wiki_entry, $wiki, $USER, $course);
        if (!$canedit) {
            # Protected Mode
            unset($ewiki_plugins["action"]["edit"]);
            unset($ewiki_plugins["action"]["info"]);
