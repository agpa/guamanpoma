<?php /**/ ?><?php
// $Id: page.tpl.php,v 1.18.2.1.1 2009/05/20 09:29:30 goba Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
	<?php print $head ?>
	<title><?php print $head_title ?></title>
	<?php print $styles ?>
	<?php print $scripts ?>
	<!--[if lt IE 7]>
	<?php print phptemplate_get_ie_styles(); ?>
	<![endif]-->
</head>
<body<?php print phptemplate_body_class($left, $right); ?>>
	<!-- Layout -->
	<div id="wrapper">
		<div id="container" class="clear-block">
			<div id="header">
				<div id="logo-floater">
					<?php
					// Prepare header
					$site_fields = array();
					if ($site_name) {
					$site_fields[] = check_plain($site_name);
					}
					if ($site_slogan) {
					$site_fields[] = check_plain($site_slogan);
					}
					$site_title = implode(' ', $site_fields);
					if ($site_fields) {
					$site_fields[0] = '<span>'. $site_fields[0] .'</span>';
					}
					$site_html = implode(' ', $site_fields);
					
					if ($logo || $site_title) {
					print '<a href="'. check_url($front_page) .'" title="'. $site_title .'">';
					if ($logo) {
					print '<img style="visibility:hidden;" height="138" src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
					}
					print '</a>';
					}
					?>
				</div>
				<div id="logo-flash">  
					<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash5/cabs/swflash.cab#version=5,0,0,0" height="141" width="900">
					<param name="MOVIE" value="themes/guamanpoma/flash/animacion.swf">
					<param name="PLAY" value="true">
					<param name="LOOP" value="true">
					<param name="WMODE" value="opaque">
					<param name="QUALITY" value="high">
					<embed src="themes/guamanpoma/flash/animacion.swf" play="true" loop="true" wmode="opaque" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" height="141" width="900">
					</object>
				</div>
			</div> <!-- /header -->
			<div id="logogp">
				<div id="buscartopgp">
					<form method="post" accept-charset="UTF-8" action="/?q=search/">
					<input type="text" title="Introduzca los t�rminos que quiera buscar." value="" size="15" name="search_block_form" maxlength="128"/>
					<input type="submit" value="" name="op" style="width:25px; height:25px; background:url(themes/guamanpoma/images/iconos/subsearch.gif) no-repeat; border:none;"/>
					<input id="form-09782b73326e28cc2f87042afbf10a4f" type="hidden" value="form-09782b73326e28cc2f87042afbf10a4f" name="form_build_id"/>
<input id="edit-search-block-form-form-token" type="hidden" value="ce71c175db26a8ae37455c4b5a9acaea" name="form_token"/>
<input id="edit-search-block-form" type="hidden" value="search_block_form" name="form_id"/>					
					</form>

					
	
				</div>
				<div id="datosgp">
					<span style="visibility:hidden; font-size:6px;">Jr. Retiro 346 Urb. Tahuantinsuyo Cusco - Per�<br />
					Telf.:(51) 84 235931 &oacute; 236202 - Fax:(51) 84 225552</span>
				</div>
				<div id="toplinksgp">
					<?php if (isset($primary_links)) : ?>
					<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
					<?php endif; ?>
					<?php /** Lista los items de la Lista Secundaria
					if (isset($secondary_links)) :
					print theme('links', $secondary_links, array('class' => 'links secondary-links'))
					endif; **/ ?>
				</div>		 	
			</div>
			<div id="limpiezagp"></div>
			
			
			
			<?php if ($left): ?>
			<div id="sidebar-left" class="sidebar">
				<?php /** if ($search_box):
				<div class="block block-theme">print $search_box </div>
				endif; **/
				print $left;
				include("imagenesbarraizquierda.php");
				?>
			</div>
			<?php endif; ?>
			<div id="center">
				<div id="squeeze">
					<div class="right-corner">
						<div class="left-corner">
							<?php //print $breadcrumb; ?>
							<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
							<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
							<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
							<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
							<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
							<?php if ($show_messages && $messages): print $messages; endif; ?>
							<?php print $help; ?>
							
							<?php if ($is_front) :?>
							<div id="maingpa">
							<?php endif; ?>
							<div class="clear-block"><?php print $content ?></div>
							<?php if ($is_front) :?>
							</div>
							<?php endif; ?>
							<?php print $feed_icons ?>
							<div id="footer">
								<div id="block-system-0" class="clear-block block block-system">
									<div class="content" style="visibility:hidden;">
									<a href="http://drupal.org">
										<img width="80" height="15" title="Sitio elaborado con Drupal, un sistema de gesti&oacute;n de contenido de c&oacute;digo abierto" alt="Sitio elaborado con Drupal, un sistema de gesti&oacute;n de contenido de c&oacute;digo abierto" src="misc/powered-blue-80x15.png"/>
									</a>
									<a href="http://didjeram.com">
										<img width="80" height="15" title="Sitio elaborado por Didjeram" alt="Sitio elaborado por Didjeram" src="misc/didjeram.png"/>
									</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- /.left-corner, /.right-corner, /#squeeze, /#center -->
			<?php if ($right): ?>
			<div id="sidebar-right" class="sidebar">
				<?php if (!$left && $search_box): ?>
				<div class="block block-theme"><?php print $search_box ?></div>
				<?php endif; ?>
				<?php print $right;
				include("imagenesbarraderecha.php");
				?>
			</div>
			<?php endif; ?>
			
		</div> <!-- /container -->
	</div>
	<!-- /layout -->
	<?php print $closure ?>
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-9061047-1', 'guamanpoma.org');
  ga('send', 'pageview');

</script>
<!-- Fin Google Analytics -->	
</body>
</html>
