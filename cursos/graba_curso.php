<?php
include('FormValidator.class.inc');
include ('application.php');
include ('../adodb/adodb/adodb.inc.php');
include('../adodb/adodb/tohtml.inc.php');
include('mail/cmailer.php');

function headerhtml($title,$css_link) {
        echo "<html><head><title>".$title."</title><link href=\"".$css_link."\" rel=\"stylesheet\" type=\"text/css\"></head>";
}

function footerhtml() {
        echo "</body></html>";
}

/* esta funcion convierte del formato dd/mm/YYYY a un timestamp */
function convierte_fecha($date) {


	$datesep=explode("/", $date);
	$year = $datesep[2];
	$month = $datesep[1];
	$day = $datesep[0];

	$newdate=mktime(0,0,0, $month,$day,$year); 
	
	return($newdate);
			
}

function enviamail($o,$d,$a,$b) {
      /* mandamos un mail de confirmacion al matriculado */
       $m = new cMailer();
       $m->AddAddress($d);
       $m->AddSender($o);
       $m->AddSubject($a);
       $m->AddMessage(stripslashes($b));
       $m->AddHost("mail.guamanpoma.org", 25, "");
       $m->Send();
}
        $fv=new FormValidator();

        // a continuacion definimos variables automaticamente para definir la cookie
        while(list($key, $val) = each($_POST)) {
                $$key=$val;
		$micook.=$val.'|';
        }
        setcookie('datoscurso',$micook);

		// controlamos que se han metido los datos solicitados obligatoriamente
        $fv->isEmpty("nombre", "Introduzca una cadena de caracteres en el campo <i>Nombre</i>");
        $fv->isEmpty("estado", "Introduzca una cadena de caracteres en el campo <i>Estado</i>");
        $fv->isEmpty("comienzo", "Introduzca una cadena de caracteres en el campo <i>Fecha Comienzo</i>");
        $fv->isEmpty("fin", "Introduzca una cadena de caracteres en el campo <i>Fecha de Finalización</i>");
        $fv->isPositive("plazas","Introduzca una cantidad positiva en <i>Plazas</i>");

        headerhtml("Grabando datos del curso...","estilo.css");

	if ($fv->isError()) {

		$errors = $fv->getErrorList();

		echo "<b>El formulario no pudo ser enviado por errores en la introducción de datos.</b>
                <p> Por favor, vuelva a enviar el formulario despues de realizar
		los siguientes cambios:";
		echo "<ul>";

		foreach ($errors as $e) {
			echo "<li>" . $e['msg'];
		}

		echo "</ul>";

                echo"<center><form>
		<input type=\"button\" value=\"Volver al Formulario\" onclick=\"history.back()\">
		</center></form>";
	}
	else {
		$db = ADONewConnection("mysql");
		
		/* conectamos a la base de datos */
		$db->Connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname) or die("No pude hacer $db-connect"); 

		/* esta consulta nos sirve para llamar a GetInsertSQL */
		$query="select * from cursos where id_curso=-1";
		$rs2=$db->Execute($query) or die("Error en la consulta Execute: $query. ". $db->ErrorMsg());
		
		/* en datosmatricula vamos a tener los datos que hay que insertar en la tabla datos_usuarios */
		  
		  $datosnuevocurso=array(
					"id_curso" => "",
					"nombre" => $_POST["nombre"],
					"activo" => $_POST["estado"],
					"comienzo" => $db->DBDate(convierte_fecha($_POST["comienzo"])),
					"fin" =>  $db->DBDate(convierte_fecha($_POST["fin"])),
					"plazas" => $_POST["plazas"],
					"cubiertas" => 0
					);
					
		 //var_dump($datosmatricula);
		  
		  // obtenemos la instruccion INSERT para los datos del nuevo curso 
		  $insercion2=$db->GetInsertSQL(&$rs2,$datosnuevocurso,$magicq=true);
		  
		  $db->Execute("LOCK TABLES cursos WRITE");
		  
		  /* insertamos datos en la tabla 'cursos' */
		  $db->Execute($insercion2) or die("Error en la insercion2:".$insercion2." ".$db->ErrorMsg());
		 
		  $db->Execute("UNLOCK TABLES");
		  
		  echo "<center>Los datos han sido insertados correctamente</center><br>";
		  echo "<center>Se ha creado el curso en la base de datos.</center><br>";
		  echo "<center><a href='http://guamanpoma.org'>Volver a la página principal</a></center>";
       
		$db->Close();
        footerhtml();
		
	}
?>
