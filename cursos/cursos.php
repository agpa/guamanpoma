<?PHP

include('application.php');
include($CFG->include.'adodb/adodb.inc.php');
include($CFG->include.'adodb/tohtml.inc.php');


$db = &ADONewConnection('mysql');
$db->Connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname) or die("Fallo al conectar con la BD");
$db->SetFetchMode(ADODB_FETCH_ASSOC);



if (isset($_GET["editar"])){
	$editar= $_GET["editar"];	
}else{
	$editar = -1;
}



function recupera_cursos($db){
	$consulta = "SELECT * FROM curso";
	$rs= $db->Execute($consulta);	
	return $rs;
}

/*
Espera una matriz asociativa con los datos de un curso: Nombre, Plazas, Cubiertas, Fecha_ini, Fecha_fin y Activo
*/
function escribe_curso($curso,$i,$id_curso){
	print("<tr>");
	
	foreach ($curso as $valor){		
		print("<td class=\"cuerpo\"  height=\"41\" bgcolor=\"#f9f9ff\">\n");			
		print($valor);		
		print("</td>\n");					
	}
	print("<td class=\"cuerpo\"  height=\"41\" bgcolor=\"#f9f9ff\">\n");
	print("<input type=\"button\" value='editar' onClick=\"JavaScript:window.open('cursos.php?editar=".$i."','_self');\">\n");
	print("<td>\n");
	print("</tr>\n");
}

function escribe_curso_edicion($curso,$id_curso){
	print("<tr>");
	print ("<form action=\"actualiza_curso.php\" method=\"GET\">");
	print ("<input type=\"hidden\" name=\"id_curso\" value=\"".$id_curso."\">");
	foreach($curso as $nombre => $valor){
		print("<td class=\"cuerpo\"  height=\"41\" bgcolor=\"#f9f9ff\">\n");			
		print("<input name='".$nombre."' value='".$valor."'>\n");		
		print("</td>\n");					
	}
	print("<td class=\"cuerpo\"  height=\"41\" bgcolor=\"#f9f9ff\">\n");
	print("<input type=\"submit\" value='guardar'>\n");
	print("<td>\n");
	print("</form>");
	print("</tr>\n");	
}

function escribe_claves($curso){
	$claves = array_keys($curso[1]);		
	for ($i=0; $i<count($claves);$i++){
		print("<th class=\"titulotabla\">".$claves[$i]."</th>");
	}	
	
}
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<META content="text/html" charset="iso-8859-15"> 
<link href="estilo.css" rel="stylesheet" type="text/css">
<STYLE>
   .mititulo { 
   	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
        font-weight: bold; 
	color: #000000;
	text-align: center;
        }
</STYLE>
<title>Gesti&oacute;n de cursos propios</title>
</head>
<body BGCOLOR="#256594">
<center> <p class="mititulo">INSCRIPCIÓN EN CURSOS DE FORMACIóN AL PROFESORADO</p>
<p class="ES-subapartado">CEVUG (Universidad de Granada)</p></center>
<center><table width="770px"><tbody>

<?PHP
	$rs=recupera_cursos($db);
	$array_cursos = $rs->getAssoc();
		
	escribe_claves($array_cursos);
	$i=0;
	foreach($array_cursos as $id_curso => $curso){
		($i==$editar)? escribe_curso_edicion($curso,$id_curso) : escribe_curso($curso,$i,$id_curso);
		$i++;
	}
	print("<tr>");
	print ("<form action=\"actualiza_curso.php\" method=\"GET\">");
	print("<input type=\"hidden\" name=\"id_curso\" value=\"-1\"> ");

	$num_cols = $rs->FieldCount();
	for ($i=0; $i<$num_cols; $i++){		
		$campo = $rs->FetchField($i);
		if ($campo->name !="id_curso") {
			print("<td class=\"cuerpo\"  height=\"41\" bgcolor=\"#f9f9ff\">\n");			
			print("<input name='".$campo->name."'>\n");
			print("</td>\n");		
		}
	}
	
	print("<td class=\"cuerpo\"  height=\"41\" bgcolor=\"#f9f9ff\">\n");
	print("<input type=\"submit\" value='guardar'>\n");
	print("<td>\n");
	print("</form>");
	print("</tr>\n");		
?>

 </tbody></table><br>
 </center>
</body>
</html>