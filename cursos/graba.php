<?php
include('FormValidator.class.inc');
include ('application.php');
include ('../adodb/adodb/adodb.inc.php');
include('../adodb/adodb/tohtml.inc.php');
include('mail/cmailer.php');

function headerhtml($title,$css_link) {
        echo "<html><head><title>".$title."</title><link href=\"".$css_link."\" rel=\"stylesheet\" type=\"text/css\"></head>";
}

function footerhtml() {
        echo "</body></html>";
}

function convierte_fecha($date) {


	$datesep=explode("/", $date);
	$year = $datesep[2];
	$month = $datesep[1];
	$day = $datesep[0];

	$newdate=mktime(0,0,0, $month,$day,$year); 
	
	return($newdate);
			
}

function enviamail($o,$d,$a,$b) {
      /* mandamos un mail de confirmacion al matriculado */
       $m = new cMailer();
       $m->AddAddress($d);
       $m->AddSender($o);
       $m->AddSubject($a);
       $m->AddMessage(stripslashes($b));
       $m->AddHost("mail.guamanpoma.org", 25, "");
       $m->Send();
}


        $fv=new FormValidator();

        // a continuacion definimos variables automaticamente
        while(list($key, $val) = each($_POST)) {
                $$key=$val;
		$micook.=$val.'|';
        }
        setcookie('datosinscripcion',$micook);

        $fv->isEmpty("dni", "Introduzca una cadena de caracteres en el campo <i>DNI</i>");
        $fv->isEmpty("nombre", "Introduzca una cadena de caracteres en el campo <i>Nombre</i>");
        $fv->isEmpty("apellidos", "Introduzca una cadena de caracteres en el campo <i>Apellidos</i>");
        $fv->isEmpty("direccion", "Introduzca una cadena de caracteres en el campo <i>Direccion Personal</i>");
        $fv->isPositive("curso","Introduzca un curso activo en el que desea matricularse en la <i>lista desplegable</i>");
        $fv->isEmailAddress("email","Introduzca una direccion de e-mail con el formato <i>usuario@dominio.subdominio</i>");

        headerhtml("Grabando datos de matricula...","estilo.css");

	if ($fv->isError()) {

		$errors = $fv->getErrorList();

		echo "<b>El formulario no pudo ser enviado por errores en la introducci�n de datos.</b>
                <p> Por favor, vuelva a enviar el formulario despues de realizar
		los siguientes cambios:";
		echo "<ul>";

		foreach ($errors as $e) {
			echo "<li>" . $e['msg'];
		}

		echo "</ul>";

                echo"<center><form>
		<input type=\"button\" value=\"Volver al Formulario\" onclick=\"history.back()\">
		</center></form>";
	}
	else {
		$db = ADONewConnection("mysql");
		//$db->debug = true;
		
		/* conectamos a la base de datos */
		$db->Connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname) or die("No pude hacer $db-connect"); 
		
		/* miramos que el curso del que se quiere matricular est� activo y tiene plazas */
		$query="select id_curso from cursos where cursos.activo!=0 and cursos.plazas > cursos.cubiertas";
		$rs3 = $db->Execute($query) or die ("Error en la consulta Execute: $query. " . $db->ErrorMsg());
		
		$valido=0;
		
		while ((!$rs3->EOF) && (!$valido)) {
		  if ($rs3->fields[0] == $_POST["curso"]) {
		    $valido=1;
		    break;
		  }
		  $rs3->MoveNext();
		}
		
		/* comprobamos que es un registro v�lido */
		if ($valido) {

		$query="select * from usuarios_cursos where dni=-1 and id_curso=-1";
		$rs2=$db->Execute($query) or die("Error en la consulta Execute: $query. ". $db->ErrorMsg());
		
     	  $query="select dni,id_curso from usuarios_cursos where dni=\"".$_POST["dni"]."\" and id_curso=\"".$_POST["curso"]."\"";
		  $rs4 = $db->Execute($query) or die("Error en la consulta Execute: $query. ". $db->ErrorMsg());
		  
		  /* en datosmatricula vamos a tener los datos que hay que insertar en la tabla datos_usuarios */
		  
		  $datosmatricula=array(
					"id_curso" => $_POST["curso"],
					"dni" => $_POST["dni"],
					"nombre" => $_POST["nombre"],
					"apellidos" => $_POST["apellidos"],
					"nacimiento" => ($_POST["nacimiento"]!="")?$db->DBDate(convierte_fecha($_POST["nacimiento"])):"0000-00-00",
					"organizacion" => $_POST["organizacion"],
					"procedencia" => $_POST["procedencia"],
					"profesion" => $_POST["profesion"],
					"direccion" => $_POST["direccion"],
					"tlf_organizacion" => $_POST["tlf_organizacion"],
					"tlf_personal" => $_POST["tlf_personal"],
					"e_mail" => $_POST["email"]
					);
					
	 //var_dump($datosmatricula);
		  
		  $insercion2=$db->GetInsertSQL(&$rs2,$datosmatricula,$magicq=true);
		  
		  $db->Execute("LOCK TABLES cursos,usuarios_cursos WRITE");

                 /* Si el usuario ya est� matriculado en dicho curso, cancelamos la operacion */
                 if ($rs4->RecordCount()!=0) {
                        die("<div class=\"error\">Usted ya se dio de alta en este curso</div>");
                        $db->Close();
                 }
		  
		  /* insertamos datos en la tabla 'matricula' */
		  $db->Execute($insercion2) or die("Error en la insercion2:".$insercion2." ".$db->ErrorMsg());
		  
		  /* aumentamos el valor de plazas cubiertas de ese curso */
          $query="update cursos set cubiertas=cubiertas+1 where id_curso=\"".$_POST["curso"]."\"";
		  $db->Execute($query) or die("Error en la consulta Execute: $query. " . $db->ErrorMsg());

		  $db->Execute("UNLOCK TABLES");
		  

		  // construimos el cuerpo del mensaje de correo que le enviamos al matriculado confirmando
		  // su inscripci�n. 
          $sql = "select nombre,id_curso from cursos where cursos.id_curso="."\"".$_POST["curso"]."\"";
		  $rs3 = $db->Execute($sql) or die("Error en la consulta $sql. " . $db->ErrorMsg());
		  
		  // se actualiza el codigo de alumno para dicho curso
		  $sql = "update usuarios_cursos set num_alumno=num_alumno+1 where id_curso=\"".$_POST["curso"]."\"";
  		  $rs4 = $db->Execute($sql) or die("Error en la consulta $sql. " . $db->ErrorMsg());
		  
		  $sql = "select id_curso, num_alumno from usuarios_cursos where id_curso="."\"".$_POST["curso"]."\"";
		  $rs5 = $db->Execute($sql) or die("Error en la consulta $sql. " . $db->ErrorMsg());
		  
          $body="Este es un mensaje para confirmarle su inscripcion en el curso '".$rs3->fields[0].".'\n";
		  $body.="Tu c�digo de alumno para este curso es el siguiente: ".$rs5->fields[0]."-".$rs5->fields[1]."\n";
          enviamail("informes@guamanpoma.org",$email,"Confirmacion de matricula en ".$rs3->fields[0],$body);
		  
 		  // mensaje avisando de que todo ha ido bien
		  echo "<center>Sus datos personales han sido insertados correctamente.</center><br>";
		  echo "<center>Ha sido inscrito en el curso.</center><br>";
		  echo "<center>Pulse <a href=\"codigo.php?curso=".$rs3->fields[0]."&codigo=".$rs5->fields[0]."-".$rs5->fields[1]."\">aqui</a> para imprimir su c�digo de inscripci�n.<br><br><br>";
		  echo "<center><a href='http://guamanpoma.org'>Volver a la p�gina principal</a></center>";

		}
		else {
		  /* si no quedan plazas libres notificamos */
          die("<div class=\"error\">El curso del que quiere matricularse no tiene plazas.</div>");

		}
	        
		$db->Close();
         footerhtml();
		
	}
?>
