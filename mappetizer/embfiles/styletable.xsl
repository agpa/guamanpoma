<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="field">No</xsl:param>
<xsl:param name="datatype">number</xsl:param>
<xsl:param name="orderby">ascending</xsl:param>
<xsl:output method="html"/>
<xsl:template match="/">
<h1 class="recTable">Datos de <xsl:value-of select="map/title" /></h1>
<p>(Instrucciones: Mueva el cursor sobre las filas para resaltar los objetos respectivos en el mapa. Oprima simult�neamente la tecla "Control" para acercarse a cada objeto.)</p>
<table class="recTable">
<tr>
<th>N�m.</th>
<xsl:for-each select="map/tab/fs/*">
<th>
<nobr>
<xsl:value-of select="." />
<a>
<xsl:attribute name="href">javascript:sortTable('<xsl:number level="single" count="fs/*" format="1" />','<xsl:value-of select="@datatype" />','ascending');</xsl:attribute>
<img src="../pictures/sortascending.png" class="imgAsc" alt="Ordenar ascendentemente" />
</a>
<a>
<xsl:attribute name="href">javascript:sortTable('<xsl:number level="single" count="fs/*" format="1" />','<xsl:value-of select="@datatype" />','descending');</xsl:attribute>
<img src="../pictures/sortdescending.png" class="imgAsc" alt="Ordenar descendentemente" />
</a>
</nobr>
</th>
</xsl:for-each>
</tr>
<xsl:for-each select="map/tab/records/rec">
<xsl:sort data-type="{$datatype}" order="{$orderby}" lang="en" select="c/*[local-name() = $field]"  />
<tr>
<xsl:if test="position() mod 2 = 0">
<xsl:attribute name="style">background-color:#ECE9D8</xsl:attribute>
</xsl:if>
<xsl:attribute name="onmouseover">window.opener.mv_hiliteRec('<xsl:for-each select="ts/*"><xsl:value-of select="." />,</xsl:for-each><xsl:value-of select="@id" />','<xsl:value-of select="XMin" />','<xsl:value-of select="XMax" />','<xsl:value-of select="YMin" />','<xsl:value-of select="YMax" />',this,window,event);</xsl:attribute>
<xsl:attribute name="onmouseout">window.opener.mv_recOut(this);</xsl:attribute>
<td><xsl:value-of select="No" /></td>
<xsl:for-each select="c/*">
<xsl:choose>
<xsl:when test="contains(current(),'@')">
<td><a>
<xsl:attribute name="href">mailto:<xsl:value-of select="." /></xsl:attribute>
<xsl:value-of select="." /></a></td>
</xsl:when>
<xsl:when test="contains(current(),'http://') or contains(current(),'https://')">
<td><a>
<xsl:if test="contains(current(),';')">
<xsl:variable name="title" select="substring-after(current(),';')" />
<xsl:variable name="url2" select="substring-before(current(),';')" />
<xsl:attribute name="href"><xsl:value-of select="$url2" /></xsl:attribute>
<xsl:attribute name="target">MVFullWin</xsl:attribute>
<xsl:value-of select="$title" />
</xsl:if>
<xsl:if test="contains(current(),';') = false()">
<xsl:attribute name="href"><xsl:value-of select="." /></xsl:attribute>
<xsl:attribute name="target">MVFullWin</xsl:attribute>
<xsl:value-of select="." />
</xsl:if>
</a></td>
</xsl:when>
<xsl:when test="contains(current(),'url:')">
<td><a>
<xsl:variable name="url" select="substring-after(current(),'url:')" />
<xsl:if test="contains($url,';')">
<xsl:variable name="title" select="substring-after($url,';')" />
<xsl:variable name="url2" select="substring-before($url,';')" />
<xsl:attribute name="href">../<xsl:value-of select="$url2" /></xsl:attribute>
<xsl:attribute name="target">MVFullWin</xsl:attribute>
<xsl:value-of select="$title" />
</xsl:if>
<xsl:if test="contains($url,';') = false()">
<xsl:attribute name="href">../<xsl:value-of select="$url" /></xsl:attribute>
<xsl:attribute name="target">MVFullWin</xsl:attribute>
<xsl:value-of select="$url" />
</xsl:if>
</a></td>
</xsl:when>
<xsl:when test="contains(current(),'js:')">
<td><a>
<xsl:variable name="url" select="substring-after(current(),'js:')" />
<xsl:if test="contains($url,';')">
<xsl:variable name="title" select="substring-after($url,';')" />
<xsl:variable name="url2" select="substring-before($url,';')" />
<xsl:attribute name="href">javascript:window.opener.<xsl:value-of select="$url2" /></xsl:attribute>
<xsl:value-of select="$title" />
</xsl:if>
<xsl:if test="contains($url,';') = false()">
<xsl:attribute name="href">javascript:window.opener.<xsl:value-of select="$url" /></xsl:attribute>
<xsl:value-of select="$url" />
</xsl:if>
</a></td>
</xsl:when>
<xsl:otherwise>
<td><xsl:value-of select="." />&#160;</td>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</tr>
</xsl:for-each>
</table>
</xsl:template>
</xsl:stylesheet>
