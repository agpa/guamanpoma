// Author: Guaman Poma/Creator: Mappetizer  8.3.17 for ArcGIS by uismedia (http://www.mappetizer.de); 31/08/2011 06:16:17 p.m.
dojo.require("dijit.TitlePane");
dojo.addOnLoad(function() {
  //Do not delete or change this function
  var theArray = window.location.search.substr(1,window.location.search.length).split("|");
  if (theArray[0] == 0) {
    var node;
    if (dojo.byId("MVlegend") != null) {
      var leg = mv_embedSVGNode(dojo.doc,"MVlegendDoc","228px","767px","embfiles/legend.svg");
      node = mv_makeFullNode(dojo.doc,"h2",{ },"Leyenda");
      dojo.byId("MVlegend").appendChild(node);
      dojo.byId("MVlegend").appendChild(leg);
    }

  }
  var map = mv_embedSVGNode(dojo.doc,"MVmapDoc","448px","311px","embfiles/map.svg");
  dojo.byId("MVmap").appendChild(map);

  if (dojo.isIE < 9) {
    checkAndGetSVGViewer();
  }
  mv_checkLoaded();
});

function mv_DocSettings() {
  //Do not delete or change this function;
  mv_Doc.userSettings("white","silver","red","yellow","#ECE9D8",false,false);
  mv_Doc.txtIESupport = "Para mostrar esta p�gina necesita el programa gratuito Adobe SVG Viewer 3.0.\n(http://www.adobe.com/svg/viewer/install/main.html)";
  mv_Doc.txtQueryNoRec = "No se encontraron registros.";
  mv_Doc.txtFlicker = "Highlight object";
  mv_Doc.txtZoomTo = "Zoom to object";
  mv_Doc.txtPrint = "Imprimir";
  mv_Doc.txtClose = "Cerrar consulta";
  mv_Doc.CheckStatus = "a16uMD4";
  mv_Doc.PrintStatus = true;
}

function mv_MapSettings() {
  //Do not delete or change this function;
  new MV.Layer("th0",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th1",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th2",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th3",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th4",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th5",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th6",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th7",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th8",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th9",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th10",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th11",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th12",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th13",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th14",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th15",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th16",true,true,true,true,false,"FeatureLayer","");
  mv_Map.userSettings(167877.6,8511614.7,94.6004444444444,1,true,3393,"");
}

function mv_initializeLegend() {
  //Do not delete or change this function;
}

function mv_initializeShareGeomLegend() {
  //Do not delete or change this function;
}

function mv_ScalebarSettings() {
  //Do not delete or change this function;
  mv_Scalebar.userSettings(2,105.2,10000);
}

function mv_initializeSettings() {
  //Do not delete or change this function;
  mv_declareClassIdentifyObject();
  mv_declareClassIdentifyTP();

}

function mv_initializeSettings2() {
 //Do not delete or change this function;
}


function mv_showHelp() {
  var theWin = window.open("embfiles/help.html","MVHelp", "width=475,height=576,top=50,left=50,toolbar=no,menubar=no,location=no,hotkeys=no,resizable=yes,scrollbars=yes,dependent=yes,status=no");
  theWin.focus();
}


function mv_userInit() {
//This function is for your own scripts, it will be called on loading, do not delete it
//  mv_alert("function mv_userInit()");
}

function mv_userMVDocToolbar(objButton) {
//This function is for your own toolbar buttons, do not delete it
//  switch(objButton.id) {
//      case "MVtoolbar.mybutton":
//          objButton.Function = "testFunc('hier',2)";
//          objButton.State = 1;  //this button is a radio button
//          break;
//  }
}

