// Author: Guaman Poma/Creator: Mappetizer  8.3.17 for ArcGIS by uismedia (http://www.mappetizer.de); 31/08/2011 06:16:17 p.m.
dojo.require("dijit.TitlePane");
dojo.require("dojo.parser");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.form.Button");
dojo.require("dijit.Toolbar");

dojo.addOnLoad(function() {
  //Do not delete or change this function
  var leg = mv_embedSVGNode(dojo.doc,"MVlegendDoc","228px","767px","embfiles/legend.svg");
  dojo.byId("MVlegend").appendChild(leg);

  var map = mv_embedSVGNode(dojo.doc,"MVmapDoc","448px","311px","embfiles/map.svg");
  dojo.byId("MVmap").appendChild(map);

  if (dojo.isIE < 9) {
    checkAndGetSVGViewer();
  }
  // Add functions to toolbar
  dojo.connect(dojo.byId("MVtoolbar.zoomin"),"onclick",mv_zoomInTool);
  dojo.connect(dojo.byId("MVtoolbar.zoomout"),"onclick",mv_zoomOutTool);
  dojo.connect(dojo.byId("MVtoolbar.pan"),"onclick",mv_panTool);
  dojo.connect(dojo.byId("MVtoolbar.fullextent"),"onclick",mv_fullExtent);
  dojo.connect(dojo.byId("MVtoolbar.identify"),"onclick",mv_identifyTool);
  dojo.connect(dojo.byId("MVtoolbar.koord"),"onclick",mv_coordinateTool);
  dojo.connect(dojo.byId("MVtoolbar.measure"),"onclick",mv_measureTool);
  dojo.connect(dojo.byId("MVtoolbar.print"),"onclick",mv_print);
  dojo.connect(dojo.byId("MVtoolbar.help"),"onclick",mv_showHelp);
  dojo.connect(dojo.byId("MVmeasureToolbar.line"),"onclick",mv_measureLineTool);
  dojo.connect(dojo.byId("MVmeasureToolbar.area"),"onclick",mv_measureAreaTool);
  dojo.connect(dojo.byId("MVmeasureToolbar.sum"),"onclick",mv_measureSumTool);
  dojo.connect(dojo.byId("MVmeasureToolbar.clear"),"onclick",mv_measureClearTool);
  dojo.connect(dojo.byId("body"),"onresize",mv_resizeMap);

  mv_checkLoaded();
});

function mv_DocSettings() {
  //Do not delete or change this function;
  mv_Doc.userSettings("white","silver","red","yellow","#ECE9D8",true,false);
  mv_Doc.txtIESupport = "Para mostrar esta p�gina necesita el programa gratuito Adobe SVG Viewer 3.0.\n(http://www.adobe.com/svg/viewer/install/main.html)";
  mv_Doc.txtQueryNoRec = "No se encontraron registros.";
  mv_Doc.txtFlicker = "Highlight object";
  mv_Doc.txtZoomTo = "Zoom to object";
  mv_Doc.txtPrint = "Imprimir";
  mv_Doc.txtClose = "Cerrar consulta";
  mv_Doc.CheckStatus = "a16uMD4";
}

function mv_MapSettings() {
  //Do not delete or change this function;
  new MV.Layer("th0",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th1",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th2",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th3",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th4",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th5",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th6",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th7",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th8",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th9",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th10",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th11",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th12",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th13",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th14",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th15",true,true,true,true,false,"FeatureLayer","");
  new MV.Layer("th16",true,true,true,true,false,"FeatureLayer","");
  mv_Map.userSettings(167877.6,8511614.7,94.6004444444444,1,true,3393,"");
}

function mv_initializeLegend() {
  //Do not delete or change this function;
}

function mv_initializeShareGeomLegend() {
  //Do not delete or change this function;
}

function mv_ScalebarSettings() {
  //Do not delete or change this function;
  mv_Scalebar.userSettings(2,105.2,10000);
}

function mv_initializeSettings() {
  //Do not delete or change this function;
  mv_declareClassIdentifyObject();
  mv_declareClassIdentifyTP();

  mv_Doc.Toolbars = new MV.List();
  mv_declareClassToolbar();
  mv_createToolbar();
  dojo.byId("MVtoolbar.identify").click();
  mv_declareClassMeasure();
  mv_Measure = new MV.Measure();
  mv_Measure.userSettings("Distancia","Per�metro","�rea","Sumatorio","m","m�",1,2);
}

function mv_initializeSettings2() {
 //Do not delete or change this function;
}


function mv_showHelp() {
  var theWin = window.open("embfiles/help.html","MVHelp", "width=475,height=576,top=50,left=50,toolbar=no,menubar=no,location=no,hotkeys=no,resizable=yes,scrollbars=yes,dependent=yes,status=no");
  theWin.focus();
}


function mv_userInit() {
//This function is for your own scripts, it will be called on loading, do not delete it
//  mv_alert("function mv_userInit()");
}

function mv_userMVDocToolbar(objButton) {
//This function is for your own toolbar buttons, do not delete it
//  switch(objButton.id) {
//      case "MVtoolbar.mybutton":
//          objButton.Function = "testFunc('hier',2)";
//          objButton.State = 1;  //this button is a radio button
//          break;
//  }
}

